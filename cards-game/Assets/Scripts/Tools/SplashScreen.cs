﻿using System;
using System.Collections;
using System.Text;
using System.Web;
using UnityEngine;
using UnityEngine.UI;

public class SplashScreen : MonoBehaviour {
	[SerializeField] private AlphaAnimation logo;
	[SerializeField] private GameObject FormsRenderer;
	bool firstUpdate = true;
	bool gameLoaded = false;
    
	Subscription<Events> subscription;

	private void OnGameLoadingDone()
	{
		gameLoaded = true;
	}

	private void Awake() =>	subscription = Messanger<Events>.Subscribe(Events.GameLoadingDone, OnGameLoadingDone);
	private void OnDestroy() => subscription.Unsubscribe();

	private IEnumerator WaitCoroutine()
	{
		yield return new WaitForSeconds(3f);
		yield return new WaitWhile(() => gameLoaded == false);
		logo.T = 1f;
		logo.Target = 0f;
        logo.OnComplete = () =>
        {
            Messanger<bool, Events>.SendMessage(Events.SetMusicActive, true);
            if (PlayerPrefs.GetInt("FirstGame", 0) == 0) Messanger<string, Events>.SendMessage(Events.PlayMusic, SoundClips.GameMusics[Helper.GetIndexGameMusics()]);
            else Messanger<string, Events>.SendMessage(Events.PlayMusic, SoundClips.LobbyMusic);

            SocialManager.Login();
            gameObject.SetActive(false);
            if (PlayerPrefs.GetInt("FirstGame") == 0) // если игрок не играл еще
            {
                Debug.Log("FirstGame");
                Messanger<Events>.SendMessage(Events.FirstGame);
                TablePanel.I.GetObject("Monsters").SetActive(false);
                TablePanel.I.gameObject.SetActive(true);
            }
            else if (Helper.CountCard >= 13 && PlayerPrefs.GetInt("CompleteTraining") != 1) TrainingPanel.I.gameObject.SetActive(true); // если игрок купил 13 карт, но не прошел обучение в редакторе
            else if (!Helper.GetTraining5Passed() && PlayerPrefs.GetInt("FirstGame") > 3 && !FirstGame.intraining)
            {
                FirstGame.I.FivethTraining1();
                Debug.Log("FiveTraining");
            }
            else if (PlayerPrefs.GetInt("FirstOpenAchievement") != 1) // если первая ачивка еще не получена (всегда последний, т к в обучении всегда верно)
            {
                foreach (string achiv in Enum.GetNames(typeof(OpenAchievement))) // проходимся по ачивкам
                {
                    if (PlayerPrefs.GetInt(achiv) >= Helper.GetOpenAchievementStrings(achiv).need[0]) // если ачивка выполнена
                    {
                        GameResultsPanel.I.GetObject("Restart").GetComponent<Button>().interactable = false;// выключить рестарт и меню
                        GameResultsPanel.I.GetObject("Menu").GetComponent<Button>().interactable = false;   // выключить рестарт и меню
                        if (PlayerPrefs.GetInt("FirstOpenAchievement") == 0)
                        {
                            bool ach = false;
                            foreach (string key in Enum.GetNames(typeof(OpenAchievement)))
                            {
                                if (Helper.GetProgress(key) >= Helper.GetOpenAchievementStrings(key).need[Helper.GetLevel()])
                                {
                                    ach = true;
                                    break;
                                }
                            }
                            if (ach) SceneController.I.gameObject.GetComponent<FirstGame>().OnFirstAchievement();        // запустить обучение c начала
                        }
                        else FirstGame.I.FirstAchiv5();                                                     // человек успел забрать награду, но до разблокировки заданий не дошел
                        Debug.Log($"<color=blue>{PlayerPrefs.GetInt("FirstOpenAchievement")}</color>");
                            break;
                    }
                }
            }
            
        };
		logo.DoAlpha(ComponentType.Image);
	}

    void Update () {
		if (firstUpdate)
		{
	        // gameObject.GetComponent<Canvas>().sortingLayerName = "Foreground";
			logo.Target = 1f;
			logo.T = 1f;
			logo.DoAlpha(ComponentType.Image);
			logo.OnComplete = () =>
			{
				StartCoroutine(WaitCoroutine());
				FormsRenderer.SetActive(true);
			};
			firstUpdate = false;
        }
	}
}
