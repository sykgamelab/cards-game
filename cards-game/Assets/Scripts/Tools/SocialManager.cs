﻿using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using System.Collections;
using System.Web;
using UnityEngine.UI;
#if UNITY_IOS
using System.Threading.Tasks;
using UnityEngine.SocialPlatforms;
using UnityEngine.SocialPlatforms.GameCenter;
#endif

public class SocialManager : MonoBehaviour
{
    public static SocialManager I { get; set; } = null;
    private bool needRetry = false;
    private string[] achievements = null;
    private static bool isReportInProgress = false, isReportFailed = false;
    private static string achievementFailed = null;
    private static SGLTools.SocialManagerInstantiator.SocialManager sm;
    private static bool needLogin = false;
    public static bool AreOpenAchievementsChanged { get; set; } = false;
    private Action iosAlert = null;
    private bool canReport = false;
    public bool Synchronized { get; private set; } = false;

    private IEnumerator ProgressAwaitCoroutine()
    {
        yield return new WaitWhile(() => !ServerManager.ProgressReady);
        Dictionary<string, object> progress = ServerManager.GetProgressToSync();
        Synchronized = false;
        if (progress != null)
        {
            int localTimestamp = Helper.GetSyncTimestamp();
            int timestamp = Convert.ToInt32(progress["timestamp"]);
            //Debug.Log($"SocialManager.ProgressAwaitCoroutine: SERVER timestamp local:{localTimestamp} server:{timestamp}");
            if (timestamp > localTimestamp)
            {
                RefinementProgressPanel.I.Show((overwrite) =>
                {
                    if (overwrite)
                    {
                        Synchronized = true;
                        GameResultsPanel.I.GetObject("Menu").GetComponent<Button>().onClick.Invoke();
                        Helper.SetProgress(progress);
                        //Debug.Log("SocialManager.ProgressAwaitCoroutine: SET PROGRESS OVERWRITE");
                    }

                    Helper.SetSyncTimestamp(timestamp);
                    ServerManager.ProgressSynchronized = true;
                });
            }
            else
            {
                //Debug.Log("SocialManager.ProgressAwaitCoroutine: SET PROGRESS IGNORE");
                ServerManager.ProgressSynchronized = true;
            }
        } 
        else
        {
            //Debug.Log("SocialManager.ProgressAwaitCoroutine: PROGRESS NOT FOUND");
            ServerManager.ProgressSynchronized = true;
        }
    }

    private void Awake()
    {
        I = this;
#if UNITY_IOS
        iosAlert = () => PopUpPanel.I.ShowMessage("Please Turn On Game Center to use this Feature");
#endif
        sm = SGLTools.SocialManagerInstantiator.GetInstance();
        achievements = new string[SocialConstants.OpenAchievementsIDs.Count + SocialConstants.HiddenAchievementsIDs.Count];
        Helper.SetHaveScoreToReport(true);
        int i = 0;
        foreach (var pair in SocialConstants.OpenAchievementsIDs)
        {
            Helper.SetAchievementToReport(pair.Key, true);
            achievements[i++] = pair.Key;
        }
        foreach (var pair in SocialConstants.HiddenAchievementsIDs)
        {
            Helper.SetAchievementToReport(pair.Key, true);
            achievements[i++] = pair.Key;
        }
    }

    public void StartProgressAwaitCoroutine()
    {
        StartCoroutine(ProgressAwaitCoroutine());
    }

    public static void Login()
    {
        /*Action iosAlert = null;
#if UNITY_IOS
        iosAlert = () => InformationPanel.I.SendMessage("Please Turn On Game Center to use this Feature");
#endif
        sm.Authenticate(() =>
        {
            if (sm.LastError != null)
            {
                Debug.Log($"SocialManager.Authenticate: Authenticate FAIL. lastError = {sm.LastError}");
            }
            else
            {
                Debug.Log("SocialManager.Authenticate: Authenticate SUCCESS.");
                //ServerManager.StartServerClient(Helper.GetTicket(), Helper.GetSyncTimestamp());
            }
        }, iosAlert);*/

        needLogin = true;
    }

    public static bool IsAuthenticated()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable) return false;
        else return sm.Authenticated;
    }

    public static void ShowLeaderboard()
    {
#if UNITY_IOS
        if (!IsAuthenticated())
            PopUpPanel.I.ShowMessage("Please Turn On Game Center to use this Feature");
        else sm.ShowLeaderboard(SocialConstants.ScoresTable);
#elif UNITY_ANDROID
        if (!IsAuthenticated())
            PopUpPanel.I.ShowMessage("Необходимо авторизоваться в сервисе \"Play Игры\"");
        else sm.ShowLeaderboard(SocialConstants.ScoresTable);
#endif
    }

    private void ReportScore()
    {
        //Debug.Log("START REPORTING SCORES");
        sm.ReportScore(SocialConstants.ScoresTable, Helper.GetScore(), () =>
        {
            //Debug.Log("ReportedScore");
            if (sm.LastError != null)
            {
                needRetry = true;
                //Debug.Log("NEED RETRY REPORTING SCORES");
            }
        });
    }

    public static void ShowAchievements()
    {
#if UNITY_IOS
        if (!IsAuthenticated())
            PopUpPanel.I.ShowMessage("Please Turn On Game Center to use this Feature");
        else sm.ShowAchievements();
#elif UNITY_ANDROID
        if (!IsAuthenticated())
            PopUpPanel.I.ShowMessage("Необходимо авторизоваться в сервисе \"Play Игры\"");
        else sm.ShowAchievements();
#endif
    }

    public static void LoadAchievements(Action<List<SGLTools.SocialManagerInstantiator.SocialManager.Achievement>> callback = null)
    {
        sm.LoadAchievements(callback);
    }

    public void ReportHiddenAchievement(string id)
    {
        string achievementID = id;
#if UNITY_ANDROID
        achievementID = SocialConstants.HiddenAchievementsIDs[id];
#endif
        var type = SGLTools.SocialManagerInstantiator.SocialManager.TypeAchievement.Unlock;
        double progress = (double)PlayerPrefs.GetInt(id, 0);
        if (progress == 0)
        {
            //Debug.Log($"ReportHiddenAchievement: id = {id}, achievementID = {achievementID}, progress = {progress} -> return");
            isReportInProgress = false;
            return;
        }

        sm.ReportAchievementProgress(type, achievementID, progress, () =>
        {
            //Debug.Log($"ReportHiddenAchievement: REPORTED ACHIEVEMENT {achievementID}");
            if (sm.LastError != null)
            {
                //Debug.Log($"ReportHiddenAchievement: REPORT ACHIEVEMENT {achievementID} FAILED: {sm.LastError}");
                achievementFailed = id;
                isReportFailed = true;
            }
            else Thread.Sleep(2000);
            isReportInProgress = false;
        });
    }

    public void ReportOpenAchievement(string id)
    {
        string key = id.Substring(0, id.Length - 1);
        //Debug.Log($"ReportOpenAchievement: id = {id}, key = {key}");
        int value = PlayerPrefs.GetInt(key, 0);
        string achievementID = id;
#if UNITY_ANDROID
        achievementID = SocialConstants.OpenAchievementsIDs[id];
#endif
        if (value == 0)
        {
            //Debug.Log($"ReportOpenAchievement: id = {id}, achievementID = {achievementID}, value = {value} -> return");
            isReportInProgress = false;
            return;
        }

        var type = SGLTools.SocialManagerInstantiator.SocialManager.TypeAchievement.Increment;
        int n = int.Parse(id.Substring(id.Length - 1));
        int need = Helper.GetOpenAchievementStrings(key).need[n - 1];
        double progress = (double)value / need;

        sm.ReportAchievementProgress(type, achievementID, progress, () =>
        {
            //Debug.Log($"ReportOpenAchievement: REPORTED ACHIEVEMENT {achievementID}, value = {value}, progress = {progress}");
            if (sm.LastError != null)
            {
                //Debug.Log($"ReportOpenAchievement: REPORT ACHIEVEMENT {achievementID} FAILED: {sm.LastError}");
                achievementFailed = id;
                isReportFailed = true;
            }
            else Thread.Sleep(2000);
            isReportInProgress = false;
        });
    }

    public static void ResetAchievementsForTesters() => sm.ResetAchievementsForTesters();

    private void Update()
    {
        if (needLogin && Application.internetReachability != NetworkReachability.NotReachable)
        {
            needLogin = false;
            sm.Authenticate(() =>
            {
                if (sm.LastError != null)
                {
                    //Debug.Log($"SocialManager.Authenticate: Authenticate FAIL. lastError = {sm.LastError}");

#if UNITY_ANDROID
                    string error = "Google Play Games: " + sm.LastError;
#elif UNITY_IOS
					string error = "Gamecenter: " + sm.LastError;
#endif
                    //Debug.Log($"SocialManager.Authenticate: Authenticate FAIL - {sm.LastError}");
                }
                else
                {
                    //Debug.Log("SocialManager.Authenticate: Authenticate SUCCESS.");
                    canReport = true;
                    ServerManager.StartServerClient(Helper.GetTicket(), Helper.GetSyncTimestamp());
                }
            }, iosAlert);
        }

        if (canReport)
        {
            if (needRetry || Helper.GetHaveScoreToReport())
            {
                Helper.SetHaveScoreToReport(false);
                needRetry = false;
                ReportScore();
            }

            if (!isReportInProgress && !isReportFailed) ReportAchievements();
            else if (isReportFailed)
            {
                //Debug.Log($"FAIL ON {achievementFailed} {sm.LastError}");
                Helper.SetAchievementToReport(achievementFailed, true);
                isReportFailed = false;
            }
            else if (AreOpenAchievementsChanged) ReportAchievements();
        }
    }

    private bool IsOpenAchievement(string id) => SocialConstants.OpenAchievementsIDs.ContainsKey(id) ? true : false;

    private void ReportAchievements()
    {
        if (AreOpenAchievementsChanged)
        {
            AreOpenAchievementsChanged = false;
            for (int i = 0; i < SocialConstants.OpenAchievementsIDs.Count; i++)
            {
                //Debug.Log($"ReportAchievements: IsOpenAchievementsChanged HAS TO REPORT {achievements[i]}");
                Helper.SetAchievementToReport(achievements[i], false);
                isReportInProgress = true;
                ReportOpenAchievement(achievements[i]);
            }
        }
        else
        {
            foreach (string achievement in achievements)
            {
                if (Helper.HasAchievementToReport(achievement))
                {
                    //Debug.Log($"ReportAchievements: HAS TO REPORT {achievement}");
                    Helper.SetAchievementToReport(achievement, false);
                    isReportInProgress = true;
                    if (IsOpenAchievement(achievement)) ReportOpenAchievement(achievement);
                    else ReportHiddenAchievement(achievement);
                    break;
                }
            }
        }
    }

    public void SetWinSeriesToReport()
    {
        string ticket;
#if UNITY_EDITOR
        ticket = "1160130875fda0812c99c5e3f1a03516471a6370c4f97129b221938eb4763e63";
#else
        ticket = ServerManager.GetTicket();
#endif

        StartDisplayNameCoroutine(() =>
        {
            string postData = $"{ServerConstants.Ticket}={HttpUtility.UrlEncode(ticket)}&"
               + $"{ServerConstants.Nickname}={GetDisplayName()}&{ServerConstants.WinSeries}={Helper.GetWinSeries()}";
            ServerManager.SetWinSeriesToReport(postData);
        });
    }

    public void SetProgressToReport()
    {
        Dictionary<string, object> progressToReport = Helper.GetProgress();
        int timestamp = Convert.ToInt32(progressToReport["timestamp"]);
        Helper.SetSyncTimestamp(ServerManager.GetSyncTimestamp());
        string ticket = ServerManager.GetTicket();
        int expirationTime = ServerManager.GetTicketExpirationTime();
        Debug.Log($"SocilaManager.SetProgressToReport: progressToReport.Count = {progressToReport.Count}");
        Helper.SetTicket(ticket, expirationTime);
        if (progressToReport.Count == 360) ServerManager.SetProgressToReport(progressToReport);
    }

    private void OnDestroy() => ServerManager.StopServerClient();

    public string GetUserID()
    {
        string prefix = "editor_";
#if UNITY_IOS
        prefix = "ios_";
#elif UNITY_ANDROID
        prefix = "android_";
#endif
        return prefix + sm.UserID;
    }

    public string GetDisplayName() => sm.UserDisplayName;

    private IEnumerator DisplayNameCoroutine(Action callback)
    {
        yield return new WaitWhile(() => GetDisplayName() == null);
        callback.Invoke();
    }

    public void StartDisplayNameCoroutine(Action callback) => StartCoroutine(DisplayNameCoroutine(callback));
}