﻿using System;
using System.Threading;
using UnityEngine;

public class AndroidThread
{
    private Thread thread;
	Action entry;
	Action<object> parametrizedEntry;
	object arg;

    public AndroidThread(Action start)
    {
		//     thread = new Thread(start);
		entry = start;
		thread = new Thread(ThreadEntry);
    }

	public AndroidThread(Action<object> start, object arg)
	{
		//	thread = new Thread(start);
		thread = new Thread(ThreadEntry);
		parametrizedEntry = start;
		this.arg = arg;
	}

	public void Join()
    {
        thread.Join();
    }

	private void ThreadEntry()
	{
		if (AndroidJNI.AttachCurrentThread() != 0)
		{
			Debug.Log("AndroidThread.GetAction: Error AndroidJNI.AttachCurrentThread");
			return;
		}

		if (entry != null) entry.Invoke();
		else parametrizedEntry.Invoke(arg);
		//callback(arg);

		if (AndroidJNI.DetachCurrentThread() != 0)
		{
			Debug.Log("AndroidThread.GetAction: Error AndroidJNI.DetachCurrentThread");
		}
	}

    public void Start(object callback = null)
    {
        thread.Start();

    }

    /*public static Action<object> GetAction(Action<object> callback)
    {
        Action<object> action = (arg) => 
        {
            if (AndroidJNI.AttachCurrentThread() != 0)
            {
                Debug.Log("AndroidThread.GetAction: Error AndroidJNI.AttachCurrentThread");
                return;
            }

            callback(arg);

            if (AndroidJNI.DetachCurrentThread() != 0)
            {
                Debug.Log("AndroidThread.GetAction: Error AndroidJNI.DetachCurrentThread");
            }
        };

        return action;
    }*/
}
