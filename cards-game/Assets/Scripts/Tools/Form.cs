﻿using System.Collections.Generic;
using UnityEngine;

public class Form : MonoBehaviour
{
	protected Dictionary<string, GameObject> objects;

    public bool Started { get; set; } = false;

    public void AddObject(string key, GameObject obj)
	{
		if (objects == null) objects = new Dictionary<string, GameObject>();
		objects.Add(key, obj);
	}
	public GameObject GetObject(string key) => objects[key];
	public bool Contains(string key) => objects.ContainsKey(key);
}
