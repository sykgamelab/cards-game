﻿using UnityEngine;
using UnityEngine.UI;

public class CardRotation : MonoBehaviour
{
    public GameObject Image { get; set; }
    public GameObject Description { get; set; }
    public Sprite SpriteFront { get; set; }
    public Sprite SpriteBack { get; set; }
    public Button Back { get; set; }

    private bool needRotation, isRotationLoaded;
    private RectTransform rectTransform;
    private float speedRotation, deltaRotation = 0.1f;

    public void Rotation()
    {
        if (!Description.activeSelf)
        {
            Image.GetComponent<Image>().sprite = SpriteFront;
            Description.SetActive(false);
        }
        else
        {
            Image.GetComponent<Image>().sprite = SpriteBack;
            Description.SetActive(true);
        }

        Back.enabled = false;
        speedRotation = GameConstants.SpeedCardRotation;
        rectTransform = Image.GetComponent<RectTransform>();
        rectTransform.pivot = new Vector2(0.5f, 0.5f);
        rectTransform.localPosition = new Vector3(rectTransform.localPosition.x, rectTransform.localPosition.y, -500f);
        needRotation = true;
        isRotationLoaded = false;
    }

    private void Update()
    {
        if (needRotation)
        {
            rectTransform.Rotate(0f, speedRotation * Time.deltaTime, 0f, Space.Self);

            if (rectTransform.rotation.eulerAngles.y > 90f - deltaRotation && !isRotationLoaded)
            {
                isRotationLoaded = true;
                if (Description.activeSelf)
                {
                    Image.GetComponent<Image>().sprite = SpriteFront;
                    Description.SetActive(false);
                }
                else
                {
                    Image.GetComponent<Image>().sprite = SpriteBack;
                    Description.SetActive(true);
                }
                if (speedRotation > 0) speedRotation *= -1;
            }

            if (rectTransform.rotation.eulerAngles.y > 180f - deltaRotation)
            {
                needRotation = false;
                Back.enabled = true;
            }
        }
    }
}
