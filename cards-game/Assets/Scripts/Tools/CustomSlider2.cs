﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomSlider2 : MonoBehaviour {
    
    Image fill;
    RectTransform fillRect;

    float ratio1, ratio2, ratio3;

    private void Start()
    {
        fill = gameObject.transform.GetChild(0).GetChild(0).GetComponent<Image>();
        float ratio = Screen.width / 2160f;
        if (ratio >= 0.5f && ratio < 0.75f) (fill.sprite = URPNGLoader.LoadURPNG("3/" +"atlas_1", 4)).name = "75%"; 
        else if (ratio >= 0.25f && ratio < 0.5f) (fill.sprite = URPNGLoader.LoadURPNG("3/" +"atlas_1", 4)).name = "50%";
        else if (ratio > 0f && ratio < 0.25f) (fill.sprite = URPNGLoader.LoadURPNG("3/" +"atlas_1", 5)).name = "25%";
        else fill.sprite = URPNGLoader.LoadURPNG("3/" +"atlas_1", 5);;

        gameObject.transform.GetComponentInParent<Canvas>().sortingLayerName = "MenuPanel";
        gameObject.transform.GetComponentInParent<Canvas>().sortingOrder = 1;
    }
}
