﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class AlphaAnimation : MonoBehaviour
{
    private bool doAlpha = false;
    private float speed, elapsed;

    public float Target { get; set; }
    public float T { get; set; }
    public Action OnComplete { get; set; }
    private ComponentType componentType;

    public void DoAlpha(ComponentType componentType)
    {
        if (!doAlpha)
        {
            this.componentType = componentType;
            Color color = new Color();
            switch (componentType)
            {
                case ComponentType.Image:
                    color = GetComponent<Image>().color;
                    break;
                case ComponentType.ParticleSystem:
                    color = GetComponent<ParticleSystem>().main.startColor.color;
                    break;
            }
            doAlpha = true;
            speed = (Target - color.a) / T;
            elapsed = 0.0f;
        }
    }

    private void Update()
    {
        if (doAlpha)
        {
            float d = Time.deltaTime;
            elapsed += d;

            Color color, newColor;
            float alphaObject, alphaParticles;
            switch (componentType)
            {
                case ComponentType.Image:
                    color = GetComponent<Image>().color;
                    alphaObject = color.a + d * speed;
                    newColor = new Color(color.r, color.g, color.b, alphaObject);
                    GetComponent<Image>().color = newColor;
                    break;
                case ComponentType.ParticleSystem:
                    ParticleSystem particleSystem = GetComponent<ParticleSystem>();
                    ParticleSystem.MainModule main = particleSystem.main;

                    var colorOverLifetime = particleSystem.colorOverLifetime;
                    Color colorParticles = colorOverLifetime.color.color;
                    alphaParticles = colorParticles.a + d * speed;
                    color = main.startColor.color;
                    colorOverLifetime.color = new Color(color.r, color.g, color.b, alphaParticles);
                    alphaObject = color.a + d * speed;
                    newColor = new Color(color.r, color.g, color.b, alphaObject);
                    main.startColor = newColor;
                    break;
            }

            if (elapsed >= T)
            {
                doAlpha = false;
                OnComplete?.Invoke();
            }
        }
    }
}
