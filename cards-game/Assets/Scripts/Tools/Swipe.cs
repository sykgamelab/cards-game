﻿using System;
using UnityEngine;

public class Swipe : MonoBehaviour 
{
    public Action action;

    private readonly Vector2 xAxis = new Vector2(1, 0);
    private const float angleRange = 30;
    private const float minSwipeDistance = 50.0f;
    private const float minVelocity = 1300.0f;

    private Vector2 startPosition, endPosition, swipeVector;
    private float startTime;

    public void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            startPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            startTime = Time.time;
        }

        if (Input.GetMouseButtonUp(0))
        {
            float deltaTime = Time.time - startTime;

            endPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            swipeVector = endPosition - startPosition;

            float velocity = swipeVector.magnitude / deltaTime;

            if (velocity > minVelocity && swipeVector.magnitude > minSwipeDistance)
            {
                swipeVector.Normalize();

                float angleOfSwipe = Vector2.Dot(swipeVector, xAxis);
                angleOfSwipe = Mathf.Acos(angleOfSwipe) * Mathf.Rad2Deg;

                if (angleOfSwipe < angleRange || (180.0f - angleOfSwipe) < angleRange)
                {
                    action?.Invoke();
                }
            }
        }
    }
}
