﻿using System;
using UnityEngine;
using System.Threading;
using System.Collections.Generic;
#if UNITY_IOS
using System.Runtime.InteropServices;
using UnityEngine.SocialPlatforms;
using UnityEngine.SocialPlatforms.GameCenter;
using System.Threading.Tasks;
using System.Text;
#endif

namespace SGLTools
{
    public static class SocialManagerInstantiator
    {
        public abstract class SocialManager
        {
            public class Score
            {
                public string UID { get; set; }
                public long Value { get; set; }

                public Score(string uid, long value)
                {
                    UID = uid;
                    Value = value;
                }
            }

            public enum TypeAchievement { Unlock, Increment };

            public class Achievement
            {
                public string ID { get; set; }
                public double Value { get; set; }
                public bool Completed { get; set; }
                public long LastUpdatedTimestamp { get; set; }

                public Achievement(string id, double value, bool completed, long timestamp)
                {
                    ID = id;
                    Value = value;
                    Completed = completed;
                    LastUpdatedTimestamp = timestamp;
                }
            }

            public class VerificationSignature
            {
                public string Url { get; set; }
                public byte[] Signature { get; set; }
                public byte[] Salt { get; set; }
                public long Timestamp { get; set; }
                public string UserId { get; set; }
                public string Token { get; set; }

                public VerificationSignature(string url, byte[] signature, byte[] salt, long timestamp, string userId)
                {
                    Url = url;
                    Signature = signature;
                    Salt = salt;
                    Timestamp = timestamp;
                    UserId = userId;
                }

                public VerificationSignature(string token)
                {
                    Token = token;
                }
            }

            public abstract void Authenticate(Action callback, Action iosAlert);
            public abstract void Logout(Action callback = null);
            public abstract bool Authenticated { get; }
            public abstract string LastError { get; }
            public abstract void ShowLeaderboard(string leaderbordID);
            public abstract void LoadPlayerCurrentScore(string leaderboardID, Action<Score> callback);
            public abstract void ReportScore(string leaderbordID, long score, Action callback);
            public abstract void GenerateIdentityVerificationSignature(Action<VerificationSignature> callback);
            /* ----------------------------- Achievements ------------------------------ */
            public abstract void ShowAchievements();
            public abstract void LoadAchievements(Action<List<Achievement>> callback);
            public abstract void ReportAchievementProgress(TypeAchievement typeAchievement, string achievementID, double progress, Action callback = null);
            public abstract void ResetAchievementsForTesters();
            /* ------------------------------------------------------------------------- */
            public abstract string UserID { get; }
            public abstract string UserDisplayName { get; }
        }

#if UNITY_EDITOR
        private class UnitySocialManager : SocialManager
        {
            private bool authenticated = false;
            private string lastError = null;

            public override void Authenticate(Action callback, Action iosAlert = null)
            {
                authenticated = true;
                callback?.Invoke();
            }
            public override void Logout(Action callback = null) { Debug.Log("START Logout in Editor"); }
            public override bool Authenticated { get => authenticated; }
            public override string LastError { get => lastError; }
            public override void ShowLeaderboard(string id) => Debug.Log("START ShowLeaderboard in Editor");
            public override void LoadPlayerCurrentScore(string id, Action<Score> cb) => Debug.Log("START LoadPlayerCurrentScore in Editor");
            public override void ReportScore(string id, long score, Action cb) => Debug.Log("START ReportScore in Editor");

            public override void GenerateIdentityVerificationSignature(Action<VerificationSignature> callback)
            {
#if UNITY_ANDROID
				VerificationSignature sig = new VerificationSignature("eyJhbGciOiJSUzI1NiIsImtpZCI6IjdjMzA5ZTNhMWMxOTk5Y2IwNDA0YWI3MTI1ZWU0MGI3Y2RiY2FmN2QiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiI5MzYyNzU0NzE4MTctMWM0NG1ra3RoNDU5c2liajBlYTdia283bXFzczFhcWcuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiI5MzYyNzU0NzE4MTctbDRqMDQyZ2F0YmIxcjdpYjMyZGhhMnJyNnE1aWNrOWUuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMDk5NzExNzI4ODc5NDQyOTExMDIiLCJlbWFpbCI6ImFudG9uZ3JhYm92c2t5MjAxNEBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwibmFtZSI6ItCQ0L3RgtC-0L0g0JPRgNCw0LHQvtCy0YHQutC40LkiLCJnaXZlbl9uYW1lIjoi0JDQvdGC0L7QvSIsImZhbWlseV9uYW1lIjoi0JPRgNCw0LHQvtCy0YHQutC40LkiLCJpYXQiOjE1NDk1MjQ2MTksImV4cCI6MTU0OTUyODIxOX0.NAw4qbIlqHvREBkaigG2-rUfix5SAKatNWL4FCSsmv3Kfcdz2wZp0HyPR3LZZO5awKy5Bj2YfzmWROj0vsWxhkNnx612bUo4tWWBdpfrZB0uz-Hjo4aXnMQ6pjt6_55cY7uIr2D2xzU5GxQAH5nxIYzpYm4ZpwR5VN9yOtrz104fQT46cmG5rf-1c9701L71_AzMp__K2ZzEMU3YorRser7B6nytxiH_rKM2Ou6OaXeAU45iOu4OmvAhAborv5epGzGmPgye3Rb1tguEFEcoh1GUtyL8VKxDqBx0lexQrgJFwBIYRD2DmaHjIggcHG9uv0Shn977U6MK_u6WvHKXTA");
				callback.Invoke(sig);
#elif UNITY_IOS
#endif
            }

            /* ----------------------------- Achievements ------------------------------ */
            public override void ShowAchievements() => Debug.Log("START ShowAchievements in Editor");
            public override void LoadAchievements(Action<List<Achievement>> callback) => Debug.Log("START LoadAchievements in Editor");
            public override void ReportAchievementProgress(TypeAchievement typeAchievement, string achievementID, double progress, Action callback) => Debug.Log("START ReportAchievementProgress in Editor");
            public override void ResetAchievementsForTesters() => Debug.Log("START ResetAchievementsForTesters in Editor");
            /* ------------------------------------------------------------------------- */
            public override string UserID => "TestID";
            public override string UserDisplayName => "TestNick";
        }
#elif UNITY_IOS
        private class IOSSocialManager : SocialManager
        {
            [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
            private struct NativeVerificationSignature
            {
                public unsafe byte* url;
                public int urlSize;
                public unsafe byte* signature;
                public int signatureSize;
                public unsafe byte* salt;
                public int saltSize;
                public long timestamp;
                public unsafe byte* userId;
                public int userIdSize;
            }

            [DllImport("__Internal")]
            private static extern void authenticateNative();

            [DllImport("__Internal")]
            private static extern bool isAuthenticateCancelledNative();

            [DllImport("__Internal")]
            private static extern bool isAuthenticationDoneNative();

            [DllImport("__Internal")]
            private static extern bool isAuthenticatedNative();

            [DllImport("__Internal")]
            private static extern string getLastErrorNative();

            [DllImport("__Internal")]
            private static extern void showLeaderboardNative(string id);

            [DllImport("__Internal")]
            private static extern void reportScoreNative(long score, string id);

            [DllImport("__Internal")]
            private static extern bool isScoreReportedNative();

            [DllImport("__Internal")]
            private static extern void generateIdentityVerificationSignatureNative();

            [DllImport("__Internal")]
            private static extern bool isVerificationSignatureReadyNative();

            [DllImport("__Internal")]
            private static extern NativeVerificationSignature getVerificationSignatureNative();

            [DllImport("__Internal")]
            private static extern void loadPlayerCurrentScoreNative(string leaderboardID);

            [DllImport("__Internal")]
            private static extern bool isScoreReadyNative();

            [DllImport("__Internal")]
            private static extern string getPlayerIDNative();

            [DllImport("__Internal")]
            private static extern string getPlayerDisplayNameNative();

            [DllImport("__Internal")]
            private static extern int getPlayerScoreNative();

            [DllImport("__Internal")]
            private static extern void freeNative(IntPtr ptr);

            /* ----------------------------- Achievements ------------------------------ */
            [DllImport("__Internal")]
            private static extern bool isAchievementsReadyNative();

            [DllImport("__Internal")]
            private static extern bool isAchievementReportedNative();

            [DllImport("__Internal")]
            private static extern void showAchievementsNative();

            [DllImport("__Internal")]
            private static extern void loadAchievementsNative();

            [DllImport("__Internal")]
            private static extern int getAchievementsCountNative();

            [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
            private struct NativeAchievement
            {
                public unsafe byte* achievementID;
                public int achievementIDSize;
                public double percent;
                public bool completed;
                public long timestamp;
            }

            [DllImport("__Internal")]
            private static extern IntPtr getAchievementsNative();

            [DllImport("__Internal")]
            private static extern void reportAchievementProgressNative(string id, double progress);

            [DllImport("__Internal")]
            private static extern void resetAchievementsForTestersNative();
            /* ------------------------------------------------------------------------- */

            public override void Authenticate(Action callback = null, Action iosAlert = null)
            {
                if (!isAuthenticateCancelledNative())
                {
                    authenticateNative();
                    new Thread(() =>
                    {
                        while (!isAuthenticationDoneNative()) Thread.Sleep(1000);
                        callback?.Invoke();
                    }).Start();
                }
                else iosAlert?.Invoke();
            }

            public override void Logout(Action callback = null) {}

            public override bool Authenticated { get => isAuthenticatedNative(); }
            public override string LastError { get => getLastErrorNative(); }

            public override void ShowLeaderboard(string leaderbordID) => showLeaderboardNative(leaderbordID);

            public override void LoadPlayerCurrentScore(string leaderboardID, Action<Score> callback)
            {
                new Thread(() =>
                {
                    loadPlayerCurrentScoreNative(leaderboardID);
                    while (!isScoreReadyNative()) Thread.Sleep(1000);

                    string id = getPlayerIDNative();
                    int score = getPlayerScoreNative();

                    Debug.Log($"SocialManager.LoadPlayerCurrentScore: BEFORE LOAD CALLBACK: id = {id}, score = {score}");
                    callback?.Invoke(new Score(id, score));
                }).Start();
            }

            public override void ReportScore(string leaderboardID, long score, Action callback = null)
            {
                
                new Thread(() =>
                {
                    reportScoreNative(score, leaderboardID);
                    while (!isScoreReportedNative()) Thread.Sleep(1000);
                    callback();
                }).Start();
            }

            public override void GenerateIdentityVerificationSignature(Action<VerificationSignature> callback = null)
            {
                new Thread(() =>
                {
                    generateIdentityVerificationSignatureNative();

                    while (!isVerificationSignatureReadyNative()) Thread.Sleep(1000);

                    var nativeVerificationSignature = getVerificationSignatureNative();
                    string url = null;
                    byte[] signature = new byte[nativeVerificationSignature.signatureSize];
                    byte[] salt = new byte[nativeVerificationSignature.saltSize];
                    string userId = null;
                    unsafe
                    {
                        url = Encoding.ASCII.GetString(nativeVerificationSignature.url, nativeVerificationSignature.urlSize);
                        Marshal.Copy((IntPtr)nativeVerificationSignature.signature, signature, 0, nativeVerificationSignature.signatureSize);
                        Marshal.Copy((IntPtr)nativeVerificationSignature.salt, salt, 0, nativeVerificationSignature.saltSize);
                        userId = Encoding.ASCII.GetString(nativeVerificationSignature.userId, nativeVerificationSignature.userIdSize);

                        freeNative((IntPtr)nativeVerificationSignature.url);
                        freeNative((IntPtr)nativeVerificationSignature.signature);
                        freeNative((IntPtr)nativeVerificationSignature.salt);
                        freeNative((IntPtr)nativeVerificationSignature.userId);
                    }

                    VerificationSignature verificationSignature = new VerificationSignature(url, signature, salt,
                        nativeVerificationSignature.timestamp, userId);
                    callback?.Invoke(verificationSignature);
                }).Start();
            }

            /* ----------------------------- Achievements ------------------------------ */
            public override void ShowAchievements() => showAchievementsNative();

            public override void LoadAchievements(Action<List<Achievement>> callback)
            {
                new Thread(() =>
                {
                    loadAchievementsNative();
                    while (!isAchievementsReadyNative()) Thread.Sleep(1000);

                    List<Achievement> achievements = null;
                    int count = getAchievementsCountNative();
                    if (count > 0)
                    {
                        achievements = new List<Achievement>();
                        IntPtr ptr = getAchievementsNative();
                        NativeAchievement[] nativeAchievements = new NativeAchievement[count];

                        for (int i = 0; i < nativeAchievements.Length; i++)
                        {
                            nativeAchievements[i] = (NativeAchievement)Marshal.PtrToStructure(ptr, typeof(NativeAchievement));
                            ptr += Marshal.SizeOf(typeof(NativeAchievement));

                            string achievementID = null;
                            unsafe
                            {
                                achievementID = Encoding.ASCII.GetString(nativeAchievements[i].achievementID, nativeAchievements[i].achievementIDSize);
                                freeNative((IntPtr)nativeAchievements[i].achievementID);
                            }
                            Achievement achievement = new Achievement(achievementID, nativeAchievements[i].percent, nativeAchievements[i].completed, nativeAchievements[i].timestamp);
                            achievements.Add(achievement);
                        }
                    }
                    callback?.Invoke(achievements);
                }).Start();
            }

            public override void ReportAchievementProgress(TypeAchievement typeAchievement, string achievementID, double progress, Action callback)
            {
                new Thread(() =>
                {
                    reportAchievementProgressNative(achievementID, progress);
                    while (!isAchievementReportedNative()) Thread.Sleep(1000);
                    callback?.Invoke();
                }).Start();
            }

            public override void ResetAchievementsForTesters() => resetAchievementsForTestersNative();
            /* ------------------------------------------------------------------------- */

            public override string UserID => getPlayerIDNative();

            public override string UserDisplayName => getPlayerDisplayNameNative();
        }
#elif UNITY_ANDROID
        private class AndroidSocialManager : SocialManager
        {
            private AndroidJavaClass unityPlayerClass;
            private AndroidJavaObject activity;

            public AndroidSocialManager()
            {
                unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                activity = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");
            }

            public override void Authenticate(Action callback = null, Action iosAlert = null)
            {
                Action<object> action = (arg) =>
                {
                    //Debug.Log("DO AUTH");
                    while (Application.internetReachability == NetworkReachability.NotReachable)
                    {
                        //Debug.Log("AUTH AWAIT INTERNET");
                        Thread.Sleep(5000);
                    }

                    activity.Call("authenticate");

                    //Debug.Log("AUTH IN THREAD");
                    while (!activity.Call<bool>("isAuthenticationDone"))
                    {
                        Thread.Sleep(1000);
                        //Debug.Log("THREAD SLEEP");
                    }

                    //Debug.Log("BEFORE AUTH CALLBACK");
                    Action cb = (Action)arg;
                    cb?.Invoke();
                };

                new AndroidThread(action, callback).Start();
            }

            public override void Logout(Action callback = null)
            {
                Action<object> action = (arg) =>
                {
                    activity.Call("logout");
                    while (!activity.Call<bool>("isLogoutDone")) Thread.Sleep(1000);
                    Action cb = (Action)arg;
                    cb?.Invoke();
                };

                new AndroidThread(action, callback).Start();
            }

            public override bool Authenticated
            {
                get
                {
                    bool result = activity.Call<bool>("isAuthenticated");
                    return result;
                }
            }

            public override string LastError
            {
                get
                {
                    string le = null;

                    le = activity.Call<string>("getLastError");
                    //Debug.Log($"LastError: result = {le}");

                    return le;
                }
            }

            public override void ShowLeaderboard(string leaderboardID) => activity.Call("showLeaderboard", leaderboardID);

            public override void LoadPlayerCurrentScore(string leaderboardID, Action<Score> callback)
            {
                activity.Call("loadPlayerCurrentScore");

                Action<object> action = (arg) =>
                {
                    //Debug.Log("LOAD SCORE IN THREAD");
                    while (!activity.Call<bool>("isPlayerScoreLoaded"))
                    {
                        Thread.Sleep(1000);
                        //Debug.Log("THREAD SLEEP");
                    }

                    long playerScore = activity.Call<long>("getPlayerScore");
                    string playerUID = activity.Call<string>("getPlayerUID");

                    //Debug.Log("BEFORE LOAD SCORE CALLBACK");
                    Action<Score> cb = (Action<Score>)arg;
                    cb?.Invoke(new Score(playerUID, playerScore));
                };

                new AndroidThread(action, callback).Start();
            }

            public override void ReportScore(string leaderboardID, long score, Action callback)
            {
                Action<object> action = (arg) =>
                {
                    activity.Call("submitScore", score, leaderboardID);

                    //Debug.Log("REPORT SCORE IN THREAD");
                    while (!activity.Call<bool>("isScoreSubmited"))
                    {
                        Thread.Sleep(1000);
                        //Debug.Log("THREAD SLEEP");
                    }

                    //Debug.Log("BEFORE REPORT SCORE CALLBACK");
                    Action cb = (Action)arg;
                    cb?.Invoke();
                };

                new AndroidThread(action, callback).Start();
            }

            public override void GenerateIdentityVerificationSignature(Action<VerificationSignature> callback)
            {
                Action<object> action = (arg) =>
                {
                    //Debug.Log("GENERATE ANDROID SIGNATURE");
                    string sigStr = activity.Call<string>("generateIdentityVerificationSignature");
                    VerificationSignature sig = new VerificationSignature(sigStr);
                    Action<VerificationSignature> cb = (Action<VerificationSignature>)arg;
                    cb?.Invoke(sig);
                    //Debug.Log($"DONE GENERATE ANDROID SIGNATURE: UserId = {UserID}, Token = {sig.Token}, Url = {sig.Url}, Timestamp = {sig.Timestamp}");
                };

                new AndroidThread(action, callback).Start();
            }

            /* ----------------------------- Achievements ------------------------------ */
            public override void ShowAchievements() => activity.Call("showAchievements");

            public override void LoadAchievements(Action<List<Achievement>> callback)
            {
                Action<object> action = (arg) =>
                {
                    Action<List<Achievement>> cb = (Action<List<Achievement>>)arg;
                    List<Achievement> achievements = null;

                    activity.Call("loadAchievements");
                    while (!activity.Call<bool>("isAchievementsLoaded")) Thread.Sleep(1000);

                    AndroidJavaObject achievementsJavaObject = activity.Call<AndroidJavaObject>("getAchievements");
                    if (achievementsJavaObject != null)
                    {
                        int size = achievementsJavaObject.Call<int>("size");
                        if (size > 0)
                        {
                            achievements = new List<Achievement>();
                            for (int i = 0; i < size; i++)
                            {
                                AndroidJavaObject achievementJavaObject = achievementsJavaObject.Call<AndroidJavaObject>("get", i);
                                string id = achievementJavaObject.Call<string>("getId");
                                double value = achievementJavaObject.Call<int>("getCurrentSteps");
                                bool completed = achievementJavaObject.Call<bool>("isCompleted");
                                long timestamp = achievementJavaObject.Call<long>("getLastUpdatedTimestamp");
                                achievements.Add(new Achievement(id, value, completed, timestamp));
                            }
                        }
                    }
                    cb?.Invoke(achievements);
                };

                new AndroidThread(action, callback).Start();
            }

            public override void ReportAchievementProgress(TypeAchievement typeAchievement, string achievementID,
                double progress, Action callback)
            {
                int type = 0;
                if (typeAchievement == TypeAchievement.Increment) type = 1;

                Action<object> action = (arg) =>
                {
                    activity.Call("reportAchievementProgress", type, achievementID, progress);
                    //Debug.Log("ReportAchievementProgress.LoadAchievements.action: BEFORE SLEEP");
                    Thread.Sleep(4000);
                    //Debug.Log("ReportAchievementProgress.LoadAchievements.action: AFTER SLEEP");

                    //Debug.Log("REPORT ACHIEVEMENT PROGRESS IN THREAD");
                    while (!activity.Call<bool>("isAchievementProgressReported"))
                    {
                        Thread.Sleep(1000);
                        //Debug.Log("THREAD ACHIEVEMENT SLEEP");
                    }

                    //Debug.Log("BEFORE REPORT ACHIEVEMENT PROGRESS CALLBACK");
                    Action cb = (Action)arg;
                    cb?.Invoke();
                };

                new AndroidThread(action, callback).Start();
            }

            public override void ResetAchievementsForTesters() => activity.Call("resetAchievements");
            /* ------------------------------------------------------------------------- */

            public override string UserID => activity.Call<string>("getPlayerID");

            public override string UserDisplayName => activity.Call<string>("getPlayerDisplayName");
        }
#endif

        private static SocialManager instance = null;

        public static SocialManager GetInstance()
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                instance = new UnitySocialManager();
#elif UNITY_IOS
				instance = new IOSSocialManager();
#elif UNITY_ANDROID
                instance = new AndroidSocialManager();
#endif
            }

            return instance;
        }
    }
}