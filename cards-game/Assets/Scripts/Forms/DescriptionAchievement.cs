﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DescriptionAchievement : Form
{
    Text textAchievementName, textAchievementDescription;
    public static DescriptionAchievement I { get; set; }

    private void Awake()
    {
        Messanger<Events>.Subscribe(Events.UpdateAnchors, OnUpdateAnchors);
        I = this;
    }

    public void BackToMenu()
    {
        Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.DoorClosing);
        gameObject.SetActive(false);
    }

    private void Start()
    {
        objects["Back"].GetComponent<Button>().onClick.AddListener(BackToMenu);
        objects["BackBlur"].GetComponent<Button>().onClick.AddListener(BackToMenu);
        objects["Background"].transform.SetParent(objects["frame"].transform.parent);
        objects["frame"].transform.SetAsLastSibling();
        objects["frame"].GetComponent<Image>().raycastTarget = false;

        textAchievementName = objects["AchievementName"].GetComponent<Text>();
        textAchievementDescription = objects["AchievementDesc"].GetComponent<Text>();

        Started = true;
    }

    public void OpenDescriptionAchievement(string achievement)
    {
        gameObject.SetActive(true);
        textAchievementName.text = Helper.GetAchievementStrings(achievement).name;
        textAchievementDescription.text = Helper.GetAchievementStrings(achievement).description;
    }

    private void OnUpdateAnchors()
    {
        gameObject.SetActive(false);
    }

    public void Init(string name, string description)
    {
        textAchievementName.text = name;
        textAchievementDescription.text = description;
    }
}
