﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameResultsPanel : Form
{
    public static GameResultsPanel I { get; set; }

    Text PowerCount, RewardText;

    GameObject Restart,
        CrystalsImage,
        CrystalsText,
        GameResultText,
        SunRays,
        ResultWithCrystals;

    private void Awake()
    {
        I = this;
        Messanger<Events>.Subscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    private void OnDestroy()
    {
        Messanger<Events>.Unsubscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    private void OnUpdateAnchors()
    {
        SunRays = objects["SunRays"];
        CrystalsImage = objects["CrystalsImage"];
        CrystalsText = objects["CrystalText"];
        ResultWithCrystals = objects["WithCrystals"];
        GameResultText = objects["GameResult"];
        Restart = objects["Restart"];
        PowerCount = objects["PowerCount"].GetComponent<Text>();
        RewardText = objects["RewardText"].GetComponent<Text>();

        List<GameObject> Txts = new List<GameObject>();
        objects["GameResult"].AddComponent<Shadow>().effectDistance = new Vector2(5f, -3f);
        Txts.Add(objects["RewardText"]);
        Txts.Add(objects["PowerCount"]);
        Txts.Add(objects["PowerText"]);
        Txts.Add(objects["CrystalText"]);
        Txts.Add(objects["MonstersWon"]);
        foreach (var obj in Txts)
        {
            Outline ol = obj.AddComponent<Outline>();
            ol.effectDistance = new Vector2(1f, 1f);    
            obj.GetComponent<Text>().fontStyle = FontStyle.Bold;
        }
        Txts = null;

        PowerCount.text = PlayerPrefs.GetInt("Power").ToString();

        Restart.GetComponent<Button>().onClick.AddListener(() => PlayPanel.I.gameObject.SetActive(true));
        Restart.GetComponent<Button>().onClick.AddListener(PlayGame);

        objects["Menu"].GetComponent<Button>().onClick.AddListener(BackToMenu);
        objects["Menu"].GetComponent<Button>().onClick.AddListener(() => PlayPanel.I.BackToMenu());

        gameObject.SetActive(false);
    }

    private void LoadLocalization()
    {
        objects["MonstersWon"].GetComponent<Text>().text = "MONSTERS WIN";// "ПОБЕДИЛИ МОНСТРЫ";
        objects["PowerText"].GetComponent<Text>().text = "POWERS"; //"СИЛЫ";
        objects["RestartText"].GetComponent<Text>().text = "RESTART"; // "ЗАНОВО";
        objects["MenuText"].GetComponent<Text>().text = "MENU"; // "МЕНЮ";
    }

    public void PlayGame()
    {
        Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.PlayGame);

        TablePanel.I.gameObject.SetActive(true);
        MenuPanel.I.GoToPlay(false);
        DefaultGameState();
    }

    private void DefaultGameState()
    {
        TablePanel.I.GetObject("Monsters").gameObject.SetActive(false);
        TablePanel.I.GetObject("FreePriceForDealCard").gameObject.SetActive(false);
    }

    public void BackToMenu()
    {
        if (Helper.IsMusicActive()) Messanger<string, Events>.SendMessage(Events.PlayMusic, SoundClips.LobbyMusic);

        gameObject.SetActive(false);
        TablePanel.I.gameObject.SetActive(false);
        MenuPanel.I.gameObject.SetActive(true);
        Helper.SetStateRewardIcon();
    }

    public void GoToShop()
    {
        ShopPanel.I.SetActiveCrystalsContent(true);
        Messanger<Events>.SendMessage(Events.SwitchShop);
        ShopPanel.I.gameObject.SetActive(true);
        Messanger<ShopTabs, Events>.SendMessage(Events.OpenShopTab, ShopTabs.Crystals);
    }

    void Start()
    {
        LoadLocalization();
        Started = true;
    }

    private void StartTraining5()
    {
        MenuPanel.I.GetObject("Shop").GetComponent<Button>().interactable = true;
        MenuPanel.I.GetObject("Achievements").GetComponent<Button>().interactable = true;
        MenuPanel.I.GetObject("Collection").GetComponent<Button>().interactable = true;
        MenuPanel.I.GetObject("CrystalButton2").GetComponent<Button>().interactable = true;
        FirstGame.I.OnFivethTraining();
    }

    public void GameResult(bool win)
    {
        Messanger<bool, Events>.SendMessage(Events.SetMusicActive, false);

        // БЛОК ДЛЯ ОБУЧЕНИЯ
        if (PlayerPrefs.GetInt("FirstGame") < 4) // игрок прошел часть обучения
        {
            if (win || Helper.GetTraining3Passed()) // успешно или, только когда 3 обучение реплика 5 уже была (то есть игрок прошел 3 обучение, но проиграл)
            {
                if (PlayerPrefs.GetInt("FirstGame") == 3 && !Helper.GetTraining5Passed()) // игрок прошел обучение часть 4, пора перейти к обучению часть 5
                {
                    StartTraining5();
                }
                PlayerPrefs.SetInt("FirstGame", PlayerPrefs.GetInt("FirstGame", 0) + 1);
                FirstGame.intraining = false;
                TrainingPanel.I.gameObject.SetActive(false);   
            }
            else GameOverInTraining(); // если игрок намеренно или случайно приграл, придется пройти обучение заново
        }
        else if (PlayerPrefs.GetInt("FirstGame") == 4 && !win && PlayerPrefs.GetInt("FirstGameOver", 0) != 1) // игрок полностью прошел обучение и первый раз проиграл
        {
            PlayerPrefs.SetInt("FirstGameOver", 1);
            FirstGameOver();
        }
        else if (PlayerPrefs.GetInt("FirstGame") == 4 && !Helper.GetTraining5Passed())
        {
            StartTraining5();
        }
        else TrainingPanel.I.gameObject.SetActive(false);
        // КОНЕЦ БЛОК ДЛЯ ОБУЧЕНИЯ                           

        TablePanel.I.TrackAchievement(win);

        // блок 6 части обучения
        if (PlayerPrefs.GetInt("FirstOpenAchievement") != 1)                 // если первая ачивка еще не получена
        {
            foreach (string achiv in Enum.GetNames(typeof(OpenAchievement))) // проходимся по ачивкам
            {
                if (achiv == "spend_crystals") continue;                     // если ачивка - заработать кристаллы - ничего не делать  остальные ачивки недостижимы
                if (PlayerPrefs.GetInt(achiv) >= Helper.GetOpenAchievementStrings(achiv).need[0]) // если ачивка выполнена
                {
                    //GameResultsPanel.I.GetObject("Restart").GetComponent<Button>().interactable = false;// выключить рестарт и меню
                    //GameResultsPanel.I.GetObject("Menu").GetComponent<Button>().interactable = false;   // выключить рестарт и меню
                    SceneController.I.gameObject.GetComponent<FirstGame>().OnFirstAchievement();          // запустить обучение
                    break;
                }
            }
        }
        gameObject.SetActive(true); // зачем?
        PlayerPrefs.SetInt("GameIsNotOver", 0);
        if (win)
        {
            Helper.GameResultSended = true;
            Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.WinningFanfare);

            SunRays.SetActive(true);
            GameResultText.GetComponent<Text>().text = Helper.GetLocalString("Win"); //"ПОБЕДА";
            if (PlayerPrefs.GetInt("Power", GameConstants.maxPower) < GameConstants.maxPower) Messanger<int, Events>.SendMessage(Events.AddPower, 1); //PlayerPrefs.SetInt("Power", PlayerPrefs.GetInt("Power", GameConstants.maxPower) + 1);

            Helper.TrackTaskInProgress(InGameTasks.WinGames.ToString(), 1);
            Helper.TrackTaskInProgress(InGameTasks.WinGamesInRow.ToString(), 1);
            if (PlayerParameters.Energy == PlayerParameters.MaxEnergy) Helper.TrackTaskInProgress(InGameTasks.WinGamesFullEnergy.ToString(), 1);
            if (PlayerParameters.Health == PlayerParameters.MaxHealth) Helper.TrackTaskInProgress(InGameTasks.WinGamesMaxBravery.ToString(), 1);
            if (PlayerParameters.Energy < 3 && PlayerParameters.Health < 3) Helper.TrackTaskInProgress(InGameTasks.WinGamesLess3EnergyBravery.ToString(), 1);
            if (PlayerParameters.Health < 3) Helper.TrackTaskInProgress(InGameTasks.WinGamesLess3Bravery.ToString(), 1);
            // win games
            
            if (Helper.GetTraining5Passed())
            {
                Helper.AddWinSeries();
                SceneController.SaveParams(); // saver coding winseries
                if (Helper.GetWinSeries() >= GameConstants.MinCountWinForWinSeries) SocialManager.I.SetWinSeriesToReport();
                int winSeries = Helper.GetWinSeries();
                if (winSeries >= 5 && winSeries <= 10 && PlayerPrefs.GetInt("WinSeriesReplics", 0) < winSeries)
                {
                    string method = $"Win{winSeries}TimesInARowReplica1";
                    FirstGame firstGame = SceneController.I.GetComponent<FirstGame>(); // new FirstGame();
                    var methodInfo = typeof(FirstGame).GetMethod(method);
                    methodInfo.Invoke(firstGame, null);
                    PlayerPrefs.SetInt("WinSeriesReplics", PlayerPrefs.GetInt("WinSeriesReplics", 0) + 1);
                }
            }
        }
        else
        {
            Helper.GameResultSended = true;
            Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.Fail);

            SunRays.SetActive(false);
            GameResultText.GetComponent<Text>().text = Helper.GetLocalString("GameOver"); //"ПОРАЖЕНИЕ";
            if (Helper.GetProgress(InGameTasks.WinGamesInRow.ToString()) != -1) Helper.SetProgress(InGameTasks.WinGamesInRow.ToString(), 0); // сброс прогресса

            if (Helper.GetTraining5Passed()) Helper.ResetWinSeries();
        }
        GameResultText.SetActive(true);

        /********************************  Проверка бустеров  *************************************************/
        int bonusLevel = GameConstants.BonusForLevel[Helper.GetLevel() - 1];
        float increaseCrystalsPercent = 0;
        int addCrystals = 0;
        if (Helper.GetBoosterActivated("IncreaseCrystals25") > 0) increaseCrystalsPercent += 0.25f;
        if (Helper.GetBoosterActivated("IncreaseCrystals50") > 0) increaseCrystalsPercent += 0.5f;
        if (Helper.GetBoosterActivated("IncreaseCrystals75") > 0) increaseCrystalsPercent += 0.75f; 
        if (Helper.GetBoosterActivated("CrystalsWin10") > 0) { addCrystals += 10; }
        if (Helper.GetBoosterActivated("CrystalsWin15") > 0) { addCrystals += 15; }
        //PlayPanel.I.ApplyBoosterManager();
        /*****************************************************************************************************/

        /********************************  Проверка ачивок  **************************************************/
        if (Helper.GetTraining5Passed())
            if (win && PlayerParameters.Health == 1 && Helper.GetAchievement("fears_control") == 0)
            {
                Helper.SetHiddenAchievementDone("fears_control");
            }
        if (!win)
        {
            foreach (CardPosition c in TableController.I.cardOnTable)
            {
                if (Helper.GetTraining5Passed())
                    if (c.Card != null)
                        if (c.Card.Type == CardType.Health && Helper.GetAchievement("strategist") == 0)
                        {
                            Helper.SetHiddenAchievementDone("strategist");
                        }
            }

            if (Deck.I.deck.Count + 8 == Deck.I.deckSize && TableController.I.OnGetFreePositions().Count >= 1/*т.к. карта, вызвавшая функцию еще на столе*/
                && Helper.GetAchievement("the_will_to_win") == 0)
            {
                Helper.SetHiddenAchievementDone("the_will_to_win");
            }

            if (Helper.GetTraining5Passed())
                if (TableController.I.namelast == "DeleteFromFieldAndOneHealth" && Helper.GetAchievement("extremely_dangerous") == 0)
                {
                    Helper.SetHiddenAchievementDone("extremely_dangerous");
                }
        }

        // проверка ачивки "Открыть все редкие достижения"
        if (Helper.GetAchievement("superstar") == 0 && Helper.IsAchievementSuperstarOpened())
        {
            Helper.SetHiddenAchievementDone("superstar");
        }
        /*****************************************************************************************************/

        // *******************************  Здесь выводится подсчет  ************************************ 
        if (win || Helper.GetBoosterActivated("CrystalsForLose") > 0)
        {
            ResultPanelParameters(true);
            RewardText.text = Helper.GetLocalString("GameWithCrystals").Replace("X", $"{PlayerParameters.DeadMonsters}").Replace("Y", $"{PlayerParameters.Crystal}").Replace("Z", $"{bonusLevel}");

            int plusCrystals = (int)((PlayerParameters.Crystal + PlayerParameters.DeadMonsters + bonusLevel) * (1 + increaseCrystalsPercent)) + addCrystals;
            CrystalsText.GetComponent<Text>().text = "+" + plusCrystals.ToString();
            Helper.AddCrystals(plusCrystals);

            Helper.TrackTaskInProgress(InGameTasks.GetCrystals.ToString(), plusCrystals);
            Helper.TrackTaskInProgress(InGameTasks.CollectCrystal.ToString(), plusCrystals);

            if (Helper.TaskInProgress(DailyTasks.TransformCards.ToString())) Helper.SetProgress(DailyTasks.TransformCards.ToString(), PlayerParameters.TransformCard);

            Messanger<Events>.SendMessage(Events.CrystalUpdate);
        }
        else
        {
            ResultPanelParameters(false);
            RewardText.text = "";
        }

        PowerCount.text = PlayerPrefs.GetInt("Power").ToString();

        Helper.TrackTaskInProgress(InGameTasks.PlayGames.ToString(), 1);
        SocialManager.AreOpenAchievementsChanged = true;
                                                                                                                                         
        Helper.AddBattleCount();
        if (Helper.GetBattleCount() >= GameConstants.Battles25ForTraining8) if (!Helper.GetTraining8Passed()) FirstGame.I.Part8();

        if (Helper.GetBattleCount() % GameConstants.CountBattlesForSetProgressToReport == 0)
            SocialManager.I.SetProgressToReport();
    }

    public void GameOverInTraining()
    {
        objects["Menu"].GetComponent<Button>().interactable = false;
        objects["Restart"].GetComponent<Button>().interactable = false;
        FirstGame.I.GameOverInTraining();
    }

    private void ResultPanelParameters(bool withCrystals)
    {
        GetObject("WithCrystals").SetActive(withCrystals);
        GetObject("CrystalsImage").SetActive(withCrystals);
        GetObject("LoseLine").SetActive(!withCrystals);
        if (withCrystals) { if (GetObject("PowerPlace").transform.childCount < 1) GetObject("Power").transform.SetParent(GetObject("PowerPlace").transform, false); }
        else if (GetObject("GameOverPowerPlace").transform.childCount < 1) GetObject("Power").transform.SetParent(GetObject("GameOverPowerPlace").transform, false);
    }

    public void FirstGameOver()
    {
        objects["Menu"].GetComponent<Button>().interactable = false;
        objects["Restart"].GetComponent<Button>().interactable = false;

        FirstGame.I.FirstGameOver1();
    }
}