﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SocialPlatforms;
using System.Threading;
#if UNITY_IOS
using UnityEngine.iOS;
#endif

public class MenuPanel : Form
{
    public static MenuPanel I { get; set; }

    GameObject CrystalText, PlayButton, rewardImage, tasksButton;
    public Button Play;
    Color clouds;
    Image imgClouds;

    bool isReadyLoad = false, isDailyReceived = false;
    int statusDailyRequest = 0;
    string dailyNumbers = null;

    private void StartSoundOpenPanel()
    {
        Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.DoorOpening);
    }

    public void GoToTasks()
    {
        StartSoundOpenPanel();

        TasksPanel.I.gameObject.SetActive(true);
        gameObject.SetActive(false);
        TasksPanel.I.GetObject("Task1").GetComponent<CurrTask>().Init();
        TasksPanel.I.GetObject("Task2").GetComponent<CurrTask>().Init();
        TasksPanel.I.GetObject("Task3").GetComponent<CurrTask>().Init();
        TasksPanel.I.InitDaily();
    }

    public void GoToAchievement()
    {
        StartSoundOpenPanel();

        ProgressPanel.I.gameObject.SetActive(true);
        ProgressPanel.I.RefreshOpenAchievementStats();
        ProgressPanel.I.RefreshHiddenAchievementStats();
    }

    public void GoToSettings()
    {
        // для теста
        /*int crystals = 100000;
        if (Helper.GetCrystals() < crystals)
        {   
            Helper.AddCrystals(crystals);
            CrystalText.GetComponent<Text>().text = Helper.GetCrystals().ToString();
        }
        Debug.Log($"<color=red>LEVEL = {Helper.GetLevel()}</color>");*/

        StartSoundOpenPanel();

        SettingsPanel.I.gameObject.SetActive(true);
    }

    public void GoToCollection()
    {
        StartSoundOpenPanel();

        CollectionPanel.I.gameObject.SetActive(true);
        gameObject.SetActive(false);
        CollectionPanel.I.OpenCollection();
    }

    public void GoToPlay(bool sound = true)
    {
        if (sound)
        {
            Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.OpenPlayPanel);
        }

        PlayPanel.I.gameObject.SetActive(true);
        PlayPanel.I.InitBoosters(false);
        PlayButton = objects["PlayImg"];
        objects["Play"].GetComponent<StartGame>().enabled = true;
        Play.onClick.RemoveAllListeners();
        Play.GetComponent<StartGame>().Refresh();
        Play.onClick.AddListener(PlayPanel.I.PlayGame);
        objects["PlayImg"].GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/" + "button_play", 0);
        PlayButton.transform.SetParent(PlayPanel.I.GetObject("PlaceForPlay").transform, false);
    }

    public void GoToCards()
    {
        StartSoundOpenPanel();
        GoToShop(ShopTabs.Cards);
    }

    public void GoToCrystals()
    {
        StartSoundOpenPanel();
        GoToShop(ShopTabs.Crystals);
        ShopPanel.I.SetActiveCrystalsContent(true);
    }

    public void GoToBoost()
    {
        StartSoundOpenPanel();
        GoToShop(ShopTabs.Boosts);
    }

    public void GoToShop(ShopTabs tab)
    {
        gameObject.SetActive(false);
        Messanger<Events>.SendMessage(Events.SwitchShop);
        ShopPanel.I.gameObject.SetActive(true);
        Messanger<ShopTabs, Events>.SendMessage(Events.OpenShopTab, tab);
        if (PlayerPrefs.GetInt("FirstGoToShop") == 0)
        {
            TrainingPanel.I.gameObject.SetActive(true);
            TrainingPanel.I.GetObject("Training").SetActive(true);
            TrainingPanel.I.GetObject("Replica").GetComponent<Text>().text = Helper.GetTrainingStrings().FirstGoToShop[1].replica;
            TrainingPanel.I.GetObject("NumberReplica").GetComponent<Text>().text = "1/2";
            TrainingPanel.I.GetObject("TrainingButton").GetComponent<Button>().onClick.RemoveAllListeners();
            TrainingPanel.I.GetObject("TrainingButton").GetComponent<Button>().onClick.AddListener(() =>
            {
                TrainingPanel.I.GetObject("Replica").GetComponent<Text>().text = Helper.GetTrainingStrings().FirstGoToShop[2].replica;
                TrainingPanel.I.GetObject("NumberReplica").GetComponent<Text>().text = "2/2";
                TrainingPanel.I.GetObject("TrainingButton").GetComponent<Button>().onClick.RemoveAllListeners();
                TrainingPanel.I.GetObject("TrainingButton").GetComponent<Button>().onClick.AddListener(() => TrainingPanel.I.gameObject.SetActive(false));
                PlayerPrefs.SetInt("FirstGoToShop", 1);
            });
        }
    }

    private void Awake()
    {
        I = this;
        Messanger<Events>.Subscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    private void OnDestroy()
    {
        Messanger<Events>.Unsubscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    private void OnUpdateAnchors()
    {
        Play = objects["Play"].GetComponent<Button>();
        objects["PlayImg"].GetComponent<Image>().raycastTarget = false;
        if (Helper.CountCard >= 13 && PlayerPrefs.GetInt("CompleteTraining") != 1) FirstGame.I.OnTraining(false);
        GameObject fullPowerParent = objects["imgFullPower"].transform.parent.gameObject;
        objects["imgFullPower"].transform.SetParent(objects["bgEnergy"].transform);
        if (GetObject("PwrSlider").transform.GetChild(1).GetComponent<Slider>().value < 1) objects["imgFullPower"].GetComponent<Image>().enabled = false;

        SocialManager.I.StartProgressAwaitCoroutine();
        Messanger<Events>.SendMessage(Events.CrystalUpdate);
        Destroy(fullPowerParent);
    }

    public void HideDailyIcon()
    {
        rewardImage.SetActive(false);
    }

    void Start()
    {
        objects["ViewAds"].SetActive(false); // выключаем, пока нет рекламы
        CrystalText = objects["CrystalText2"];
        CrystalText.AddComponent<CrystalsUpdate>();
        CrystalText.AddComponent<Shadow>();
        objects["CrystalButton2"].GetComponent<Button>().onClick.AddListener(GoToCrystals);
        
        objects["PowerButton"].GetComponent<Button>().onClick.AddListener(() =>
        {
            Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.DoorOpening);
            ReplenishPowerPanel.I.gameObject.SetActive(true);
        });

        GameObject pwrSlider = Instantiate(Resources.Load<GameObject>("Prefabs/PowerSlider"), objects["PwrSlider"].transform);

        objects["Achievements"].GetComponent<Button>().onClick.AddListener(GoToAchievement);
        objects["Shop"].GetComponent<Button>().onClick.AddListener(GoToCards);
        objects["Collection"].GetComponent<Button>().onClick.AddListener(GoToCollection);
        if (PlayerPrefs.GetInt("FirstOpenAchievement") == -1) objects["Shop"].GetComponent<Button>().onClick.AddListener(FirstGame.I.OnFirstAchievement);
        objects["Play"].GetComponent<Button>().onClick.AddListener(() => GoToPlay());
        objects["Settings"].GetComponent<Button>().onClick.AddListener(GoToSettings);
        tasksButton = GetObject("Tasks");
        tasksButton.GetComponent<Button>().onClick.AddListener(GoToTasks);

        if (PlayerPrefs.GetInt("FirstGame") != 4)
        {
            objects["Shop"].GetComponent<Button>().interactable = false;
            objects["Achievements"].GetComponent<Button>().interactable = false;
            objects["Collection"].GetComponent<Button>().interactable = false;
            objects["CrystalButton2"].GetComponent<Button>().interactable = false;
        }
        if (PlayerPrefs.GetInt("FirstOpenAchievement") != 1) tasksButton.SetActive(false);

        rewardImage = GetObject("Reward");
        rewardImage.SetActive(false);

        if (!Helper.GetTraining6Passed()) objects["Tasks"].SetActive(false);
        else objects["Tasks"].SetActive(true);

        imgClouds = objects["clouds"].GetComponent<Image>();
        clouds = imgClouds.color;

        Started = true;

        if (Helper.GetTraining6Passed())
            StartCoroutine(StartRequestDailyQuests(ServerConstants.DailyStartSeconds));
    }

    private float speed = 0.12f;
    public void Update()
    {
        if (Helper.GetTraining6Passed() && TasksPanel.I != null && statusDailyRequest != 0)
        {
            if (statusDailyRequest == 200 && dailyNumbers.Length > 2) Helper.SetLastDailyServerNumbers(dailyNumbers);

            if (Application.internetReachability != NetworkReachability.NotReachable && Helper.GetLastDailyServerNumbers() != Helper.GetDailyLocalNumbers())
            {
                TasksPanel.I.GetObject("TakeDaily").SetActive(true);
                rewardImage.SetActive(true);
            }
            else
            {
                TasksPanel.I.GetObject("TakeDaily").SetActive(false);
                Helper.SetStateRewardIcon();
            }

            if (dailyNumbers.Length == 2 && !isDailyReceived)
            {
                StartCoroutine(StartRequestDailyQuests(ServerConstants.DailyTimeoutSeconds));
            }

            statusDailyRequest = 0;
        }

        if (Started)
        {
            if (clouds.a < 200f / 255f) speed = -speed;
            else if (clouds.a >= 1.2f) { speed = -speed; clouds.a = 1f; }
            clouds.a += Time.deltaTime * speed;
            imgClouds.color = clouds;
        }
    }

    private IEnumerator StartRequestDailyQuests(int delayInSeconds = 0)
    {
        yield return new WaitForSeconds(delayInSeconds);
        statusDailyRequest = 0;
        string ticket;
#if UNITY_EDITOR
        ticket = "1160130875fda0812c99c5e3f1a03516471a6370c4f97129b221938eb4763e63";
#else
        ticket = ServerManager.GetTicket();
#endif
        ServerManager.StartGetRequest(ticket, ServerConstants.UriGetDailyQuests, (code, response) =>
        {
            Debug.Log($"MenuPanel.StartRequestDailyQuests: status = {code}, response = {response}");
            if (code == 200)
            {
                var dict = Json.Deserialize(response) as Dictionary<string, object>;
                dailyNumbers = string.Join(";", (List<object>)dict["numbers"]);
                if (dailyNumbers.Length > 2) isDailyReceived = true;
            }
            else dailyNumbers = "-1";
            statusDailyRequest = code;
        });
    }
}
