﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TablePanel : Form {

    public static TablePanel I { get; set; }
    GameObject ScoreBoardObj, CardsOnTable, Deck;
    Text HealthCount, EnergyCount, CrystalCount;

    private void Awake()
    {
        I = this;
        Messanger<Events>.Subscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    private void OnDestroy()
    {
        Messanger<Events>.Unsubscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    public void TrackAchievement(bool win)
    {
        if (Helper.GetTraining5Passed())
        {
            if (win)
            {
                PlayerPrefs.SetInt("winGame", PlayerPrefs.GetInt("winGame", 0) + 1);
                if (PlayerPrefs.GetInt("winGame") == 3)
                {
                    Helper.SetAchievement("win_3_games_in_row", Helper.GetAchievement("win_3_games_in_row") + 1);
                    PlayerPrefs.SetInt("winGame", 0);
                }
            }
            else PlayerPrefs.SetInt("winGame", 0);
        }
    }

    private void Localization()
    {
        string text = "Вы уверены, что хотите завершить партию? Если вы завершите её сейчас, это засчитается за проигрыш.";
        PopUpPanel.I.ShowMessage(text);
    }

    public void BackToMenu()
    {
        Helper.ResetWinSeries();

        Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.YesBackLobby);
        if (Helper.IsMusicActive()) Messanger<string, Events>.SendMessage(Events.PlayMusic, SoundClips.LobbyMusic);

        TrackAchievement(false);
        //PlayPanel.I.ApplyBoosterManager();
        PlayPanel.I.BackToMenu(false);

        gameObject.SetActive(false);
        MenuPanel.I.gameObject.SetActive(true);
        TrainingPanel.I.gameObject.SetActive(false);
        
        PopUpPanel.I.OnClickOk = null;

        Helper.SetStateRewardIcon();
    }

    void Start () {
        objects["Back"].GetComponent<Button>().onClick.AddListener(() => 
        {
            Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.Grunting);
            Localization();
            PopUpPanel.I.gameObject.SetActive(true);
            PopUpPanel.I.OnClickOk = BackToMenu;
            PopUpPanel.I.OnClickCancel = () => Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.PlayGame);
        });
        
        //newobj.name = "InfectCard";
        //TablePanel.I.AddObject(newobj.name, newobj);
        
        ScoreBoardObj = objects["ScoreBoard"];
        ScoreBoard s = this.ScoreBoardObj.AddComponent<ScoreBoard>();
        s.Health = objects["HealthCount"].GetComponent<Text>();
        s.Energy = objects["EnergyCount"].GetComponent<Text>();
        s.Crystal = objects["CrystalCount"].GetComponent<Text>();

        CardsOnTable = objects["CardOnTable"];

        TableController table = CardsOnTable.AddComponent<TableController>();
        
        table.cardOnTable = new CardPosition[8] {objects["CardPosition1"].AddComponent<CardPosition>(),
                                                 objects["CardPosition2"].AddComponent<CardPosition>(),
                                                 objects["CardPosition3"].AddComponent<CardPosition>(),
                                                 objects["CardPosition4"].AddComponent<CardPosition>(),
                                                 objects["CardPosition5"].AddComponent<CardPosition>(),
                                                 objects["CardPosition6"].AddComponent<CardPosition>(),
                                                 objects["CardPosition7"].AddComponent<CardPosition>(),
                                                 objects["CardPosition8"].AddComponent<CardPosition>()
                                                 };
        for (int i = 0; i < 8; i++)
        {
            table.cardOnTable[i].position = i;
            table.cardOnTable[i].Init();
        }

        GameObject infectSprite;

        for (int i = 0; i < 8; i++)
        {
            infectSprite = Instantiate(table.cardOnTable[i].gameObject, table.cardOnTable[i].transform.parent);
            Destroy(infectSprite.GetComponent<Button>());
            Destroy(infectSprite.GetComponent<CardPosition>());
            Destroy(infectSprite.transform.GetChild(2).gameObject);
            Destroy(infectSprite.transform.GetChild(1).gameObject);
            Destroy(infectSprite.transform.GetChild(0).gameObject);
            infectSprite.transform.SetAsLastSibling();
            infectSprite.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/atlas_cards", 4);
            infectSprite.GetComponent<Image>().raycastTarget = false;
            infectSprite.SetActive(false);
        }

        Deck = objects["Deck"];
        Deck deck = Deck.AddComponent<Deck>();
        deck.PriceForDealCardCrystal = objects["CrystalsForDeal"];
        deck.PriceForDealCardHealth = objects["HealthsForDeal"];
        deck.FreePrice = objects["FreePriceForDealCard"];
        deck.DeckCount = objects["DeckCount"].GetComponent<Text>();
        objects["DeckCount"].AddComponent<Shadow>().effectDistance = new Vector2(4, -4);
        objects["CardText"].AddComponent<Shadow>().effectDistance = new Vector2(4, -4);
        objects["MonsterCount"].AddComponent<Shadow>().effectDistance = new Vector2(4, -4);
        objects["MonsterText"].AddComponent<Shadow>().effectDistance = new Vector2(4, -4);
        objects["HealthsForDeal"].AddComponent<Shadow>().effectDistance = new Vector2(4, -4);
        objects["CrystalsForDeal"].AddComponent<Shadow>().effectDistance = new Vector2(4, -4);
        objects["FreePriceForDealCard"].AddComponent<Shadow>().effectDistance = new Vector2(4, -4);

        Started = true;
    }

    private void OnUpdateAnchors()
    {
        objects["FreePriceForDealCard"].SetActive(false);
        objects["Table"].transform.SetAsLastSibling();

        objects["flyingParametr"].SetActive(false);

        gameObject.SetActive(false);
    }
    
}
