﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionPanel : Form
{
    public static CollectionPanel I { get; set; }
    GameObject prefab;
    GameObject CrystalText, scrollMonster, scrollAbility, MonsterButton, AbilityButton;
    Text MonsterText, AbilityText, Weight;
    Dictionary<string, GameObject> abilityList;
    private GameObject card;
    public int weight;

    private void OnUpdateAnchors()
    {
        objects["Button"].transform.parent.SetParent(objects["Background"].transform.parent, true);  // в старте кнопку разворачивает
        gameObject.SetActive(false);
        ResizeGridContent();
    }

    private void LoadLocalization()
    {
        objects["Headline"].GetComponent<Text>().text = "COLLECTION";
        objects["MonsterTab"].GetComponent<Text>().text = "MONSTERS";
        objects["AbilityTab"].GetComponent<Text>().text = "ABILITIES";
    }

    public void BackToMenu()
    {
        Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.DoorClosing);

        objects["Button"].transform.GetChild(0).GetComponent<Text>().text = "Editor activate";
        objects["Button"].GetComponent<Button>().onClick.RemoveAllListeners();
        gameObject.SetActive(false);
        MenuPanel.I.gameObject.SetActive(true);
        DeleteCollection();
        TrainingPanel.I.gameObject.SetActive(false);
        Helper.SetStateRewardIcon();
    }

    public void GoToShop()
    {
        ShopPanel.I.SetActiveCrystalsContent(true);
        Messanger<Events>.SendMessage(Events.SwitchShop);
        ShopPanel.I.gameObject.SetActive(true);
        Messanger<ShopTabs, Events>.SendMessage(Events.OpenShopTab, ShopTabs.Crystals);
    }

    private void Awake()
    {
        I = this;
        Messanger<Events>.Subscribe(Events.UpdateAnchors, OnUpdateAnchors); 
    }

    private void OnDestroy()
    {
        Messanger<Events>.Unsubscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    private void ResizeGridContent()
    {
        GameObject ContentInBG = objects["Background"];
        scrollAbility = Instantiate(Resources.Load<GameObject>("Prefabs/ScrollView"), ContentInBG.transform);
        scrollMonster = Instantiate(Resources.Load<GameObject>("Prefabs/ScrollView"), ContentInBG.transform);
        GameObject monsterContent = scrollMonster.transform.GetChild(0).GetChild(0).gameObject;
        GameObject abilityContent = scrollAbility.transform.GetChild(0).GetChild(0).gameObject;

        GridLayoutGroup gridMonsters = monsterContent.GetComponent<GridLayoutGroup>();
        float width = ContentInBG.GetComponent<RectTransform>().rect.width;
        float height = ContentInBG.GetComponent<RectTransform>().rect.height;

        RectTransform rectPrefab = objects["CardPrefab"].GetComponent<RectTransform>();
        float prefabWidth = rectPrefab.rect.width;
        float prefabHeight = rectPrefab.rect.height;
        float newWidth = 0.26f * width;
        float newHeight = newWidth / prefabWidth * prefabHeight;
        float scroollbarWidth = scrollMonster.transform.GetChild(2).GetComponent<RectTransform>().rect.width;
        float spacingX = (width - newWidth * 3f - scroollbarWidth) / 4f;

        gridMonsters.padding.top = gridMonsters.padding.bottom = (int)(width * 0.04f);
        gridMonsters.padding.left = gridMonsters.padding.right = (int)spacingX;
        gridMonsters.spacing = new Vector2(spacingX, 0.03f * height);
        gridMonsters.cellSize = new Vector2(newWidth, newHeight);

        GridLayoutGroup gridAbilities = abilityContent.GetComponent<GridLayoutGroup>();

        gridAbilities.padding.top = gridAbilities.padding.bottom = (int)(width * 0.04f);
        gridAbilities.padding.left = gridAbilities.padding.right = (int)spacingX;
        gridAbilities.spacing = new Vector2(spacingX, 0.03f * height);
        gridAbilities.cellSize = new Vector2(newWidth, newHeight);
    }

    void Start()
    {
        LoadLocalization();
        CrystalText = objects["CrystalText"];
        CrystalText.AddComponent<CrystalsUpdate>();
        CrystalText.AddComponent<Shadow>();

        objects["CrystalButton"].GetComponent<Button>().onClick.AddListener(GoToShop);

        objects["Back"].GetComponent<Button>().onClick.AddListener(BackToMenu);

        objects["Background"].transform.SetParent(objects["frame"].transform.parent);
        objects["frame"].transform.SetAsLastSibling();
        objects["frame"].GetComponent<Image>().raycastTarget = false;

        

        MonsterButton = objects["Monster"];
        MonsterButton.GetComponent<Button>().onClick.AddListener(() => OpenTab(true, true));
        AbilityButton = objects["Ability"];
        AbilityButton.GetComponent<Button>().onClick.AddListener(() => OpenTab(false, true));
        MonsterText = objects["MonsterTab"].GetComponent<Text>();
        AbilityText = objects["AbilityTab"].GetComponent<Text>();
        Weight = objects["WeightSet"].GetComponent<Text>();
                                  
        prefab = objects["CardPrefab"];

        Started = true;
    }


    // переключение цвета вкладок коллекций
    public void OpenTab(bool monster, bool clicked)
    {
        if (clicked)
            Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.Selecting);

        if (monster)
        {
            scrollMonster.SetActive(true);
            scrollAbility.SetActive(false);
            objects["Button"].SetActive(false);
            MonsterButton.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/" + "settings_atlas", 20);
            AbilityButton.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/" + "settings_atlas", 19);
            MonsterText.color = new Color(106f / 255f, 65f / 255f, 23f / 255f);
            AbilityText.color = new Color(216f / 255f, 169f / 255f, 77f / 255f);
        }
        else
        {
            scrollMonster.SetActive(false);
            scrollAbility.SetActive(true);
            if (abilityList.Count > 12) objects["Button"].SetActive(true);
            else objects["Button"].SetActive(false);
            MonsterButton.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/" + "settings_atlas", 19);
            AbilityButton.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/" + "settings_atlas", 20);

            MonsterText.color = new Color(216f / 255f, 169f / 255f, 77f / 255f);
            AbilityText.color = new Color(106f / 255f, 65f / 255f, 23f / 255f);
        }
    }

    public void OpenCollection()
    {
        Draw();
        prefab.SetActive(false);
        OpenTab(true, false);
    }
    
    public void refresh()
    {
        DeleteCollection();
        Draw();
    }

    private void Draw()
    {
        abilityList = new Dictionary<string, GameObject>();
        GameObject newObj;
        GameObject image;

        foreach (string monster in Enum.GetNames(typeof(Monsters)))
        {
            newObj = Instantiate(prefab, scrollMonster.transform.GetChild(0).GetChild(0));
            newObj.SetActive(true);
            image = newObj.transform.GetChild(0).gameObject;
            if (PlayerPrefs.GetInt(monster) == 1) image.AddComponent<ClickHandler>().LongPress = () =>
            {
                Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.Growling);
                CollectionCard.I.OnDescriptionCardActivate(monster);
            };
            if (PlayerPrefs.GetInt(monster) == 0) image.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/" + "atlas_cards", 0);
            else image.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/" + GameConstants.PathToSprites[CardType.Monster], Helper.GetMonstersStrings(monster).numberSprite);

            if (PlayerPrefs.GetInt(monster) == 0) image.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = "???";
            else image.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = Helper.GetMonstersStrings(monster).name;
        }

        newObj = Instantiate(prefab, scrollAbility.transform.GetChild(0).GetChild(0));
        image = newObj.transform.GetChild(0).gameObject;
        image.AddComponent<ClickHandler>().LongPress = () =>
        {
            Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.PressAbility);
            CollectionCard.I.OnDescriptionCardActivate("Madness");
        };
        image.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/" +GameConstants.PathToSprites[CardType.Madness], Helper.GetMadnessDescription().sprite);
        image.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = Helper.GetMadnessDescription().name;
        newObj.SetActive(true);
        abilityList.Add("Madness", newObj);
        //image.GetComponent<Image>().color = new Color(1f, 1f, 1f, 159f / 255f);

        foreach (string ability in Enum.GetNames(typeof(Ability)))
        {
            if (PlayerPrefs.GetInt(ability) == 0) continue;
            newObj = Instantiate(prefab, scrollAbility.transform.GetChild(0).GetChild(0));
            newObj.SetActive(true);
            image = newObj.transform.GetChild(0).gameObject;
            image.AddComponent<ClickHandler>().LongPress = () =>
            {
                Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.PressAbility);
                CollectionCard.I.OnDescriptionCardActivate(ability);
            };
            image.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/" + GameConstants.PathToSprites[CardType.Ability], Helper.GetAbilityDescription(ability).sprite);
 
            if (PlayerPrefs.GetInt(ability) == 1)
            {
                SetCardActive(newObj/*is parent from image*/, false); //image.GetComponent<Image>().color = new Color(100f / 255f, 100f / 255f, 100f / 255f);
            }
            abilityList.Add(ability, newObj);
            image.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = Helper.GetAbilityDescription(ability).name;
        }

        weight = 0;
        for(int i = (12 > abilityList.Count ? abilityList.Count : 12); i >= 1; i--)
        {
            abilityList[PlayerPrefs.GetString("ability" + i.ToString())].transform.SetAsFirstSibling();
            if (PlayerPrefs.GetString("ability" + i.ToString()) != "Madness")
                weight += Helper.GetAbilityDescription(PlayerPrefs.GetString("ability" + i.ToString())).significance;
            else weight++;
        }

        if (abilityList.Count > 12)
        {
            objects["Button"].GetComponent<Button>().onClick.RemoveAllListeners();
            objects["Button"].GetComponent<Button>().onClick.AddListener(EditorActivity);// можно купить пересортировку карт
            if (Helper.GetBooster("ReAbilities") == 0)
            {
                objects["Button"].transform.GetChild(0).GetComponent<Text>().text = Helper.GetLocalString("EditorButtonNotFree");//"Редактор (-500 кристаллов)";  // x
            }
            else
            {
                objects["Button"].transform.GetChild(0).GetComponent<Text>().text = Helper.GetLocalString("EditorButtonFree");//"Редактор (бесплатно)";        // x
            }
        }
        else objects["Button"].SetActive(false);
    }

    public void DeleteCollection()
    {
        GameObject ability = scrollAbility.transform.GetChild(0).GetChild(0).gameObject;
        for (int i = 0; i < ability.transform.childCount; i++) Destroy(ability.transform.GetChild(i).gameObject);
        GameObject monster = scrollMonster.transform.GetChild(0).GetChild(0).gameObject;
        for (int i = 0; i < monster.transform.childCount; i++) Destroy(monster.transform.GetChild(i).gameObject);
    }

    public void EditorActivity()
    {
        Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.EditCards);
        bool editorActive = true;
        if (Helper.GetBooster("ReAbilities") == 0)
        {
            int crystals = Helper.GetCrystals();
            if (crystals >= 500)
            {
                Helper.AddCrystals(-500);
                Messanger<Events>.SendMessage(Events.CrystalUpdate);
            }
            else
            {
                editorActive = false;
                PopUpPanel.I.ShowMessage("Недостаточно кристаллов");
            }
        }
        else
        {
            Helper.SetBooster("ReAbilities", Helper.GetBooster("ReAbilities") - 1);
        }
        if (editorActive)
        {
            weight = 1;
            for (int i = (12 > abilityList.Count ? abilityList.Count : 12); i >= 1; i--)//активация первых 12 карт
            {
                string ability = PlayerPrefs.GetString("ability" + i.ToString());
                if (ability == "Madness") abilityList[ability].transform.GetChild(0).GetComponent<Image>().color = new Color(1f, 1f, 1f, 159f / 255f);
                abilityList[ability].transform.GetChild(0).gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
                if (ability != "Madness")
                {
                    weight += Helper.GetAbilityDescription(ability).significance;
                    abilityList[ability].transform.GetChild(0).gameObject.GetComponent<Button>().onClick.AddListener(() => ExchangeIt(ability));
                    // Debug.Log("Onclick on card: " + ability);
                }
            }
            foreach (KeyValuePair<string, GameObject> a in abilityList)//показать значимость
            {
                if (a.Key == "Madness") a.Value.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().text = Helper.GetMadnessDescription().significance.ToString();
                else a.Value.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().text = Helper.GetAbilityDescription(a.Key).significance.ToString();
            }
            Weight.text = "ВЕС: " + weight + "/75";
            objects["Button"].GetComponent<Button>().onClick.RemoveListener(EditorActivity);
            objects["Button"].GetComponent<Button>().onClick.AddListener(SaveEditing);
            objects["Button"].transform.GetChild(0).GetComponent<Text>().text = Helper.GetLocalString("Save");
        }
    }

    public void ExchangeIt(string cardName)
    {
        abilityList[cardName].transform.GetChild(0).gameObject.GetComponent<Image>().color = Color.green;
        card = abilityList[cardName];
        bool yes = false;
        foreach (KeyValuePair<string, GameObject> ability in abilityList)
        {
            if (ability.Value.transform.GetSiblingIndex() >= 12)
            {
                if (Helper.GetAbilityDescription(ability.Key).significance + (weight - Helper.GetAbilityDescription(cardName).significance) <= 75)
                {
                    string key = ability.Key;
                    ability.Value.transform.GetChild(0).gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
                    ability.Value.transform.GetChild(0).gameObject.GetComponent<Button>().onClick.AddListener(() => ExchangeForIt(key));
                    SetCardActive(ability.Value, true);
                    //ability.Value.transform.GetChild(0).gameObject.GetComponent<Image>().color = Color.white;            
                    yes = true;
                }
            }
            else if (ability.Value == card)
            {
                string key = ability.Key;
                ability.Value.transform.GetChild(0).gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
                ability.Value.transform.GetChild(0).gameObject.GetComponent<Button>().onClick.AddListener(() => ExchangeForIt(key));
            }
            else
            {
                ability.Value.transform.GetChild(0).gameObject.GetComponent<Image>().color = new Color(1f, 1f, 1f, 159f / 255f);
            }
        }
        if (!yes)
        {
            ExchangeForIt(cardName);
        }
    }

    public void ExchangeForIt(string cardName)
    {
        Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.AddCard);

        int count = abilityList[cardName].transform.GetSiblingIndex();
        abilityList[cardName].transform.SetSiblingIndex(card.transform.GetSiblingIndex());
        card.transform.SetSiblingIndex(count);
        SetCardActive(card, true);
        //card.transform.GetChild(0).gameObject.GetComponent<Image>().color = Color.white;
        card = null;
        weight = 1;
        foreach (KeyValuePair<string, GameObject> ability in abilityList)
        {
            if (ability.Value.transform.GetSiblingIndex() >= 12)
            {
                SetCardActive(ability.Value, false);
                //ability.Value.transform.GetChild(0).gameObject.GetComponent<Image>().color = new Color(100f / 255f, 100f / 255f, 100f / 255f);
                ability.Value.transform.GetChild(0).gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
            }
            else
            {
                if (ability.Key != "Madness")
                {
                    weight += Helper.GetAbilityDescription(ability.Key).significance;
                    SetCardActive(ability.Value, true);
                    //ability.Value.transform.GetChild(0).gameObject.GetComponent<Image>().color = Color.white;
                    ability.Value.transform.GetChild(0).gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
                    ability.Value.transform.GetChild(0).gameObject.GetComponent<Button>().onClick.AddListener(() => ExchangeIt(ability.Key));
                }
                else ability.Value.transform.GetChild(0).gameObject.GetComponent<Image>().color = new Color(1f, 1f, 1f, 159f / 255f);
                
            }
        }
        Weight.text = "ВЕС: " + weight + "/75";
    }

    public void SaveEditing()
    {
        Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.CardsShuffling);

        weight = 1;
        foreach(KeyValuePair<string, GameObject> a in abilityList)
        {
            a.Value.transform.GetChild(0).GetComponent<Button>().onClick.RemoveAllListeners();
            a.Value.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().text = "";
            if (a.Value.transform.GetSiblingIndex() < 12)
            {
                PlayerPrefs.SetInt(a.Key, 2);
                PlayerPrefs.SetString("ability" + (a.Value.transform.GetSiblingIndex() + 1), a.Key);
                if (a.Key != "Madness") weight += Helper.GetAbilityDescription(a.Key).significance;
            }
            else
            {
                PlayerPrefs.SetInt(a.Key, 1);
                if (a.Key != "Madness") SetCardActive(a.Value, true); //a.Value.transform.GetChild(0).gameObject.GetComponent<Image>().color = Color.white;
                else a.Value.transform.GetChild(0).gameObject.GetComponent<Image>().color = new Color(1f, 1f, 1f, 159f / 255f);
            }

        }
        objects["Button"].transform.GetChild(0).GetComponent<Text>().text = Helper.GetLocalString("EditorButtonNotFree"); //"Редактор (-500 крист)";
        DeleteCollection();
        Draw();
        Weight.text = "";
    }

    /// <summary>
    /// card is cloned GameObject
    /// </summary>
    /// <param name="card"></param>
    /// <param name="active"></param>
    public void SetCardActive(GameObject card, bool active)
    {
        Image img = card.transform.GetChild(0).GetComponent<Image>();
        Text significance = img.gameObject.transform.GetChild(0).GetChild(0).GetComponent<Text>();
        Text locName = img.gameObject.transform.GetChild(1).GetChild(0).GetComponent<Text>();
        if (active)
        {
            img.color = Color.white;
            significance.color = Color.white;
            locName.color = Color.white;
        }
        else
        {
            img.color = new Color(100f / 255f, 100f / 255f, 100f / 255f);
            significance.color = new Color(100f / 255f, 100f / 255f, 100f / 255f);
            locName.color = new Color(100f / 255f, 100f / 255f, 100f / 255f);
        }
    }
}

