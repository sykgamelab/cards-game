﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_IOS
using UnityEngine.iOS;
#endif

public class SettingsPanel : Form {
    public static SettingsPanel I { get; set; }
    private Sprite stateOff, stateOn;
    private Image toggleSoundImg, toggleMusicImg;

    private string notReachable;

    private void Awake()
    {
        I = this;
        Messanger<Events>.Subscribe(Events.UpdateAnchors, OnUpdateAnchors); 
    }

    private void OnDestroy()
    {
        Messanger<Events>.Unsubscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    private void LoadLocalization()
    {
        objects["Headline"].GetComponent<Text>().text = "SETTINGS";
        objects["SoundsText"].GetComponent<Text>().text = "SOUNDS";
        objects["MusicText"].GetComponent<Text>().text = "MUSIC";
        objects["LanguageText"].GetComponent<Text>().text = "LANGUAGE";  
        objects["MoreGamesText"].GetComponent<Text>().text = "More games";        
        objects["RateUsText"].GetComponent<Text>().text = "Rate us";
        notReachable = "Нет соединения с интернетом";
    }

    private void OnMusicClick()
    {
        Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.ToggleSettings);

        Debug.Log($"OnMusicClick: MusicActive = {Helper.IsMusicActive()}");
        bool active = Helper.IsMusicActive();
        active = !active;

        if (active) toggleMusicImg.sprite = stateOn;
        else toggleMusicImg.sprite = stateOff;

        Messanger<bool, Events>.SendMessage(Events.SetMusicActive, active);
        Helper.SetMusicActive(active);
        Messanger<string, Events>.SendMessage(Events.PlayMusic, SoundClips.LobbyMusic);
    }

    private void OnSoundClick()
    {
        Debug.Log($"OnSoundClick: SoundActive = {Helper.IsSoundActive()}");
        bool active = Helper.IsSoundActive();
        active = !active;

        if (active) toggleSoundImg.sprite = stateOn;
        else toggleSoundImg.sprite = stateOff;

        Messanger<bool, Events>.SendMessage(Events.SetSoundActive, active);
        Helper.SetSoundActive(active);
        Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.ToggleSettings);
    }

    void Start () {
        //LoadLocalization();
        objects["Back"].GetComponent<Button>().onClick.AddListener(BackToMenu);
        objects["BackBlur"].GetComponent<Button>().onClick.AddListener(BackToMenu);
        objects["RateUs"].GetComponent<Button>().onClick.AddListener(OnClickRateUs);
        objects["MoreGames"].GetComponent<Button>().onClick.AddListener(OnMoreGamesClick);

        objects["Background"].transform.SetParent(objects["frame"].transform.parent);
        objects["frame"].transform.SetAsLastSibling();
        objects["frame"].GetComponent<Image>().raycastTarget = false;

        objects["LanguageButton"].GetComponent<Button>().onClick.AddListener(() =>
        {
            Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.DoorOpening);
            LanguagePanel.I.gameObject.SetActive(true);
        });

        GetObject("SoundsButton").GetComponent<Button>().onClick.AddListener(OnSoundClick);
        GetObject("MusicButton").GetComponent<Button>().onClick.AddListener(OnMusicClick);
        toggleSoundImg = GetObject("SoundsButton").GetComponent<Image>();
        toggleMusicImg = GetObject("MusicButton").GetComponent<Image>();

        stateOn = URPNGLoader.LoadURPNG("3/settings_atlas", 6);
        stateOff = URPNGLoader.LoadURPNG("3/settings_atlas", 7);

        if (Helper.IsSoundActive()) toggleSoundImg.sprite = stateOn;
        else toggleSoundImg.sprite = stateOff;

        if (Helper.IsMusicActive()) toggleMusicImg.sprite = stateOn;
        else toggleMusicImg.sprite = stateOff;

        Image frame = objects["frame"].GetComponent<Image>();

        Started = true;
    }

    private void OnUpdateAnchors()
    {
        LoadLocalization();
        gameObject.SetActive(false);
    }

    public void BackToMenu()
    {
        Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.DoorClosing);
        gameObject.SetActive(false);
    }

    private IEnumerator WaitSoundCoroutine(Action action)
    {
        yield return new WaitForSeconds(Effects.ClipLength);
        action.Invoke();
    }

    private void OnMoreGamesClick()
    {
        Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.OtherGames);

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            PopUpPanel.I.ShowMessage(notReachable);
        }
        else
        {
            Action action = () =>
            {
#if UNITY_IOS
                Application.OpenURL("https://itunes.apple.com/developer/alexandr-podosinnikov/id1241783955");
#elif UNITY_ANDROID
                Application.OpenURL("https://play.google.com/store/apps/dev?id=5337549402106903769");
#endif
            };
            StartCoroutine(WaitSoundCoroutine(action));
        }
    }

    public void OnClickRateUs()
    {
        Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.OtherGames);

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            PopUpPanel.I.ShowMessage(notReachable);
        }
        else
        {
            Action action = () =>
            {
#if UNITY_IOS
            if (!Device.RequestStoreReview())
            {
                Application.OpenURL("itms-apps://itunes.apple.com/app/1440479503");
            }
#elif UNITY_ANDROID
                Application.OpenURL($"http://play.google.com/store/apps/details/?id=urmobi.games.infectedkingdom");
#elif UNITY_EDITOR
                Application.OpenURL($"http://liveangarsk.ru/files/images/7f28b0022c6e0543e59aa2e325948047-208095711.gif");
#endif
            };

            StartCoroutine(WaitSoundCoroutine(action));
        }
    }
}
