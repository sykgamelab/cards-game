﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LanguagePanel : Form {

    public static LanguagePanel I { get; set; }
    private string[] languages = {"de", "en", "pt", "ru", "es", "fr" };

    //public Dictionary<string, Tuple<Button, Text, GameObject>> LanguageButtons = new Dictionary<string, Tuple<Button, Text, GameObject>>();
    private void Awake()
    {
        I = this;
        Messanger<Events>.Subscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    private void OnDestroy()
    {
        Messanger<Events>.Unsubscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    void Start()
    {
        SetLanguage(Helper.GetLangCode());
        objects["Frame"].GetComponent<Image>().raycastTarget = false;
        objects["btnBlur"].GetComponent<Button>().onClick.AddListener(() => gameObject.SetActive(false));
        objects["Back"].GetComponent<Button>().onClick.AddListener(() => 
        {
            Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.DoorClosing);
            gameObject.SetActive(false);
        });
        foreach (string lang in languages) AddListenerChangeLanguage(lang);
        Started = true;
    }
    
    private void AddListenerChangeLanguage(string lang)
    {
        objects[$"{lang}Button"].GetComponent<Button>().onClick.AddListener(() =>
        {
            if (objects[$"{lang}ImgArrow"].transform.parent.name != $"{lang}BigArrow")
            {
                foreach (string language in languages)
                {
                    objects[$"{language}ImgArrow"].transform.SetParent(objects[$"{language}MiniArrow"].transform, false);
                    objects[$"{language}Button"].GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/" + "settings_atlas", 16);
                    objects[$"{language}Text"].GetComponent<Text>().color = new Color(93f / 255f, 56f / 255f, 21f / 255f);
                }
                SetLanguage(lang);
                gameObject.SetActive(false);
            }
        });
    }

    private void SetLanguage(string lang)
    {
        objects[$"{lang}ImgArrow"].transform.SetParent(objects[$"{lang}BigArrow"].transform, false);
        objects[$"{lang}Button"].GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/" + "settings_atlas", 14);
        objects[$"{lang}Text"].GetComponent<Text>().color = new Color(237f / 255f, 202f / 255f, 140f / 255f);
        SettingsPanel.I.GetObject("SetLanguageText").GetComponent<Text>().text = lang.ToUpper();
        if (Started) Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.Accepting);
    }

    private void OnUpdateAnchors()
    {
        gameObject.SetActive(false);
    }
}
