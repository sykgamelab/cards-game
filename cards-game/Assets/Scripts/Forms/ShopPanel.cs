﻿using UnityEngine;
using UnityEngine.UI;

public class ShopPanel : Form {
    public static ShopPanel I { get; set; }
    
    GameObject CrystalText, ContentInBG;
    GameObject BoostTab, CardsTab, CrystalTab;
    Button button1000, button2700, button6400, button16500, button46000, buttonBack;
    Text textButton1000, textButton2700, textButton6400, textButton16500, textButton46000;

    private void Awake()
    {
        I = this;
        Messanger<Events>.Subscribe(Events.UpdateAnchors, OnUpdateAnchors);
        Messanger<Events>.Subscribe(Events.UnlockShop, OnUnlock);
    }

    private void OnDestroy()
    {
        Messanger<Events>.Unsubscribe(Events.UpdateAnchors, OnUpdateAnchors);
        Messanger<Events>.Unsubscribe(Events.UnlockShop, OnUnlock);
    }

    private void LoadLocalization()
    {
        objects["Headline"].GetComponent<Text>().text = "SHOP";
        objects["BoostTabName"].GetComponent<Text>().text = "BOOSTERS";
        objects["CardsTabName"].GetComponent<Text>().text = "CARDS";
        objects["CrystalsTabName"].GetComponent<Text>().text = "CRYSTALS";
    }

    private void FixButtons()
    {
        Helper.FixButton(button1000);
        Helper.FixButton(button2700);
        Helper.FixButton(button6400);
        Helper.FixButton(button16500);
        Helper.FixButton(button46000);
        Helper.FixButton(buttonBack);
        Helper.FixButton(BoostTab.GetComponent<Button>());
        Helper.FixButton(CardsTab.GetComponent<Button>());
        ChangeTextAlphaChanel(false);
    }

    private void UnfixButtons()
    {
        Helper.UnfixButton(button1000);
        Helper.UnfixButton(button2700);
        Helper.UnfixButton(button6400);
        Helper.UnfixButton(button16500);
        Helper.UnfixButton(button46000);
        Helper.UnfixButton(buttonBack);
        Helper.UnfixButton(BoostTab.GetComponent<Button>());
        Helper.UnfixButton(CardsTab.GetComponent<Button>());
        ChangeTextAlphaChanel(true);
    }

    private void ChangeTextAlphaChanel(bool unfix)
    {
        float value = unfix ? 1f : 128f / 255f;
        textButton1000.color = new Color(1f, 1f, 1f, value);
        textButton2700.color = new Color(1f, 1f, 1f, value);
        textButton6400.color = new Color(1f, 1f, 1f, value);
        textButton16500.color = new Color(1f, 1f, 1f, value);
        textButton46000.color = new Color(1f, 1f, 1f, value);
    }

    private void OnUnlock() => UnfixButtons();

    private void OnUpdateAnchors()
    {  
        gameObject.SetActive(false);
        ResizeGridContent();
    }

    public void BackToMenu()
    {
        Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.DoorClosing);

        SetActiveCrystalsContent(false);
        gameObject.SetActive(false);
        //PlayPanel.I.BackToMenu();
        if (!FirstGame.intraining) TrainingPanel.I.gameObject.SetActive(false);
        Messanger<Events>.SendMessage(Events.CloseShop);

        if (Helper.GetTraining6Passed()) TasksPanel.I.InitDaily();

        Helper.SetStateRewardIcon();
        MenuPanel.I.gameObject.SetActive(true);
    }

    private void ResizeGridContent()
    {
        ContentInBG = objects["Background"]; // ShopContent вообще не ровно скомпонован, карты наезжают на пол рамки и только после скрываются за маской, с BG всё ОК
        GameObject cardsContent, boostContent;
        GameObject scrollView = Instantiate(Resources.Load<GameObject>("Prefabs/ScrollView"), ContentInBG.transform); //Resources.Load<GameObject>("Prefabs/Scroll View");
        GameObject scrollViewBoosters = Instantiate(Resources.Load<GameObject>("Prefabs/ScrollView"), ContentInBG.transform);

        scrollView.GetComponent<RectTransform>().offsetMax -= new Vector2(9f, 9f);
        scrollView.GetComponent<RectTransform>().offsetMin += new Vector2(9f, 9f);

        scrollViewBoosters.GetComponent<RectTransform>().offsetMax -= new Vector2(9f, 9f);
        scrollViewBoosters.GetComponent<RectTransform>().offsetMin += new Vector2(9f, 9f);

        cardsContent = scrollView.transform.GetChild(0).GetChild(0).gameObject;
        boostContent = scrollViewBoosters.transform.GetChild(0).GetChild(0).gameObject;

        GridLayoutGroup grid = cardsContent.GetComponent<GridLayoutGroup>();
        float width = ContentInBG.transform.parent.GetComponent<RectTransform>().rect.width;
        float height = ContentInBG.transform.parent.GetComponent<RectTransform>().rect.height;

        RectTransform rectPrefab = objects["CardPrefab"].GetComponent<RectTransform>();
        float prefabWidth = rectPrefab.rect.width;
        float prefabHeight = rectPrefab.rect.height;
        float newWidth = 0.26f * width;
        float newHeight = newWidth / prefabWidth * prefabHeight;
        float scroollbarWidth = scrollView.transform.GetChild(2).GetComponent<RectTransform>().rect.width;
        float spacingX = 0.036f * width; //(width - newWidth * 3 - scroollbarWidth - 2) / 4;  // scrollbarWidth = 20

        grid.padding.top = grid.padding.bottom = (int)(width * 0.04f);            // 4% отступ снизу и сверху
        grid.padding.left = (int)((width - 2 * spacingX - 3 * newWidth - scroollbarWidth) / 2) - 9;
        grid.padding.right = (int)(width - 2 * spacingX - 3 * newWidth - scroollbarWidth - grid.padding.left);
        grid.spacing = new Vector2(spacingX, 0.03f * height);    // 3,4% промежуток между картами по Y 
        grid.cellSize = new Vector2(newWidth, newHeight);

        spacingX = 14f;
        GridLayoutGroup gridBoosters = boostContent.GetComponent<GridLayoutGroup>();
        gridBoosters.padding.top = gridBoosters.padding.bottom = (int)(width * 0.04f);
        newWidth += 10;
        gridBoosters.padding.left = (int)((width - newWidth * 3f - scroollbarWidth - 2 * spacingX - 2f) / 2f) + 8;
        gridBoosters.padding.right = (int)((width - newWidth * 3f - scroollbarWidth - 2 * spacingX - gridBoosters.padding.left - 2f));
        gridBoosters.spacing = new Vector2(spacingX, 0.03f * height);
        gridBoosters.cellSize = new Vector2(newWidth, newWidth / 293f * 407f);  // ширина и высота исходного префаба 293x407 

        (cardsContent.AddComponent<CardsContent>()).Prefab = objects["CardPrefab"];
        (boostContent.AddComponent<BoostsContent>()).Prefab = Resources.Load<GameObject>("Prefabs/BoostPrefab");

        ThreeShopTabs a = BoostTab.AddComponent<ThreeShopTabs>();
        a.GameObjectTab = scrollViewBoosters;
        a.text = objects["BoostTabName"].GetComponent<Text>();

        ThreeShopTabs b = CardsTab.AddComponent<ThreeShopTabs>();
        b.GameObjectTab = scrollView;
        b.text = objects["CardsTabName"].GetComponent<Text>();

        ThreeShopTabs c = CrystalTab.AddComponent<ThreeShopTabs>();
        c.GameObjectTab = objects["CrystalsContent"];
        c.text = objects["CrystalsTabName"].GetComponent<Text>();
    }

    void Start() {
        LoadLocalization();
        objects["Background"].transform.SetParent(objects["frame"].transform.parent);
        //objects["frame"].transform.SetParent(gameObject.transform, false);
        objects["frame"].transform.SetAsLastSibling();
        objects["frame"].GetComponent<Image>().raycastTarget = false;
        objects["Content"].transform.SetAsLastSibling();

        buttonBack = GetObject("BackFromShop").GetComponent<Button>();
        buttonBack.onClick.AddListener(BackToMenu);
        objects["LocalName"].AddComponent<Shadow>().effectDistance = new Vector2(2, -2); 
        objects["PriceForCard"].AddComponent<Shadow>().effectDistance = new Vector2(2, -2); 

        CrystalText = objects["CrystalText1"];
        CrystalText.AddComponent<CrystalsUpdate>();
        CrystalText.AddComponent<Shadow>();

        gameObject.AddComponent<StoreListener>();
                                              
        objects["CardPrefab"].SetActive(false);

        button1000 = GetObject("Button1000").GetComponent<Button>();
        button2700 = GetObject("Button2700").GetComponent<Button>();
        button6400 = GetObject("Button6400").GetComponent<Button>();
        button16500 = GetObject("Button16500").GetComponent<Button>();
        button46000 = GetObject("Button46000").GetComponent<Button>();

        textButton1000 = GetObject("TextButton1000").GetComponent<Text>();
        textButton2700 = GetObject("TextButton2700").GetComponent<Text>();
        textButton6400 = GetObject("TextButton6400").GetComponent<Text>();
        textButton16500 = GetObject("TextButton16500").GetComponent<Text>();
        textButton46000 = GetObject("TextButton46000").GetComponent<Text>();

        button1000.onClick.AddListener(() =>
        {
            FixButtons();
            PurcaseCrystal("1000crystals");
        });
        button2700.onClick.AddListener(() =>
        {
            FixButtons();
            PurcaseCrystal("2700crystals");
        });
        button6400.onClick.AddListener(() =>
        {
            FixButtons();
            PurcaseCrystal("6400crystals");
        });
        button16500.onClick.AddListener(() =>
        {
            FixButtons();
            PurcaseCrystal("16500crystals");
        });
        button46000.onClick.AddListener(() =>
        {
            FixButtons();
            PurcaseCrystal("46000crystals");
        });

        SetActiveCrystalsContent(false);

        /* objects["Sale1000"].gameObject.SetActive(Helper.GetCrystalsDescription ("1000crystal").sale);
         objects["Sale2700"].gameObject.SetActive(Helper.GetCrystalsDescription ("2700crystal").sale);
         objects["Sale6400"].gameObject.SetActive(Helper.GetCrystalsDescription ("6400crystal").sale);
         objects["Sale16500"].gameObject.SetActive(Helper.GetCrystalsDescription("16500crystal").sale);
         objects["Sale46000"].gameObject.SetActive(Helper.GetCrystalsDescription("46000crystal").sale);  */

        CrystalTab = objects["Crystals"];
        CrystalTab.GetComponent<Button>().onClick.AddListener(() => {
            Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.Selecting);
            SetActiveCrystalsContent(true);
        });

        BoostTab = objects["Boosts"];
        BoostTab.GetComponent<Button>().onClick.AddListener(() => {
            Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.Selecting);
            SetActiveCrystalsContent(false);
        });

        CardsTab = objects["Cards"];
        CardsTab.GetComponent<Button>().onClick.AddListener(() => {
            Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.Selecting);
            SetActiveCrystalsContent(false);
        });

        Started = true;
    }

    public void SetActiveCrystalsContent(bool active)
    {
        objects["CrsContent_1"].SetActive(active);
        objects["CrsContent_2"].SetActive(active);
    }

    public void PurcaseCrystal(string crystal)
    {
        Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.BuyCrystals);
        Messanger<string, Events>.SendMessage(Events.BuyProduct, crystal);
        Messanger<Events>.SendMessage(Events.CrystalUpdate);

        SocialManager.I.SetProgressToReport();
    }

    private void OnEnable()
    {
        if (!Started) return;
    }

    public void UpdatePrices()
    {
        if (StoreListener.Prices != null)
        {
            string[] prices = new string[Helper.KeyCrystal.Count];
            string price, productId;

            for (int i = 0; i < prices.Length; i++)
            {
                productId = Helper.KeyCrystal[i];
                price = StoreListener.Prices[productId];
                if (price != null && price != "") prices[i] = price;
                else prices[i] = $"{Helper.GetCrystalsDescription(productId).money}$";
            }

            textButton1000.text = prices[0];
            textButton2700.text = prices[1];
            textButton6400.text = prices[2];
            textButton16500.text = prices[3];
            textButton46000.text = prices[4];
        }
    }
}
