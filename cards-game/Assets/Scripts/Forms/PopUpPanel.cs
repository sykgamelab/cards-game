﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpPanel : Form {
    public static PopUpPanel I { get; set; }
    public Action OnClickOk, OnClickCancel;

    private void Awake()
    {
        I = this;
        Messanger<Events>.Subscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    private void OnDestroy()
    {
        Messanger<Events>.Unsubscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    void Start () {
        LoadLocalization();
        objects["bgTexture"].transform.SetParent(objects["Frame"].transform.parent);
        objects["Frame"].transform.SetAsLastSibling();
        objects["SendOk"].GetComponent<Button>().onClick.AddListener(    () => { gameObject.SetActive(false); OnClickOk?.Invoke(); });
        objects["SendCancel"].GetComponent<Button>().onClick.AddListener(() => { gameObject.SetActive(false); OnClickCancel?.Invoke(); });
        Started = true;
	}

    private void LoadLocalization()
    {
        objects["PopUpName"].GetComponent<Text>().text = "ARE YOU SURE?";
        objects["MessageText"].GetComponent<Text>().text = "Вы уверены, что хотите завершить партию? Если вы завершите ее сейчас, это засчитается за проигрыш.";
        objects["TextOk"].GetComponent<Text>().text = "OK";
        objects["TextCancel"].GetComponent<Text>().text = "CANCEL";
    }

    private void OnUpdateAnchors()
    {
        gameObject.SetActive(false);
    }

    public void ShowMessage(string message)
    {
        GetObject("MessageText").GetComponent<Text>().text = message;
        gameObject.SetActive(true);
    }
}
