﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class CollectionCard : Form
{
    public string Key { get; set; }

    public static CollectionCard I { get; set; }

    GameObject CardImage;
    Button buttonBack;
    Text CardName;
    GameObject CardDescription;
    Sprite spriteFront, spriteBack;
    GameObject MonsterDescription;
    Text MonsterText;
    CardRotation cardRotation;

    public void SetUnActive()
    {
        Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.CloseCard);

        CardDescription.SetActive(false);
        gameObject.SetActive(false);
        MonsterDescription.SetActive(false);
    }

    private void Awake()
    {
        I = this;
        Messanger<Events>.Subscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    private void OnDestroy()
    {
        Messanger<Events>.Unsubscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    void Start()
    {
        buttonBack = GetObject("Back").GetComponent<Button>();
        buttonBack.onClick.AddListener(SetUnActive);

        CardImage = objects["CardImage"];
        CardImage.GetComponent<Button>().targetGraphic = null;
        CardImage.AddComponent<Swipe>().action = OnSwipe;

        cardRotation = CardImage.AddComponent<CardRotation>();
        spriteBack = URPNGLoader.LoadURPNG("3/atlas_cards", 0);

        objects["Background"].transform.SetParent(objects["frame"].transform.parent);
        objects["frame"].transform.SetAsLastSibling();
        objects["frame"].GetComponent<Image>().raycastTarget = false;

        CardName = objects["CardName"].GetComponent<Text>();
        CardName.gameObject.AddComponent<Shadow>().effectDistance = new Vector2(5, -5);
        CardDescription = objects["CardDescription"];
        CardDescription.SetActive(false);
        CardDescription.AddComponent<Shadow>().effectDistance = new Vector2(5, -5);
        MonsterDescription = objects["MonsterDescription"];
        MonsterText = objects["TextMonster"].GetComponent<Text>();
        Started = true;
    }

    public void OnDescriptionActivate(CardInfo card, int price)
    {
        CardDescription.SetActive(false);
        if (card.Type == CardType.Ability) InitAbility(card);
        if (card.Type == CardType.Madness) InitMadness(card);
        else InitCard(card, price);
    }

    public void OnDescriptionCardActivate(string card)
    {
        Key = card;
        foreach (string ab in Enum.GetNames(typeof(Ability)))
        {
            if (ab == Key)
            {
                InitAbility(ab);
                break;
            }
        }
        foreach (string mon in Enum.GetNames(typeof(Monsters)))
        {
            if (mon == Key)
            {
                InitMonster(mon);
                break;
            }
        }
        if (Key == "Madness")
        {
            InitMadness(Key);
        }
    }

    private void OnUpdateAnchors()
    {
        gameObject.SetActive(false);
        objects["MonsterDescription"].SetActive(false);
    }

    public void InitCard(CardInfo card, int price)
    {
        gameObject.SetActive(true);
        spriteFront = card.sprite;
        CardImage.GetComponent<Image>().sprite = spriteFront;
        switch (card.Type)
        {
            case CardType.Crystal:
                CardDescription.GetComponent<Text>().text = Helper.GetLocalString("AddsXCrystals").Replace(" X ", $"  {price.ToString()}  ");
                CardName.text = Helper.GetLocalString("TitleCardCrystal");
                break;
            case CardType.Energy:
                CardDescription.GetComponent<Text>().text = Helper.GetLocalString("AddsXEnergy").Replace(" X ", $"  {price.ToString()}  ");
                CardName.text = Helper.GetLocalString("TitleCardEnergy");
                break;
            case CardType.Health:
                CardDescription.GetComponent<Text>().text = Helper.GetLocalString("AddsXBravery").Replace(" X ", $"  {price.ToString()}  ");
                CardName.text = Helper.GetLocalString("TitleCardBravery");
                break;
            case CardType.Monster:
                InitMonster(card.Name);
                break;
        }
    }

    public void DescriptionCard(CardInfo card)
    {
        gameObject.SetActive(true);
        CardDescription.GetComponent<Text>().text = card.LocalName;
        CardName.text = card.LocalName;
        CardImage.GetComponent<Image>().sprite = card.sprite;
    }

    private void InitMadness(CardInfo card)
    {
        gameObject.SetActive(true);
        CardName.text = Helper.GetMadnessDescription().name;
        spriteFront = card.sprite;
        CardImage.GetComponent<Image>().sprite = spriteFront;
        CardDescription.GetComponent<Text>().text = Helper.GetMadnessDescription().description;
    }

    private void InitAbility(CardInfo card)
    {
        Key = card.Name;
        gameObject.SetActive(true);
        CardName.text = Helper.GetAbilityDescription(Key).name;
        spriteFront = card.sprite;
        CardImage.GetComponent<Image>().sprite = spriteFront;
        CardDescription.GetComponent<Text>().text = Helper.GetAbilityDescription(Key).description;
    }

    private void InitAbility(string ab)
    {
        gameObject.SetActive(true);
        CardName.text = Helper.GetAbilityDescription(Key).name;
        spriteFront = URPNGLoader.LoadURPNG("3/" + GameConstants.PathToSprites[CardType.Ability], Helper.GetAbilityDescription(ab).sprite);
        CardImage.GetComponent<Image>().sprite = spriteFront;
        CardDescription.GetComponent<Text>().text = Helper.GetAbilityDescription(Key).description;
    }

    private void InitMonster(string mon)
    {
        gameObject.SetActive(true);
        CardName.text = Helper.GetMonstersStrings(mon).name;
        spriteFront = URPNGLoader.LoadURPNG("3/" + GameConstants.PathToSprites[CardType.Monster], Helper.GetMonstersStrings(mon).numberSprite);
        CardImage.GetComponent<Image>().sprite = spriteFront;
        CardDescription.GetComponent<Text>().text = Helper.GetLocalString("TakesXBravery").Replace(" X ", $"  {Helper.GetMonstersStrings(mon).strength}  ");  // "Забирает " + Helper.GetMonstersStrings(mon).strength + " вашей храбрости.";
        MonsterDescription.SetActive(true);
        MonsterText.text = Helper.GetMonstersStrings(mon).description;
    }

    private void InitMadness(string card)
    {
        Key = card;
        gameObject.SetActive(true);
        CardName.text = Helper.GetMadnessDescription().name;
        spriteFront = URPNGLoader.LoadURPNG("3/" + GameConstants.PathToSprites[CardType.Madness], Helper.GetMadnessDescription().sprite);
        CardImage.GetComponent<Image>().sprite = spriteFront;
        CardDescription.GetComponent<Text>().text = Helper.GetMadnessDescription().description;
    }

    public void OnSwipe()
    {
        Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.CardTurning);

        cardRotation.Image = CardImage;
        cardRotation.Description = CardDescription;
        cardRotation.SpriteFront = spriteFront;
        cardRotation.SpriteBack = spriteBack;
        cardRotation.Back = buttonBack;
        cardRotation.Rotation();
    }
}
