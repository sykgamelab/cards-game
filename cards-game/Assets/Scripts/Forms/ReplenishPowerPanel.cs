﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReplenishPowerPanel : Form {
    public static ReplenishPowerPanel I { get; set; }

    private void Awake()
    {
        I = this;
        Messanger<Events>.Subscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    private void OnDestroy()
    {
        Messanger<Events>.Unsubscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    private void LoadLocalization()
    {
        objects["ReplenishPower"].GetComponent<Text>().text = "ADD POWER";
        objects["ReplenishPower"].GetComponent<Text>().text = "ADD POWER";
        objects["BuyOnePowerText"].GetComponent<Text>().text = "BUY";
        objects["BuyFullPowerText"].GetComponent<Text>().text = "BUY";
        objects["FullText"].GetComponent<Text>().text = "FULL";
    }

    void Start()
    {
        LoadLocalization();
        objects["Back"].GetComponent<Button>().onClick.AddListener(BackToMenu);
        objects["BackBlur"].GetComponent<Button>().onClick.AddListener(BackToMenu);

        objects["Background"].transform.SetParent(objects["frame"].transform.parent);
        objects["frame"].transform.SetAsLastSibling();
        objects["frame"].GetComponent<Image>().raycastTarget = false;

        objects["ReplenishPower"].AddComponent<Shadow>().effectDistance = new Vector2(5, -5);

        objects["BuyOne"].GetComponent<Button>().onClick.AddListener(() => BuyPower(false));
        objects["BuyFull"].GetComponent<Button>().onClick.AddListener(() => BuyPower(true));

        Started = true;
    }
    private void OnUpdateAnchors()
    {
        gameObject.SetActive(false);
    }

    public void BackToMenu()
    {
        Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.DoorClosing);

        gameObject.SetActive(false);
        Helper.SetStateRewardIcon();
    }

    private void BuyPower(bool full)
    {
        if (PlayerPrefs.GetInt("Power", GameConstants.maxPower) < GameConstants.maxPower)
        {
            if (full)
            {
                int crystal = PlayerPrefs.GetInt("Crystal");
                if (crystal >= 1500) // воткнуть 1500 в Helper
                {
                    Helper.AddCrystals(-1500);
                    if (Helper.GetTraining5Passed())
                    {
                        Helper.TrackTaskInProgress(InGameTasks.SpendCrystals.ToString(), 1500); 
                        Helper.SetAchievement("spend_crystals", Helper.GetAchievement("spend_crystals") + 1500);
                    }

                    Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.CrystalsExploding);
                    Messanger<int, Events>.SendMessage(Events.AddPower, 30);
                    Messanger<Events>.SendMessage(Events.CrystalUpdate);
                    //MenuPanel.I.Play.GetComponent<StartGame>().Refresh();  // подисывает на кнопку Play в лобби списание сил

                    SocialManager.I.SetProgressToReport();
                }
                else
                {
                    PopUpPanel.I.ShowMessage("Недостаточно кристаллов");
                }
            }
            else
            {
                int crystal = PlayerPrefs.GetInt("Crystal");
                if (crystal >= 150) // воткнуть 150 в Helper
                {
                    Helper.AddCrystals(-150);
                    if (Helper.GetTraining5Passed())
                    {
                        if (Helper.GetTraining5Passed())
                        {
                            Helper.TrackTaskInProgress(InGameTasks.SpendCrystals.ToString(), 150); 
                            Helper.SetAchievement("spend_crystals", Helper.GetAchievement("spend_crystals") + 150);
                        }
                    }

                    Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.CrystalsExploding);
                    Messanger<int, Events>.SendMessage(Events.AddPower, 1);
                    Messanger<Events>.SendMessage(Events.CrystalUpdate);
                    //MenuPanel.I.Play.GetComponent<StartGame>().Refresh();   // подисывает на кнопку Play в лобби списание сил

                    SocialManager.I.SetProgressToReport();
                }
                else
                {
                    PopUpPanel.I.ShowMessage("Недостаточно кристаллов");
                }
            }
        }
        else
        {
            PopUpPanel.I.ShowMessage("У вас максимальное количество сил");
        }
    }
}
