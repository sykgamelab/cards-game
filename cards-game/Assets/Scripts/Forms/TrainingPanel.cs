using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TrainingPanel : Form
{
    public static TrainingPanel I { get; set; }
    public GameObject Arrow { get; set; }
    private Action NextStep;
    RectTransform rectTransform;

    private void Awake()
    {
        I = this;
        Messanger<Events>.Subscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    private void OnDestroy()
    {
        Messanger<Events>.Unsubscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    void Start()
    {
        objects["Booster"].SetActive(false);
        objects["ScreenBlock"].SetActive(false);
        objects["ScreenBlock2"].SetActive(false);
        objects["Background"].transform.SetParent(objects["frame"].transform.parent);
        objects["frame"].transform.SetAsLastSibling();
        objects["frame"].GetComponent<Image>().raycastTarget = false;

        objects["EnergyArrow"].GetComponent<Image>().raycastTarget = false;      objects["EnergyArrow"].SetActive(false);
        objects["DeckArrow"].GetComponent<Image>().raycastTarget = false;        objects["DeckArrow"].SetActive(false);
        objects["AchievementArrow"].GetComponent<Image>().raycastTarget = false; objects["AchievementArrow"].SetActive(false);
        objects["BackArrow"].GetComponent<Image>().raycastTarget = false;        objects["BackArrow"].SetActive(false);
        objects["CollectionArrow"].GetComponent<Image>().raycastTarget = false;  objects["CollectionArrow"].SetActive(false);
        objects["AbilityInCollectionArrow"].GetComponent<Image>().raycastTarget = false; objects["AbilityInCollectionArrow"].SetActive(false);
        objects["ArrowToCardInCollection"].GetComponent<Image>().raycastTarget = false;  objects["ArrowToCardInCollection"].SetActive(false);

        objects["TrainingButton"].AddComponent<TrainingButton>();

        Started = true;
    }

    private void OnUpdateAnchors()
    {
        GameObject guide = objects["Guide"];
        Transform guideParent = guide.transform.parent;
        guideParent.GetComponent<RectTransform>().pivot = new Vector2(0.144088f , 0f);
        guideParent.SetParent(GetObject("frame").transform, true);
        objects["frame"].GetComponent<RectTransform>().pivot = new Vector2(0.036934f * 750f / Screen.width, 0.051877f * 750f / Screen.width);    // ����������� �������� ��� ������, ������� �����
        guideParent.position = objects["frame"].transform.position;
        guide.GetComponent<Image>().raycastTarget = false;

        gameObject.SetActive(false);
        Arrow = objects["Arrow"];
        Arrow.SetActive(false);            
    } 

    bool waitdone = false;         
    IEnumerator WaitCorutine(float time = 1f, Action action = null)
    {
        if (!waitdone)
        {
            waitdone = true;
            yield return new WaitForSeconds(time);
        }
        action.Invoke();
        waitdone = false;
    }
}
