﻿using System;
using UnityEngine.UI;

public class RefinementProgressPanel : Form
{
    public static RefinementProgressPanel I { get; set; }
    private Text textTitle, textMessage, textBtnYes, textBtnNo;
    Action<bool> callback;

    public void OnYesClick()
    {
        callback?.Invoke(true);
        gameObject.SetActive(false);
    }

    public void OnNoClick()
    {
        callback?.Invoke(false);
        gameObject.SetActive(false);
    }

    public void Show(Action<bool> callback)
    {
        this.callback = callback;
        gameObject.SetActive(true);
    }

    private void OnUpdateAnchors() => gameObject.SetActive(false);

    private void Awake()
    {
        I = this;
        Messanger<Events>.Subscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    private void Localization()
    {
        textTitle.text = "ВЫ УВЕРЕНЫ?";
        string messageRu = "Вы точно хотите обновить прогресс?";
        string messageEn = "Are you sure you want to update progress?";
        string messageDe = "Möchten Sie den Fortschritt wirklich aktualisieren?";
        string messageEs = "¿Estás seguro de que quieres actualizar el progreso?";
        string messageFr = "Êtes-vous sûr de vouloir mettre à jour vos progrès?";
        textMessage.text = messageRu;
        textBtnYes.text = "Да";
        textBtnNo.text = "Нет";
    }

    private void Start()
    {
        objects["refinementProgressBgTexture"].transform.SetParent(objects["refinementProgressFrame"].transform.parent);
        objects["refinementProgressFrame"].transform.SetAsLastSibling();

        textTitle = objects["refinementProgressTitle"].GetComponent<Text>();
        textMessage = objects["refinementProgressText"].GetComponent<Text>();
        textBtnYes = objects["refinementProgressYesText"].GetComponent<Text>();
        textBtnNo = objects["refinementProgressNoText"].GetComponent<Text>();
        objects["refinementProgressYes"].GetComponent<Button>().onClick.AddListener(OnYesClick);
        objects["refinementProgressNo"].GetComponent<Button>().onClick.AddListener(OnNoClick);

        Localization();
        Started = true;
    }

    private void OnDestroy() => Messanger<Events>.Unsubscribe(Events.UpdateAnchors, OnUpdateAnchors);
}
