﻿using System;
using System.Text;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressPanel : Form
{
    private Text textTitle, textBtnAchievements, textBtnRaitings;
    private GameObject objBtnCrystal, objTextCrystal, objAchievements, objRaitings, scrollAchievements, scrollRaitings, errorMessage = null;
    public Transform ScrollAchievementsContent; 
    private Transform scrollRaitingsContent;
    private float width, height, multiple, spacingX, cellWidthPlayers, cellHeightPlayers;
    private bool responseLoaded = false;
    private int statusCode = 0;
    private string response = null;
    public static ProgressPanel I { get; set; }

    private void OnUpdateAnchors() => gameObject.SetActive(false);

    private void Awake()
    {
        I = this;
        Messanger<Events>.Subscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    private void Localization()
    {
        textTitle.text = "GAME PROGRESS"; //"ПРОГРЕСС ИГРЫ";       // localize
        textBtnAchievements.text = "ACHIEVEMENTS"; //"ДОСТИЖЕНИЯ"; // localize
        textBtnRaitings.text = "RATINGS"; //"РЕЙТИНГИ";            // localize

    }

    public void GoToShop()
    {
        gameObject.SetActive(false);
        ShopPanel.I.SetActiveCrystalsContent(true);
        Messanger<Events>.SendMessage(Events.SwitchShop);
        ShopPanel.I.gameObject.SetActive(true);
        Messanger<ShopTabs, Events>.SendMessage(Events.OpenShopTab, ShopTabs.Crystals);
    }

    private void GoToMenu()
    {
        Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.DoorClosing);

        gameObject.SetActive(false);
        MenuPanel.I.gameObject.SetActive(true);
        Helper.SetStateRewardIcon();
    }

    public void OpenTab(bool achievements)
    {
        Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.Selecting);

        errorMessage?.SetActive(false);
        if (achievements)
        {
            scrollAchievements.SetActive(true);
            scrollRaitings.SetActive(false);
            objAchievements.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/settings_atlas", 20);
            objRaitings.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/settings_atlas", 19);
            textBtnAchievements.color = new Color(106f / 255f, 65f / 255f, 23f / 255f, 255f);
            textBtnRaitings.color = new Color(216f / 255f, 169f / 255f, 77f / 255f, 255f);
        }
        else
        {
            scrollAchievements.SetActive(false);
            scrollRaitings.SetActive(true);
            objAchievements.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/settings_atlas", 19);
            objRaitings.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/settings_atlas", 20);
            textBtnAchievements.color = new Color(216f / 255f, 169f / 255f, 77f / 255f, 255f);
            textBtnRaitings.color = new Color(106f / 255f, 65f / 255f, 23f / 255f, 255f);
            LoadPlayers();
        }
    }

    private void LoadOpenAchievements()
    {
        GameObject container = new GameObject();
        container.name = "progressScrollContainer1";
        GridLayoutGroup grid = container.AddComponent<GridLayoutGroup>();
        grid.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
        grid.constraintCount = 3;
        grid.padding.top = grid.padding.bottom = (int)(multiple * width * 0.04f);
        grid.padding.left = grid.padding.right = (int)(multiple * height * 0.026f);
        grid.spacing = new Vector2(spacingX, spacingX);
        grid.cellSize = new Vector2(0.27f * width * multiple, 0.226f * height * multiple);

        GameObject prefab = objects["prefabOpenAchievement"];

        foreach (var obj in Enum.GetValues(typeof(OpenAchievement))) Instantiate(prefab, container.transform);
        container.transform.parent = ScrollAchievementsContent;
        RefreshOpenAchievementStats();
        prefab.gameObject.SetActive(false);
    }

    public void SetDefaultContentPosition()
    {
        ScrollAchievementsContent.GetComponent<RectTransform>().localPosition = new Vector3(ScrollAchievementsContent.position.x, 0f, 0f);
        ScrollAchievementsContent.parent.parent.GetChild(2).GetComponent<Scrollbar>().value = 1f;
    }

    public void RefreshOpenAchievementStats()
    {
        int i = 0;
        foreach (var obj in Enum.GetValues(typeof(OpenAchievement)))
        {
            string key = obj.ToString();
            OpenAchievementsStrings openAchievementsStrings = Helper.GetOpenAchievementStrings(key);
            GameObject newObj = ScrollAchievementsContent.GetChild(0).GetChild(i++).gameObject;
            int level = 0;
            if (PlayerPrefs.GetInt(key, 0) >= openAchievementsStrings.need[0] && Helper.GetOpenAchievementLevel(key) >= 0)
            {
                if (PlayerPrefs.GetInt(key, 0) >= openAchievementsStrings.need[1] && Helper.GetOpenAchievementLevel(key) >= 1)
                {
                    if (PlayerPrefs.GetInt(key, 0) >= openAchievementsStrings.need[2] && Helper.GetOpenAchievementLevel(key) >= 2) level = 3;
                    else level = 2;
                }
                else level = 1;
            }
            int idxNeed = (level == 3) ? 2 : level;
                                                   
            int idxSprite = openAchievementsStrings.sprite[Helper.GetOpenAchievementLevel(key)];
            newObj.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/atlas_achievements", idxSprite);
            Button reward = newObj.transform.GetChild(0).GetChild(0).GetChild(1).GetChild(1).GetChild(0).GetComponent<Button>();
            Text scoreText = reward.gameObject.transform.GetChild(1).GetChild(1).GetChild(0).GetComponent<Text>();
            string description = openAchievementsStrings.description;
            if (Helper.GetOpenAchievementLevel(key) == 3)
            {
                reward.gameObject.SetActive(false);
                description = openAchievementsStrings.description.Replace($"X", $"  {openAchievementsStrings.need[idxNeed]}  ");
            }
            else
            {
                reward.gameObject.SetActive(true);  // кнопка вкючена только из-за текста прогресса внутри неё. слушателя на ней нет, пока игрок не выполнит ачивку
                if (PlayerPrefs.GetInt(key, 0) >= openAchievementsStrings.need[level - 1 >= 0 ? level - 1 : 0] && 
                    (Helper.GetOpenAchievementLevel(key) < level))
                {
                    scoreText.text = Helper.GetLocalString("TakeMonster");
                    reward.onClick.RemoveAllListeners();
                    reward.onClick.AddListener(() =>
                    {
                        OnTakeReward(key);
                        level = Helper.GetOpenAchievementLevel(key);
                        idxSprite = openAchievementsStrings.sprite[level];
                        newObj.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/atlas_achievements", idxSprite);
                        if (level == 3)
                        {
                            reward.gameObject.SetActive(false);
                        }
                        else
                        {                                     
                            int total = openAchievementsStrings.need[idxNeed];
                            description = openAchievementsStrings.description.Replace($"X", $"  {total}  ");
                            newObj.transform.GetChild(0).GetChild(0).GetChild(3).GetChild(1).GetChild(0).GetComponent<Text>().text = description.ToUpper();
                            scoreText.text = $"{PlayerPrefs.GetInt(key, 0)}/{total}";
                        }
                        reward.onClick.RemoveAllListeners();

                        if (Helper.GetTraining6Passed()) RefreshOpenAchievementStats();
                    });
                    description = description.Replace("X", $"  {openAchievementsStrings.need[level-1]}  ");
                }
                else
                {
                    description = description.Replace("X", $"  {openAchievementsStrings.need[idxNeed]}  ");
                    scoreText.text = $"{PlayerPrefs.GetInt(key, 0)}/{openAchievementsStrings.need[idxNeed]}";
                }
            }

            newObj.transform.GetChild(0).GetChild(0).GetChild(3).GetChild(1).GetChild(0).GetComponent<Text>().text = description.ToUpper();
        }
    }

    public void RefreshHiddenAchievementStats()
    {
        if (Started)
        {
            int i = 0;
            foreach (var key in Enum.GetValues(typeof(HiddenAchievement)))
            {
                AchievementsStrings achievementsStrings = Helper.GetAchievementStrings(key.ToString());
                GameObject image = ScrollAchievementsContent.GetChild(3).GetChild(i).GetChild(0).GetChild(0).gameObject;
                i++;
                if (Helper.IsAchievementOpen(key.ToString()))
                {
                    if (image.GetComponent<Button>() == null)
                    {
                        image.AddComponent<Button>();
                        image.AddComponent<ClickHandler>().LongPress = () =>
                        {
                            Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.PressHiddenAchievement);
                            DescriptionAchievement.I.Init(achievementsStrings.name, achievementsStrings.description);
                            DescriptionAchievement.I.gameObject.SetActive(true);
                        };
                    }

                    image.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/achievements_atlas", achievementsStrings.sprite);
                }
                else
                    image.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/atlas_achievements", 24);
            }
        }
    }

    private void OnTakeReward(string achievement)
    {
        Messanger<string, Events>.SendMessage(Events.PlayAchievementEffect, SoundClips.GetAchievement);
        int level = Helper.GetOpenAchievementLevel(achievement) == 0 ? 1 : Helper.GetOpenAchievementLevel(achievement);
        SocialManager.I.ReportOpenAchievement($"{achievement}{level}");

        List<string> monster = new List<string>();
        foreach (string m in Enum.GetNames(typeof(Monsters))) if (PlayerPrefs.GetInt(m) == 0) monster.Add(m);
        if (monster.Count != 0)
        {
            int f = UnityEngine.Random.Range(0, monster.Count);
            string mon = monster[f];
            PlayerPrefs.SetInt(mon, 1);
            CollectionCard.I.OnDescriptionCardActivate(mon);
            Helper.TrackTaskInProgress(InGameTasks.OpenNewMonster.ToString(), 1);
        }
        else Debug.Log("Player unlocked all monsters");
        Helper.AddOpenAchievementLevel(achievement, Helper.GetOpenAchievementLevel(achievement));
        Debug.Log($"<color=green>Helper.GetTraining6Passed() = {Helper.GetTraining6Passed()}</color>");
    }

    private void LoadTitleHiddenAchievements()
    {
        GameObject container, prefab, newObj;
        GridLayoutGroup grid;
        for (int i = 2; i < 4; i++)
        {
            container = new GameObject();
            container.name = "progressScrollContainer" + i;
            grid = container.AddComponent<GridLayoutGroup>();
            grid.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
            grid.constraintCount = 1;
            grid.padding.left = (int)(multiple * height * 0.026f);
            grid.spacing = new Vector2(0, 0);
            if (i == 2)
            {
                prefab = objects["prefabArrowAndText"];
                grid.padding.right = grid.padding.left;
                grid.padding.bottom = (int)(multiple * height * 0.01f);
                grid.cellSize = new Vector2(0.921f * width * multiple, 0.043f * height * multiple);
            }
            else
            {
                prefab = objects["prefabLine"];
                grid.cellSize = new Vector2(0.921f * width * multiple, 0.027f * height * multiple);
            }

            newObj = Instantiate(prefab, container.transform);
            if (i == 2) newObj.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = "РЕДКИЕ ДОСТИЖЕНИЯ";  // localize
            prefab.SetActive(false);
            container.transform.parent = ScrollAchievementsContent;
        }
    }

    private void LoadHiddenAchievements()
    {
        GameObject container = new GameObject();
        container.name = "progressScrollContainer4";
        GridLayoutGroup grid = container.AddComponent<GridLayoutGroup>();
        grid.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
        grid.constraintCount = 3;
        grid.padding.bottom = (int)(multiple * width * 0.04f);
        grid.padding.left = grid.padding.right = (int)(multiple * height * 0.026f);
        grid.spacing = new Vector2(spacingX, spacingX);
        grid.cellSize = new Vector2(0.27f * width * multiple, 0.27f * width * multiple);

        GameObject newObj, prefab = objects["prefabHiddenAchievement"];
        foreach (var key in Enum.GetValues(typeof(HiddenAchievement)))
        {
            AchievementsStrings achievementsStrings = Helper.GetAchievementStrings(key.ToString());
            newObj = Instantiate(prefab, container.transform);
            newObj.name = $"prefabHiddenAchievement{key.ToString()}(Clone)";
            GameObject image = newObj.transform.GetChild(0).GetChild(0).gameObject;
            Destroy(image.GetComponent<AspectRatioFitter>());
            RectTransform rect = image.GetComponent<RectTransform>();
            rect.offsetMax = new Vector2(0, 0);

            if (Helper.IsAchievementOpen(key.ToString()))
            {
                image.AddComponent<Button>();
                image.AddComponent<ClickHandler>().LongPress = () =>
                {
                    Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.PressHiddenAchievement);
                    DescriptionAchievement.I.Init(achievementsStrings.name, achievementsStrings.description);
                    DescriptionAchievement.I.gameObject.SetActive(true);
                };
                image.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/achievements_atlas", achievementsStrings.sprite);
            }
            else
                image.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/atlas_achievements", 24);
        }
        container.transform.parent = ScrollAchievementsContent;
        prefab.SetActive(false);
    }

    private void LoadRaitings()
    {
        VerticalLayoutGroup layoutGroup = scrollRaitingsContent.GetComponent<VerticalLayoutGroup>();
        layoutGroup.padding.top = layoutGroup.padding.bottom = (int)(multiple * width * 0.04f);

        GameObject container, newObj, prefab, currentSeriesVictories = new GameObject();
        currentSeriesVictories.name = "progressCurrentSeriesVictories";
        Text textVictories = currentSeriesVictories.AddComponent<Text>();
        textVictories.text = "ТЕКУЩАЯ СЕРИЯ ПОБЕД: ";
        textVictories.font = Resources.Load<Font>("Fonts/AlegreyaSans-ExtraBold");
        textVictories.color = new Color(106f / 255f, 65f / 255f, 23f / 255f, 255f);
        float scale = Screen.width / XmlRenderer.RequiredReferenceWidth;
        textVictories.resizeTextMaxSize = (int)(35 * scale);
        textVictories.resizeTextForBestFit = true;
        textVictories.alignment = TextAnchor.MiddleCenter;
        currentSeriesVictories.transform.parent = scrollRaitingsContent;

        newObj = Instantiate(ScrollAchievementsContent.GetChild(1).gameObject, scrollRaitingsContent);
        newObj.GetComponent<GridLayoutGroup>().padding.top = (int)(multiple * width * 0.02f);
        newObj.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<Text>().text = "СЕРИЯ ПОБЕД: ";     // localize
        newObj = Instantiate(ScrollAchievementsContent.GetChild(2).gameObject, scrollRaitingsContent);

        prefab = objects["prefabRaitingSeries"];
        container = new GameObject();
        GridLayoutGroup grid = container.AddComponent<GridLayoutGroup>();
        grid.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
        grid.constraintCount = 1;
        grid.padding.left = (int)(multiple * height * 0.05f);
        grid.spacing = new Vector2(0, 0);
        cellWidthPlayers = 0.921f * width * multiple;
        cellHeightPlayers = 0.042f * height * multiple;
        grid.cellSize = new Vector2(cellWidthPlayers, cellHeightPlayers);
        newObj = Instantiate(prefab, container.transform);
        container.transform.parent = scrollRaitingsContent;

        prefab.SetActive(false);
        scrollRaitings.SetActive(false);
    }

    private void InitPlayers()
    {
        Debug.Log($"ProgressPanel.LoadPlayers: statusCode = {statusCode}, response = {response}");

        GameObject obj1, obj2, obj3, obj4, newObj;
        obj1 = scrollRaitingsContent.GetChild(0).gameObject;
        obj2 = scrollRaitingsContent.GetChild(1).gameObject;
        obj3 = scrollRaitingsContent.GetChild(2).gameObject;
        obj4 = scrollRaitingsContent.GetChild(3).gameObject;

        if (scrollRaitingsContent.childCount > 4)
            for (int i = scrollRaitingsContent.childCount - 1; i > 3; i--) DestroyImmediate(scrollRaitingsContent.GetChild(i).gameObject);

        string nickAuthUser;
#if UNITY_EDITOR
        nickAuthUser = "test_nick";
#else
        nickAuthUser = SocialManager.I.GetDisplayName();
#endif

        if (statusCode == 200)
        {
            obj2.SetActive(true);
            obj3.GetComponent<GridLayoutGroup>().padding.top = 0;
            obj4.SetActive(true);
            var dict = Json.Deserialize(response) as Dictionary<string, object>;

            List<int> keys = new List<int>();
            foreach (var key in dict.Keys) keys.Add(int.Parse(key));
            keys.Sort((x, y) => y.CompareTo(x));
            
            obj1.GetComponent<Text>().text = $"ТЕКУЩАЯ СЕРИЯ ПОБЕД: {Helper.GetWinSeries()}";       // localize
            for (int i = 0; i < keys.Count; i++)
            {
                string players;
                List<object> listPlayers = (List<object>)dict[keys[i].ToString()];
                StringBuilder sb = new StringBuilder();
                if (listPlayers.Contains(nickAuthUser))
                {
                    sb.Append(nickAuthUser);
                    foreach (var nick in listPlayers)
                    {
                        if (nick.ToString() != nickAuthUser)
                        {
                            sb.AppendLine();
                            sb.Append(nick);
                        }
                    }
                    players = sb.ToString();
                }
                else players = string.Join("\n", listPlayers);
                if (i != dict.Count - 1)
                {
                    newObj = Instantiate(obj2, scrollRaitingsContent);
                    newObj = Instantiate(obj3, scrollRaitingsContent);
                    newObj = Instantiate(obj4, scrollRaitingsContent);
                }
                scrollRaitingsContent.GetChild(i * 3 + 1).GetChild(0).GetChild(2).GetChild(0).GetComponent<Text>().text = $"СЕРИЯ ПОБЕД: {keys[i]}";                         // localize
                scrollRaitingsContent.GetChild(i * 3 + 3).GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().text = players;
                scrollRaitingsContent.GetChild(i * 3 + 3).GetComponent<GridLayoutGroup>().cellSize = new Vector2(cellWidthPlayers, cellHeightPlayers * listPlayers.Count);
            }
        }
        else
        {
            obj1.GetComponent<Text>().text = $"ТЕКУЩАЯ СЕРИЯ ПОБЕД: {Helper.GetWinSeries()}";          // localize
            obj2.SetActive(false);
            obj3.GetComponent<GridLayoutGroup>().padding.top = scrollRaitingsContent.GetComponent<VerticalLayoutGroup>().padding.top / 2;
            if (errorMessage == null) errorMessage = Instantiate(obj1, objects["progressBackground"].transform);
            errorMessage.name = "progressErrorMessage";
            errorMessage.transform.GetComponent<Text>().text = "НЕТ ДОСТУПА К ИНТЕРНЕТУ\n ИЛИ НЕДОСТУПЕН СЕРВЕР";      // localize
            RectTransform rect = errorMessage.GetComponent<RectTransform>();
            Debug.Log($"bg_size = {objects["progressBackground"].GetComponent<RectTransform>().sizeDelta}");
            rect.sizeDelta = new Vector2(0.8f * width * multiple, 0.8f * width * multiple / 2);
            rect.anchorMin = new Vector2(0.5f, 0.5f);
            rect.anchorMax = new Vector2(0.5f, 0.5f);
            rect.anchoredPosition = new Vector2(0f, 0f);
            errorMessage.SetActive(true);
            obj4.SetActive(false);
        }
    }

    private void LoadPlayers()
    {
        string ticket;
#if UNITY_EDITOR
        ticket = "1160130875fda0812c99c5e3f1a03516471a6370c4f97129b221938eb4763e63";
#elif UNITY_IOS || UNITY_ANDROID
        ticket = ServerManager.GetTicket();
#endif

        ServerManager.StartGetRequest(ticket, ServerConstants.UriGetWinSeries, (code, message) =>
        {
            statusCode = code;
            response = message;
            responseLoaded = true;
        });
    }

    private void LoadContents()
    {
        errorMessage?.SetActive(false);
        LoadOpenAchievements();
        LoadTitleHiddenAchievements();
        LoadHiddenAchievements();
        LoadRaitings();
    }

    private void Start()
    {
        objects["progressBackground"].transform.SetParent(objects["progressFrame"].transform.parent);
        objects["progressFrame"].transform.SetAsLastSibling();
        objects["progressFrame"].GetComponent<Image>().raycastTarget = false;

        textTitle = objects["progressTitle"].GetComponent<Text>();
        textBtnAchievements = objects["progressAchievementsText"].GetComponent<Text>();
        textBtnRaitings = objects["progressRaitingsText"].GetComponent<Text>();

        objTextCrystal = objects["progressCrystalText"];
        objTextCrystal.AddComponent<CrystalsUpdate>();
        objTextCrystal.AddComponent<Shadow>();

        objects["progressBack"].GetComponent<Button>().onClick.AddListener(GoToMenu);
        objBtnCrystal = objects["progressCrystalButton"];
        objBtnCrystal.GetComponent<Button>().onClick.AddListener(GoToShop);
        objAchievements = objects["progressAchievements"];
        objAchievements.GetComponent<Button>().onClick.AddListener(() => OpenTab(true));
        objRaitings = objects["progressRaitings"];
        objRaitings.GetComponent<Button>().onClick.AddListener(() => OpenTab(false));

        GameObject content = objects["progressBackground"];
        scrollAchievements = Instantiate(Resources.Load<GameObject>("Prefabs/FlexibleScrollView"), content.transform);
        ScrollAchievementsContent = scrollAchievements.transform.GetChild(0).GetChild(0);
        scrollRaitings = Instantiate(Resources.Load<GameObject>("Prefabs/FlexibleScrollView"), content.transform);
        scrollRaitingsContent = scrollRaitings.transform.GetChild(0).GetChild(0);

        width = content.GetComponent<RectTransform>().rect.width;
        height = content.GetComponent<RectTransform>().rect.height;
        multiple = Screen.width / gameObject.GetComponent<RectTransform>().rect.width;
        spacingX = 0.034f * width * multiple;

        LoadContents();
        Localization();
        Started = true;
    }

    public void Update()
    {
        if (responseLoaded && scrollRaitings != null && scrollRaitings.activeSelf)
        {
            SocialManager.I.StartDisplayNameCoroutine(() => InitPlayers());
            responseLoaded = false;
        }
    }

    private void OnDestroy() => Messanger<Events>.Unsubscribe(Events.UpdateAnchors, OnUpdateAnchors);

    private void OnEnable()
    {
        errorMessage?.SetActive(false);
        if (scrollRaitings && scrollRaitings.activeSelf)
        {
            LoadPlayers();
        }
    }
}
