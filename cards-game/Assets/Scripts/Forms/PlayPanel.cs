﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayPanel : Form
{
    public static PlayPanel I { get; set; }

    private Button[] buttonsBooster;
    private Image[] imagesBooster;
    private Text[] textsBtnApplyBooster, textsNameBooster;
    private GameObject PlayImg, PlayButton;
    private Button Play;
    private List<string> boosters, purchasedBoosters, randomBoosters;
    private bool boostersSorted = false;

    private void Awake()
    {
        I = this;
        Messanger<Events>.Subscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    private void OnDestroy()
    {
        Messanger<Events>.Unsubscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    private void LoadLocalization()
    {
        objects["ReadyForGame"].GetComponent<Text>().text = "ГОТОВ К ИГРЕ?";
        objects["Underhead"].GetComponent<Text>().text = "Ты можешь купить и применить буст здесь:";
    }

    public void BackToMenu(bool sound = true)
    {
        if (sound)
        {
            Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.ClosePlayPanel);
        }

        PlayImg.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/atlas_1", 15);
        PlayImg.transform.SetParent(MenuPanel.I.GetObject("PlaceForPlay").transform, false);
        PlayButton.GetComponent<StartGame>().enabled = false;
        Play.onClick.RemoveAllListeners();

        Play.onClick.AddListener(() => MenuPanel.I.GoToPlay());
        gameObject.SetActive(false);
    }

    public void PlayGame()
    {
        if (Helper.IsMusicActive()) Messanger<bool, Events>.SendMessage(Events.SetMusicActive, false);
        Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.PlayGame);
        if (Helper.IsMusicActive())
        {
            Messanger<bool, Events>.SendMessage(Events.SetMusicActive, true);
            Messanger<string, Events>.SendMessage(Events.PlayMusic, SoundClips.GameMusics[Helper.GetIndexGameMusics()]);
            Helper.SetNextIndexGameMusics();
        }

        PopUpPanel.I.OnClickOk = TablePanel.I.BackToMenu;
        TablePanel.I.gameObject.SetActive(true);
        gameObject.SetActive(false);
        MenuPanel.I.gameObject.SetActive(false);
        GameResultsPanel.I.gameObject.SetActive(false);
    }

    void Start()
    {
        LoadLocalization();
        objects["Back"].GetComponent<Button>().onClick.AddListener(() => BackToMenu());
        objects["BackBlur"].GetComponent<Button>().onClick.AddListener(() => BackToMenu());
        objects["Background"].transform.SetParent(objects["frame"].transform.parent);
        objects["frame"].transform.SetAsLastSibling();
        objects["frame"].GetComponent<Image>().raycastTarget = false;

        // CLEAR TIMERS
        /*foreach (var b in Enum.GetNames(typeof(Boosts)))
        {
            //Helper.DeleteBooster(b);
            //Helper.DeleteBoosterActivated(b);
            //Helper.DeleteTimeBooster(b);
        }  */
        //Helper.SetBoosterActivated("ReAbilities", 0);

        buttonsBooster = new Button[3];
        buttonsBooster[0] = objects["PlayPanelBtnBooster1"].GetComponent<Button>();
        buttonsBooster[1] = objects["PlayPanelBtnBooster2"].GetComponent<Button>();
        buttonsBooster[2] = objects["PlayPanelBtnBooster3"].GetComponent<Button>();

        imagesBooster = new Image[3];
        imagesBooster[0] = objects["PlayPanelImgBooster1"].GetComponent<Image>();
        imagesBooster[1] = objects["PlayPanelImgBooster2"].GetComponent<Image>();
        imagesBooster[2] = objects["PlayPanelImgBooster3"].GetComponent<Image>();

        textsBtnApplyBooster = new Text[3];
        textsBtnApplyBooster[0] = objects["PlayPanelBtnTextBooster1"].GetComponent<Text>();
        textsBtnApplyBooster[1] = objects["PlayPanelBtnTextBooster2"].GetComponent<Text>();
        textsBtnApplyBooster[2] = objects["PlayPanelBtnTextBooster3"].GetComponent<Text>();

        textsNameBooster = new Text[3];
        textsNameBooster[0] = objects["PlayPanelNameBooster1"].GetComponent<Text>();
        textsNameBooster[1] = objects["PlayPanelNameBooster2"].GetComponent<Text>();
        textsNameBooster[2] = objects["PlayPanelNameBooster3"].GetComponent<Text>();

        foreach (Text t in textsNameBooster) t.gameObject.AddComponent<Shadow>().effectDistance = new Vector2(2, -2);
        foreach (Text t in textsBtnApplyBooster) t.gameObject.AddComponent<Shadow>().effectDistance = new Vector2(2, -2);

        boosters = new List<string>();
        purchasedBoosters = new List<string>();
        randomBoosters = new List<string>();
        InitBoosters(true);
        Started = true;
    }

    public void InitBoosters(bool create)
    {
        if (create)
        {
            boosters.Clear();
            purchasedBoosters.Clear();
            randomBoosters.Clear();
            CreateBoosters();
        }

        SortBoosters();
        ChangeStatesBoosters();
    }

    private void OnUpdateAnchors()
    {
        I.AddObject("PlayImg", MenuPanel.I.GetObject("PlayImg"));
        I.AddObject("Play", MenuPanel.I.GetObject("Play"));
        PlayImg = objects["PlayImg"];
        PlayButton = objects["Play"];

        PlayButton.AddComponent<StartGame>();
        Play = PlayButton.GetComponent<Button>();
        PlayButton.GetComponent<StartGame>().enabled = false;
        gameObject.SetActive(false);
    }

    private void FillListsBoosters()
    {
        foreach (string name in Enum.GetNames(typeof(Boosts)))
        {
            if (name == "ReAbilities") continue;

            // проверка активированных бустеров
            if (Helper.GetTimeBooster(name) < DateTime.Now)
            {
                Helper.DeleteTimeBooster(name);
                Helper.SetBoosterActivated(name, 0);
            }
            if (Helper.GetBoosterActivated(name) != 0 && boosters.Count < 3) boosters.Add(name);
            if (boosters.Count == 3) break;

            // проверка купленных и неактивных бустеров
            if (Helper.GetBooster(name) > 0 && Helper.GetBoosterActivated(name) == 0)
            {
                purchasedBoosters.Add(name);
            }

            // проверка платных, некупленных и неактивных бустеров
            if (Helper.GetBoostDescription(name).price > 0 && Helper.GetBooster(name) <= 0 && Helper.GetBoosterActivated(name) == 0)
            {
                randomBoosters.Add(name);
            }
        }
    }

    private void AddBoosters()
    {
        foreach (string name in purchasedBoosters)
        {
            boosters.Add(name);
            if (boosters.Count == 3) break;
        }

        int randomIndex;
        while (boosters.Count < 3)
        {
            randomIndex = UnityEngine.Random.Range(0, randomBoosters.Count);
            boosters.Add(randomBoosters[randomIndex]);
            randomBoosters.RemoveAt(randomIndex);
        }
    }
    
    private void CreateBoosters()
    {
        FillListsBoosters();
        if (boosters.Count < 3) AddBoosters();
    }

    private void ChangeStatesBoosters()
    {
        for (int i = 0; i < boosters.Count; i++) InitBooster(boosters[i], i);
    }

    private void InitBooster(string name, int i)
    {
        BoostersStrings boostersStrings = Helper.GetBoostDescription(name);
        textsNameBooster[i].text = boostersStrings.name;
        // (0 - activate, 1 - deactivate)
        imagesBooster[i].sprite = URPNGLoader.LoadURPNG("3/boosts_atlas", boostersStrings.sprite[1]);
        BoostTimer timer = textsBtnApplyBooster[i].gameObject.GetComponent<BoostTimer>();
        if (timer != null) Destroy(timer);
        textsBtnApplyBooster[i].text = Helper.GetBoostDescription(name).price + " крист.";    // localize
        buttonsBooster[i].onClick.RemoveAllListeners();
        if (Helper.GetBooster(name) > 0 || Helper.GetBoosterActivated(name) != 0) //бустер куплен или применен
        {
            if (IsBoosterApplied(name))
            {
                textsBtnApplyBooster[i].gameObject.AddComponent<BoostTimer>().Init(name); // отображаем таймер
                imagesBooster[i].sprite = URPNGLoader.LoadURPNG("3/boosts_atlas", boostersStrings.sprite[0]);
                buttonsBooster[i].gameObject.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/settings_atlas", 15);//взять номер спрайта из хелпера
            }
            else
            {
                textsBtnApplyBooster[i].text = Helper.GetLocalString("Apply");
                buttonsBooster[i].gameObject.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/atlas_without_compress", 2);//взять номер спрайта из хелпера
                buttonsBooster[i].onClick.AddListener(() => ApplyBooster(name, true));
            }
        }
        else
        {
            buttonsBooster[i].gameObject.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/settings_atlas", 13);//взять номер спрайта из хелпера
            buttonsBooster[i].onClick.AddListener(() => BuyBooster(name));
        }
    }

    //public void ApplyBoosterManager() {}

    private bool IsBoosterApplied(string name)
    {
        return Helper.GetBoosterActivated(name) == 0 ? false : true;
    }

    private void SortBoosters()
    {
        boosters.Sort(
            delegate(string b1, string b2)
            {
                int compareActivate = Helper.GetBoosterActivated(b2).CompareTo(Helper.GetBoosterActivated(b1));
                if (compareActivate == 0)
                {
                    return Helper.GetTimeBooster(b1).CompareTo(Helper.GetTimeBooster(b2));
                }
                return compareActivate;
            }
        );
    }

    private void ApplyBooster(string name, bool applied)
    {
        if (name != "ReAbilities" && name != "InfinitePower")
        {
            Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.UseBooster);

            Helper.SetBoosterActivated(name, applied ? GameConstants.TimeActionPurchasedBooster : 0);
            Helper.SetBooster(name, Helper.GetBooster(name) - 1 * (applied ? 1 : -1));
            Helper.SetTimeBooster(name, DateTime.Now.AddHours(GameConstants.TimeActionPurchasedBooster));
            Messanger<Events>.SendMessage(Events.StartBoostTimer);
            Messanger<Events>.SendMessage(Events.OnCheckSimilarBoost);

            InitBooster(name, boosters.IndexOf(name));
        }
    }

    public void BuyBooster(string name)
    {
        int price = Helper.GetBoostDescription(name).price;
        int crystals = Helper.GetCrystals();
        if (crystals >= price)
        {
            Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.CrystalsExploding);

            Helper.AddCrystals(-price);        
            Messanger<Events>.SendMessage(Events.CrystalUpdate);
            if (name != "InfinitePower")
            {
                Helper.SetBooster(name, 1);
                Helper.Recount();
                InitBooster(name, boosters.IndexOf(name));
            }
            else
            {
                Messanger<int, Events>.SendMessage(Events.AddPower, GameConstants.maxPower); // таким образом этот буст действует как покупка полной энергии
                InitBoosters(true);
            }

            if (Helper.GetTraining5Passed())
            {
                Helper.TrackTaskInProgress(InGameTasks.SpendCrystals.ToString(), price);
                Helper.SetAchievement("spend_crystals", Helper.GetAchievement("spend_crystals") + price);
            }
        }
        else
        {
            PopUpPanel.I.ShowMessage("Недостаточно кристаллов");
        }
    }
}
