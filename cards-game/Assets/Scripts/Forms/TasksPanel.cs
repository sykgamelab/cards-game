﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TasksPanel : Form
{          
    Button btnTakeDaily;
    GameObject[] dailyTasks = null;

    GameObject CrystalText, Back;

    public static TasksPanel I { get; set; }

    private void Awake()
    {
        I = this;
        Messanger<Events>.Subscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    private void OnDestroy()
    {
        Messanger<Events>.Unsubscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    private void LoadLocalization()
    {
        objects["Headline"].GetComponent<Text>().text = "TASKS"; //"ЗАДАНИЯ";
        objects["DailyText"].GetComponent<Text>().text = "DAILY TASKS"; // "ЕЖЕДНЕВНЫЕ";
        objects["TakeDailyText"].GetComponent<Text>().text = "TAKE TASKS"; // "ВЗЯТЬ ЗАДАНИЯ";
        objects["TaskText"].GetComponent<Text>().text = "CURRENT TASKS";//"ТЕКУЩИЕ";
        for (int i = 1; i <= 3; i++)
        {
            objects[$"DailyTakeRewardText{i}"].GetComponent<Text>().text = "TAKE REWARD";
            objects[$"TaskTakeRewardText{i}"].GetComponent<Text>().text = "TAKE REWARD";
        }
    }

    void Start()
    {
        LoadLocalization();
        CrystalText = objects["CrystalText1"];
        CrystalText.AddComponent<CrystalsUpdate>();
        CrystalText.AddComponent<Shadow>();

        objects["CrystalButton1"].GetComponent<Button>().onClick.AddListener(GoToShop);

        Back = objects["Back"];
        Back.GetComponent<Button>().onClick.AddListener(BackToMenu);
        objects["frame"].GetComponent<Image>().raycastTarget = false;

        dailyTasks = new GameObject[3];
        for (int i = 1; i <= 3; i++) dailyTasks[i - 1] = objects[$"DailyTask{i}"];
        btnTakeDaily = objects["TakeDaily"].GetComponent<Button>();
        btnTakeDaily.onClick.RemoveAllListeners();
        btnTakeDaily.onClick.AddListener(() =>
        {
            Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.TakeDaily);
            Helper.SetDailyLocalNumbers(Helper.GetLastDailyServerNumbers());
            MenuPanel.I.HideDailyIcon();
            btnTakeDaily.gameObject.SetActive(false);
            InitDaily(true);
        });
        InitDaily();

        Started = true;
    }

    private void ResetDailyProgress()
    {
        foreach (var daily in Enum.GetNames(typeof(DailyTasks)))
        {
            Helper.DeleteProgress(daily);
            Helper.ResetDailyRewardReceived(daily);
        }
    }

    public void InitDaily(bool clicked = false)
    {
        if (clicked) ResetDailyProgress();

        DailyStrings dailyStrings;
        string[] numbers = Helper.GetDailyLocalNumbers().Split(';');
        int number;
        for (int i = 0; i < numbers.Length; i++)
        {
            number = int.Parse(numbers[i]);
            if (number != -1)
            {
                dailyStrings = Helper.GetDailyStrings(number);
                if (clicked) Helper.SetToProgress(dailyStrings.Key);
                dailyTasks[i].GetComponent<DailyTask>().Init(dailyStrings);
            }
        }
    }

    private void OnUpdateAnchors()
    {
        if (Helper.GetTraining6Passed())
        {
            GetObject("Task1").GetComponent<CurrTask>().Init();
            GetObject("Task2").GetComponent<CurrTask>().Init();
            GetObject("Task3").GetComponent<CurrTask>().Init();
        }
        gameObject.SetActive(false);
    }

    public void BackToMenu()
    {
        Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.DoorClosing);

        gameObject.SetActive(false);
        MenuPanel.I.gameObject.SetActive(true);
        Helper.SetStateRewardIcon();
    }

    public void GoToShop()
    {
        ShopPanel.I.SetActiveCrystalsContent(true);
        Messanger<Events>.SendMessage(Events.SwitchShop);
        ShopPanel.I.gameObject.SetActive(true);
        Messanger<ShopTabs, Events>.SendMessage(Events.OpenShopTab, ShopTabs.Crystals);
    }
}
