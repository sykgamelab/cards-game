﻿using UnityEngine;
using UnityEngine.UI;

public class DescriptionBooster : Form {
    public static DescriptionBooster I { get; private set; }

    GameObject image, description;
    Text textName;
    Sprite spriteFront, spriteBack;
    Button buttonBack;
    CardRotation cardRotation;

    public void OnClose()
    {
        image.SetActive(false);
        gameObject.SetActive(false);
    }

    public void OnSwipe()
    {
        Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.CardTurning);

        cardRotation.Image = image;
        cardRotation.Description = description;
        cardRotation.SpriteFront = spriteFront;
        cardRotation.SpriteBack = spriteBack;
        cardRotation.Back = buttonBack;
        cardRotation.Rotation();
    }

    private void OnUpdateAnchors()
    {
        gameObject.SetActive(false);
    }

    private void Awake()
    {
        I = this;
        Messanger<Events>.Subscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    private void OnDestroy()
    {
        Messanger<Events>.Unsubscribe(Events.UpdateAnchors, OnUpdateAnchors);
    }

    public void Start()
    {
        buttonBack = GetObject("CloseDescriptionBooster").GetComponent<Button>();
        buttonBack.onClick.AddListener(OnClose);

        image = GetObject("ButtonDescriptionBooster");
        image.GetComponent<Button>().targetGraphic = null;
        image.AddComponent<Swipe>().action = OnSwipe;

        cardRotation = image.AddComponent<CardRotation>();
        spriteBack = URPNGLoader.LoadURPNG("3/boosts_atlas", 18);

        textName = GetObject("TextNameBooster").GetComponent<Text>();
        textName.gameObject.AddComponent<Shadow>().effectDistance = new Vector2(5, -5);

        description = GetObject("TextDescriptionBooster");
        description.AddComponent<Shadow>().effectDistance = new Vector2(5, -5);
        description.SetActive(false);

        Started = true;
    }

    public void Init(string name)
    {
        BoostersStrings boostersStrings = Helper.GetBoostDescription(name);
        textName.text = boostersStrings.name;
        spriteFront = URPNGLoader.LoadURPNG("3/boosts_atlas", boostersStrings.sprite[0]);
        image.GetComponent<Image>().sprite = spriteFront;
        description.GetComponent<Text>().text = boostersStrings.description;

        description.SetActive(false);
        image.SetActive(true);
        gameObject.SetActive(true);
    }
}
