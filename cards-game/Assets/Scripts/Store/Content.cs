﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Content : MonoBehaviour { 
    public GameObject Prefab;
    protected bool isDrawed = false;

    private void Awake()
    {
        Messanger<Events>.Subscribe(Events.SwitchShop, StartShop);
        Messanger<Events>.Subscribe(Events.CloseShop, CloseShop);
    }
    private void OnDestroy()
    {
        Messanger<Events>.Unsubscribe(Events.SwitchShop, StartShop);
        Messanger<Events>.Unsubscribe(Events.CloseShop, CloseShop);
    }

    virtual protected void StartShop() { }
    virtual protected void CloseShop()
    {
        int num = gameObject.transform.childCount;
        for (int i = 0; i < num; i++)
        {
            Destroy(gameObject.transform.GetChild(i).gameObject);
        }
    }

    virtual protected void Draw() { }

    /* НЕ УДАЛЯЙ 
    protected void RecountCellSize(int number)
    {
        GridLayoutGroup grid = gameObject.GetComponent<GridLayoutGroup>();
        float width = ShopPanel.I.GetObject("Content").GetComponent<RectTransform>().rect.width;
        float height = ShopPanel.I.GetObject("Content").GetComponent<Rect>().height;
        grid.spacing = new Vector2(width * 0.05f, height * 0.05f);
        grid.padding.top = (int)(height * 0.03f);
        grid.padding.bottom = (int)(height * 0.03f);

        grid.padding.left = (int)(width * 0.06f);
        grid.padding.right = (int)(width * 0.06f);

        float ratio = width * 0.26f / grid.cellSize.x;
        grid.cellSize = new Vector2(width * 0.26f, ratio * grid.cellSize.y);
    } */
}
