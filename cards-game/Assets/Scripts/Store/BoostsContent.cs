﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class BoostsContent : Content {
    // Prefab загружается в ShopPanel при старте
    override protected void StartShop()
    {
        int number = Enum.GetNames(typeof(Boosts)).Length;
        if (!isDrawed) Draw();
        isDrawed = true;
    }
    override protected void CloseShop()
    {
        base.CloseShop();
        isDrawed = false;
    }

    void RecountCellSize()
    {
        float bgWidth = ShopPanel.I.GetObject("Background").GetComponent<RectTransform>().rect.width;
                                                                                    
        GridLayoutGroup grid = gameObject.GetComponent<GridLayoutGroup>();
        float ratio = bgWidth / 1000f;
        grid.padding = new RectOffset(49, 31, 55, 55);
        grid.cellSize = new Vector2(Prefab.GetComponent<RectTransform>().rect.width * ratio, Prefab.GetComponent<RectTransform>().rect.height * ratio);
        grid.spacing = new Vector2(20f, 40f);  
    }

    override protected void Draw()
    {
        //RecountCellSize();
        GameObject newObj;
        string[] arr = Enum.GetNames(typeof(Boosts));
        List<string> boosters = arr.OfType<string>().ToList();

        boosters.Sort((x, y) => Helper.GetTimeBooster(y).CompareTo(Helper.GetTimeBooster(x)));

        foreach (string boost in boosters)
        {
            if (Helper.GetBoostDescription(boost).price == 0 && PlayerPrefs.GetInt(boost + "A") == 0)
                if (PlayerPrefs.GetInt(boost) == 0) continue;

            BoostersStrings boostersStrings = Helper.GetBoostDescription(boost);
            newObj = Instantiate(Prefab, transform);

            GameObject button = newObj.transform.GetChild(0).gameObject;
            button.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/atlas_without_compress", 1); // button
            button.AddComponent<BuyBoost>().Init(boost);
            GameObject textPrice = button.transform.GetChild(0).gameObject;
            GameObject textTime = button.transform.GetChild(1).gameObject;
            textPrice.GetComponent<Text>().text = boostersStrings.price.ToString();
            BoostTimer boostTimer = textTime.GetComponent<BoostTimer>();
            if (boostTimer == null) boostTimer = textTime.AddComponent<BoostTimer>();
            boostTimer.Init(boost);

            GameObject image = newObj.transform.GetChild(1).gameObject;
            image.AddComponent<ClickHandler>().LongPress = () =>
            {
                Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.CardShowing);
                DescriptionBooster.I.Init(boost);
            };
            int boosterIndex =  0;
            if (Helper.GetBoosterActivated(boost) == 0) boosterIndex = 1;
            else
            {
                textPrice.SetActive(false);
                textTime.SetActive(true);
                button.transform.parent.GetChild(2).gameObject.SetActive(false);
            }
            image.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/boosts_atlas", boostersStrings.sprite[boosterIndex]);
            Text boostName = image.transform.GetChild(0).GetComponent<Text>();
            boostName.resizeTextMaxSize = (int)(boostName.resizeTextMaxSize * Screen.width / XmlRenderer.RequiredReferenceWidth); // масштабирование текста
            boostName.text = boostersStrings.name;
            
            if (Helper.GetBoosterActivated(boost) != 0) newObj.transform.SetAsFirstSibling();
        }

        isDrawed = true;
    } 
}
