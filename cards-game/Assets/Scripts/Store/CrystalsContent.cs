﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CrystalsContent : Content {
    GameObject button, crystals, Price, sale;

    override protected void StartShop()
    {
        if (!isDrawed)  Draw();
        isDrawed = true;
    }
    override protected void CloseShop()
    {
        base.CloseShop();
        isDrawed = false;
    }

    protected override void Draw()
    {
        GameObject newObj;
        int t = Helper.KeyCrystal.Count;
        int count = 0;
        foreach(string cryst in Helper.KeyCrystal)
        {
            newObj = Instantiate(Prefab, transform);
            newObj.transform.GetChild(3).gameObject.AddComponent<BuyCrystal>().Init(cryst);
            newObj.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = $"{Helper.GetCrystalsDescription(cryst).crystal}";
            newObj.transform.GetChild(1).GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/" +"atlas_shop_crystals", count);
            newObj.transform.GetChild(3).GetChild(0).GetComponent<Text>().text = $"{Helper.GetCrystalsDescription(cryst).money}$";
            newObj.transform.GetChild(2).gameObject.SetActive(Helper.GetCrystalsDescription(cryst).sale); 
            count++;
            
        }
        isDrawed = true;
    }
}
