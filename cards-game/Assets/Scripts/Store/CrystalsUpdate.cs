﻿using UnityEngine;
using UnityEngine.UI;

public class CrystalsUpdate : MonoBehaviour {

    void Awake()
    {
        Messanger<Events>.Subscribe(Events.CrystalUpdate, OnCrystalUpdate);
    }
	
    public void OnCrystalUpdate()
    {
        gameObject.GetComponent<Text>().text = Helper.GetCrystals().ToString();
    }
    
	void Start()
    {
        OnCrystalUpdate();
    }

    private void OnDestroy()
    {
        Messanger<Events>.Unsubscribe(Events.CrystalUpdate, OnCrystalUpdate);
    }
}
