﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class StoreListener : MonoBehaviour, IStoreListener
{
    private Subscription<string, Events> subscription;
    private bool initializing = false, initializeFailed = false;
    IStoreController controller;
    IExtensionProvider extension;
    private string[] productIds;
    public static Dictionary<string, string> Prices { get; private set; } = null;

    private bool IsInitialized() => controller != null && extension != null;

    private void Init()
    {
        if (IsInitialized()) return;

        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        int productCount = Helper.KeyCrystal.Count;
        productIds = new string[productCount];
        for (int i = 0; i < productCount; i++)
        {
            productIds[i] = Helper.KeyCrystal[i];
            builder.AddProduct(productIds[i], ProductType.Consumable, new IDs
            {
                { productIds[i], MacAppStore.Name },
                { productIds[i], GooglePlay.Name }
            });
        }
        
        UnityPurchasing.Initialize(this, builder);
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extension)
    {
        initializing = false;

        this.controller = controller;
        this.extension = extension;

        Dictionary<string, string> prices = new Dictionary<string, string>();
        string localizedPrice;
        for (int i = 0; i < productIds.Length; i++)
        {
            Product product = controller.products.WithID(productIds[i]);
            localizedPrice = product.metadata.localizedPriceString;
            prices.Add(productIds[i], localizedPrice);
        }

        Prices = prices;
        ShopPanel.I.UpdatePrices();
    }

    private void OnBuyProduct(string id)
    {
        PopUpPanel.I.OnClickOk = null;
        PopUpPanel.I.OnClickCancel = null;

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            PopUpPanel.I.ShowMessage("Ошибка: Нет соединения с интернетом");
            Messanger<Events>.SendMessage(Events.UnlockShop);
            return;
        }
        else if (IsInitialized())
        {
            Debug.Log("STORE_LISTENER: RUN OnBuyProduct");
            if (controller == null)
            {
                Debug.Log("STORE CONTROLLER IS NULL");
            }
            Product product = controller.products.WithID(id);
            if (product == null || !product.availableToPurchase)
            {
                Debug.Log($"{id} is unavailable or not existing");
                PopUpPanel.I.ShowMessage("Ошибка: Продукт в магазине не найден.");
                Messanger<Events>.SendMessage(Events.UnlockShop);
                return;
            }

            Debug.Log($"Buying: {product.definition.id}");
            controller.InitiatePurchase(product);
        }
        else
        {
            PopUpPanel.I.ShowMessage("Ошибка: Инициализация не завершена.");
            Messanger<Events>.SendMessage(Events.UnlockShop);
        }
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        initializing = false;
        initializeFailed = true;
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        if (!failureReason.ToString().Contains("UserCancelled"))
        {
            Debug.Log($"OnPurchaseFailed: FAIL. Product: '{product.definition.storeSpecificId}', PurchaseFailureReason: {failureReason}");
            PopUpPanel.I.ShowMessage("Ошибка покупки");
        }

        Messanger<Events>.SendMessage(Events.UnlockShop);
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        string productId = args.purchasedProduct.definition.id;
        int crystals = Helper.GetCrystals();
        int amount = Helper.GetCrystalsDescription(productId).crystal;

        if (productId.Contains("crystals"))
        {
            Helper.AddCrystals(amount);
            Messanger<Events>.SendMessage(Events.CrystalUpdate);
            Messanger<Events>.SendMessage(Events.UnlockShop);
        }

        SocialManager.I.SetProgressToReport();

        return PurchaseProcessingResult.Complete;
    }

    private void Awake() => subscription = Messanger<string, Events>.Subscribe(Events.BuyProduct, OnBuyProduct);

    private void Start() => Init();

    private void Update()
    {
        if (!initializing && initializeFailed) Init();
    }

    private void OnDestroy() => subscription.Unsubscribe();
}
