﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ThreeShopTabs : MonoBehaviour {
    public GameObject GameObjectTab;
    public Text text;

    private void Awake()
    {
        Messanger<ShopTabs, Events>.Subscribe(Events.OpenShopTab, OpenTab);
    }
    void Start () {
        gameObject.GetComponent<Button>().onClick.AddListener(OnClick);
    }

    private void OnClick()
    {
        Messanger<ShopTabs, Events>.SendMessage(Events.OpenShopTab, (ShopTabs)Enum.Parse(typeof(ShopTabs), gameObject.name));
    }

	private void OpenTab(ShopTabs tab)
    {
        if ((ShopTabs)Enum.Parse(typeof(ShopTabs), gameObject.name) == tab) 
        {
            Messanger<Events>.SendMessage(Events.SwitchShop);
            GameObjectTab.SetActive(true);
            if (tab == ShopTabs.Boosts) Messanger<Events>.SendMessage(Events.StartBoostTimer);
            gameObject.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/" + "atlas_shop_crystals", 11);
            text.color = new Color(106f / 255f, 65f / 255f, 23f / 255f);
        }
        else 
        {
            GameObjectTab.SetActive(false);
            gameObject.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/" + "atlas_shop_crystals", 12);
            text.color = new Color(216f / 255f, 169f / 255f, 77f / 255f);
        }
    }
    private void OnDestroy()
    {
        Messanger<ShopTabs, Events>.Unsubscribe(Events.OpenShopTab, OpenTab);
    }
}
