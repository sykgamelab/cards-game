﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyCrystal : MonoBehaviour {
    private string id { get; set; }
    public void Init(string crs)
    {
        id = crs;
        gameObject.GetComponent<Button>().onClick.AddListener(PurcaseCrystal);
    }
    
    public void PurcaseCrystal()
    {
        Messanger<string, Events>.SendMessage(Events.BuyProduct, id);
        Messanger<Events>.SendMessage(Events.CrystalUpdate);
    }

}
