﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class BuyCard : MonoBehaviour
{
    public string Key { get; set; }
    public int Price { get; set; }

    public void Init(string key)
    {
        Key = key;
        Price = Helper.GetAbilityDescription(Key).price;
        if (!Helper.AbilityCardIsLocked(key)) gameObject.GetComponent<Button>().interactable = false;
        else
        {
            gameObject.GetComponent<Button>().onClick.AddListener(OnBuyCard);
        }
    }

    public void OnBuyCard()
    {
        int crystals = Helper.GetCrystals();
        if (crystals >= Price)
        {
            /* покупка карт по уровням и последующая выборка в коллекциях */
            int sum = 1; // так как карта безумия не в картах способностей
            foreach (string a in Enum.GetNames(typeof(Ability))) if (!Helper.AbilityCardIsLocked(a)) sum++;
            if (sum < 12) PlayerPrefs.SetInt(Key, 2);  
            else PlayerPrefs.SetInt(Key, 1);

            Helper.AddCrystals(-Price);                                                          
            Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.CrystalsExploding);
            Messanger<Events>.SendMessage(Events.CrystalUpdate);
            //PlayPanel.I.ApplyBoosterManager();

            Helper.TrackTaskInProgress(InGameTasks.BuyAbilityCard.ToString(), 1);  
            Helper.TrackTaskInProgress(InGameTasks.SpendCrystals.ToString(), Price); 
            Helper.SetAchievement("spend_crystals", Helper.GetAchievement("spend_crystals") + Price);

            gameObject.GetComponent<Button>().interactable = false;
            Helper.Recount();

            for(int i = 1; i <= 12; i++)
            {
                if (PlayerPrefs.GetString("ability" + i.ToString(), "") == "")
                {
                    PlayerPrefs.SetString("ability" + i.ToString(), Key);
                    PlayerPrefs.SetInt(Key, 2);
                    break;
                }
            }

            Helper.GetLevel();
            if (Helper.CountCard >= 12 && Helper.GetTraining5Passed() && !Helper.GetTraining7Passed()) FirstGame.I.OnTraining(true);

            if (PlayerPrefs.GetInt("FirstOpenAchievement") != 1 && Helper.GetAchievement("spend_crystals") >= Helper.GetOpenAchievementStrings("spend_crystals").need[0])
            {
                Button buttonBack = ShopPanel.I.GetObject("BackFromShop").GetComponent<Button>();
                buttonBack.onClick.AddListener(() =>
                {
                    FirstGame.I.OnFirstAchievement();
                    buttonBack.onClick.RemoveAllListeners();
                    buttonBack.onClick.AddListener(ShopPanel.I.BackToMenu);
                });
            }

            Messanger<string, Events>.SendMessage(Events.ChangeContent, Key);
            //Destroy(gameObject.transform.parent.parent.gameObject);
            gameObject.transform.parent.parent.gameObject.SetActive(false); // не понимаю почему с удалением не робит
            //Destroy(gameObject.transform.parent.parent.gameObject, 0.7f);

            SocialManager.I.SetProgressToReport();
        }
        else
        {
            PopUpPanel.I.ShowMessage("Недостаточно кристаллов");
        }

        int c;
        int zero, one, two; zero = one = two = 0;
        foreach (string ability in Enum.GetNames(typeof(Ability)))

            switch (c = PlayerPrefs.GetInt(ability, 0))
            {
                case 0: zero++; break;
                case 1: one++; break;
                case 2: two++; break;
            }

        Debug.Log($"\n{zero}    {one}    {two}");
    }
}
