﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardsContent : Content
{
    GameObject button, image;
    private Dictionary<string, int> cardList;
    public Dictionary<string, GameObject> Cards;

    private void Start()
    {
        Messanger<string, Events>.Subscribe(Events.ChangeContent, OnChangeContent);
    }

    private void OnDestroy()
    {
        Messanger<string, Events>.Unsubscribe(Events.ChangeContent, OnChangeContent);
    }

    private void OnChangeContent(string card)
    {
        cardList.Remove(card);
        //int sum = 1;
        int num = 1;
        foreach (string ability in Enum.GetNames(typeof(Ability)))
        {
            if (!Helper.AbilityCardIsLocked(ability))
            {
                //sum += Helper.GetAbilityDescription(ability).significance;
                num++;
            }
        }

        //Debug.Log($"  sum: {sum}\n                  num: {num}");

        foreach (KeyValuePair<string, GameObject> c in Cards)
        {
            AbilityCardsLimits acl = Helper.GetCardLimit(num);
            if (Helper.GetAbilityDescription(c.Key).significance <= acl.openCardsWeight) Activate(c.Key, c.Value);
        }
    }

    private void Deactivate(GameObject obj)
    {
        GameObject image = obj.transform.GetChild(0).GetChild(0).gameObject;
        GameObject button = obj.transform.GetChild(1).GetChild(0).gameObject;
        image.GetComponent<Image>().color = new Color(100f / 255f, 100f / 255f, 100f / 255f, 225f / 255f);
        image.transform.GetChild(0).GetChild(0).GetComponent<Text>().color = new Color(100f / 255f, 100f / 255f, 100f / 255f);
        button.GetComponent<Button>().onClick.RemoveAllListeners();
        button.GetComponent<Button>().onClick.AddListener(() => 
            PopUpPanel.I.ShowMessage("Вы не можете купить эту карту. У вас слишком низкий уровень. Покупайте карты более низких уровней, чтобы прокачать свой уровень"));
        button.GetComponent<Image>().color = new Color(100f / 255f, 100f / 255f, 100f / 255f, 225f / 255f);
        button.transform.GetChild(0).GetChild(0).GetComponent<Text>().color = new Color(100f / 255f, 100f / 255f, 100f / 255f);
    }

    private void Activate(string cardName, GameObject obj)
    {
        GameObject image = obj.transform.GetChild(0).GetChild(0).gameObject;
        GameObject button = obj.transform.GetChild(1).GetChild(0).gameObject;
        image.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
        image.transform.GetChild(0).GetChild(0).GetComponent<Text>().color = new Color(1f, 1f, 1f, 1f);
        button.GetComponent<Button>().onClick.RemoveAllListeners();
        button.GetComponent<BuyCard>().Init(cardName);
        button.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
        button.transform.GetChild(0).GetChild(0).GetComponent<Text>().color = new Color(1f, 1f, 1f, 1f);
    }

    override protected void StartShop()
    {
        if (!isDrawed) Draw();
        isDrawed = true;
    }

    override protected void CloseShop()
    {
        base.CloseShop();
        isDrawed = false;
    }

    override protected void Draw()
    {
        int weight = Helper.CountCard;
        GameObject newObj;

        int i = 0;

        //int sum = 1; // + карта безумия
        int purch = 1;

        foreach (string ability in Enum.GetNames(typeof(Ability)))
        {
            if (PlayerPrefs.GetInt(ability) == 1 || PlayerPrefs.GetInt(ability) == 2)
            {
                //sum += Helper.GetAbilityDescription(ability).significance;
                purch++;
                continue;
            }
        }

        cardList = new Dictionary<string, int>();
        AbilityCardsLimits acl = Helper.GetCardLimit(purch);
        Cards = new Dictionary<string, GameObject>(); 

        foreach (string ability in Enum.GetNames(typeof(Ability)))
        {
            if (PlayerPrefs.GetInt(ability) != 0) continue;
            newObj = Instantiate(Prefab, transform);
            newObj.SetActive(true);

            image = newObj.transform.GetChild(0).GetChild(0).gameObject;
            image.AddComponent<ClickHandler>().LongPress = () =>
            {
                Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.CardShowing);
                CollectionCard.I.OnDescriptionCardActivate(ability);
            };
            image.GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/" + GameConstants.PathToSprites[CardType.Ability], Helper.GetAbilityDescription(ability).sprite);
            image.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = Helper.GetAbilityDescription(ability).name;

            button = newObj.transform.GetChild(1).GetChild(0).gameObject;
            button.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = Helper.GetAbilityDescription(ability).price.ToString();
            button.AddComponent<BuyCard>();
            button.GetComponent<BuyCard>().Init(ability);

            int significance = Helper.GetAbilityDescription(ability).significance;

            if (purch <= 12)
            {
                if (acl.openCardsWeight < significance) Deactivate(newObj);
                else Activate(ability, newObj);
            }
            else Activate(ability, newObj);
                                                                                            
            Cards.Add(ability, newObj);
            cardList.Add(ability, i++);
        }
        isDrawed = true;
    } 
}
