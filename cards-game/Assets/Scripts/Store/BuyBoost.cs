﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyBoost : MonoBehaviour {

    public string Key { get; set; }
    public int Price { get; set; }

    private void Awake()
    {
        Messanger<Events>.Subscribe(Events.OnCheckSimilarBoost, CheckSimilarBoost);
    }

    private void Start()
    {
        gameObject.transform.parent.GetChild(2).GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/atlas_without_compress", 0); // кристаллик
    }

    private void OnDestroy()
    {
        Messanger<Events>.Unsubscribe(Events.OnCheckSimilarBoost, CheckSimilarBoost);
    }

    public void Init(string key)
    {
        Key = key;
        Price = Helper.GetBoostDescription(Key).price;
        CheckSimilarBoost();
    }

    public void CheckSimilarBoost()
    {
        gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
        if ((Key == "InfectedMonsters15" && (Helper.GetBoosterActivated("InfectedMonsters10") != 0 || Helper.GetBoosterActivated("InfectedMonsters20") != 0)) ||
            (Key == "IncreaseCrystals50" && (Helper.GetBoosterActivated("IncreaseCrystals25") != 0 || Helper.GetBoosterActivated("IncreaseCrystals75") != 0)) || 
             Key == "ReAbilities")
        {
            gameObject.GetComponent<Button>().interactable = false;
        }
        // если этот буст нельзя купить и он не активирован (в обучении выдают 2 раза такие бусты)
        else if (Helper.GetBoostDescription(Key).price == 0 && Helper.GetBoosterActivated(Key) != 1) 
        {
            gameObject.GetComponent<Button>().interactable = true;
            SetActivePurchasedBooster(true); //gameObject.GetComponent<Button>().onClick.AddListener(ApplyBoost);
        }
        else
        {
            gameObject.GetComponent<Button>().interactable = true;
            if (Helper.GetBoosterActivated(Key) != 0) gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
            else if (Helper.GetBooster(Key) == 1)
            {
                SetActivePurchasedBooster(true); //gameObject.GetComponent<Button>().onClick.AddListener(ApplyBoost);
            }
            else gameObject.GetComponent<Button>().onClick.AddListener(OnBuyBoost);
        }
    }

    public void OnBuyBoost()
    {
        int crystals = Helper.GetCrystals();
        if (crystals >= Price)
        {
            //if (Helper.GetTraining5Passed())
            if (Key == "InfinitePower")
            {
                if (PlayerPrefs.GetInt("Power") < GameConstants.maxPower)
                {
                    Helper.AddCrystals(-Price);
                    Messanger<int, Events>.SendMessage(Events.AddPower, GameConstants.maxPower);
                }
                else PopUpPanel.I.ShowMessage("У вас максимальное количество сил");
                CheckSimilarBoost();
            }
            else
            {
                Helper.AddCrystals(-Price);
                Helper.SetBooster(Key, 1); // буст куплен
                /* Если буст сразу активируется после покупки
                 * PlayerPrefs.SetInt(Key + "A", 1);
                 * PlayerPrefs.SetInt(boost[f] + "A", 1);  // чёрта лысого, а это зачем?
                 */
                //PlayPanel.I.ApplyBoosterManager();
                PlayPanel.I.InitBoosters(true);
                Helper.Recount();
            }  
            Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.CrystalsExploding);
            Messanger<Events>.SendMessage(Events.CrystalUpdate);   
            if (Helper.GetTraining5Passed())
            {
                Helper.TrackTaskInProgress(InGameTasks.SpendCrystals.ToString(), Price);
                Helper.SetAchievement("spend_crystals", Helper.GetAchievement("spend_crystals") + Price);
            }

            SocialManager.I.SetProgressToReport();
        }
        else
        {
            PopUpPanel.I.ShowMessage("Недостаточно кристаллов");
        }
    }

    private void ApplyBoost()
    {
        ApplyBooster(Key, GameConstants.TimeActionPurchasedBooster, gameObject);    
    }
                                                              
    public static void ApplyBoostFromTasks(string key, int timeOfAction)
    {
        if (key == Boosts.x2Power.ToString()) TimerController.ActivateBooster(DateTime.Now.AddHours(1.0), 2);
        Helper.SetBoosterActivated(key, 1);           
        Helper.SetTimeBooster(key, DateTime.Now.AddHours(timeOfAction));
        PlayPanel.I.InitBoosters(true);
        Messanger<Events>.SendMessage(Events.StartBoostTimer);
    }

    /// <summary>
    /// timeOfAction in hours
    /// </summary>
    /// <param name="key"></param>
    /// <param name="timeOfAction"></param>
    public static void ApplyBooster(string key, int timeOfAction, GameObject gameObject)
    {
        if (key != "ReAbilities")
        {
            if (key == "InfinitePower" && PlayerPrefs.GetInt("Power") != GameConstants.maxPower) Messanger<int, Events>.SendMessage(Events.AddPower, GameConstants.maxPower);
            Helper.SetBoosterActivated(key, timeOfAction); 
            Helper.SetBooster(key, Helper.GetBooster(key) - 1); // используем бустер, уменьшаем количество
            Helper.SetTimeBooster(key, DateTime.Now.AddHours(timeOfAction));
            if (key == Boosts.x2Power.ToString())
            {
                Helper.SetBoosterActivated(key, 1);
                Helper.SetTimeBooster(key, DateTime.Now.AddHours(1.0));
                TimerController.ActivateBooster(DateTime.Now.AddHours(1.0), 2);
            }
            Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.UseBooster);
            Messanger<Events>.SendMessage(Events.StartBoostTimer);
            //PlayPanel.I.ApplyBoosterManager();
            PlayPanel.I.InitBoosters(true);
            Messanger<Events>.SendMessage(Events.OnCheckSimilarBoost);
            BoostersStrings boostersStrings = Helper.GetBoostDescription(key);
            gameObject.transform.parent.GetChild(1).GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/boosts_atlas", boostersStrings.sprite[0]);

            SocialManager.I.SetProgressToReport();
        }
    }

    private void SetActivePurchasedBooster(bool purchased)
    {
        gameObject.transform.GetChild(0).gameObject.SetActive(!purchased);         // текст кнопки с ценой
        gameObject.transform.parent.GetChild(2).gameObject.SetActive(!purchased);  // картинка с кристаллом
        gameObject.transform.GetChild(1).gameObject.SetActive(purchased);          // текст кнопки для слова ПРИМЕНИТЬ и отображения обратного отсчёта
        
        if (purchased)
        {                  
            gameObject.GetComponent<Button>().onClick.AddListener(ApplyBoost);
            gameObject.transform.GetChild(1).GetComponent<Text>().text = Helper.GetLocalString("Apply");
        }
        else
        {
            
        }
    }
}
