﻿using System;
using System.IO;
using UnityEngine;

public class UrmobiPNGReadException : Exception
{
	public UrmobiPNGReadException(string message) : base(message) { }
}

internal static class URPNGReader
{
	public struct Selection
	{
		public int num, x, y, w, h, bx, by, bz, bw;
	}
    
	public static Tuple<Selection[], Sprite[]> ReadURPNG(Stream stream)
	{
		using (BinaryReader br = new BinaryReader(stream))
		{
			int count = br.ReadInt32();
			Selection[] selections = new Selection[count];
			for (int i = 0; i < count; i++)
			{
				selections[i] = new Selection
				{
					num = br.ReadInt32(),
					x = br.ReadInt32(),
					y = br.ReadInt32(),
					w = br.ReadInt32(),
					h = br.ReadInt32(),
					bx = br.ReadInt32(),
					by = br.ReadInt32(),
					bz = br.ReadInt32(),
					bw = br.ReadInt32()
				};
				int crc = br.ReadInt32();
			}

			int chunkSize = 32768;
			byte[] data = new byte[chunkSize];


			count = 0;
			int read = 0;


			while ((read = br.Read(data, count, chunkSize)) > 0)
			{
				count += read;
				if (chunkSize + count > data.Length)
				{
					Array.Resize(ref data, data.Length * 2);
				}
			}

			Array.Resize(ref data, count);

            Texture2D mainTex;
            if (SystemInfo.SupportsTextureFormat(TextureFormat.ASTC_RGBA_4x4))
            {
                mainTex = new Texture2D(1, 1, TextureFormat.ASTC_RGBA_4x4, false);
            }
            else if (SystemInfo.SupportsTextureFormat(TextureFormat.ETC2_RGBA8))
            {
                mainTex = new Texture2D(1, 1, TextureFormat.ETC2_RGBA8, false);
            }
            else
            {
                mainTex = new Texture2D(1, 1);
            }
            Debug.Log($"URPNGReader: mainTex.format = {mainTex.format}");
            mainTex.filterMode = FilterMode.Bilinear;
			mainTex.LoadImage(data);
            //mainTex.Compress(true);
            //GC.Collect();
/*            void ChangeTexSize(float ratio)
            {
                TextureScale.Bilinear(mainTex, (int)(ratio * mainTex.width), (int)(ratio * mainTex.height));
                mainTex.Apply();
                //TextureScale.Bilinear(mainTex, )
                for (int i = 0; i < selections.Length; i++)
                {
                    selections[i].x  = (int)(ratio * selections[i].x );
                    selections[i].y  = (int)(ratio * selections[i].y );
                    selections[i].w  = (int)(ratio * selections[i].w );
                    selections[i].h  = (int)(ratio * selections[i].h );
                    selections[i].bx = (int)(ratio * selections[i].bx);
                    selections[i].by = (int)(ratio * selections[i].by);
                    selections[i].bz = (int)(ratio * selections[i].bz);
                    selections[i].bw = (int)(ratio * selections[i].bw);
                }
            }

            float size2of3 = 0.67f;
            float size1of2 = 0.5f;
            float size1of3 = 0.33f;

            if (Screen.width < 1000 && Screen.height < 1000)
            {
                //ChangeTexSize(size1of3);
            }
            else if (Screen.width < 2000 && Screen.height < 2000)
            {
                //ChangeTexSize(size1of2);
            }
            else if (Screen.width < 3000 && Screen.height < 3000)
            {
               // ChangeTexSize(size2of3);
            }
*/
            Sprite[] sprites = new Sprite[count];
			if (selections.Length > 0)
			{
				sprites = new Sprite[selections.Length];
				for (int i = 0; i < selections.Length; i++)
				{
					Selection s = selections[i];

					Vector4 border;
					if (s.bx != 0 || s.by != 0 || s.bz != 0 || s.bw != 0)
					{
						border = new Vector4(s.bx, s.by, s.bz, s.bw);
					}
					else
					{
						border = Vector4.zero;
					}

					sprites[i] = Sprite.Create(mainTex, new Rect(s.x, mainTex.height - s.y - s.h, s.w, s.h), new Vector2(0.5f, 0.5f), 1000.0f, 1, SpriteMeshType.FullRect, border);
				}
			}
			else
			{
				sprites = new Sprite[1];
				sprites[0] = Sprite.Create(mainTex, new Rect(0.0f, 0.0f, mainTex.width, mainTex.height), new Vector2(0.5f, 0.5f), 1000.0f, 1, SpriteMeshType.FullRect);
			}

			return new Tuple<Selection[], Sprite[]>(selections,sprites);
		}
	}

	public static Selection[] ReadSelections(Stream stream)
	{
		using (BinaryReader br = new BinaryReader(stream))
		{
			int count = br.ReadInt32();
			Selection[] selections = new Selection[count];
			for (int i = 0; i < count; i++)
			{
				selections[i] = new Selection
				{
					num = br.ReadInt32(),
					x = br.ReadInt32(),
					y = br.ReadInt32(),
					w = br.ReadInt32(),
					h = br.ReadInt32(),
					bx = br.ReadInt32(),
					by = br.ReadInt32(),
					bz = br.ReadInt32(),
					bw = br.ReadInt32()
				};
				int crc = br.ReadInt32();
			}

			return selections;
		}
	}
}