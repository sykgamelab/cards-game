﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class URPNGLoader
{
    private static Dictionary<string, Dictionary<int, Sprite>> layouts = null;
	private static Dictionary<string, Dictionary<int, URPNGReader.Selection>> selectionLayouts = null;


    public static Sprite LoadURPNG(string layoutName, int idx)
    {
        if (layouts == null) layouts = new Dictionary<string, Dictionary<int, Sprite>>();

        if (!layouts.ContainsKey(layoutName))
        {
            TextAsset bytes = Resources.Load<TextAsset>($"Sprites/{layoutName}");
            MemoryStream ms = new MemoryStream(bytes.bytes);
			Tuple<URPNGReader.Selection[], Sprite[]> tuple = URPNGReader.ReadURPNG(ms);

			Dictionary<int, Sprite> layout = new Dictionary<int, Sprite>();

			for (int i = 0; i < tuple.Item1.Length; i++)
			{
				int num = tuple.Item1[i].num;
				layout.Add(num, tuple.Item2[i]);
			}

            layouts.Add(layoutName, layout);
            if (!layouts.ContainsKey(layoutName)) return null;
        }

        if (!layouts[layoutName].ContainsKey(idx))
        {
            Debug.Log($"index {idx} is not present at layout {layoutName}.");
            return null;
        }

        return layouts[layoutName][idx];
    }

	public static (float w, float h) LoadOriginalSize(string layoutName, int idx)
	{
		if (selectionLayouts == null) selectionLayouts = new Dictionary<string, Dictionary<int, URPNGReader.Selection>>();

		if (!selectionLayouts.ContainsKey(layoutName))
		{
			TextAsset bytes = Resources.Load<TextAsset>($"Sprites/3/{layoutName}");
            /*int a;
            if (bytes == null)
                a = 0; */
			MemoryStream ms = new MemoryStream(bytes.bytes);
			URPNGReader.Selection[] selections = URPNGReader.ReadSelections(ms);

			Dictionary<int, URPNGReader.Selection> layout = new Dictionary<int, URPNGReader.Selection>();

			for (int i = 0; i < selections.Length; i++)
			{
				int num = selections[i].num;
				layout.Add(num, selections[i]);
			}

			selectionLayouts.Add(layoutName, layout);
			if (!selectionLayouts.ContainsKey(layoutName)) return (-1, -1);
		}

		if (!selectionLayouts[layoutName].ContainsKey(idx))
		{
			Debug.Log($"index {idx} is not present at layout {layoutName}.");
			return (-1, -1);
		}

		return (selectionLayouts[layoutName][idx].w, selectionLayouts[layoutName][idx].h);
	}

#if UNITY_EDITOR
    public static void Clear()
    {
        layouts.Clear();
		selectionLayouts.Clear();
    }
#endif
}