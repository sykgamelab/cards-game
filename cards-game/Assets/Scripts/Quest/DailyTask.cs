﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class DailyTask : MonoBehaviour
{
    private Image imageRewardIcon;
    private Text textTask;
    private Text textProgress;
    private Image imageRewardBooster;
    private Button buttonTakeReward;
    private string number;
    public bool IsDailyDone { get; private set; } = false;

    public string Task;
    public RewardType Type;
    public int Progress;

    private void SaveVariables()
    {
        if (number == null) number = gameObject.name.Substring(gameObject.name.Length - 1);
        if (imageRewardIcon == null) imageRewardIcon = TasksPanel.I.GetObject($"DailyReward{number}").GetComponent<Image>();
        if (textTask == null) textTask = TasksPanel.I.GetObject($"DailyTaskText{number}").GetComponent<Text>();
        if (textProgress == null) textProgress = TasksPanel.I.GetObject($"ProgressDailyTask{number}").GetComponent<Text>();
        if (imageRewardBooster == null) imageRewardBooster = TasksPanel.I.GetObject($"DailyReceivedAwardBooster{number}").GetComponent<Image>();
        if (buttonTakeReward == null) buttonTakeReward = TasksPanel.I.GetObject($"DailyTakeReward{number}").GetComponent<Button>();
    }

    public void Init(DailyStrings dailyStrings)
    {
        SaveVariables();
        string name = dailyStrings.Key;
        int current = Helper.GetProgress(name);
        int need = dailyStrings.need[Helper.GetLevel() - 1];

        if (current == need) IsDailyDone = true;
        else IsDailyDone = false;

        if (Helper.GetDailyRewardReceived(name) == -1)
        {
            SetTexts(false, current, need, dailyStrings.description);
            SetImages(false, name);
            SetButton(false, current, need, name);
        }
        else
        {
            SetTexts(true, current, need, dailyStrings.description);
            SetImages(true, name);
            SetButton(true, current, need, name);
        }
    }

    private void SetImages(bool isRewardReceived, string name)
    {
        if (isRewardReceived)
        {
            int index = Helper.GetDailyRewardReceived(name);
            imageRewardBooster.sprite = URPNGLoader.LoadURPNG("3/tasks_atlas", index);
            imageRewardBooster.gameObject.SetActive(true);
            imageRewardIcon.gameObject.SetActive(false);
        }
        else
        {
            imageRewardBooster.gameObject.SetActive(false);
            imageRewardIcon.gameObject.SetActive(true);
        }
    }

    private void SetTexts(bool isRewardReceived, int current = 0, int need = 0, string description = null)
    {
        if (isRewardReceived)
        {
            textTask.gameObject.SetActive(false);
            textProgress.gameObject.SetActive(false);
        }
        else
        {
            textTask.text = description.Replace(" X ", $" {need} ");
            textProgress.text = $"{current}/{need}";
            textTask.gameObject.SetActive(true);
            textProgress.gameObject.SetActive(true);
        }
    }

    private void SetButton(bool isRewardReceived, int current, int need, string name)
    {
        if (isRewardReceived) buttonTakeReward.gameObject.SetActive(false);
        else if (current >= need)
        {
            buttonTakeReward.onClick.RemoveAllListeners();
            buttonTakeReward.onClick.AddListener(() =>
            {
                string[] boosters = Enum.GetNames(typeof(Boosts));
                int randomIndex = UnityEngine.Random.Range(0, boosters.Length);
                string key = boosters[randomIndex];
                BoostersStrings boostersStrings = Helper.GetBoostDescription(key);
                BuyBoost.ApplyBoostFromTasks(key, GameConstants.TimeActionReceivedBooster);
                Helper.SetDailyRewardReceived(name, boostersStrings.tasksSprite);
                buttonTakeReward.gameObject.SetActive(false);
                SetImages(true, name);
                SetTexts(true);
                Helper.DeleteProgress(name);
            });
            buttonTakeReward.gameObject.SetActive(true);
        }
        else buttonTakeReward.gameObject.SetActive(false);
    }

    public RewardType GetRewardType()
    {
        return RewardType.Booster;
    }
}
