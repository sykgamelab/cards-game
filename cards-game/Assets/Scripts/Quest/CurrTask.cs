﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CurrTask : MonoBehaviour
{
    Image RewardIcon;              // Иконка приза в углу
    Button TakeReward;             // Кнопка получение приза
    Text TaskText;                 // Текст задачи 
    Text ProgressText;
    Text CrystalsReceivedAward;    // количество получаемых кристаллов
    Image CrystalsImage;           // изображение кристаллов внутри карты
    Image ReceivedAwardBooster;    // изображение бустера внутри карты 

    private int TaskNumber;
    public string Task { get; private set; }
    public RewardType Type { get; private set; }
    public int Progress { get; private set; }
    private int max;
    public bool IsTaskDone { get; private set; } = false;
   
    private void SetTask(int taskNumber)
    {
        TaskNumber = taskNumber;                                 
        Task = Helper.GetTasksStrings(taskNumber).description.Replace(" X ", $" {Helper.GetTasksStrings(taskNumber).need.ToString()} ");
        Type = GetRewardType();
        Helper.SetPageTask(gameObject.name, taskNumber);  // gameObject.name - номер "плитки" задания (Task1, Task2, Task3)                             
        Helper.SetToProgress(Helper.GetTasksStrings(taskNumber).Key);
        Helper.SetLastTaskNumber(taskNumber);
    }

    public void LoadTask()
    {
        if (!PlayerPrefs.HasKey(gameObject.name)) SetTask(GetNextTask());
        TaskNumber = Helper.GetTaskNumber(gameObject.name);  // gameObject.name - номер "плитки" задания (Task1, Task2, Task3)
        SetTask(TaskNumber);
        Progress = Helper.GetProgress(Helper.GetTasksStrings(TaskNumber).Key);
        TaskText.text = Task;
        max = Helper.GetTasksStrings(TaskNumber).need;
        Task = Helper.GetTasksStrings(TaskNumber).description.Replace(" X ", $" {max} ");
        Type = GetRewardType();
                               
        UpdateStats();
    }

    private void ParametersInProgress(bool inProgress)
    {
        TaskText.gameObject.SetActive(inProgress);
        ProgressText.gameObject.SetActive(inProgress);
        RewardIcon.gameObject.SetActive(inProgress);

        TakeReward.gameObject.SetActive(!inProgress);

        CrystalsReceivedAward.gameObject.SetActive(false);
        CrystalsImage.gameObject.SetActive(false);
        ReceivedAwardBooster.gameObject.SetActive(false);
    }

    /// <summary>
    /// Устанавливает параметры при невыполненной задаче
    /// </summary>
    private void TaskInProgress()
    {
        ParametersInProgress(true);
        if (Type == RewardType.Booster) RewardIcon.sprite = URPNGLoader.LoadURPNG("3/tasks_atlas", 2);  // переключатель
        else RewardIcon.sprite = URPNGLoader.LoadURPNG("3/tasks_atlas", 1);                             // получаемой награды  
    }

    /// <summary>
    /// Устанавливает параметры при выполненной задаче
    /// </summary>
    private void TaskDone()
    {
        ParametersInProgress(false);
        if (Type == RewardType.Booster)
        {
            ReceivedAwardBooster.gameObject.SetActive(true);
            ReceivedAwardBooster.sprite = URPNGLoader.LoadURPNG("3/tasks_atlas", Helper.GetBoostDescription(Helper.GetTasksStrings(TaskNumber).reward).tasksSprite);
            // подогнать нужный спрайт бустера
        }
        else
        {
            CrystalsReceivedAward.gameObject.SetActive(true);
            CrystalsImage.gameObject.SetActive(true);
            CrystalsReceivedAward.text = GetRewardCrystals().ToString();
        }
        TakeReward.onClick.RemoveListener(OnTakeReward); // Кажется это глупо, но нужно удалять слушаеля если он уже был
        TakeReward.onClick.AddListener(OnTakeReward);
    }

    private int GetRewardCrystals()
    {
        string str = Helper.GetTasksStrings(TaskNumber).reward;
        str = str.Replace("crystals", "");
        return Convert.ToInt32(str);
    }

    private void OnTakeReward()
    {
        Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.GetReward);

        if (Type == RewardType.Booster)
        {
            BuyBoost.ApplyBoostFromTasks(Helper.GetTasksStrings(TaskNumber).reward, GameConstants.TimeActionReceivedBooster); // получение награды, установка нового задания
        }
        else
        {
            int crs = GetRewardCrystals();
            Helper.AddCrystals(crs);
            Messanger<Events>.SendMessage(Events.CrystalUpdate);
            Helper.TrackTaskInProgress(InGameTasks.CollectCrystal.ToString(), crs);
        }
        Helper.DeleteProgress(Helper.GetTasksStrings(TaskNumber).Key);  
        SetTask(GetNextTask());                     
        Helper.SetPageTask(gameObject.name, TaskNumber);
        
        Init();              
        IsTaskDone = false;
        TakeReward.onClick.RemoveListener(OnTakeReward);
        //LoadTask();
    }

    public RewardType GetRewardType()
    {
        string str = Helper.GetTasksStrings(TaskNumber).reward;
        if (str.Length >= 8 && str.Substring(0, 8) == "crystals") return RewardType.Crystals; else return RewardType.Booster;
    }

    // что нужно сделать при старте игры 
    //   объявление всех gameObjectов
    public void Init()
    {
        string taskNum = gameObject.name.Substring(gameObject.name.Length - 1);  // последний символ - номер карты текущих заданий 
        RewardIcon = TasksPanel.I.GetObject($"Reward{taskNum}").GetComponent<Image>();
        TaskText = TasksPanel.I.GetObject($"TaskText{taskNum}").GetComponent<Text>();
        ProgressText = TasksPanel.I.GetObject($"ProgressTask{taskNum}").GetComponent<Text>();
        TakeReward = TasksPanel.I.GetObject($"TaskTakeReward{taskNum}").GetComponent<Button>();
        CrystalsReceivedAward = TasksPanel.I.GetObject($"CrystalsReceivedAward{taskNum}").GetComponent<Text>();
        CrystalsImage = TasksPanel.I.GetObject($"CrystalsImage{taskNum}").GetComponent<Image>();
        ReceivedAwardBooster = TasksPanel.I.GetObject($"TaskReceivedAwardBooster{taskNum}").GetComponent<Image>();
        LoadTask();
    }

    private int GetNextTask()
    {
        int tskNumber = Helper.GetLastTaskNumber() + 1 > 29 ? 0 : Helper.GetLastTaskNumber() + 1;   // (0-29)30 - количество заданий
        Helper.SetLastTaskNumber(tskNumber);
        for (int i = 1; i <= 3; i++)
        {
            if (Helper.GetTaskNumber($"Task{i}") == tskNumber)
            {
                Debug.Log(tskNumber);
                tskNumber = GetNextTask();
                Debug.Log(tskNumber);
            }
        }
        return tskNumber; 
    }

    public void UpdateStats()
    {   
        int progress = Progress < max ? Progress : max;
        ProgressText.text = $"{progress}/{max}";
        if (Progress < max)
        {
            TaskInProgress();
        }
        else
        {
            IsTaskDone = true;
            TaskDone();
        }
    }
}
