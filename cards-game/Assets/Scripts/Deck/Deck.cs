﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Deck : MonoBehaviour
{
    public static Deck I;
    public List<CardInfo> deck;
    private List<int> freePosition;
    private int deckPrice;
    public int deckSize;
    private int monsterCount;
    private Dictionary<CardType, int> nums;
    private Subscription<Events>[] eventsSubscriptions;
                                                                                        
    private Dictionary<CardType, float> percent = new Dictionary<CardType, float>();

    public Text DeckCount;
    public GameObject PriceForDealCardCrystal;
    public GameObject PriceForDealCardHealth;
    public GameObject FreePrice;

    private void Awake()
    {
        I = this;
        eventsSubscriptions = new Subscription<Events>[]
        {
            Messanger<Events>.Subscribe(Events.StartGame, OnStartGame),
            Messanger<Events>.Subscribe(Events.FirstGame, OnFirstGame)
        };
    }

    private void OnFirstGame()
    {
        //Debug.Log("Это первая игра!");
        //первая игра не случайная, у всех она одна и та же.
        deckPrice = 3;
        gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
        gameObject.GetComponent<Button>().onClick.AddListener(DealCards);
        if (deck == null) deck = new List<CardInfo>();
        else deck.Clear();
        if (percent == null) percent = new Dictionary<CardType, float>();
        else percent.Clear();
        if (nums == null) nums = new Dictionary<CardType, int>();
        else nums.Clear();
        // генерация деки не случайно, добавляются определенные карты 
        if (PlayerPrefs.GetInt("FirstGame") == 0) { FirstGenerDeck(); deckSize = deck.Count; }
        else if (PlayerPrefs.GetInt("FirstGame") == 1) { SecondGenerDeck(); deckSize = deck.Count; }
        else if (PlayerPrefs.GetInt("FirstGame") == 2) { ThirdGenerDeck(); deckSize = deck.Count; }
        else if (PlayerPrefs.GetInt("FirstGame") == 3) { FourthGenerDeck(); deckSize = deck.Count; }
        else OnStartGame(); 

        if (Helper.GetBoosterActivated("DamageMonsters") > 0)
        {
            List<CardInfo> monsterCard = new List<CardInfo>();
            foreach (CardInfo card in deck) if (card.Type == CardType.Monster) monsterCard.Add(card);
            foreach (CardPosition card in TableController.I.cardOnTable) if (card.IsUsed() && !card.IsPlayed) if (card.Card.Type == CardType.Monster) monsterCard.Add(card.Card);
            foreach (CardInfo m in monsterCard) if (m.Price != 1) m.Price--;
        }

        DealCards();
        RefreshDeckCard();
        Debug.Log($"{this.name}: {PlayerPrefs.GetInt("FirstGame")} replica");
        FirstGame.I.OnFirstGame();
    }

    private void OnDestroy()
    {
        foreach (Subscription<Events> eventsSubscription in eventsSubscriptions) eventsSubscription.Unsubscribe();
    }

    public void OnStartGame()
    {
        Debug.Log("UsuallyDeck");
        deckPrice = 3;
        gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
        gameObject.GetComponent<Button>().onClick.AddListener(DealCards);
        if (deck == null) deck = new List<CardInfo>();
        else deck.Clear(); 
        if (percent == null) percent = new Dictionary<CardType, float>();
        else percent.Clear();
        if (nums == null) nums = new Dictionary<CardType, int>();
        else nums.Clear();
        GenerDeck();
    }

    private void TestDeck()
    {
        deck.Add(CardInfo.Random(CardType.Monster));
        
        deck.Add(CardInfo.Random(CardType.Monster));
        
        deck.Add(CardInfo.Random(CardType.Crystal));
        deck.Add(new CardInfo("AddOnePriceOnField"));
        deck.Add(CardInfo.Random(CardType.Energy));
        deck.Add(CardInfo.Random(CardType.Health));
        deck.Add(CardInfo.Random(CardType.Crystal));
        deck.Add(CardInfo.Random(CardType.Monster));
        deck.Add(CardInfo.Random(CardType.Health));
        deck.Add(CardInfo.Random(CardType.Monster));
        deck.Add(CardInfo.Random(CardType.Energy));
        deck.Add(CardInfo.Random(CardType.Crystal));
        deck.Add(CardInfo.Random(CardType.Health)); deck.Add(CardInfo.Random(CardType.Health));
        deck.Add(CardInfo.Random(CardType.Monster));
        deck.Add(new CardInfo("ReplaceRandomsOnFieldWithCardsFromDeckNumberMonsterCardsOnField"));
        deck.Add(CardInfo.Random(CardType.Monster));
        deck.Add(CardInfo.Random(CardType.Energy));
        deck.Add(CardInfo.Random(CardType.Crystal));
        
        deck.Add(CardInfo.Random(CardType.Monster));
        deck.Add(new CardInfo("DeleteFromFieldAndOneHealth"));
        deck.Add(CardInfo.Random(CardType.Health));          
    }

    private void GenerDeck()
    {
        CalculateDeckSize();
        PersentCardCalculation();
        NumCardCalculation();
        deckPrice = 3;
        //TestDeck();
        monsterCount = nums[CardType.Monster];
        GenerateCards();
        Shuffle();

        if (Helper.GetBoosterActivated("DamageMonsters") > 0)
        {
            List<CardInfo> monsterCard = new List<CardInfo>();
            foreach (CardInfo card in deck) if (card.Type == CardType.Monster) monsterCard.Add(card);
            foreach (CardPosition card in TableController.I.cardOnTable) if (card.IsUsed() && !card.IsPlayed) if (card.Card.Type == CardType.Monster) monsterCard.Add(card.Card);
            foreach (CardInfo m in monsterCard) if (m.Price != 1) m.Price--;
        }

        DealCards();
        RefreshDeckCard();
        TablePanel.I.GetObject("Block").SetActive(false);
    }

    private void CalculateDeckSize()
    {
        switch (Helper.GetLevel())
        {
            case 1: deckSize = UnityEngine.Random.Range(20, 26); break;
            case 2: deckSize = UnityEngine.Random.Range(26, 31); break;
            case 3: deckSize = UnityEngine.Random.Range(31, 41); break;
            case 4: deckSize = UnityEngine.Random.Range(41, 51); break;
            case 5: deckSize = UnityEngine.Random.Range(51, 66); break;
            default: deckSize = 0; break;
        }
    }
    private void PersentCardCalculation()
    {
        float sp = 0;
        foreach(CardType type in Enum.GetValues(typeof(CardType)))
        {
            switch (type)
            {
                case CardType.Crystal: percent.Add(type, UnityEngine.Random.Range(0.1f, 0.14f));  sp += percent[type];  break;
                case CardType.Energy:  percent.Add(type, UnityEngine.Random.Range(0.16f, 0.18f)); sp += percent[type]; break;
                case CardType.Health:  percent.Add(type, UnityEngine.Random.Range(0.2f, 0.3f));  sp += percent[type]; break;
                case CardType.Monster: percent.Add(type, UnityEngine.Random.Range(0.3f, 0.34f));   sp += percent[type]; break;
            }
        }
        percent[CardType.Madness] = percent[CardType.Ability] = (1f - sp) / 2f;
    }

    /* Здесь должен быть комментарий о принципе расчёта */
    private void NumCardCalculation()
    {
        nums = new Dictionary<CardType, int>();
        /* проходим по percent, т.к. percent в порядке приоритета */
        int unlockAbility = 0;
        foreach (string name in Enum.GetNames(typeof(Ability))) if (!Helper.AbilityCardIsLocked(name)) { unlockAbility++; }
       
        if (unlockAbility == 0) percent[(int)CardType.Ability] = 0;
        foreach (CardType type in percent.Keys) nums.Add(type, Convert.ToInt32(percent[type] * deckSize));
        int sum = 0;
        foreach (int num in nums.Values) sum += num;
        if (sum < deckSize) for (int i = 0; i < deckSize - sum; i++) nums[CardType.Monster]++;
        else                for (int i = 0; i < sum - deckSize; i++) nums[CardType.Health]--;
    }

    // it's worked baskoff!!!!
    private void GenerateCards()
    {
        foreach (CardType type in Enum.GetValues(typeof(CardType)))  /*-*/ for (int i = 0; i < nums[type]; i++) /*-*/ deck.Add(CardInfo.Random(type));
    }
      
    /***************************  Функции, использующиеся во время игры  ****************************/
    public void DealCards()
    {
        if (deck.Count != 0)
        {
            Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.DealCards);

            freePosition = new List<int>(TableController.I.OnGetFreePositions());
            if (freePosition.Count != 0)
            {
                CheckBoost();
                freePosition = new List<int>(TableController.I.OnGetFreePositions());
                //------------------- Tasks, Achievements ----------------------------
                if (Helper.GetTraining5Passed())
                {
                    if (PlayerParameters.Health <= deckPrice - PlayerParameters.Crystal && Helper.GetAchievement("dreamer") == 0)
                    {
                        Helper.SetHiddenAchievementDone("dreamer");
                    }
                    if (PlayerParameters.Crystal < deckPrice) Helper.TrackTaskInProgress(InGameTasks.DailyCardsForBravery.ToString(), 1);
                    Helper.TrackTaskInProgress(DailyTasks.SpendCrystalOnDeal.ToString(), deckPrice < PlayerParameters.Crystal ? deckPrice : PlayerParameters.Crystal);
                    if (deckPrice == 0)
                    {
                        Helper.TrackTaskInProgress(InGameTasks.FreeDealCards.ToString(), 1);
                        Helper.TrackTaskInProgress(DailyTasks.UseFreeDeal.ToString(), 1);
                    }
                }
                //-----------------------------------------------------------------------
                PlayerParameters.AddTo(-deckPrice, PlayerParameters.Characteristic.Crystal);

                foreach (int pos in freePosition)
                {
                    if (PlayerParameters.Health != 0)
                    {
                        int lastElement = deck.Count - 1;
                        deck[lastElement].PositionOnTable = pos;
                        if (deck[lastElement].Infected) deck[lastElement].Price++;
                        Messanger<CardInfo, int, Events>.SendMessage(Events.OnSendCard, deck[lastElement], pos);
                        deck.RemoveAt(lastElement);

                        if (deck.Count == 0) break;
                    }
                }
                TableController.I.Infect();
                if (PlayerPrefs.GetInt("FirstGame") == 1) FirstGame.I.SecondGameTrigger();
                else if (PlayerPrefs.GetInt("FirstGame") == 2 && !Helper.GetTraining3Passed()) FirstGame.I.ThirdGameReplica5();
                else if (PlayerPrefs.GetInt("FirstGame") == 3) FirstGame.I.FourthGameTrigger();
                RefreshDeckCard();
                UpdateMonsterCount();
            }
        }
    }

    private void CheckBoost()
    {
        if (Helper.GetBoosterActivated("BraveryDealCard") != 0) PlayerParameters.AddTo(2, PlayerParameters.Characteristic.Health);
        if (Helper.GetBoosterActivated("TwoCrystForDeal") != 0) PlayerParameters.Crystal += 2;

        int InfectedMonsters = 0;
        if (Helper.GetBoosterActivated("InfectedMonsters15") != 0)
        {
            InfectedMonsters += 15;
        }
        if (Helper.GetBoosterActivated("InfectedMonsters10") != 0)
        {
            InfectedMonsters += 10;
        }
        if (Helper.GetBoosterActivated("InfectedMonsters20") != 0)
        {
            InfectedMonsters += 20;
        }
        if (InfectedMonsters != 0)
        {
            int f = UnityEngine.Random.Range(0, 100);
            if (f < InfectedMonsters)
            {
                InfectMonster();
            }
        }
    }

    private void InfectMonster()
    {
        List<CardInfo> monsterCard = new List<CardInfo>();
        foreach (CardInfo card in deck) if (card.Type == CardType.Monster) monsterCard.Add(card);
        foreach (CardPosition card in TableController.I.cardOnTable) if (card.IsUsed() && !card.IsPlayed) if (card.Card.Type == CardType.Monster) monsterCard.Add(card.Card);
        if (monsterCard.Count != 0)
        {
            Helper.TrackTaskInProgress(InGameTasks.InfectedMonsters.ToString(), 1); 
            monsterCard[UnityEngine.Random.Range(0, monsterCard.Count)].Infected = true;
        }
    }

    public void Delete(CardInfo card)
    {
        // Helper.TrackTaskInProgress(InGameTasks.DeleteCardsAbility.ToString(), 1);  неверно
        if (card.Type == CardType.Monster)
        {                                                                                       
            Helper.TrackTaskInProgress(InGameTasks.KillMonster.ToString(), 1);
            if (card.Price == 1)
            {
                Helper.TrackTaskInProgress(InGameTasks.KillMonster1Level.ToString(), 1);
                Helper.TrackTaskInProgress(DailyTasks.MurderMonster1Level.ToString(), 1);
            }
            if (card.Price == 9)
            {
                Helper.TrackTaskInProgress(InGameTasks.KillMonster9Level.ToString(), 1);
                Helper.TrackTaskInProgress(DailyTasks.MurderMonster9Level.ToString(), 1);
            }
        }
        
        deck.Remove(card);
    }

    public void DeleteAt(int f) => Delete(deck[f]); 
    
    public void UpdateMonsterCount()
    {
        monsterCount = 0;
        foreach (CardInfo card in deck) if (card.Type == CardType.Monster) monsterCount++;
        GameObject count = TablePanel.I.GetObject("MonsterCount").gameObject;
        GameObject text = TablePanel.I.GetObject("MonsterText").gameObject;
        count.GetComponent<Text>().text = monsterCount.ToString();
    }

    public void Shuffle()
    {
        List<CardInfo> Shuff = new List<CardInfo>();
        while (deck.Count != 0)
        {
            int k = UnityEngine.Random.Range(0, deck.Count);
            Shuff.Add(deck[k]);
            deck.Remove(deck[k]);
        }
        deck = Shuff;
    }

    public void Interact(bool on)
    {
        Button btnDeck = gameObject.GetComponent<Button>();
        btnDeck.interactable = on;
        if (!on)
        {
            TablePanel.I.GetObject("DECK1").GetComponent<Image>().color = new Color(100f / 255f, 100f / 255f, 100f / 255f);
            TablePanel.I.GetObject("DECK2").GetComponent<Image>().color = new Color(100f / 255f, 100f / 255f, 100f / 255f);
            DeckCount.color = new Color(111f / 255f, 114f / 255f, 95f / 255f);
            TablePanel.I.GetObject("CardText").GetComponent<Text>().color = new Color(111f / 255f, 114f / 255f, 95f / 255f);
            TablePanel.I.GetObject("HealthsForDeal").GetComponent<Image>().color = new Color(100f / 255f, 100f / 255f, 100f / 255f);
            TablePanel.I.GetObject("HealthsForDealText").GetComponent<Text>().color = new Color(111f / 255f, 114f / 255f, 95f / 255f);
            TablePanel.I.GetObject("CrystalsForDeal").GetComponent<Image>().color = new Color(100f / 255f, 100f / 255f, 100f / 255f);
            TablePanel.I.GetObject("CrystalsForDealText").GetComponent<Text>().color = new Color(111f / 255f, 114f / 255f, 95f / 255f);
            TablePanel.I.GetObject("FreePriceForDealCard").GetComponent<Text>().color = new Color(111f / 255f, 114f / 255f, 95f / 255f);

            ColorBlock colorBlock = btnDeck.colors;
            colorBlock.disabledColor = Color.white;
            btnDeck.colors = colorBlock;

            gameObject.GetComponent<Image>().color = new Color(100f / 255f, 100f / 255f, 100f / 255f);
            gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
        }
        else
        {
            TablePanel.I.GetObject("DECK1").GetComponent<Image>().color = Color.white;
            TablePanel.I.GetObject("DECK2").GetComponent<Image>().color = Color.white;
            DeckCount.color = new Color(211f / 255f, 214f / 255f, 195f / 255f);
            TablePanel.I.GetObject("CardText").GetComponent<Text>().color = new Color(211f / 255f, 214f / 255f, 195f / 255f);
            TablePanel.I.GetObject("HealthsForDeal").GetComponent<Image>().color = Color.white;
            TablePanel.I.GetObject("HealthsForDealText").GetComponent<Text>().color = new Color(211f / 255f, 214f / 255f, 195f / 255f);
            TablePanel.I.GetObject("CrystalsForDeal").GetComponent<Image>().color = Color.white;
            TablePanel.I.GetObject("CrystalsForDealText").GetComponent<Text>().color = new Color(211f / 255f, 214f / 255f, 195f / 255f);
            TablePanel.I.GetObject("FreePriceForDealCard").GetComponent<Text>().color = new Color(211f / 255f, 214f / 255f, 195f / 255f);

            ColorBlock colorBlock = btnDeck.colors;
            colorBlock.disabledColor = new Color(200f / 255f, 200f / 255f, 200f / 255f, 128f / 255f);
            btnDeck.colors = colorBlock;

            gameObject.GetComponent<Image>().color = Color.white;
            gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
            gameObject.GetComponent<Button>().onClick.AddListener(DealCards);
        }
        VisibilityClick(on);
    }

    public void VisibilityClick(bool visible)
    {
        Button btnDeck = gameObject.GetComponent<Button>();
        if (visible)
        {
            ColorBlock colorBlock = btnDeck.colors;
            colorBlock.pressedColor = new Color(200f / 255f, 200f / 255f, 200f / 255f, 1f);
            btnDeck.colors = colorBlock;
        }
        else
        {
            ColorBlock colorBlock = btnDeck.colors;
            colorBlock.pressedColor = Color.white;
            btnDeck.colors = colorBlock;
        }
    }

    public void FirstGenerDeck()
    {
        // 9 карт в деке рандомные цены (монстры до 5) и рандомный порядок
        deck.Add(CardInfo.Random(CardType.Energy));
        deck.Add(new CardInfo(CardType.Health, 5));
        deck.Add(new CardInfo(CardType.Madness));
        deck.Add(new CardInfo(CardType.Madness));
        deck.Add(new CardInfo(CardType.Monster, 6));
        deck.Add(new CardInfo(CardType.Monster, 3));
        //CardInfo m1 = CardInfo.Random(CardType.Monster); while (m1.Price > 8) m1 = CardInfo.Random(CardType.Monster); deck.Add(m1);
        //CardInfo m2 = CardInfo.Random(CardType.Monster); while (m2.Price > 9 - m1.Price) m2 = CardInfo.Random(CardType.Monster); deck.Add(m2);
        deck.Add(CardInfo.Random(CardType.Crystal));
        deck.Add(CardInfo.Random(CardType.Crystal));
        // карты обучения определенные с определенными ценами
        deck.Add(new CardInfo(CardType.Health, 6));
        deck.Add(new CardInfo(CardType.Energy, 9));
        deck.Add(new CardInfo(CardType.Madness));
        deck.Add(new CardInfo(CardType.Monster, 6));
        deck.Add(new CardInfo(CardType.Madness));
        deck.Add(new CardInfo(CardType.Madness));
        deck.Add(new CardInfo(CardType.Crystal, 9));
        deck.Add(new CardInfo(CardType.Madness));
    }

    public void SecondGenerDeck()
    {
        // 18 карт в деке (так же рандомные цены и рандомный порядок)
        deck.Add(new CardInfo(CardType.Energy, 6));
        deck.Add(new CardInfo(CardType.Energy, 4));
        deck.Add(new CardInfo(CardType.Madness));
        deck.Add(new CardInfo(CardType.Madness));
        deck.Add(new CardInfo(CardType.Madness));
        deck.Add(new CardInfo(CardType.Madness));
        deck.Add(CardInfo.Random(CardType.Crystal));
        deck.Add(CardInfo.Random(CardType.Crystal));
        deck.Add(CardInfo.Random(CardType.Crystal));
        deck.Add(new CardInfo(CardType.Health, 8));
        deck.Add(new CardInfo(CardType.Health, 6));
        deck.Add(new CardInfo(CardType.Health, 5));
        deck.Add(new CardInfo(CardType.Health, 3));
        deck.Add(new CardInfo(CardType.Monster, 2));
        deck.Add(new CardInfo(CardType.Monster, 3));
        deck.Add(new CardInfo(CardType.Monster, 4));
        deck.Add(new CardInfo(CardType.Monster, 5));
        deck.Add(new CardInfo(CardType.Monster, 6));
        Shuffle();

        // карты обучения
        List<CardInfo> table = new List<CardInfo>();

        table.Add(new CardInfo(CardType.Energy, 5));
        table.Add(CardInfo.Random(CardType.Crystal));
        table.Add(new CardInfo(CardType.Health, 2));
        table.Add(new CardInfo(CardType.Health, 8));
        table.Add(new CardInfo(CardType.Monster, 1));
        table.Add(new CardInfo(CardType.Monster, 5));
        table.Add(new CardInfo(CardType.Monster, 7));
        //добавляем в деку стол в рандомном порядке
        while (table.Count != 0)
        {
            int k = UnityEngine.Random.Range(0, table.Count);
            deck.Add(table[k]);
            table.Remove(table[k]);
        }

    }

    public void ThirdGenerDeck()
    {
        // 18 карт в деке (так же рандомные цены и рандомный порядок)
        deck.Add(new CardInfo(CardType.Crystal, 5));
        deck.Add(new CardInfo(CardType.Monster, 1));
        deck.Add(new CardInfo(CardType.Madness));
        deck.Add(new CardInfo(CardType.Madness));
        deck.Add(new CardInfo(CardType.Crystal, 3));
        deck.Add(new CardInfo(CardType.Health, 5));
        deck.Add(new CardInfo(CardType.Monster, 3));
        deck.Add(new CardInfo(CardType.Energy, 7));
        deck.Add(new CardInfo(CardType.Crystal, 4));
        deck.Add(new CardInfo(CardType.Health, 7));
        deck.Add(new CardInfo(CardType.Monster, 6));
        deck.Add(new CardInfo(CardType.Madness));
        deck.Add(new CardInfo("FreeDeal"));
        deck.Add(new CardInfo(CardType.Health, 6));
        deck.Add(new CardInfo(CardType.Monster, 3));
        deck.Add(new CardInfo(CardType.Energy, 4));
        deck.Add(new CardInfo(CardType.Crystal, 8));
        deck.Add(new CardInfo(CardType.Monster, 5));

        // карты обучения
        List<CardInfo> table = new List<CardInfo>();

        table.Add(new CardInfo(CardType.Energy, 6));
        table.Add(new CardInfo(CardType.Crystal, 4));
        table.Add(new CardInfo(CardType.Health, 8));
        table.Add(new CardInfo(CardType.Health, 4));
        table.Add(new CardInfo(CardType.Monster, 9));
        table.Add(new CardInfo(CardType.Monster, 5));
        table.Add(new CardInfo(CardType.Monster, 3));
        //добавляем в деку стол в рандомном порядке
        while (table.Count != 0)
        {
            int k = UnityEngine.Random.Range(0, table.Count);
            deck.Add(table[k]);
            table.Remove(table[k]);
        }
    }

    public void FourthGenerDeck()
    {
        Debug.Log("FourthGenerDeck");
        // 18 карт в деке (так же рандомные цены и рандомный порядок)
        deck.Add(new CardInfo(CardType.Monster, 7));
        deck.Add(new CardInfo(CardType.Crystal, 6));
        deck.Add(new CardInfo(CardType.Madness));
        deck.Add(new CardInfo(CardType.Madness));
        deck.Add(new CardInfo(CardType.Monster, 7));
        deck.Add(new CardInfo(CardType.Health, 8));
        deck.Add(new CardInfo(CardType.Energy, 5));
        deck.Add(new CardInfo("DamageRandomMonster"));
        deck.Add(new CardInfo(CardType.Monster, 4));
        deck.Add(new CardInfo(CardType.Crystal, 5));
        deck.Add(new CardInfo("DamageRandomMonster"));
        deck.Add(new CardInfo(CardType.Energy, 3));
        deck.Add(new CardInfo(CardType.Monster, 3));
        deck.Add(new CardInfo(CardType.Monster, 2));
        deck.Add(new CardInfo(CardType.Health, 7));
        deck.Add(new CardInfo(CardType.Energy, 3));
        deck.Add(new CardInfo(CardType.Crystal, 8));
        deck.Add(new CardInfo(CardType.Monster, 3));

        // карты обучения
        deck.Add(new CardInfo(CardType.Madness));
        deck.Add(new CardInfo(CardType.Monster, 1));
        deck.Add(new CardInfo(CardType.Health, 6));
        deck.Add(new CardInfo("FreeDeal"));
        deck.Add(new CardInfo(CardType.Monster, 4));
        deck.Add(new CardInfo(CardType.Health, 4));
        deck.Add(new CardInfo(CardType.Energy, 4));
    }

    public void RefreshDeckCard()
    {
        if (deck.Count == 0)
        {
            TablePanel.I.GetObject("DECK1").gameObject.SetActive(false);
            TablePanel.I.GetObject("DECK2").gameObject.SetActive(false);
            TablePanel.I.GetObject("DECK3").gameObject.SetActive(false);
        }
        else
        {
            if (deck.Count == 1)
            {
                TablePanel.I.GetObject("DECK1").gameObject.SetActive(false);
                TablePanel.I.GetObject("DECK2").gameObject.SetActive(false);
                TablePanel.I.GetObject("DECK3").gameObject.SetActive(true);
                gameObject.transform.position = TablePanel.I.GetObject("DECK1").transform.position;
            }
            else if (deck.Count == 2)
            {
                TablePanel.I.GetObject("DECK1").gameObject.SetActive(true);
                TablePanel.I.GetObject("DECK2").gameObject.SetActive(false);
                TablePanel.I.GetObject("DECK3").gameObject.SetActive(true);
                gameObject.transform.position = TablePanel.I.GetObject("DECK2").transform.position;
            }
            else
            {
                TablePanel.I.GetObject("DECK1").gameObject.SetActive(true);
                TablePanel.I.GetObject("DECK2").gameObject.SetActive(true);
                TablePanel.I.GetObject("DECK3").gameObject.SetActive(true);
                gameObject.transform.position = TablePanel.I.GetObject("DECK3").transform.position;
                gameObject.GetComponent<RectTransform>().offsetMax = new Vector2(0, 0);
                gameObject.GetComponent<RectTransform>().offsetMin = new Vector2(0, 0);
            }

            

            DeckCount.text = deck.Count.ToString();

            if (Helper.GetLangCode() == "ru")
            {
                if (deck.Count == 11 || deck.Count == 12 || deck.Count == 13 || deck.Count == 14) TablePanel.I.GetObject("CardText").GetComponent<Text>().text = "КАРТ";
                else if (deck.Count % 10 == 2 || deck.Count % 10 == 3 || deck.Count % 10 == 4)    TablePanel.I.GetObject("CardText").GetComponent<Text>().text = "КАРТЫ";
                else if (deck.Count % 10 == 1) TablePanel.I.GetObject("CardText").GetComponent<Text>().text = "КАРТА";
                else TablePanel.I.GetObject("CardText").GetComponent<Text>().text = "КАРТ";
            }  

            if (deckPrice > 0)
            {
                FreePrice.SetActive(false);
                if (PlayerParameters.Crystal >= deckPrice)
                {
                    PriceForDealCardCrystal.SetActive(true);
                    PriceForDealCardCrystal.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = $"{-deckPrice}";
                    PriceForDealCardCrystal.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = $"{-deckPrice}";
                    PriceForDealCardHealth.SetActive(false);
                }
                else
                {
                    if (PlayerParameters.Crystal > 0)
                    {
                        PriceForDealCardCrystal.SetActive(true);
                        PriceForDealCardCrystal.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = $"-{PlayerParameters.Crystal}";
                        PriceForDealCardCrystal.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = $"-{PlayerParameters.Crystal}";
                    }
                    else PriceForDealCardCrystal.SetActive(false);
                    PriceForDealCardHealth.SetActive(true);
                    PriceForDealCardHealth.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = $"-{deckPrice - PlayerParameters.Crystal}";
                    PriceForDealCardHealth.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = $"-{deckPrice - PlayerParameters.Crystal}";
                }
            }
            else
            {
                PriceForDealCardCrystal.SetActive(false);
                PriceForDealCardHealth.SetActive(false);
                FreePrice.SetActive(true);
            }
            UpdateMonsterCount();
        }
    }

    public void FreeDealCards(bool forever)
    {
        if (forever) deckPrice = 0;
        else
        {
            int tmp = deckPrice;
            deckPrice = 0;
            DealCards();
            deckPrice = tmp;
        }
    }
    public List<CardInfo> ReturnDeck()
    {
        return deck;
    }

    public void AddToDeck(CardInfo card)
    {
        card.PositionOnTable = -1;
        deck.Add(card);
    }

    public void Infect() // теперь в деке заражённые карты не теряют хп
    {
        /*List<CardInfo> del = new List<CardInfo>();
        foreach (CardInfo card in deck)
        {
            if (card.Type != CardType.Ability && card.Type != CardType.Madness && card.Infected )
            {
                card.Price--;
                if (card.Price <= 0) del.Add(card);
            }
        }
        foreach (CardInfo card in del)
        {
            if (card.Type == CardType.Monster) PlayerPrefs.SetInt("kill_monster_infect", PlayerPrefs.GetInt("kill_monster_infect", 0) + 1);
            deck.Remove(card);
        }
        RefreshDeckCard(); UpdateMonsterCount();*/
    }
}
