﻿using UnityEngine;

public class AchievementsEffects : MonoBehaviour
{
    private AudioSource source;
    private bool active;
    private Subscription<string, Events> stringSubscription;
    private Subscription<bool, Events> boolSubscription;

    private void OnPlayAchievementEffect(string clip)
    {
        //Debug.Log($"OnPlayAchievementEffect: IsSoundActive = {Helper.IsSoundActive()}, clip = {clip}");
        if (!Helper.IsSoundActive() || !active) return;

        AudioClip audioClip = Resources.Load<AudioClip>($"Audio/{clip}");
        source.clip = audioClip;
        source.Play();
    }

    private void OnSetSoundActive(bool active) => this.active = active;

    private void Awake()
    {
        stringSubscription = Messanger<string, Events>.Subscribe(Events.PlayAchievementEffect, OnPlayAchievementEffect);
        boolSubscription = Messanger<bool, Events>.Subscribe(Events.SetSoundActive, OnSetSoundActive);
    }

    private void OnDestroy()
    {
        stringSubscription.Unsubscribe();
        boolSubscription.Unsubscribe();
    }

    private void Start()
    {
        source = GetComponent<AudioSource>();
        active = Helper.IsSoundActive();
    }
}