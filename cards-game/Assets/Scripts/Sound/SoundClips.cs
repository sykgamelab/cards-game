﻿public static class SoundClips
{
    public const string Accepting = "accepting";
    public const string CardShowing = "card_showing";
    public const string CardTurning = "card_turning";
    public const string CardsOnTheTable = "cards_on_the_table";
    public const string CardsShuffling = "cards_shuffling";
    public const string CrystalsExploding = "crystals_exploding";
    public const string DoorClosing = "door_closing";
    public const string DoorClosing2 = "door_closing2";
    public const string DoorOpening = "door_opening";
    public const string DoorOpening2 = "door_opening2";
    public const string Growling = "growling";
    public const string Grunting = "grunting";
    public const string LobbyMusic = "lobby_music";
    public const string Selecting = "selecting";
    public const string WinningFanfare = "winning_fanfare";
    public const string OtherGames = "other_games";
    public const string ToggleSettings = "toggle_settings";
    public const string UseBooster = "use_booster";
    public const string BuyCrystals = "buy_crystals";
    public const string PressAbility = "press_ability";
    public const string EditCards = "edit_cards";
    public const string AddCard = "add_card";
    public const string PressHiddenAchievement = "press_hidden_achievement";
    public const string GetReward = "get_reward";
    public const string TakeDaily = "take_daily";
    public const string OpenPlayPanel = "open_play_panel";
    public const string ClosePlayPanel = "close_play_panel";
    public const string PlayGame = "play_game";
    public const string DestroyMonster = "destroy_monster";
    public const string UseAbility = "use_ability";
    public const string CloseCard = "close_card";
    public const string TakeEnergy = "take_energy";
    public const string TakeHealth = "take_health";
    public const string Fail = "fail";
    public const string YesBackLobby = "yes_back_lobby";
    public const string GetAchievement = "get_achievement";
    public const string DealCards = "deal_cards";
    public const string WarningTraining = "warning_training";
    public static readonly string[] GameMusics = new string[]
        { "game_music1", "game_music2", "game_music3", "game_music4", "game_music5", "game_music6" };
}
