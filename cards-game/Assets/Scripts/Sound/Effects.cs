﻿using UnityEngine;

public class Effects : MonoBehaviour
{
    private AudioSource source;
    private bool active;
    private Subscription<string, Events> stringSubscription;
    private Subscription<bool, Events> boolSubscription;

    public static float ClipLength { get; private set; }

    private void OnPlayEffect(string clip)
    {
        ClipLength = 0;

        //Debug.Log($"OnPlayEffect: IsSoundActive = {Helper.IsSoundActive()}, clip = {clip}");
        if (!Helper.IsSoundActive() || !active) return;

        AudioClip audioClip = Resources.Load<AudioClip>($"Audio/{clip}");
        ClipLength = audioClip.length;
        source.clip = audioClip;
        source.Play();
    }

    private void OnSetSoundActive(bool active) => this.active = active;

    private void Awake()
    {
        stringSubscription = Messanger<string, Events>.Subscribe(Events.PlayEffect, OnPlayEffect);
        boolSubscription = Messanger<bool, Events>.Subscribe(Events.SetSoundActive, OnSetSoundActive);
    }

    private void OnDestroy()
    {
        stringSubscription.Unsubscribe();
        boolSubscription.Unsubscribe();
    }

    private void Start()
    {
        source = GetComponent<AudioSource>();
        active = Helper.IsSoundActive();
    }
}