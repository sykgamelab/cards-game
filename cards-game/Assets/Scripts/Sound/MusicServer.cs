﻿using System;
using UnityEngine;

public class MusicServer : MonoBehaviour
{
    AudioSource source;
    bool active, changeVolume = false;
    Subscription<bool, Events> boolSubscription;
    Subscription<string, Events> stringSubscription;
    float volume, needVolume;
    string clip = null;

    private void OnSetMusicActive(bool active)
    {
        this.active = active;

        if (Helper.IsMusicActive() && active) source.Play();
        else source.Stop();
    }

    private void OnPlayMusic(string clip)
    {
        //Debug.Log($"OnPlayMusic: clip = {clip}, MusicActive {Helper.IsMusicActive()}, SoundActive = {Helper.IsSoundActive()}, Volume = {source.volume}");
        if (!Helper.IsMusicActive()) return;

        if (clip == SoundClips.LobbyMusic)
        {
            this.clip = null;
            source.loop = true;
        }
        else
        {
            this.clip = clip;
            source.loop = false;
        }

        AudioClip audioClip = Resources.Load<AudioClip>($"Audio/{clip}");
        source.clip = audioClip;

        source.Play();
    }

    private void OnMuffleVolume() => ChangeVolume(false);
    private void OnNormalVolume() => ChangeVolume(true);

    private void ChangeVolume(bool normal)
    {
        Debug.Log($"ChangeVolume: normal = {normal}");
        if (normal)
        {
            volume = GameConstants.MinMusicVolume;
            needVolume = 1f;
            changeVolume = true;
        }
        else
        {
            volume = 1f;
            needVolume = GameConstants.MinMusicVolume;
            changeVolume = true;
        }
    }

    private void Awake()
    {
        boolSubscription = Messanger<bool, Events>.Subscribe(Events.SetMusicActive, OnSetMusicActive);
        stringSubscription = Messanger<string, Events>.Subscribe(Events.PlayMusic, OnPlayMusic);
    }

    private void OnDestroy()
    {
        boolSubscription.Unsubscribe();
        stringSubscription.Unsubscribe();
    }

    private void Start()
    {
        source = GetComponent<AudioSource>();
        source.clip = Resources.Load<AudioClip>($"Audio/music");
        active = Helper.IsMusicActive();
    }

    private void Update()
    {
        if (changeVolume && volume < needVolume)
        {
            volume += 0.01f;
            source.volume = volume;
            if (volume > needVolume) changeVolume = false;
        }
        else if (changeVolume && volume > needVolume + 0.01)
        {
            volume -= 0.01f;
            source.volume = volume;
            if (volume < needVolume) changeVolume = false;
        }

        // зацикливание музыки в партии, после окончания трека воспроизведение следующего из массива
        if (active && clip != null && !source.isPlaying)
        {
            string music = SoundClips.GameMusics[Helper.GetIndexGameMusics()];
            AudioClip audioClip = Resources.Load<AudioClip>($"Audio/{music}");
            source.clip = audioClip;
            source.Play();
            Helper.SetNextIndexGameMusics();
        }
    }
}