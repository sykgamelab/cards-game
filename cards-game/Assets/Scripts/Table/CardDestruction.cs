﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardDestruction : MonoBehaviour
{
    [SerializeField] SpriteRenderer spriteRenderer;
    public PolygonCollider2D polygonCollider2D;
    public Explodable explodable;
    public Rigidbody2D rigidBody2D;
    private ExplosionForce explosionForce;
    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();  
    }         

    public void Explode()
    {
        gameObject.GetComponent<Explodable>().explode();
        ExplosionForce ef = gameObject.GetComponent<ExplosionForce>();
        ef?.doExplosion(transform.position);  
    } 

    public void InitCardDestruction(Sprite sprite)
    {
        spriteRenderer.sprite = sprite;     
        gameObject.transform.localScale = new Vector3(5.14f, 5.14f, 0f);

        AddPolygonCollider();     

        rigidBody2D = gameObject.GetComponent<Rigidbody2D>();
        rigidBody2D.gravityScale = 0f;
        rigidBody2D.mass = 0.5f;

        //gameObject.transform.position = TablePanel.I.GetObject("CardPosition1").transform.position;
        gameObject.AddComponent<ExplodeOnClick>();
        gameObject.AddComponent<ExplosionForce>();
    }

    private void AddPolygonCollider()
    {
        polygonCollider2D = gameObject.AddComponent<PolygonCollider2D>();
        explodable = gameObject.AddComponent<Explodable>();
        explodable.shatterType = Explodable.ShatterType.Voronoi;
        explodable.extraPoints = 25;
        explodable.sortingLayerName = "GameResultsPanel";
        explodable.fragmentInEditor();
    } 
}
