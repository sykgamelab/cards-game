﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FirstGame : MonoBehaviour // добавляется к обекту SceneController. По умолчанию панель обучения выключена всегда
{
    public static bool intraining = false;
    [HideInInspector]
    public static FirstGame I;
    [HideInInspector]
    public static GameObject BigTrainingWindow, BigTrainingClick;
    [HideInInspector]
    public static Text BigReplica;
    [HideInInspector]
    public static Text BigNumberReplica;
    public static Image Emotion;

    private static void SetNextReplica(Dictionary<int, Replica> Part, int nextReplica)
    {
        BigNumberReplica.text = $"{nextReplica}/{Part.Count}";
        BigReplica.text = Part[nextReplica].replica;
        Emotion.sprite = URPNGLoader.LoadURPNG("3/guide", Convert.ToInt32(Part[nextReplica].emotion));  // не доделано
    }

    void Awake()
    {
        I = this;
    }

    private static void InitObjects()
    {
        BigTrainingWindow = TrainingPanel.I.GetObject("Training");
        BigTrainingClick = TrainingPanel.I.GetObject("TrainingButton");
        BigReplica = TrainingPanel.I.GetObject("Replica").GetComponent<Text>();
        BigNumberReplica = TrainingPanel.I.GetObject("NumberReplica").GetComponent<Text>();
        Emotion = TrainingPanel.I.GetObject("Guide").GetComponent<Image>();
    }

    public void OnFirstGame()
    {
        intraining = true;

        InitObjects();

        BigTrainingWindow.SetActive(true);
        BigTrainingClick.transform.position = new Vector3(BigTrainingClick.transform.position.x, TrainingPanel.I.GetObject("TrainingParent").transform.position.y, 1f);

        if (PlayerPrefs.GetInt("FirstGame") == 0)
        {
            TrainingPanel.I.gameObject.SetActive(true);
            SetNextReplica(Helper.GetTrainingStrings().FirstGame, 1);
            BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
            BigTrainingClick.GetComponent<Button>().onClick.AddListener(Step2);
            TrainingPanel.I.GetObject("ScreenBlock").SetActive(true);
            foreach (CardPosition card in TableController.I.cardOnTable)
            {
                card.GetComponent<Button>().onClick.RemoveAllListeners();
                card.Click = () => Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.WarningTraining);
                card.gameObject.GetComponent<Image>().color = Color.white;
            }
        }
        else if (PlayerPrefs.GetInt("FirstGame") == 1)
        {
            Helper.SetNextIndexGameMusics();
        }
        else if (PlayerPrefs.GetInt("FirstGame") == 2)
        {
            TrainingPanel.I.gameObject.SetActive(true);
            SetNextReplica(Helper.GetTrainingStrings().ThirdGame, 1);
            BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
            BigTrainingClick.GetComponent<Button>().onClick.AddListener(ThirdGameReplica2);
            foreach (CardPosition card in TableController.I.cardOnTable)
            {
                card.GetComponent<Button>().onClick.RemoveAllListeners();
                card.Click = () => Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.WarningTraining);
                card.SetCardActive(true);
                card.VisibilityClick(false);
            }
            foreach (var card in TableController.I.cardOnTable) if (card.Card.Type == CardType.Monster) if (card.Card.Price == 9)
                        if (card.position == 6 || card.position == 7)
                        {
                            CardInfo ci = TableController.I.cardOnTable[4].Card;
                            Debug.Log("Replaced cards");
                            TableController.I.cardOnTable[4].SetCard(card.Card, 4);
                            TableController.I.cardOnTable[card.position].SetCard(ci, card.position);
                        }
        }
        else if (PlayerPrefs.GetInt("FirstGame") == 3)
        {
            // Ничего делать не надо

        }
        else
        {
            BigReplica.text = "Извините, произошла ошибка";
            BigNumberReplica.text = "0/-1";
            BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
            BigTrainingClick.GetComponent<Button>().onClick.AddListener(() => TrainingPanel.I.gameObject.SetActive(false));
        }

        Deck.I.gameObject.GetComponent<Button>().onClick.RemoveListener(Step22);
        Deck.I.gameObject.GetComponent<Image>().color = Color.white;
        Deck.I.Interact(true);
    }

    // начало 5-го обучения
    internal void OnFivethTraining()
    {
        GameResultsPanel.I.GetObject("Restart").GetComponent<Button>().interactable = false;
        GameResultsPanel.I.GetObject("Menu").GetComponent<Button>().onClick.AddListener(FivethTraining1);  // убирается в 11 реплике
        foreach (string openAchievement in Enum.GetNames(typeof(OpenAchievement))) PlayerPrefs.DeleteKey(openAchievement);
    }


    public void FivethTraining1()
    {
        TrainingPanel.I.gameObject.SetActive(true);

        MenuPanel.I.GetObject("Shop").GetComponent<Button>().interactable = true;
        MenuPanel.I.GetObject("Achievements").GetComponent<Button>().interactable = true;
        MenuPanel.I.GetObject("Collection").GetComponent<Button>().interactable = true;
        MenuPanel.I.GetObject("CrystalButton2").GetComponent<Button>().interactable = true;

        intraining = true;
        InitObjects();

        BigTrainingWindow.SetActive(true);

        SetNextReplica(Helper.GetTrainingStrings().TourOfMenu, 1);
        BigTrainingClick.transform.position = new Vector3(BigTrainingClick.transform.position.x, TrainingPanel.I.GetObject("TrainingParent").transform.position.y);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(FivethTraining2);
        TrainingPanel.I.GetObject("ScreenBlock").SetActive(false);
        TrainingPanel.I.GetObject("ScreenBlock2").SetActive(true);
    }

    // Заход в ачивки
    void FivethTraining2()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(FivethTraining2);
        TrainingPanel.I.GetObject("AchievementArrow").GetComponent<Arrow>().StartArrowAnimation(null, 30f);
        SetNextReplica(Helper.GetTrainingStrings().TourOfMenu, 2);
        BigTrainingClick.transform.position = new Vector3(BigTrainingClick.transform.position.x, TrainingPanel.I.GetObject("TrainingButtonUp").transform.position.y);
        TrainingPanel.I.GetObject("ScreenBlock").SetActive(true);
        TrainingPanel.I.GetObject("ScreenBlock2").SetActive(false);
        MenuPanel.I.GetObject("Achievements").transform.SetParent(TrainingPanel.I.GetObject("ScreenBlock").transform, true);
        MenuPanel.I.GetObject("Achievements").GetComponent<Button>().onClick.AddListener(FivethTraining3);
    }

    void FivethTraining3()
    {
        BigTrainingClick.transform.position = new Vector3(BigTrainingClick.transform.position.x, TrainingPanel.I.GetObject("TrainingParent").transform.position.y);
        TrainingPanel.I.GetObject("AchievementArrow").GetComponent<Arrow>().ResetPosition();

        MenuPanel.I.GetObject("Achievements").GetComponent<Button>().onClick.RemoveListener(FivethTraining3);
        MenuPanel.I.GetObject("Achievements").transform.SetParent(MenuPanel.I.GetObject("AchievementParent").transform, true);

        TrainingPanel.I.GetObject("ScreenBlock").SetActive(false);
        TrainingPanel.I.GetObject("ScreenBlock2").SetActive(true);

        SetNextReplica(Helper.GetTrainingStrings().TourOfMenu, 3);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(FivethTraining4);
    }

    void FivethTraining4()
    {
        ProgressPanel.I.GetObject("progressBack").transform.SetParent(TrainingPanel.I.GetObject("ScreenBlock").transform, true);

        TrainingPanel.I.GetObject("BackArrow").GetComponent<Arrow>().StartArrowAnimation(null, 200f, 100f);
        TrainingPanel.I.GetObject("ScreenBlock").SetActive(true);
        TrainingPanel.I.GetObject("ScreenBlock2").SetActive(false);

        SetNextReplica(Helper.GetTrainingStrings().TourOfMenu, 4);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        ProgressPanel.I.GetObject("progressBack").GetComponent<Button>().onClick.AddListener(FivethTraining5);
    }

    void FivethTraining5()
    {
        TrainingPanel.I.GetObject("ScreenBlock").SetActive(false);
        TrainingPanel.I.GetObject("ScreenBlock2").SetActive(true);
        TrainingPanel.I.GetObject("BackArrow").GetComponent<Arrow>().ResetPosition();

        ProgressPanel.I.GetObject("progressBack").transform.SetParent(ProgressPanel.I.GetObject("progressBackParent").transform, true);
        ProgressPanel.I.GetObject("progressBack").GetComponent<Button>().onClick.RemoveListener(FivethTraining5);

        SetNextReplica(Helper.GetTrainingStrings().TourOfMenu, 5);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(FivethTraining6);
    }

    // Заход в коллекции
    void FivethTraining6()
    {
        MenuPanel.I.GetObject("Collection").transform.SetParent(TrainingPanel.I.GetObject("ScreenBlock").transform, true);
        MenuPanel.I.GetObject("Collection").GetComponent<Button>().onClick.AddListener(FivethTraining7);
        BigTrainingClick.transform.position = new Vector3(BigTrainingClick.transform.position.x, TrainingPanel.I.GetObject("TrainingButtonUp").transform.position.y);
        TrainingPanel.I.GetObject("CollectionArrow").GetComponent<Arrow>().StartArrowAnimation(null, 10f, 100f);
        TrainingPanel.I.GetObject("ScreenBlock").SetActive(true);
        TrainingPanel.I.GetObject("ScreenBlock2").SetActive(false);

        SetNextReplica(Helper.GetTrainingStrings().TourOfMenu, 6);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(FivethTraining6);
    }

    // 5-е обучение после захода в коллекции
    void FivethTraining7()
    {
        BigTrainingClick.transform.position = new Vector3(BigTrainingClick.transform.position.x, TrainingPanel.I.GetObject("TrainingParent").transform.position.y);
        MenuPanel.I.GetObject("Collection").transform.SetParent(MenuPanel.I.GetObject("CollectionParent").transform, true);
        MenuPanel.I.GetObject("Collection").GetComponent<Button>().onClick.RemoveListener(FivethTraining7);

        TrainingPanel.I.GetObject("CollectionArrow").GetComponent<Arrow>().ResetPosition();
        TrainingPanel.I.GetObject("ScreenBlock2").SetActive(false);

        TrainingPanel.I.GetObject("ScreenBlock").SetActive(false);
        TrainingPanel.I.GetObject("ScreenBlock2").SetActive(true);

        SetNextReplica(Helper.GetTrainingStrings().TourOfMenu, 7);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(FivethTraining8);
    }

    // Заход в способности
    void FivethTraining8()
    {
        TrainingPanel.I.GetObject("AbilityInCollectionArrow").GetComponent<Arrow>().StartArrowAnimation(null, 200f, 100f);
        CollectionPanel.I.GetObject("Ability").transform.SetParent(TrainingPanel.I.GetObject("ScreenBlock").transform, true);  // выделяем вкладку способностей
        CollectionPanel.I.GetObject("Ability").GetComponent<Button>().onClick.AddListener(FivethTraining9);                    // ставим слушателя на след шаг

        TrainingPanel.I.GetObject("ScreenBlock").SetActive(true);
        TrainingPanel.I.GetObject("ScreenBlock2").SetActive(false);

        SetNextReplica(Helper.GetTrainingStrings().TourOfMenu, 8);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
    }

    void FivethTraining9()
    {
        TrainingPanel.I.GetObject("AbilityInCollectionArrow").GetComponent<Arrow>().ResetPosition();
        CollectionPanel.I.GetObject("Ability").transform.SetParent(CollectionPanel.I.GetObject("AbilityParent").transform, true);
        CollectionPanel.I.GetObject("Ability").GetComponent<Button>().onClick.RemoveListener(FivethTraining9);

        TrainingPanel.I.GetObject("ScreenBlock").SetActive(false);
        TrainingPanel.I.GetObject("ScreenBlock2").SetActive(true);

        SetNextReplica(Helper.GetTrainingStrings().TourOfMenu, 9);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(FivethTraining10);
    }

    void FivethTraining10()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        TrainingPanel.I.GetObject("ArrowToCardInCollection").GetComponent<Arrow>().StartArrowAnimation(() => BigTrainingClick.GetComponent<Button>().onClick.AddListener(FivethTraining11), 200f, 2f);

        SetNextReplica(Helper.GetTrainingStrings().TourOfMenu, 10);

    }

    void FivethTraining11()
    {
        SetNextReplica(Helper.GetTrainingStrings().TourOfMenu, 11);      // 11/11
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(() => { TrainingPanel.I.gameObject.SetActive(false); });

        TrainingPanel.I.GetObject("ScreenBlock2").SetActive(false);
        GameResultsPanel.I.GetObject("Restart").GetComponent<Button>().interactable = true;
        GameResultsPanel.I.GetObject("Menu").GetComponent<Button>().onClick.RemoveListener(FivethTraining1);
        Helper.SetTraining5Passed();
        intraining = false;
    }

    bool WasTrigger = false;
    internal void FourthGameTrigger()
    {
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.Card != null)
            {
                if (card.Card.Name == "DamageRandomMonster")
                {
                    if (WasTrigger == false) // Триггер срабатывает только один раз
                    {
                        Trigger4();
                        WasTrigger = true;
                        break;
                    }
                }
                else
                {
                    if (card.Card.Name == "FreeDeal")
                    {
                        card.Click = () =>
                        {
                            if (TableController.I.OnGetFreePositions().Count == 0 && Deck.I.deck.Count != 0) Trigger3();
                            else card.Play();
                            card.Click = card.Play;
                        };
                    }
                    else
                    {
                        card.Click = card.Play;
                    }
                }
            }
        }
    }

    void Trigger3()//игрок нажал на freeDeal но нет свободного места и дека не пустая
    {
        Debug.Log("FreeDeal");
        TrainingPanel.I.gameObject.SetActive(true);
        TrainingPanel.I.GetObject("ScreenBlock").SetActive(true);
        SetNextReplica(Helper.GetTrainingStrings().FourthGame, 1);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(() => { TrainingPanel.I.gameObject.SetActive(false); TrainingPanel.I.GetObject("ScreenBlock").SetActive(false); });
    }

    void Trigger4()//на стол в первый раз вышла карта ЛучСвета
    {
        TrainingPanel.I.gameObject.SetActive(true);
        SetNextReplica(Helper.GetTrainingStrings().FourthGame, 2);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        Deck.I.Interact(false);
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.Card != null)
            {
                if (card.Card.Name == "DamageRandomMonster")
                {
                    // Если у карты положение на столе в нижней строке, то её надо переместить
                    if (card.position >= 6)
                    {
                        CardInfo ci = new CardInfo(); ci = card.Card;
                        CardPosition cp = TableController.I.cardOnTable[4];
                        card.SetCard(cp.Card, card.position); // берём карту с центра pos = 4
                        cp.SetCard(ci, 4);
                        cp.LongPress = () => { cp.OnTap(); TrainingPanel.I.gameObject.SetActive(false); };
                        cp.SetCardActive(true);
                    }
                    else
                    {
                        card.Click = () => Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.WarningTraining);
                        card.LongPress = () => { card.OnTap(); TrainingPanel.I.gameObject.SetActive(false); };
                        card.SetCardActive(true); //cards.gameObject.GetComponent<Image>().color = Color.white;
                    }
                }
                else
                {
                    card.Click = () => Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.WarningTraining);
                    card.SetCardActive(false); 
                }
            }
        }
        CollectionCard.I.GetObject("Back").GetComponent<Button>().onClick.AddListener(onclick);

    }

    void onclick()
    {
        CollectionCard.I.GetObject("Back").GetComponent<Button>().onClick.RemoveListener(onclick);
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.Card != null)
            {
                //if (cards.Card.Name == "DamageRandomMonster")
                {
                    card.Click = card.Play;
                    card.LongPress = card.OnTap;
                    card.SetCardActive(true); // card.gameObject.GetComponent<Image>().color = Color.white;
                }
            }
        }
        TrainingPanel.I.gameObject.SetActive(true);
        SetNextReplica(Helper.GetTrainingStrings().FourthGame, 3);
        Deck.I.Interact(true);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(() => { TrainingPanel.I.gameObject.SetActive(false); });

    }

    internal void ThirdGameReplica2()
    {
        SetNextReplica(Helper.GetTrainingStrings().ThirdGame, 2);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(ThirdGameReplica2);
        Deck.I.Interact(false);
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.Card != null)
            {
                if (card.Card.Type == CardType.Monster && card.Card.Price == 9)
                {
                    card.Click = () => { card.SetCardActive(true); card.Play(); ThirdGameReplica3(); };
                    card.SetCardActive(true);
                    card.VisibilityClick(true);
                }
                else
                {
                    card.Click = () => Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.WarningTraining);
                    card.SetCardActive(false); 
                }
            }
        }
    }

    internal void ThirdGameReplica3()
    {
        SetNextReplica(Helper.GetTrainingStrings().ThirdGame, 3);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(() => TrainingPanel.I.gameObject.SetActive(false));
        Deck.I.Interact(true);
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.Card != null)
            {
                if (card.Card.Type == CardType.Health)
                {
                    card.Click = () => { card.SetCardActive(true); card.Play(); ThirdGameReplica4(); };
                    card.SetCardActive(true);
                    card.VisibilityClick(true);
                }
                else if (card.Card.Type == CardType.Monster)
                {
                    card.Click = () => Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.WarningTraining);
                    card.SetCardActive(true);
                }
                else
                {
                    card.Click = card.Play;
                    card.SetCardActive(true);
                    card.VisibilityClick(true);
                }
            }
        }
    }

    internal void ThirdGameReplica4()
    {
        foreach (CardPosition cp in TableController.I.cardOnTable) if (cp.Card != null) cp.Click = () => cp.Play();
                                                  
        TrainingPanel.I.gameObject.SetActive(true);
        SetNextReplica(Helper.GetTrainingStrings().ThirdGame, 4);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(() => TrainingPanel.I.gameObject.SetActive(false));
        Deck.I.Interact(true);
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.Card != null)
            {
                card.Click = card.Play;
                card.SetCardActive(true);  
            }
        }
    }
    
    internal void ThirdGameReplica5()
    {
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.Card != null)
            {
                if (card.Card.Name == "FreeDeal" && !card.IsPlayed)
                {
                    //----------------------------------------------------------
                    foreach (CardPosition cards in TableController.I.cardOnTable)
                    {
                        if (cards.Card != null)
                        {
                            if (cards.Card.Name == "FreeDeal")
                            {
                                cards.Click = () => Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.WarningTraining);
                                cards.LongPress = () => Debug.Log("LongPress");
                                cards.SetCardActive(true); //cards.gameObject.GetComponent<Image>().color = Color.white;
                            }
                            else
                            {
                                cards.Click = () => Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.WarningTraining);
                                cards.SetCardActive(false); //cards.gameObject.GetComponent<Image>().color = new Color(100f / 255f, 100f / 255f, 100f / 255f, 1f);
                            }
                        }
                    } 
                    TrainingPanel.I.gameObject.SetActive(true);
                    SetNextReplica(Helper.GetTrainingStrings().ThirdGame, 5);
                    BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
                    BigTrainingClick.GetComponent<Button>().onClick.AddListener(ThirdGameReplica6);
                    Deck.I.Interact(false);
                    break;
                    //----------------------------------------------------------
                }
            }
        }
    }

    internal void ThirdGameReplica6()
    {
        SetNextReplica(Helper.GetTrainingStrings().ThirdGame, 6);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();

        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.Card != null)
            {
                if (card.Card.Name == "FreeDeal")
                {
                    if (card.position >= 6)
                    {
                        CardInfo ci = new CardInfo(); ci = card.Card;
                        CardPosition cp = TableController.I.cardOnTable[4];
                        card.SetCard(cp.Card, card.position); // берём карту с центра pos = 4
                        cp.Delete();
                        cp.SetCard(ci, 4);
                        cp.LongPress = () => { cp.OnTap(); TrainingPanel.I.gameObject.SetActive(false); CollectionCard.I.GetObject("Back").GetComponent<Button>().onClick.AddListener(ThirdGameReplica7); };
                        cp.SetCardActive(true);
                    }
                    else
                    {
                        card.Click = () => Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.WarningTraining);
                        card.LongPress = () => { card.OnTap(); TrainingPanel.I.gameObject.SetActive(false); CollectionCard.I.GetObject("Back").GetComponent<Button>().onClick.AddListener(ThirdGameReplica7); };
                        card.SetCardActive(true);
                    }
                }
                else
                {
                    card.Click = () => Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.WarningTraining);
                    card.SetCardActive(false); 
                }
            }
        }
    }

    internal void ThirdGameReplica7()
    {
        CollectionCard.I.GetObject("Back").GetComponent<Button>().onClick.RemoveListener(ThirdGameReplica7);
        TrainingPanel.I.gameObject.SetActive(true);
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.Card != null)
            {
                card.Click = card.Play;
                card.LongPress = card.OnTap;
                card.SetCardActive(true); 
            }
        }
        Deck.I.gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
        Deck.I.Interact(true);
        SetNextReplica(Helper.GetTrainingStrings().ThirdGame, 7);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(ThirdGameReplica8);
    }

    internal void ThirdGameReplica8()
    {
        SetNextReplica(Helper.GetTrainingStrings().ThirdGame, 8);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(() => TrainingPanel.I.gameObject.SetActive(false));
        Helper.SetTraining3Passed();
    }

    void Trigger1()// игрок нажал на карту храбрости, а храбрости у него 10
    {
        TrainingPanel.I.gameObject.SetActive(true);
        TrainingPanel.I.GetObject("ScreenBlock").SetActive(true);
        SetNextReplica(Helper.GetTrainingStrings().SecondGame, 1);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(() => { TrainingPanel.I.gameObject.SetActive(false); TrainingPanel.I.GetObject("ScreenBlock").SetActive(false); });
    }

    public void SecondGameTrigger()
    {
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.Card != null)
            {
                if (card.Card.Type == CardType.Health)
                {
                    card.Click = () =>
                    {
                        if (PlayerParameters.Health >= 10) Trigger1();
                        else card.Play();
                        Debug.Log("Health");
                        SecondGameTrigger();
                    };
                }
                else if (card.Card.Type == CardType.Monster)
                {
                    card.Click = () =>
                    {
                        if (PlayerParameters.Health <= card.Card.Price || (PlayerParameters.Energy == 0 && (PlayerParameters.Health + 1) <= card.Card.Price)) Trigger2();
                        else card.Play();
                        card.Click = card.Play;
                        Debug.Log("Monster");
                    };
                }
                else
                {
                    card.Click = card.Play;
                }
            }
        }
    }

    void Trigger2()
    {
        TrainingPanel.I.gameObject.SetActive(true);
        TrainingPanel.I.GetObject("ScreenBlock").SetActive(true);
        SetNextReplica(Helper.GetTrainingStrings().SecondGame, 2);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(() => { TrainingPanel.I.gameObject.SetActive(false); TrainingPanel.I.GetObject("ScreenBlock").SetActive(false); });
    }

    // начало 7-го обучения
    public void OnTraining(bool completed)
    {
        intraining = true;
        InitObjects();

        TrainingPanel.I.gameObject.SetActive(true);
        if (ShopPanel.I.gameObject.activeInHierarchy == false) TrainingPanel.I.GetObject("ScreenBlock").SetActive(true);
        else ShopPanel.I.GetObject("BackFromShop").GetComponent<Button>().onClick.AddListener(Training3);   // нужно как-то отписать

        BigTrainingWindow.SetActive(true);

        SetNextReplica(Helper.GetTrainingStrings().Buy13Cards, 1);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        if (completed) BigTrainingClick.GetComponent<Button>().onClick.AddListener(() => Training2(completed));
        else BigTrainingClick.GetComponent<Button>().onClick.AddListener(Training3);

        if (completed && SocialManager.I.Synchronized)
        {
            GameObject screenBlock = TrainingPanel.I.GetObject("ScreenBlock");
            screenBlock.SetActive(true);
            ShopPanel.I.GetObject("BackFromShop").transform.SetParent(screenBlock.transform, true);
        }
    }

    void Training2(bool isCompletedOnTraining = false)
    {
        if (ShopPanel.I.gameObject.activeInHierarchy) ShopPanel.I.GetObject("BackFromShop").GetComponent<Button>().onClick.RemoveListener(Training3);
        if (isCompletedOnTraining) BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Training3);

        SetNextReplica(Helper.GetTrainingStrings().Buy13Cards, 2);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(() => Training2());
        
        //BigTrainingClick.GetComponent<Button>().onClick.AddListener(() => BigTrainingClick.SetActive(false));
        ShopPanel.I.GetObject("BackFromShop").GetComponent<Button>().onClick.AddListener(Training3);
    }

    // 7-е обучение после покупки 13 карт способностей (выход в меню)
    void Training3()
    {
        Helper.SetBooster("ReAbilities", 1);

        ShopPanel.I.GetObject("BackFromShop").GetComponent<Button>().onClick.RemoveListener(Training3);
        if (SocialManager.I.Synchronized) ShopPanel.I.GetObject("BackFromShop").transform.SetParent(ShopPanel.I.transform.GetChild(6), true);

        BigTrainingClick.transform.position = new Vector3(BigTrainingClick.transform.position.x, TrainingPanel.I.GetObject("TrainingButtonUp").transform.position.y);

        MenuPanel.I.GetObject("Collection").transform.SetParent(TrainingPanel.I.GetObject("ScreenBlock").transform, true);
        TrainingPanel.I.GetObject("ScreenBlock").SetActive(true);

        SetNextReplica(Helper.GetTrainingStrings().Buy13Cards, 3);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();

        TrainingPanel.I.GetObject("CollectionArrow").GetComponent<Arrow>().StartArrowAnimation(null, 10f);

        MenuPanel.I.GetObject("Collection").GetComponent<Button>().onClick.AddListener(Training3_5);
    }

    // 7-е обучение после анимации в меню
    void Training3_5()
    {
        TrainingPanel.I.GetObject("CollectionArrow").GetComponent<Arrow>().ResetPosition();
        //TrainingPanel.I.GetObject("AbilityInCollectionArrow").GetComponent<Arrow>().ResetPosition();

        MenuPanel.I.GetObject("Collection").transform.SetParent(MenuPanel.I.GetObject("CollectionParent").transform, true);
        MenuPanel.I.GetObject("Collection").GetComponent<Button>().onClick.RemoveListener(Training3_5);

        CollectionPanel.I.GetObject("CrystalButton").GetComponent<Button>().interactable = false;
        CollectionPanel.I.GetObject("Back").GetComponent<Button>().interactable = false;
        BigTrainingClick.transform.position = new Vector3(BigTrainingClick.transform.position.x, TrainingPanel.I.GetObject("TrainingParent").transform.position.y);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();

        CollectionPanel.I.GetObject("Ability").transform.SetParent(TrainingPanel.I.GetObject("ScreenBlock").transform, true);
        CollectionPanel.I.GetObject("Ability").GetComponent<Button>().onClick.AddListener(Training4);

        TrainingPanel.I.GetObject("AbilityInCollectionArrow").GetComponent<Arrow>().StartArrowAnimation(null, 190f); //SetActive(true);
    }

    // обучение 7 реплика 4
    void Training4()
    {
        TrainingPanel.I.GetObject("AbilityInCollectionArrow").GetComponent<Arrow>().ResetPosition();
        CollectionPanel.I.GetObject("Ability").transform.SetParent(CollectionPanel.I.GetObject("AbilityParent").transform, true);
        CollectionPanel.I.GetObject("Ability").GetComponent<Button>().onClick.RemoveListener(Training4);
        TrainingPanel.I.GetObject("ScreenBlock").SetActive(false);
        TrainingPanel.I.GetObject("ScreenBlock2").SetActive(true);
        TrainingPanel.I.gameObject.SetActive(true);

        
        CollectionPanel.I.GetObject("Button").GetComponent<Button>().interactable = false;

        BigTrainingClick.transform.position = new Vector3(BigTrainingClick.transform.position.x, TrainingPanel.I.GetObject("TrainingButtonUp").transform.position.y);
        SetNextReplica(Helper.GetTrainingStrings().Buy13Cards, 4);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Training5);
    }

    void Training5()
    {
        SetNextReplica(Helper.GetTrainingStrings().Buy13Cards, 5);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Training5);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Training6);
    }

    void Training6()
    {
        SetNextReplica(Helper.GetTrainingStrings().Buy13Cards, 6);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Training6);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Training7);
    }

    void Training7()
    {
        SetNextReplica(Helper.GetTrainingStrings().Buy13Cards, 7);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Training7);
        TrainingPanel.I.GetObject("ScreenBlock").SetActive(true);
        TrainingPanel.I.GetObject("ScreenBlock2").SetActive(false);

        // прокрутка до 13 карты + гид наверх
        CollectionPanel.I.GetObject("Button").transform.SetParent(TrainingPanel.I.GetObject("ScreenBlock").transform, true);
        CollectionPanel.I.GetObject("Button").GetComponent<Button>().interactable = true;
        CollectionPanel.I.GetObject("Button").GetComponent<Button>().onClick.AddListener(Training8);
    }

    void Training8()
    {
        CollectionPanel.I.GetObject("Button").GetComponent<Button>().onClick.RemoveListener(Training8);
        CollectionPanel.I.GetObject("Button").transform.SetParent(CollectionPanel.I.GetObject("ParentButton").transform, true);
        CollectionPanel.I.GetObject("Button").GetComponent<Button>().interactable = false;

        SetNextReplica(Helper.GetTrainingStrings().Buy13Cards, 8);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Training9);
    }

    void Training9()
    {
        SetNextReplica(Helper.GetTrainingStrings().Buy13Cards, 9);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Training9);

        BigTrainingClick.GetComponent<Button>().onClick.AddListener(() =>
        {
            TrainingPanel.I.gameObject.SetActive(false);
            TrainingPanel.I.GetObject("ScreenBlock").SetActive(false);
        });
        CollectionPanel.I.GetObject("Button").GetComponent<Button>().interactable = true;
        CollectionPanel.I.GetObject("Button").GetComponent<Button>().onClick.AddListener(Training10);
    }

    void Training10()
    {
        CollectionPanel.I.GetObject("Button").GetComponent<Button>().onClick.RemoveListener(Training10);

        TrainingPanel.I.gameObject.SetActive(true);

        SetNextReplica(Helper.GetTrainingStrings().Buy13Cards, 10);

        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Training11);
    }


    void Training11()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        SetNumberStateForBooster(true);
        TrainingPanel.I.GetObject("Booster").SetActive(true);
        TrainingPanel.I.GetObject("Booster").GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/boosts_atlas", 4); // взять номер спрайта из хелпера
        TrainingPanel.I.GetObject("BoosterName").GetComponent<Text>().text = Helper.GetBoostDescription(Helper.GetTrainingStrings().Buy13Cards[11].replica).name;
        string booster = Helper.GetTrainingStrings().Buy13Cards[11].replica;
        PlayerPrefs.SetInt(booster, 1);
        // спрайт буста по ключу
        
        BigReplica.text = ""; // буст (Без эмоции)
        Emotion.gameObject.SetActive(false);
        BigNumberReplica.text = "11/12";     //особый случай, где не надо отображать гида и реплику

        //CollectionPanel.I.GetObject("Button").transform.GetChild(0).GetComponent<Text>().text = "Editor (now free)";
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Training12);
        BigTrainingClick.SetActive(true);
    }

    void Training12()
    {
        SetNumberStateForBooster(false);
        Emotion.gameObject.SetActive(true);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();

        PlayerPrefs.SetInt("CompleteTraining", 1);
        TrainingPanel.I.GetObject("Booster").SetActive(false);

        SetNextReplica(Helper.GetTrainingStrings().Buy13Cards, 12);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(() => TrainingPanel.I.gameObject.SetActive(false));

        CollectionPanel.I.GetObject("CrystalButton").GetComponent<Button>().interactable = true;
        CollectionPanel.I.GetObject("Back").GetComponent<Button>().interactable = true;

        intraining = false;
        Helper.SetTraining7Passed();
    }

    void Step2()
    {
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 2);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Step3);
    }

    void Step3()
    {
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 3);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Step4);
    }

    void Step4()
    {
        TrainingPanel.I.GetObject("ScreenBlock").SetActive(false);
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 4);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        Deck.I.Interact(false);
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.Card != null)
            {
                if (card.Card.Type == CardType.Crystal)
                {
                    card.Click = () => { card.Play(); Step5(); }; // Здесь нужна еще анимация кристаллов.
                    card.SetCardActive(true); 
                }
                else
                {
                    card.Click = () => Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.WarningTraining);
                    card.SetCardActive(false); 
                }
            }
        }
    }

    void Step5()
    {
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 5);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Step6);
        // корутина для указателя
    }

    void Step6()
    {
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 6);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        Arrow arrow = TrainingPanel.I.GetObject("Arrow").GetComponent<Arrow>();
        GameObject scrBlck = TrainingPanel.I.GetObject("ScreenBlock2");
        scrBlck.SetActive(true);
        Action action = () =>
        {
            scrBlck.SetActive(false);
            BigTrainingClick.GetComponent<Button>().onClick.AddListener(Step7);
        };                           
        arrow.StartArrowAnimation(action, 190f, 3f);

        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        //Training.GetComponent<Button>().onClick.AddListener(Step7);
    }

    public void Step7()
    {
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 7);

        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Step8);
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.Card != null)
            {
                card.Click = () => Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.WarningTraining);
                if (card.Card.Type == CardType.Madness)
                {
                    card.SetCardActive(true);
                    card.VisibilityClick(false);
                }
                else
                {
                    card.SetCardActive(false); 
                }
            }
        }
    }

    void Step8()
    {
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 8);

        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Step9);
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.Card != null)
            {
                card.Click = () => Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.WarningTraining);
                if (card.Card.Type == CardType.Madness)
                {
                    card.SetCardActive(true); // card.gameObject.GetComponent<Image>().color = Color.white;
                }
                else
                {
                    card.SetCardActive(false); 
                }
            }
        }
    }

    void Step9()
    {
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 9);

        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.Card != null)
            {
                if (card.Card.Type == CardType.Madness)
                {
                    card.Click = () => { card.Play(); Step10(); };
                    card.SetCardActive(true);
                    card.VisibilityClick(true);
                }
                else
                {
                    card.Click = () => Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.WarningTraining);
                    card.SetCardActive(false); 
                }
            }
        }
    }

    void Step10()
    {
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 10);

        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.Card != null)
            {
                if (card.Card.Type == CardType.Madness)
                {
                    card.Click = () => { card.Play(); DeleteAllMadness(); };
                    card.SetCardActive(true); 
                }
                else
                {
                    card.Click = () => Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.WarningTraining);
                    card.SetCardActive(false); 
                }
            }
        }
    }

    void DeleteAllMadness()
    {
        if (TableController.I.OnGetNonFreePositions().Count == 3)
        {
            Step11();
        }
        else
        {
            foreach (CardPosition card in TableController.I.cardOnTable)
            {
                if (card.Card != null)
                {
                    if (card.Card.Type == CardType.Madness)
                    {
                        card.Click = () => { card.Play(); DeleteAllMadness(); };
                        card.SetCardActive(true); 
                    }
                    else
                    {
                        card.Click = () => Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.WarningTraining);
                        card.SetCardActive(false); 
                    }
                }
            }
        }
    }

    void Step11()
    {
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 11);

        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Step12);
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.Card != null)
            {
                card.Click = () => Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.WarningTraining);
                if (card.Card.Type == CardType.Monster)
                {
                    card.SetCardActive(true); 
                    card.VisibilityClick(false);
                }
                else
                {
                    card.SetCardActive(false); 
                }
            }
        }
    }

    void Step12()
    {
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 12);

        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Step13);
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.Card != null)
            {
                card.Click = () => Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.WarningTraining);
                if (card.Card.Type == CardType.Monster)
                {
                    card.SetCardActive(true); 
                }
                else
                {
                    card.SetCardActive(false); 
                }
            }
        }
    }

    void Step13()
    {
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 13);

        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.Card != null)
            {
                if (card.Card.Type == CardType.Monster)
                {
                    card.Click = () => { Step14(); card.Play(); }; // Здесь нужна анимация монстра
                    card.SetCardActive(true);
                    card.VisibilityClick(true);
                }
                else
                {
                    card.Click = () => Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.WarningTraining);
                    card.SetCardActive(false);
                }
            }
        }
    }

    void Step14()
    {
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 14);

        TrainingPanel.I.GetObject("Arrow").GetComponent<Arrow>().StartArrowAnimation(null, 190f, 5f); 

        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Step15);
    }

    void Step15()
    {
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 15);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Step16);
    }


    void Step16()
    {
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 16);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Step17);
    }

    void Step17()
    {
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 17);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        AnimationUp();
    }
    bool animup = false;
    void AnimationUp() => animup = true;

    private void Update()
    {
        if (animup)
        {
            float speed = 2.1f;
            if (BigTrainingClick.transform.position.y <= TrainingPanel.I.GetObject("TrainingButtonUp").transform.position.y)
            {
                BigTrainingClick.transform.position += new Vector3(0, speed * Time.deltaTime);
            }
            else
            {
                animup = false;
                BigTrainingClick.transform.position = new Vector3(BigTrainingClick.transform.position.x, TrainingPanel.I.GetObject("TrainingButtonUp").transform.position.y);
                Step18();
            }
        }
    }

    void Step18()
    {
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 18);

        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.Card != null)
            {
                if (card.Card.Type == CardType.Health)
                {
                    card.Click = () => { Step19(); card.SetCardActive(true); card.Play(); }; // Здесь нужна анимация храбрости
                    card.SetCardActive(true); 
                }
                else
                {
                    card.Click = () => Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.WarningTraining);
                    card.SetCardActive(false); 
                }
            }
        }
    }

    void Step19()
    {
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 19);

        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Step20);
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.Card != null)
            {
                card.Click = () => Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.WarningTraining);
                if (card.Card.Type == CardType.Energy)
                {
                    card.SetCardActive(true);
                    card.VisibilityClick(false);
                }
                else
                {
                    card.SetCardActive(false); 
                }
            }
        }
    }

    void Step20()
    {
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 20);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.Card != null)
            {
                if (card.Card.Type == CardType.Energy)
                {
                    card.Click = () => { card.Play(); Step21(); }; // Здесь нужна анимация энергии
                    card.SetCardActive(true);
                    card.VisibilityClick(true);
                }
                else
                {
                    card.Click = () => Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.WarningTraining);
                    card.SetCardActive(false); 
                }
            }
        }
    }

    void Step21()
    {
        TrainingPanel.I.GetObject("EnergyArrow").GetComponent<Arrow>().StartArrowAnimation(null, 190f, 5f);

        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 21);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Step22);
    }

    void Step22()
    {
        TrainingPanel.I.GetObject("EnergyArrow").GetComponent<Arrow>().ResetPosition();
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 22);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Step23);
    }

    void Step23()
    {
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 23);
        Deck.I.Interact(true);
        Deck.I.VisibilityClick(false);
        Deck.I.gameObject.GetComponent<Button>().onClick.RemoveAllListeners();

        TrainingPanel.I.GetObject("DeckArrow").GetComponent<Arrow>().StartArrowAnimation(null, 35f);

        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Step24);
    }

    void Step24()
    {
        TrainingPanel.I.GetObject("DeckArrow").GetComponent<Arrow>().ResetPosition();
        Deck.I.Interact(true);
        Deck.I.GetComponent<Button>().onClick.AddListener(Step25);
        Deck.I.VisibilityClick(true);
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 24);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        Deck.I.Shuffle();
        Deck.I.GetComponent<Button>().onClick.AddListener(Step25); // Здесь нужна анимация кристаллов
    }

    void Step25()
    {
        TrainingPanel.I.GetObject("ScreenBlock").SetActive(true);
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 25);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Step26);
    }

    void Step26()
    {
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 26);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Step27);
    }

    void Step27()
    {
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 27);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Step28);
    }

    void Step28()
    {
        TrainingPanel.I.GetObject("ScreenBlock").SetActive(false);
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            card.Click = card.Play;
            card.SetCardActive(true); 
        }
        SetNextReplica(Helper.GetTrainingStrings().FirstGame, 28);

        BigTrainingClick.GetComponent<Button>().onClick.AddListener(() =>
        {
            TrainingPanel.I.gameObject.SetActive(false);
            BigTrainingClick.transform.position = new Vector3(BigTrainingClick.transform.position.x, TrainingPanel.I.GetObject("TrainingParent").transform.position.y);
        });

        intraining = false;

    }

    public void OnFirstAchievement()
    {
        TrainingPanel.I.GetObject("ScreenBlock").SetActive(true);

        // обнуление внутриигровых заданий
        foreach (var task in Enum.GetNames(typeof(InGameTasks))) PlayerPrefs.DeleteKey(task);

        PlayerPrefs.SetInt("FirstOpenAchievement", -1);
        TrainingPanel.I.gameObject.SetActive(true);
        intraining = true;
        InitObjects();

        BigTrainingWindow.SetActive(true);
        BigTrainingClick.SetActive(true);
        BigTrainingClick.transform.position = new Vector3(BigTrainingClick.transform.position.x, TrainingPanel.I.GetObject("TrainingParent").transform.position.y);
        SetNextReplica(Helper.GetTrainingStrings().FirstAchivement, 1);

        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(FirstAchiv2);
    }

    void FirstAchiv2()
    {
        GameResultsPanel.I.BackToMenu();
        if (MenuPanel.I.GetObject("PlaceForPlay").transform.childCount == 0) PlayPanel.I.BackToMenu();

        SetNextReplica(Helper.GetTrainingStrings().FirstAchivement, 2);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(FirstAchiv3);
        BigTrainingClick.transform.position = new Vector3(BigTrainingClick.transform.position.x, TrainingPanel.I.GetObject("TrainingButtonUp").transform.position.y);
    }

    void FirstAchiv3()
    {
        MenuPanel.I.GetObject("Achievements").transform.SetParent(TrainingPanel.I.GetObject("ScreenBlock").transform, true);
        TrainingPanel.I.GetObject("ScreenBlock").SetActive(true);
        TrainingPanel.I.GetObject("AchievementArrow").GetComponent<Arrow>().StartArrowAnimation(null, 30f);
        SetNextReplica(Helper.GetTrainingStrings().FirstAchivement, 3);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        MenuPanel.I.GetObject("Achievements").GetComponent<Button>().onClick.AddListener(FirstAchiv35);
        ProgressPanel.I.SetDefaultContentPosition();
    }

    void FirstAchiv35()
    {
        TrainingPanel.I.GetObject("AchievementArrow").GetComponent<Arrow>().ResetPosition();
        ProgressPanel.I.OpenTab(true);
        MenuPanel.I.GetObject("Achievements").GetComponent<Button>().onClick.RemoveListener(FirstAchiv35);
        MenuPanel.I.GetObject("Achievements").transform.SetParent(MenuPanel.I.GetObject("AchievementParent").transform, true);
        BigTrainingClick.transform.position = new Vector3(BigTrainingClick.transform.position.x, TrainingPanel.I.GetObject("TrainingParent").transform.position.y);
        
        Transform openAchContainer = ProgressPanel.I.ScrollAchievementsContent.transform.GetChild(0);
        GameObject rewardBtn;
        Transform rewardParent;
        int indxOpenAch = 0;
        foreach (var openAch in Enum.GetNames(typeof(OpenAchievement)))
        {
            rewardParent = openAchContainer.GetChild(indxOpenAch).GetChild(0).GetChild(0).GetChild(1).GetChild(1).transform;
            rewardBtn = rewardParent.GetChild(0).gameObject;
            if (Helper.GetAchievement(openAch) >= Helper.GetOpenAchievementStrings(openAch).need[0])
            {
                rewardBtn.transform.SetParent(TrainingPanel.I.GetObject("ScreenBlock").transform, true);
                rewardBtn.GetComponent<Button>().onClick.AddListener(() => FirstAchiv4(rewardParent, rewardBtn));
                break;
            }

            indxOpenAch++;            
        }                                                        
    }

    void FirstAchiv4(Transform rewardParent, GameObject reward)
    {
        reward.GetComponent<Button>().onClick.RemoveAllListeners();
        reward.transform.SetParent(rewardParent, true);
        ProgressPanel.I.RefreshOpenAchievementStats();
        
        PlayerPrefs.SetInt("FirstOpenAchievement", 2); // ключ для сейва состояния  
        SetNextReplica(Helper.GetTrainingStrings().FirstAchivement, 4);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(() => { TrainingPanel.I.gameObject.SetActive(false); });

        ProgressPanel.I.GetObject("progressBack").GetComponent<Button>().onClick.AddListener(FirstAchiv5);
    }

    public void FirstAchiv5()
    {
        ProgressPanel.I.GetObject("progressBack").GetComponent<Button>().onClick.RemoveListener(FirstAchiv5);
        TrainingPanel.I.GetObject("ScreenBlock").SetActive(true);
        PlayerPrefs.SetInt("FirstOpenAchievement", -1);
        TrainingPanel.I.gameObject.SetActive(true);
        intraining = true;

        InitObjects();

        BigTrainingWindow.SetActive(true);
        BigTrainingClick.transform.position = new Vector3(BigTrainingClick.transform.position.x, TrainingPanel.I.GetObject("TrainingParent").transform.position.y);

        TrainingPanel.I.GetObject("ScreenBlock").SetActive(true);
        TrainingPanel.I.gameObject.SetActive(true);
        SetNextReplica(Helper.GetTrainingStrings().FirstAchivement, 5);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(FirstAchiv6);
    }

    void FirstAchiv6()
    {
        SetNextReplica(Helper.GetTrainingStrings().FirstAchivement, 6);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(FirstAchiv7);
    }

    void FirstAchiv7()
    {
        BigReplica.text = ""; // буст (без эмоции)
        TrainingPanel.I.GetObject("Booster").SetActive(true);
        TrainingPanel.I.GetObject("Booster").GetComponent<Image>().sprite = URPNGLoader.LoadURPNG("3/boosts_atlas", 1);
        TrainingPanel.I.GetObject("BoosterName").GetComponent<Text>().text = Helper.GetBoostDescription(Helper.GetTrainingStrings().FirstAchivement[7].replica).name;
        string boostName = Helper.GetTrainingStrings().FirstAchivement[7].replica;
        PlayerPrefs.SetInt(boostName, PlayerPrefs.GetInt(boostName) + 1);
        BigNumberReplica.text = "7/10";

        SetNumberStateForBooster(true);

        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(FirstAchiv8);
        Emotion.gameObject.SetActive(false);
    }

    void FirstAchiv8()
    {
        SetNumberStateForBooster(false);
                                        
        Emotion.gameObject.SetActive(true);
        TrainingPanel.I.GetObject("Booster").SetActive(false);

        SetNextReplica(Helper.GetTrainingStrings().FirstAchivement, 8);

        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(FirstAchiv9);

        TasksPanel.I.InitDaily(true);
        TasksPanel.I.GetObject("TakeDaily").SetActive(false);
        MenuPanel.I.GetObject("Tasks").transform.SetParent(TrainingPanel.I.GetObject("ScreenBlock").transform, true);
        MenuPanel.I.GetObject("Tasks").GetComponent<Button>().onClick.RemoveAllListeners();
        MenuPanel.I.GetObject("Tasks").SetActive(true);// здесь нужна анимация свечения кнопки
    }

    void FirstAchiv9()
    {
        SetNextReplica(Helper.GetTrainingStrings().FirstAchivement, 9);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        MenuPanel.I.GetObject("Tasks").GetComponent<Button>().onClick.AddListener(MenuPanel.I.GoToTasks);
        MenuPanel.I.GetObject("Tasks").GetComponent<Button>().onClick.AddListener(FirstAchiv10);
        PlayerPrefs.DeleteKey("TaskNumber");
        PlayerPrefs.DeleteKey("Task1");
        PlayerPrefs.DeleteKey("Task2");
        PlayerPrefs.DeleteKey("Task3");
    }

    void FirstAchiv10()
    {
        MenuPanel.I.GetObject("Tasks").GetComponent<Button>().onClick.RemoveListener(FirstAchiv10);
        MenuPanel.I.GetObject("Tasks").transform.SetParent(MenuPanel.I.GetObject("TasksParent").transform, true);
        MenuPanel.I.GetObject("Shop").GetComponent<Button>().onClick.RemoveListener(OnFirstAchievement);
        MenuPanel.I.GetObject("Reward").SetActive(false);
        TrainingPanel.I.GetObject("ScreenBlock").SetActive(false);

        PlayerPrefs.SetInt("FirstOpenAchievement", 1);
        SetNextReplica(Helper.GetTrainingStrings().FirstAchivement, 10); // 10/10
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(() => TrainingPanel.I.gameObject.SetActive(false));
        GameResultsPanel.I.GetObject("Restart").GetComponent<Button>().interactable = true;
        GameResultsPanel.I.GetObject("Menu").GetComponent<Button>().interactable = true;
        Helper.SetTraining6Passed();
        //TasksPanel.I.GetObject("TakeTask").GetComponent<Button>().onClick.Invoke(); 

        intraining = false;
    }

    public void Part8()
    {
        intraining = true;
        GameResultsPanel.I.GetObject("Restart").GetComponent<Button>().interactable = false;
        OnFirstGame();
        BigTrainingClick.gameObject.SetActive(false);
        GameResultsPanel.I.GetObject("Menu").GetComponent<Button>().onClick.AddListener(Part8Replica1);
    }

    void Part8Replica1()
    {
        GameResultsPanel.I.GetObject("Restart").GetComponent<Button>().interactable = true;
        GameResultsPanel.I.GetObject("Menu").GetComponent<Button>().onClick.RemoveListener(Part8Replica1);
        BigTrainingClick.gameObject.SetActive(true);
        TrainingPanel.I.gameObject.SetActive(true);
        TrainingPanel.I.GetObject("ScreenBlock").SetActive(true);
        BigTrainingClick.SetActive(true);
        SetNextReplica(Helper.GetTrainingStrings().Played25fights, 1);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Part8Replica2);
    }

    void Part8Replica2()
    {                                        
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Part8Replica2);

        SetNextReplica(Helper.GetTrainingStrings().Played25fights, 2);
        BigTrainingClick.transform.position = new Vector3(BigTrainingClick.transform.position.x, TrainingPanel.I.GetObject("TrainingButtonUp").transform.position.y, 1f);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Part8Replica3);
    }

    void Part8Replica3()
    {
        SetNextReplica(Helper.GetTrainingStrings().Played25fights, 3);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Part8Replica3);
        TrainingPanel.I.GetObject("AchievementArrow").GetComponent<Arrow>().StartArrowAnimation(null, 30f);
        ProgressPanel.I.GetObject("progressRaitings").GetComponent<Button>().onClick.Invoke();
        Button btn = MenuPanel.I.GetObject("Achievements").GetComponent<Button>();  
        btn.onClick.AddListener(Part8Replica4);
        btn.transform.SetParent(TrainingPanel.I.GetObject("ScreenBlock").transform);
    }

    void Part8Replica4()
    {
        Button btn = MenuPanel.I.GetObject("Achievements").GetComponent<Button>();
        btn.onClick.RemoveListener(Part8Replica4);
        btn.transform.SetParent(MenuPanel.I.GetObject("AchievementParent").transform);
        BigTrainingClick.transform.position = new Vector3(BigTrainingClick.transform.position.x, TrainingPanel.I.GetObject("TrainingParent").transform.position.y, 1f);

        TrainingPanel.I.GetObject("AchievementArrow").GetComponent<Arrow>().ResetPosition();
        SetNextReplica(Helper.GetTrainingStrings().Played25fights, 4);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Part8Replica3);

        BigTrainingClick.GetComponent<Button>().onClick.AddListener(() => { TrainingPanel.I.GetObject("ScreenBlock").SetActive(false); TrainingPanel.I.gameObject.SetActive(false); });
        //выключить тренировку, поставить ключ завершения обучения  
        Helper.SetTraining8Passed();
        intraining = false;
    }

    public void FirstGameOver1()
    {
        InitObjects();
        TrainingPanel.I.gameObject.SetActive(true);
        BigTrainingWindow.SetActive(true);
        SetNextReplica(Helper.GetTrainingStrings().FirstGameOver, 1);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(FirstGameOver2);
    }

    private void FirstGameOver2()
    {
        SetNextReplica(Helper.GetTrainingStrings().FirstGameOver, 2);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(FirstGameOver2);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(() =>
        {
            TrainingPanel.I.gameObject.SetActive(false);
            GameResultsPanel.I.GetObject("Menu").GetComponent<Button>().interactable = true;
            GameResultsPanel.I.GetObject("Restart").GetComponent<Button>().interactable = true;
        });
        PlayerPrefs.SetInt("FirstGameOver", 1);
    }

    public void GameOverInTraining()
    {
        TrainingPanel.I.gameObject.SetActive(true);
        BigTrainingWindow.SetActive(true);
        SetNextReplica(Helper.GetTrainingStrings().GameOverInTraining, 1);
        TrainingPanel.I.GetObject("NumberReplica").GetComponent<Text>().text = "1/2";
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(() =>
        {
            if (!Helper.GetTraining3Passed()) SetNextReplica(Helper.GetTrainingStrings().GameOverInTraining, 3);
            else SetNextReplica(Helper.GetTrainingStrings().GameOverInTraining, 2);
            TrainingPanel.I.GetObject("NumberReplica").GetComponent<Text>().text = "2/2";
            BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
            BigTrainingClick.GetComponent<Button>().onClick.AddListener(() =>
            {
                TrainingPanel.I.gameObject.SetActive(false);
                GameResultsPanel.I.GetObject("Menu").GetComponent<Button>().interactable = true;
                GameResultsPanel.I.GetObject("Restart").GetComponent<Button>().interactable = true;
            });
        });
    }

    // true - booster number 
    // false - replica number
    void SetNumberStateForBooster(bool toBooster) 
    {
        Transform replTF = BigNumberReplica.gameObject.transform;
        if (toBooster)
        {
            replTF.GetComponent<RectTransform>().pivot = new Vector2(0f, 1f);
            replTF.position -= new Vector3((replTF.position.x - TrainingPanel.I.GetObject("Booster").transform.position.x), 0f, 0f);
        }
        else
        {
            replTF.position = new Vector3(replTF.parent.position.x, replTF.parent.position.y, replTF.parent.position.z);
        }
    }

    private void StartReplica()
    {
        InitObjects();
        TrainingPanel.I.gameObject.SetActive(true);
        BigTrainingWindow.SetActive(true);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
    }

    private void CloseReplica()
    {
        TrainingPanel.I.gameObject.SetActive(false);
        BigTrainingWindow.SetActive(false);
        BigTrainingClick.GetComponent<Button>().onClick.RemoveAllListeners();
    }

    public void RandomSessionReplica()
    {
        StartReplica();
        int randomIndex = UnityEngine.Random.Range(0, 20);
        SetNextReplica(Helper.GetTrainingStrings().RandomSessionReplics, randomIndex);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(CloseReplica);
    }

    public void Win5TimesInARowReplica1()
    {
        StartReplica();
        SetNextReplica(Helper.GetTrainingStrings().Win5TimesInARow, 1);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Win5TimesInARowReplica2);
    }
    private void Win5TimesInARowReplica2()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Win5TimesInARowReplica2);
        SetNextReplica(Helper.GetTrainingStrings().Win5TimesInARow, 2);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Win5TimesInARowReplica3);
    }
    private void Win5TimesInARowReplica3()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Win5TimesInARowReplica3);
        SetNextReplica(Helper.GetTrainingStrings().Win5TimesInARow, 3);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(CloseReplica);
    }

    public void Win6TimesInARowReplica1()
    {
        StartReplica();
        SetNextReplica(Helper.GetTrainingStrings().Win6TimesInARow, 1);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Win6TimesInARowReplica2);
    }
    private void Win6TimesInARowReplica2()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Win6TimesInARowReplica2);
        SetNextReplica(Helper.GetTrainingStrings().Win6TimesInARow, 2);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Win6TimesInARowReplica3);
    }
    private void Win6TimesInARowReplica3()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Win6TimesInARowReplica3);
        SetNextReplica(Helper.GetTrainingStrings().Win6TimesInARow, 3);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(CloseReplica);
    }

    public void Win7TimesInARowReplica1()
    {
        StartReplica();
        SetNextReplica(Helper.GetTrainingStrings().Win7TimesInARow, 1);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Win7TimesInARowReplica2);
    }
    private void Win7TimesInARowReplica2()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Win7TimesInARowReplica2);
        SetNextReplica(Helper.GetTrainingStrings().Win7TimesInARow, 2);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(CloseReplica);
    }

    public void Win8TimesInARowReplica1()
    {
        StartReplica();
        SetNextReplica(Helper.GetTrainingStrings().Win8TimesInARow, 1);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(() => Win8TimesInARowReplica2());
    }
    private void Win8TimesInARowReplica2()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Win8TimesInARowReplica2);
        SetNextReplica(Helper.GetTrainingStrings().Win8TimesInARow, 2);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Win8TimesInARowReplica3);
    }
    private void Win8TimesInARowReplica3()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Win8TimesInARowReplica3);
        SetNextReplica(Helper.GetTrainingStrings().Win8TimesInARow, 3);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Win8TimesInARowReplica4);
    }
    private void Win8TimesInARowReplica4()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Win8TimesInARowReplica4);
        SetNextReplica(Helper.GetTrainingStrings().Win8TimesInARow, 4);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(CloseReplica);
    }

    public void Win9TimesInARowReplica1()
    {
        StartReplica();
        SetNextReplica(Helper.GetTrainingStrings().Win9TimesInARow, 1);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Win9TimesInARowReplica2);
    }
    private void Win9TimesInARowReplica2()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Win9TimesInARowReplica2);
        SetNextReplica(Helper.GetTrainingStrings().Win9TimesInARow, 2);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Win9TimesInARowReplica3);
    }
    private void Win9TimesInARowReplica3()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Win9TimesInARowReplica3);
        SetNextReplica(Helper.GetTrainingStrings().Win9TimesInARow, 3);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(CloseReplica);
    }

    public void Win10TimesInARowReplica1()
    {
        StartReplica();
        SetNextReplica(Helper.GetTrainingStrings().Win10TimesInARow, 1);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Win10TimesInARowReplica2);
    }
    private void Win10TimesInARowReplica2()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Win10TimesInARowReplica2);
        SetNextReplica(Helper.GetTrainingStrings().Win10TimesInARow, 2);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Win10TimesInARowReplica3);
    }
    private void Win10TimesInARowReplica3()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Win10TimesInARowReplica3);
        SetNextReplica(Helper.GetTrainingStrings().Win10TimesInARow, 3);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(CloseReplica);
    }

    public void InGame3DayReplica1()
    {
        StartReplica();
        SetNextReplica(Helper.GetTrainingStrings().InGame3Day, 1);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(InGame3DayReplica2);
    }
    private void InGame3DayReplica2()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(InGame3DayReplica2);
        SetNextReplica(Helper.GetTrainingStrings().InGame3Day, 2);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(InGame4DayReplica1);
    }
    public void InGame4DayReplica1()
    {
        StartReplica();
        SetNextReplica(Helper.GetTrainingStrings().InGame4Day, 1);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(InGame4DayReplica2);
    }
    private void InGame4DayReplica2()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(InGame4DayReplica2);
        SetNextReplica(Helper.GetTrainingStrings().InGame4Day, 2);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(CloseReplica);
    }
    private void InGame5DayReplica1()
    {
        StartReplica();
        SetNextReplica(Helper.GetTrainingStrings().InGame5Day, 1);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(InGame5DayReplica2);
    }
    private void InGame5DayReplica2()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(InGame5DayReplica2);
        SetNextReplica(Helper.GetTrainingStrings().InGame5Day, 2);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(InGame5DayReplica3);
    }
    private void InGame5DayReplica3()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(InGame5DayReplica3);
        SetNextReplica(Helper.GetTrainingStrings().InGame5Day, 3);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(CloseReplica);
    }

    public void InGame7DayReplica1()
    {
        StartReplica();
        SetNextReplica(Helper.GetTrainingStrings().InGame7Day, 1);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(InGame7DayReplica2);
    }
    private void InGame7DayReplica2()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(InGame7DayReplica2);
        SetNextReplica(Helper.GetTrainingStrings().InGame7Day, 2);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(InGame7DayReplica3);
    }
    private void InGame7DayReplica3()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(InGame7DayReplica3);
        SetNextReplica(Helper.GetTrainingStrings().InGame7Day, 3);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(InGame7DayReplica4);
    }
    private void InGame7DayReplica4()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(InGame7DayReplica4);
        SetNextReplica(Helper.GetTrainingStrings().InGame7Day, 4);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(CloseReplica);
    }

    public void InGame10DayReplica1()
    {
        StartReplica();
        SetNextReplica(Helper.GetTrainingStrings().InGame10Day, 1);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(InGame10DayReplica2);
    }
    private void InGame10DayReplica2()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(InGame10DayReplica2);
        SetNextReplica(Helper.GetTrainingStrings().InGame10Day, 2);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(InGame10DayReplica3);
    }
    private void InGame10DayReplica3()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(InGame10DayReplica3);
        SetNextReplica(Helper.GetTrainingStrings().InGame10Day, 3);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(InGame10DayReplica4);
    }
    private void InGame10DayReplica4()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(InGame10DayReplica4);
        SetNextReplica(Helper.GetTrainingStrings().InGame10Day, 4);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(CloseReplica);
    }

    private void InGame15DayReplica1()
    {
        StartReplica();
        SetNextReplica(Helper.GetTrainingStrings().InGame15Day, 1);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(InGame15DayReplica2);
    }
    private void InGame15DayReplica2()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(InGame15DayReplica2);
        SetNextReplica(Helper.GetTrainingStrings().InGame15Day, 2);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(InGame15DayReplica3);
    }
    private void InGame15DayReplica3()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(InGame15DayReplica3);
        SetNextReplica(Helper.GetTrainingStrings().InGame15Day, 3);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(CloseReplica);
    }

    public void InGame20DayReplica1()
    {
        StartReplica();
        SetNextReplica(Helper.GetTrainingStrings().InGame20Day, 1);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(InGame20DayReplica2);
    }
    private void InGame20DayReplica2()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(InGame20DayReplica2);
        SetNextReplica(Helper.GetTrainingStrings().InGame20Day, 2);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(CloseReplica);
    }

    public void InGame25DayReplica1()
    {
        StartReplica();
        SetNextReplica(Helper.GetTrainingStrings().InGame25Day, 1);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(InGame25DayReplica2);
    }
    private void InGame25DayReplica2()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(InGame25DayReplica2);
        SetNextReplica(Helper.GetTrainingStrings().InGame25Day, 2);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(InGame25DayReplica3);
    }
    private void InGame25DayReplica3()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(InGame25DayReplica3);
        SetNextReplica(Helper.GetTrainingStrings().InGame25Day, 3);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(InGame25DayReplica4);
    }
    private void InGame25DayReplica4()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(InGame25DayReplica4);
        SetNextReplica(Helper.GetTrainingStrings().InGame25Day, 4);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(InGame25DayReplica5);
    }
    private void InGame25DayReplica5()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(InGame25DayReplica5);
        SetNextReplica(Helper.GetTrainingStrings().InGame25Day, 5);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(CloseReplica);
    }

    public void InGame30DayReplica1()
    {
        StartReplica();
        SetNextReplica(Helper.GetTrainingStrings().InGame30Day, 1);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(InGame30DayReplica2);
    }
    private void InGame30DayReplica2()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(InGame30DayReplica2);
        SetNextReplica(Helper.GetTrainingStrings().InGame30Day, 2);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(CloseReplica);
    }

    public void Opened18CardsAbilitiesReplica1()
    {
        StartReplica();
        SetNextReplica(Helper.GetTrainingStrings().Opened18CardsAbilities, 1);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Opened18CardsAbilitiesReplica2);
    }
    private void Opened18CardsAbilitiesReplica2()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Opened18CardsAbilitiesReplica2);
        SetNextReplica(Helper.GetTrainingStrings().Opened18CardsAbilities, 2);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Opened18CardsAbilitiesReplica3);
    }
    private void Opened18CardsAbilitiesReplica3()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Opened18CardsAbilitiesReplica3);
        SetNextReplica(Helper.GetTrainingStrings().Opened18CardsAbilities, 3);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Opened18CardsAbilitiesReplica4);
    }
    private void Opened18CardsAbilitiesReplica4()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Opened18CardsAbilitiesReplica4);
        SetNextReplica(Helper.GetTrainingStrings().Opened18CardsAbilities, 4);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Opened18CardsAbilitiesReplica5);
    }
    private void Opened18CardsAbilitiesReplica5()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Opened18CardsAbilitiesReplica5);
        SetNextReplica(Helper.GetTrainingStrings().Opened18CardsAbilities, 5);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(CloseReplica);
    }

    private void Opened25CardsAbilitiesReplica1()
    {
        StartReplica();
        SetNextReplica(Helper.GetTrainingStrings().Opened25CardsAbilities, 1);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Opened25CardsAbilitiesReplica2);
    }
    private void Opened25CardsAbilitiesReplica2()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Opened25CardsAbilitiesReplica2);
        SetNextReplica(Helper.GetTrainingStrings().Opened25CardsAbilities, 2);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Opened25CardsAbilitiesReplica3);
    }
    private void Opened25CardsAbilitiesReplica3()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Opened25CardsAbilitiesReplica3);
        SetNextReplica(Helper.GetTrainingStrings().Opened25CardsAbilities, 3);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Opened25CardsAbilitiesReplica4);
    }
    private void Opened25CardsAbilitiesReplica4()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Opened25CardsAbilitiesReplica4);
        SetNextReplica(Helper.GetTrainingStrings().Opened25CardsAbilities, 4);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Opened25CardsAbilitiesReplica5);
    }
    private void Opened25CardsAbilitiesReplica5()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Opened25CardsAbilitiesReplica5);
        SetNextReplica(Helper.GetTrainingStrings().Opened25CardsAbilities, 5);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(CloseReplica);
    }

    public void Opened35CardsAbilitiesReplica1()
    {
        StartReplica();
        SetNextReplica(Helper.GetTrainingStrings().Opened35CardsAbilities, 1);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Opened35CardsAbilitiesReplica2);
    }
    private void Opened35CardsAbilitiesReplica2()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Opened35CardsAbilitiesReplica2);
        SetNextReplica(Helper.GetTrainingStrings().Opened35CardsAbilities, 2);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Opened35CardsAbilitiesReplica3);
    }
    private void Opened35CardsAbilitiesReplica3()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Opened35CardsAbilitiesReplica3);
        SetNextReplica(Helper.GetTrainingStrings().Opened35CardsAbilities, 3);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Opened35CardsAbilitiesReplica4);
    }
    private void Opened35CardsAbilitiesReplica4()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Opened35CardsAbilitiesReplica4);
        SetNextReplica(Helper.GetTrainingStrings().Opened35CardsAbilities, 4);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Opened35CardsAbilitiesReplica5);
    }
    private void Opened35CardsAbilitiesReplica5()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Opened35CardsAbilitiesReplica5);
        SetNextReplica(Helper.GetTrainingStrings().Opened35CardsAbilities, 5);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(CloseReplica);
    }

    public void Opened51CardsAbilitiesReplica1()
    {
        StartReplica();
        SetNextReplica(Helper.GetTrainingStrings().Opened51CardsAbilities, 1);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Opened51CardsAbilitiesReplica2);
    }
    private void Opened51CardsAbilitiesReplica2()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Opened51CardsAbilitiesReplica2);
        SetNextReplica(Helper.GetTrainingStrings().Opened51CardsAbilities, 2);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Opened51CardsAbilitiesReplica3);
    }
    private void Opened51CardsAbilitiesReplica3()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Opened51CardsAbilitiesReplica3);
        SetNextReplica(Helper.GetTrainingStrings().Opened51CardsAbilities, 3);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Opened51CardsAbilitiesReplica4);
    }
    private void Opened51CardsAbilitiesReplica4()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Opened51CardsAbilitiesReplica4);
        SetNextReplica(Helper.GetTrainingStrings().Opened51CardsAbilities, 4);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Opened51CardsAbilitiesReplica5);
    }
    private void Opened51CardsAbilitiesReplica5()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Opened51CardsAbilitiesReplica5);
        SetNextReplica(Helper.GetTrainingStrings().Opened51CardsAbilities, 5);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(CloseReplica);
    }

    public void Spent1000CrystalsReplica1()
    {
        StartReplica();
        SetNextReplica(Helper.GetTrainingStrings().Spent1000Crystals, 1);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Spent1000CrystalsReplica2);
    }
    private void Spent1000CrystalsReplica2()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Spent1000CrystalsReplica2);
        SetNextReplica(Helper.GetTrainingStrings().Spent1000Crystals, 2);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Spent1000CrystalsReplica3);
    }
    private void Spent1000CrystalsReplica3()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Spent1000CrystalsReplica3);
        SetNextReplica(Helper.GetTrainingStrings().Spent1000Crystals, 3);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Spent1000CrystalsReplica4);
    }
    private void Spent1000CrystalsReplica4()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Spent1000CrystalsReplica4);
        SetNextReplica(Helper.GetTrainingStrings().Spent1000Crystals, 4);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Spent1000CrystalsReplica5);
    }
    private void Spent1000CrystalsReplica5()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Spent1000CrystalsReplica5);
        SetNextReplica(Helper.GetTrainingStrings().Spent1000Crystals, 5);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(CloseReplica);
    }

    public void Spent4000CrystalsReplica1()
    {
        StartReplica();
        SetNextReplica(Helper.GetTrainingStrings().Spent4000Crystals, 1);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Spent4000CrystalsReplica2);
    }
    private void Spent4000CrystalsReplica2()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Spent4000CrystalsReplica2);
        SetNextReplica(Helper.GetTrainingStrings().Spent4000Crystals, 2);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Spent4000CrystalsReplica3);
    }
    private void Spent4000CrystalsReplica3()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Spent4000CrystalsReplica3);
        SetNextReplica(Helper.GetTrainingStrings().Spent4000Crystals, 3);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(CloseReplica);
    }

    public void Spent7000CrystalsReplica1()
    {
        StartReplica();
        SetNextReplica(Helper.GetTrainingStrings().Spent7000Crystals, 1);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Spent7000CrystalsReplica2);
    }
    private void Spent7000CrystalsReplica2()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Spent7000CrystalsReplica2);
        SetNextReplica(Helper.GetTrainingStrings().Spent7000Crystals, 2);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Spent7000CrystalsReplica3);
    }
    private void Spent7000CrystalsReplica3()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Spent7000CrystalsReplica3);
        SetNextReplica(Helper.GetTrainingStrings().Spent7000Crystals, 3);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Spent7000CrystalsReplica4);
    }
    private void Spent7000CrystalsReplica4()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Spent7000CrystalsReplica4);
        SetNextReplica(Helper.GetTrainingStrings().Spent7000Crystals, 4);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(Spent7000CrystalsReplica5);
    }
    private void Spent7000CrystalsReplica5()
    {
        BigTrainingClick.GetComponent<Button>().onClick.RemoveListener(Spent7000CrystalsReplica5);
        SetNextReplica(Helper.GetTrainingStrings().Spent7000Crystals, 5);
        BigTrainingClick.GetComponent<Button>().onClick.AddListener(CloseReplica);
    }

}
