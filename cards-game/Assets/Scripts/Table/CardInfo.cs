﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CardInfo {

    public CardType Type { get; set; }
    public int Price { get; set; }
    public string Name { get; set; }
    public string LocalName { get; private set; }
    public string Description { get; private set; }
    public bool Infected { get; set; }
    public Sprite sprite { get; set; }
    
    public int PositionOnTable { get; set; } = -1;

    private const int maxMonsterHealthCrystalPrice = 9, maxEnergyPrice = 7;
    private const int minEnergyCrystalPrice = 3, minMonsterHealthPrice = 1;

    public int CardPriceEnergy { get; set; } = 1;
    
    public bool SubtractOnePrice()
    {
        Price--;
        if (Price == 0) return true;
        else return false;
    }

    public bool SubtractPrice(int price)
    {
        Price -= price;
        if (Price <= 0) return true;
        else return false;
    }

    public void Play()
    {
        if (Type == CardType.Crystal)
        {
            Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.CrystalsExploding);
        }

        if (Helper.GetTraining5Passed())
        {
            if (PlayerParameters.Energy == 0)
            {
                if (Type == CardType.Monster && PlayerParameters.Health <= Price + CardPriceEnergy && Helper.GetAchievement("on_its_wave") == 0)
                {
                    Helper.SetHiddenAchievementDone("on_its_wave");
                }
                else if (Type == CardType.Crystal && PlayerParameters.Health == 1 && Helper.GetAchievement("on_its_wave") == 0)
                {
                    Helper.SetHiddenAchievementDone("on_its_wave");
                }
            }
        }
        switch (Type)
        {
            case CardType.Monster:
                if (Helper.GetTraining5Passed())
                    if (PlayerParameters.Health == 1 && Price == 1 && Helper.GetAchievement("pacifist") == 0)
                    {
                        Helper.SetHiddenAchievementDone("pacifist");
                    }
                int oldHealth = PlayerParameters.Health;
                PlayerParameters.AddTo(-Price, PlayerParameters.Characteristic.Health);
                if (PlayerParameters.Health > 0)
                {
                    Helper.TrackTaskInProgress(DailyTasks.DamageMonstersBravery.ToString(), oldHealth - PlayerParameters.Health);
                    PlayerParameters.DeadMonsters++;
                }                              
                if (Infected) Helper.TrackTaskInProgress(DailyTasks.KillInfectMonsterBravery.ToString(), 1); 
                break;
            case CardType.Crystal:
                if (Helper.GetTraining5Passed())
                    if (Helper.GetBoosterActivated("OneBraveryCrystals") > 0) PlayerParameters.AddTo(1, PlayerParameters.Characteristic.Health);
                PlayerParameters.Crystal += Price;
                Helper.TrackTaskInProgress(DailyTasks.PlayCrystals.ToString(), 1);
                if (Price == 3) Helper.TrackTaskInProgress(DailyTasks.UseCrystal3Level.ToString(), 1);
                if (Price == 9) Helper.TrackTaskInProgress(DailyTasks.UseCrystal9Level.ToString(), 1);

                break;
            case CardType.Energy:
                if (Helper.GetTraining5Passed())
                    if (Helper.GetBoosterActivated("OneBraveryEnergy") > 0) PlayerParameters.AddTo(1, PlayerParameters.Characteristic.Health);
                PlayerParameters.AddTo(Price, PlayerParameters.Characteristic.Energy);
                PlayerParameters.Energy += CardPriceEnergy;
                Helper.TrackTaskInProgress(InGameTasks.StashEnergy.ToString(), CardPriceEnergy);
                Helper.TrackTaskInProgress(DailyTasks.PlayEnergy.ToString(), 1);

                if (Price == 7) Helper.TrackTaskInProgress(DailyTasks.UseEnergy7Level.ToString(), 1);
                break;
            case CardType.Health:
                if (PlayerParameters.Health == PlayerParameters.MaxHealth && Price == 9)
                {
                    if (Helper.GetTraining5Passed())
                    {
                        if (PlayerParameters.MaxHealth == 10 && Helper.GetAchievement("bravery") == 0)
                        {
                            //Messanger<string, Events>.SendMessage(Events.PlayAchievementEffect, SoundClips.GetAchievement);
                            SocialManager.I.ReportHiddenAchievement("bravery");
                            Helper.SetAchievement("bravery", 1);
                        }
                        else if (PlayerParameters.MaxHealth > 10 && Helper.GetAchievement("megabravery") == 0)
                        {
                            //Messanger<string, Events>.SendMessage(Events.PlayAchievementEffect, SoundClips.GetAchievement);
                            SocialManager.I.ReportHiddenAchievement("megabravery");
                            Helper.SetAchievement("megabravery", 1);
                        }
                    }
                }
                if (Price == 9) Helper.TrackTaskInProgress(DailyTasks.UseBravery9Level.ToString(), 1);
                if (Price == 1) Helper.TrackTaskInProgress(DailyTasks.UseBravery1Level.ToString(), 1);
                PlayerParameters.AddTo(Price, PlayerParameters.Characteristic.Health);
                break;
            case CardType.Ability:
                if (Helper.GetBoosterActivated("OneBraveryAbility") > 0) PlayerParameters.AddTo(1, PlayerParameters.Characteristic.Health);
                Helper.TrackTaskInProgress(InGameTasks.UseAbiltyCards.ToString(), 1);
                break;
            case CardType.Madness:
                Helper.TrackTaskInProgress(InGameTasks.UseMadness.ToString(), 1);
                Helper.TrackTaskInProgress(DailyTasks.PlayMadness.ToString(), 1);
                break;
        }
    }

    public static CardInfo Random() => Random((CardType)UnityEngine.Random.Range(0, Enum.GetNames(typeof(CardType)).Length));
    
    public static CardInfo Random(CardType type)
    {
        switch(type)
        {
            case CardType.Ability: return new CardInfo(RandomAbilityType().ToString());
            case CardType.Madness: return new CardInfo(CardType.Madness); 
            case CardType.Monster:
            case CardType.Health:  return new CardInfo(type, RandWithTables(GameConstants.TableForMonstersAndHealth, minMonsterHealthPrice, maxMonsterHealthCrystalPrice));
            case CardType.Energy:  return new CardInfo(type, RandWithTables(GameConstants.TableForEnergy, minEnergyCrystalPrice, maxEnergyPrice));
            case CardType.Crystal: return new CardInfo(type, RandWithTables(GameConstants.TableForCrystal, minEnergyCrystalPrice, maxMonsterHealthCrystalPrice));
            default: return new CardInfo(CardType.Madness); 
        }
    }
    
    public static int RandWithTables(int[] table, int min, int max)
    {
        int s = UnityEngine.Random.Range(0, 101);

        for (int i = 1; i < table.Length; i++)
            {
                if (s > table[i - 1] && s <= table[i]) { return min + i - 1; }
                else if (s <= table[i]) { return min; }
            }
        return 0;
    }


    public CardInfo() { new CardInfo(CardType.Madness); }
    /* new CardInfo(CardType.Monster, 2) - генерация карт "с ценой"
     * new CardInfo(CardType.Madness) - генерация карт Безумия
     * new CardInfo("DealCard") - генерация карты способностей
     */ 
    public CardInfo(CardType type, int price)
    {
        Type = type;
        Price = price;
        switch (type)
        {
            case CardType.Crystal: LocalName = Helper.GetLocalString("TitleCardCrystal"); break;
            case CardType.Health:  LocalName = Helper.GetLocalString("TitleCardBravery"); break;
            case CardType.Energy:  LocalName = Helper.GetLocalString("TitleCardEnergy"); break;
            case CardType.Monster:
                List<string> monsters = new List<string>();
                foreach (string m in Enum.GetNames(typeof(Monsters))) if (PlayerPrefs.GetInt(m) == 1 && Helper.GetMonstersStrings(m).strength == price) monsters.Add(m);
                if (monsters.Count == 0)
                {
                    Debug.Log($"Ошибка! Разблокированные монстры с ценой {price} не найдены!");
                    Name = "Scorpio";
                    LocalName = "ОШИБКА";
                }
                else
                {
                    Name = monsters[UnityEngine.Random.Range(0, monsters.Count)];
                    LocalName = Helper.GetMonstersStrings(Name).name;
                }
                break;
        }
    }

    public CardInfo(CardType type)
    {
        Name = "Madness";
        LocalName = Helper.GetMadnessDescription().name;
        Description = Helper.GetMadnessDescription().description;
        Type = type;
    }

    public CardInfo(string cardName)
    {
        Type = CardType.Ability;
        Name = cardName;
        Price = -10; // у способностей нет цены
        Description = Helper.GetAbilityDescription(cardName).description;
        LocalName = Helper.GetAbilityDescription(cardName).name;
    }
    
    private static int GetMinPrice(CardType type)
    {
        switch (type)
        {
            case CardType.Crystal:
            case CardType.Energy:
                return minEnergyCrystalPrice;
            case CardType.Monster:
            case CardType.Health:
                return minMonsterHealthPrice;
            case CardType.Madness:
                return 0;
            default: return 0;
        }
    }

    private static int GetMaxPrice(CardType type)
    {
        switch (type)
        {
            case CardType.Crystal:
            case CardType.Health:
            case CardType.Monster:
                return maxMonsterHealthCrystalPrice;
            case CardType.Energy: return maxEnergyPrice;
            default: return 0;
        }
    }

    public static List<string> UnlockAbilityList()
    {
        List<string> unlocked = new List<string>();
        for (int i = 0; i < Enum.GetNames(typeof(Ability)).Length; i++)
        {
            string name = Enum.GetName(typeof(Ability), i);
            if (!Helper.AbilityCardIsLocked(name))
                if (PlayerPrefs.GetInt(name) == 2) unlocked.Add(name); ////////сюда
        }
        return unlocked;
    }

    public static Ability RandomAbilityType()
    {
        List<string> unlockAbility = UnlockAbilityList();
        int rnd = UnityEngine.Random.Range(-1, unlockAbility.Count + 1);
        if (rnd == -1) rnd++; if (rnd == unlockAbility.Count) rnd--;
        Ability ability = (Ability)Enum.Parse(typeof(Ability), unlockAbility[rnd]);
        return ability;
    }
}
