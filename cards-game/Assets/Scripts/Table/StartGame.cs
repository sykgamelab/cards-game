﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartGame : MonoBehaviour
{                                                       

    public void Refresh()
    {                                                                   
        gameObject.GetComponent<Button>().onClick.AddListener(OnStartGame);
    }

    void OnStartGame()
    {
        if (PlayerPrefs.GetInt("Power", GameConstants.maxPower) >= GameConstants.priceForGame)
        {
            TablePanel.I.GetObject("Block").SetActive(true);
            PlayerPrefs.SetInt("GameIsNotOver", 1);
            //PlayerPrefs.SetInt("Power", PlayerPrefs.GetInt("Power", GameConstants.maxPower) - GameConstants.priceForGame);
            Messanger<int, Events>.SendMessage(Events.SubtractPower, GameConstants.priceForGame);
            if (PlayerPrefs.GetInt("FirstGame") != 4) Messanger<Events>.SendMessage(Events.FirstGame);
            else
            {
                if (PlayerPrefs.GetInt("NeedDaily") == 1 && PlayerPrefs.GetInt("FirstOpenAchievement") == 1)
                {
                    TrainingPanel.I.gameObject.SetActive(true);
                    TrainingPanel.I.GetObject("Training").SetActive(true);
                    TrainingPanel.I.GetObject("Replica").GetComponent<Text>().text = Helper.GetTrainingStrings().DailyTaskNotTaken.replica;
                    TrainingPanel.I.GetObject("NumberReplica").GetComponent<Text>().text = "1/1";
                    TrainingPanel.I.GetObject("TrainingButton").GetComponent<Button>().onClick.RemoveAllListeners();
                    TrainingPanel.I.GetObject("TrainingButton").GetComponent<Button>().onClick.AddListener(() =>
                    {
                        TrainingPanel.I.gameObject.SetActive(false);
                    });
                }
                Messanger<Events>.SendMessage(Events.StartGame);
                Messanger<string, Events>.SendMessage(Events.OnChangePlay, "Default"); // восстановление дефолтного Play: следующие партии теперь не халявные
            }
            TablePanel.I.GetObject("Monsters").SetActive(false);
        }
        else gameObject.GetComponent<Button>().interactable = false;
    }
}
