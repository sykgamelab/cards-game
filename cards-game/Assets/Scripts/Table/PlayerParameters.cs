﻿using System;
using UnityEngine;
using UnityEngine.UI;

public static class PlayerParameters
{
    public static int Health { get; set; }
    public static int Crystal { get; set; }
    public static int Energy { get; set; }
    public static int MaxHealth { get; set; }
    public static int MaxEnergy { get; set; }

    public static int DeadMonsters { get; set; }
    public static int AbilityCards { get; set; }
    public static int TransformCard { get; set; }

    public enum Characteristic
    {
        Health,   // изначально были жизни, переименовалось в храбрость
        Energy,
        Crystal
    }

    public static void AddTo(int where, Characteristic type)
    {
        int to = 0;
        if (type == Characteristic.Health)
        {
            int oldHealth = Health;
            Health = (Health + where) < MaxHealth ? Health + where : MaxHealth;

            //-----------------------------------------------------------------
            //if (Health > 0) Helper.TrackTaskInProgress()
            if (where < 0) Helper.TrackTaskInProgress(InGameTasks.UseBravery.ToString(), oldHealth - Health < 0 ? 0 : oldHealth - Health);
        }
        else
        {
            if (type == Characteristic.Crystal)
            {
                if (where < 0) Crystal = (to = Crystal + where) > 0 ? Crystal + where : 0;
                else Crystal += where;
            }
            else
            {
                if (where < 0) Energy = (to = Energy + where) > 0 ? Energy + where : 0;
                else Energy = Energy + where < MaxEnergy ? Energy + where : MaxEnergy;

                if (to < 0) if (Health + to > 0) Helper.TrackTaskInProgress(DailyTasks.SpendBraveryNullEnergy.ToString(), 1);

                if (Helper.GetProgress("FirstEnergyZero") == -1 && Energy == 0) FirstEnergyZero();
            }
        }
        if (to < 0) Health += to;
        if (Health <= 0)
        {
            Health = 0;
            GameResultsPanel.I.GameResult(false);
        }
        Messanger<Events>.SendMessage(Events.UpdateStats);
    }

    public static void FirstEnergyZero()
    {
        TrainingPanel.I.gameObject.SetActive(true);
        TrainingPanel.I.GetObject("Training").SetActive(true);
        TrainingPanel.I.GetObject("Replica").GetComponent<Text>().text = Helper.GetTrainingStrings().FirstEnergyZero.replica;
        TrainingPanel.I.GetObject("NumberReplica").GetComponent<Text>().text = "1/1";
        TrainingPanel.I.GetObject("TrainingButton").GetComponent<Button>().onClick.RemoveAllListeners();
        TrainingPanel.I.GetObject("TrainingButton").GetComponent<Button>().onClick.AddListener(() => TrainingPanel.I.gameObject.SetActive(false));
        Helper.SetProgress("FirstEnergyZero", 1); 
    }
}
