﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TableController : MonoBehaviour {

    public string namelast = "";
    public static TableController I;
    public CardPosition[] cardOnTable;

#if UNITY_EDITOR
    int healthLog;
    int energyLog;
    int crystalLog;

    bool[] cardOnTableLog = new bool[9];
#endif

    private void Update()
    {
#if UNITY_EDITOR
        bool changed = false;
        int j = 0;
        foreach (var card in cardOnTable)
        {
            if (card.Card != null)
            {
                if (!cardOnTableLog[j])
                {
                    changed = true;
                    break;
                }
            }
            else
            {
                if (cardOnTableLog[j])
                {
                    changed = true;
                    break;
                }
            }
            j++;
        }
        if (Deck.I.deck != null)
        {
            if (PlayerParameters.Health != healthLog ||
                PlayerParameters.Energy != energyLog ||
                PlayerParameters.Crystal != crystalLog || changed)
            {
                changed = false;
                healthLog = PlayerParameters.Health;
                energyLog = PlayerParameters.Energy;
                crystalLog = PlayerParameters.Crystal;
                int i = 0;
                string strng = $"<color=green>{healthLog}</color>   <color=magenta>{energyLog}</color>   <color=yellow>{crystalLog}</color>\n\n\t";
                foreach (var card in cardOnTable)
                {
                    if (card.Card != null)
                    {
                        cardOnTableLog[i] = true;
                        switch (card.Card.Type)
                        {
                            //case CardType.Ability: strng += $"<color=cyan>{(Ability)Enum.Parse(typeof(Ability), card.Card.Name)}</color>\t\t"; break;
                            case CardType.Ability: strng += $"<color=cyan>{card.Card.LocalName.Substring(0, card.Card.LocalName.Length > 11 ? 11 : card.Card.LocalName.Length)}{(card.Card.LocalName.Length < 9 ? "   " : "")}</color>\t"; break;
                            case CardType.Madness: strng += $"<color=grey>M</color>\t\t"; break;
                            case CardType.Crystal: strng += $"<color=yellow>{card.Card.Price}</color>\t\t"; break;
                            case CardType.Monster: strng += $"<color=red>{card.Card.Price}</color>\t\t"; break;
                            case CardType.Energy: strng += $"<color=blue>{card.Card.Price}</color>\t\t"; break;
                            case CardType.Health: strng += $"<color=green>{card.Card.Price}</color>\t\t"; break;
                        }
                    }
                    else
                    {
                        strng += "null\t\t";
                        cardOnTableLog[i] = false;
                    }
                    i++;
                    strng += (i % 3) == 0 ? "\n\t" : "";
                }
                strng += $"{Deck.I.deck.Count}";
                Debug.Log(strng);
            }
        }
#endif
    }

    private void Awake()
    {
        I = this;
    }

    void Start () {
        Messanger<Events>.SendMessage(Events.UpdateStats);
    }

    public List<int> OnGetFreePositions() => GetEmptyOrEmployedPositions(false);
    public List<int> OnGetNonFreePositions() => GetEmptyOrEmployedPositions(true);

    private List<int> GetEmptyOrEmployedPositions(bool empty)
    {
        List<int> positions = new List<int>();
        for (int i = 0; i < cardOnTable.Length; i++)
            if (cardOnTable[i].GetComponent<CardPosition>().IsUsed() == empty)
                positions.Add(i);
        return positions;
    }

    public int[] ReturnNeighbors()
    {
        int f = -1;
        foreach (CardPosition card in cardOnTable) if (card.IsPlayed) f = card.position;
        switch (f)
        {
            case 0: return new int[2] { 1, 3 };
            case 1: return new int[3] { 0, 2, 4 };
            case 2: return new int[2] { 1, 5 };
            case 3: return new int[3] { 0, 4, 6 };
            case 4: return new int[4] { 1, 3, 5, 7 };
            case 5: return new int[2] { 2, 4 };
            case 6: return new int[2] { 3, 7 };
            case 7: return new int[2] { 4, 6 };
            default: return null;
        }
    }

    public void Infect()
    {
        foreach (CardPosition card in cardOnTable)
        {
            if (card.IsUsed())
            {
                if (card.Card.Type != CardType.Ability && card.Card.Type != CardType.Madness && card.Card.Infected)
                {
                    card.SetInfect(true);
                    card.Card.Price--;
                    card.CardPrice.text = card.Card.Price.ToString();
                    if (card.Card.Price <= 0)
                    {
                        if (card.Card.Type == CardType.Monster)
                        {
                            if (Helper.GetTraining5Passed())
                            {
                                Helper.SetAchievement("kill_monster_infect", Helper.GetAchievement("kill_monster_infect") + 1); //ачивка   можно перенести в CardPosition
                                Helper.TrackTaskInProgress(InGameTasks.KillInfectMonster.ToString(), 1);
                            }
                        }                       
                        card.SetInfect(false);
                        card.Delete(); // Уничтожается заражением
                    }
                }
            }

        }
    }
}
