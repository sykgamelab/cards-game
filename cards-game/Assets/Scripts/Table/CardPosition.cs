﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class CardPosition : ClickHandler
{
    public int position;
    public CardInfo Card { get; set; }
    [HideInInspector] public Text TextOnCard;
    [HideInInspector] public Text CardPrice;
    [HideInInspector] public Image ImageCard;
    private Button btnCard;
    public bool IsPlayed { get; private set; } = false;
    public Action Play;
    private Dictionary<string, Action> OverridedPlay;
    private bool firstUpdate = true;

    private void Awake()
    {
        OverridedPlay = new Dictionary<string, Action>() {
            { "NextAddEnergyHisPrice",  () => {
                                                   Card.CardPriceEnergy = -Card.Price;
                                                   Messanger<string, Events>.SendMessage(Events.OnChangePlay, "Default");
                                                   Play();
            } },
            { "NextCrystalsToTimesMore", () => {   if (Card.Type == CardType.Crystal)
                                                   {
                                                       Helper.TrackTaskInProgress(DailyTasks.AddCrystalsAbility.ToString(), Card.Price);
                                                       PlayerParameters.AddTo(Card.Price, PlayerParameters.Characteristic.Crystal);
                                                       Messanger<string, Events>.SendMessage(Events.OnChangePlay, "Default");
                                                       Play();
                                                   }
                                                   else DefaultPlay();
            } },
            { "NextMonsterEatCrystals", () =>
                {
                    if (Card.Type == CardType.Monster)
                    {
                        PlayerParameters.AddTo(-Card.Price, PlayerParameters.Characteristic.Crystal);
                        Card.Price = 0;
                        Messanger<string, Events>.SendMessage(Events.OnChangePlay, "Default");
                        Play();
                    }
                    else
                    {
                        DefaultPlay();
                    }
                }
            },
            { "NextMonsterTwoTimesWeaker", () =>
            {
                if (Card.Type == CardType.Monster)
                {
                    if (Helper.GetTraining5Passed())
                        if (Card.Price == 1 && Helper.GetAchievement("monstrophobia") == 0)
                        {
                            Helper.SetHiddenAchievementDone("monstrophobia");
                        }
                    Card.Price = Card.Price / 2;
                    Messanger<string, Events>.SendMessage(Events.OnChangePlay, "Default");
                    Play();
                }
                else DefaultPlay();
            }},
            { "FreeCards", () => {                Card.CardPriceEnergy = 0;
                                                   DefaultPlay();
            }},
            { "Default", DefaultPlay }
        };
        Messanger<Events>.Subscribe(Events.StartGame, Delete);
        Messanger<Events>.Subscribe(Events.FirstGame, Delete);
        Messanger<CardInfo, int, Events>.Subscribe(Events.OnSendCard, SetCard);
        Messanger<string, Events>.Subscribe(Events.OnChangePlay, OnChangePlay);
        Play = OverridedPlay["Default"];
    }

    private void Start()
    {
        TextOnCard.gameObject.AddComponent<Outline>();
        CardPrice.gameObject.AddComponent<Outline>();
        Shadow s1 = TextOnCard.gameObject.AddComponent<Shadow>();
        Shadow s2 = CardPrice.gameObject.AddComponent<Shadow>();
        s1.effectDistance = s2.effectDistance = new Vector2(3, -3);
        gameObject.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
        Click = Play;
        LongPress = OnTap;  

        gameObject.transform.parent.gameObject.AddComponent<CardDistribution>(); 
    }

    public void Init()
    {
        if (firstUpdate)
        {
            string str = gameObject.name;
            position = Convert.ToInt32(str.Substring(str.Length - 1)) - 1;
            ImageCard = gameObject.GetComponent<Image>();
            TextOnCard = TablePanel.I.GetObject($"TextOnCard{position + 1}").GetComponent<Text>();
            TextOnCard.raycastTarget = false;
            CardPrice = TablePanel.I.GetObject($"Price{position + 1}").GetComponent<Text>();
            btnCard = gameObject.GetComponent<Button>();

            ColorBlock colorBlock = btnCard.colors;
            colorBlock.disabledColor = Color.white;
            btnCard.colors = colorBlock;
            firstUpdate = false;
        }
    }

    private void OnChangePlay(string name)
    {
        Play = OverridedPlay[name];
        Click = Play;
    }

    public void SetInfect(bool infected)
    {
        if (Card != null)
        {
            if (Card.Type == CardType.Madness || Card.Type == CardType.Ability) return;

            if (infected)
            {
                gameObject.transform.parent.GetChild(1).gameObject.SetActive(true);
            }
            else
            {
                gameObject.transform.parent.GetChild(1).gameObject.SetActive(false);
            }
            Card.Infected = infected;
        }
    }

    /// <summary>
    /// Испуользуется, когда карта уничтожается игроком или картой способности
    /// </summary>
    public void DestroyCard()
    {
        if (Card.Type == CardType.Ability || Card.Type == CardType.Madness)
        {
            Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.UseAbility);
        }

        if (Card.Type == CardType.Energy)
        {
            Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.TakeEnergy);
        }

        if (Card.Type == CardType.Health)
        {
            Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.TakeHealth);
        }

        if (Card.Type != CardType.Ability && Card.Type != CardType.Madness)
        {
            ScoreBoard.I.StartPosAnimation = gameObject.transform.position;
            StartCoroutine(ScoreBoard.I.FlyParametrCoroutine(Card.Type));
        }
        
        if (Card.Type == CardType.Monster)
        {
            if (PlayerParameters.Health > 0)
            {
                int countCardsUsed = 0;
                foreach(CardPosition cardPosition in TableController.I.cardOnTable)
                {
                    if (cardPosition.position == position) continue;
                    if (!cardPosition.gameObject.GetComponent<Image>().IsActive()) countCardsUsed++;
                }
                if (countCardsUsed < 7)
                {
                    Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.DestroyMonster);
                }
                    
            }
            else if (!Helper.GameResultSended)
            {
                Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.Fail);
                Helper.GameResultSended = false;
            }

            Helper.TrackTaskInProgress(InGameTasks.KillMonster.ToString(), 1);
            if (Card.Price == 1)
            {
                Helper.TrackTaskInProgress(InGameTasks.KillMonster1Level.ToString(), 1);
                Helper.TrackTaskInProgress(DailyTasks.MurderMonster1Level.ToString(), 1);
            }
            if (Card.Price == 9)
            {
                Helper.TrackTaskInProgress(InGameTasks.KillMonster9Level.ToString(), 1);
                Helper.TrackTaskInProgress(DailyTasks.MurderMonster9Level.ToString(), 1);
            }
        }
        DestroyCardEffect();
        
        Delete();
    }

    /// <summary>
    /// Используется для тех случаев, когда нужно заменить карту и уничтожить заражением
    /// </summary>
    public void Delete()
    {
        if (!firstUpdate)
        {
            CardPrice.text = "";
            TextOnCard.text = "";
            ImageCard.enabled = false;
            ImageCard.sprite = null; 
            SetInfect(false);
            Card = null;
            Play = OverridedPlay["Default"];
            VisibilityClick(true);
            SetCardActive(true);
        }
    }

    private void DestroyCardEffect()
    {
        CardDestruction cardDestruction = SceneController.I.cardDestruction;
        GameObject newCD = Instantiate(cardDestruction.gameObject);     
        RectTransform rt = gameObject.GetComponent<RectTransform>();
        newCD.transform.position = new Vector3(rt.position.x, rt.position.y, 1f);
        CardDestruction cd = newCD.GetComponent<CardDestruction>();
        cd.InitCardDestruction(ImageCard.sprite);
        cd.Explode();      
    }

    private void DefaultPlay()
    {
        if (Card != null)
        {
           
            if (Helper.GetTraining5Passed())
            {
                if (Card.Type == CardType.Monster)
                {
                    Helper.SetAchievement("kill_monster", Helper.GetAchievement("kill_monster") + 1);
                }
                if (Card.Type == CardType.Ability || Card.Type == CardType.Madness) Helper.SetAchievement("use_ability_cards", Helper.GetAchievement("use_ability_cards") + 1);
            }
            if (AbilityController.I.LastUsed != null) TableController.I.namelast = AbilityController.I.LastUsed.Name;
            if (Card.Name != "AddCloneLastUsed" && Card.Name != "RepeatPreviousCard") AbilityController.I.SetLastUsed(Card);
            IsPlayed = true;
                     
            Card.Play();
            if (PlayerParameters.Health > 0)
            {
                PlayerParameters.AddTo(-Card.CardPriceEnergy, PlayerParameters.Characteristic.Energy);
            }
            else // чтобы GameResult не вызывался 2 раза (проявляются баги), но было видно, что вычет энергии произошел
            {
                if (Card.CardPriceEnergy < 0) PlayerParameters.Energy -= Card.CardPriceEnergy;
                else PlayerParameters.Energy = PlayerParameters.Energy > 0 ? PlayerParameters.Energy - Card.CardPriceEnergy : 0;
            }
            if (Card.Type == CardType.Ability)
            {
                AbilityController.I.OnPlayAbility(Card.Name);
                PlayerParameters.AbilityCards++;
            }
            IsPlayed = false;
            SetInfect(false);
            DestroyCard();
            //Deck.I.Infect();
            TableController.I.Infect();
            Messanger<Events>.SendMessage(Events.UpdateStats);
            SceneController.I.GameState();
            Deck.I.RefreshDeckCard();
            Deck.I.UpdateMonsterCount();
        }
    }

    public void SetCard(CardInfo ci, int pos)
    {
        if (pos == position)
        {
            // если нужно, чтоб все преобразованные зараженные карты остались зараженными (вне зависимости от способа преобразования)
            ImageCard.enabled = true;
            bool inf = false;
            if (Card?.Infected == true) inf = true;

            if (ci.Type != CardType.Ability && ci.Type != CardType.Madness)
            {
                CardPrice.text = ci.Price.ToString();
                TextOnCard.text = ci.LocalName;
            }
            else
            {
                CardPrice.text = "";
                TextOnCard.text = ci.LocalName;
            }  
            
            Card = new CardInfo();
            Card = ci;
            Card.sprite = URPNGLoader.LoadURPNG("3/" + GameConstants.PathToSprites[ci.Type], GameConstants.GetNumberSprite(ci)); // обязательно нужно записывать спрайт 
            ImageCard.sprite = Card.sprite;
            Card.PositionOnTable = pos;
            if (Card == null) Debug.Log("Card is null");

            if (inf != false && (Card.Type != CardType.Ability || Card.Type != CardType.Madness))
            {
                SetInfect(true);
                Card.Infected = true;//
            }                         
            gameObject.transform.parent.GetComponent<CardDistribution>().StartAnimation();
            if (PlayerPrefs.GetInt("FirstGame", 0) < 4) SetCardActive(true);
        }
    }      

    public void VisibilityClick(bool visible)
    {
        if (visible)
        {
            ColorBlock colorBlock = btnCard.colors;
            colorBlock.pressedColor = new Color(200f / 255f, 200f / 255f, 200f / 255f, 1f);
            btnCard.colors = colorBlock;
        }
        else
        {
            ColorBlock colorBlock = btnCard.colors;
            colorBlock.pressedColor = Color.white;
            btnCard.colors = colorBlock;
        }
    }

    public void SetCardActive(bool active)
    {
        Image card = gameObject.GetComponent<Image>();
        Text price = gameObject.transform.GetChild(0).GetChild(0).GetComponent<Text>();
        Text locName = gameObject.transform.GetChild(1).GetChild(0).GetComponent<Text>();
        btnCard.interactable = active;
        if (active)
        {
            card.color = Color.white;
            price.color = new Color(212f / 255f, 216f / 225f, 197f / 255f);
            locName.color = new Color(212f / 255f, 216f / 225f, 197f / 255f);
        }
        else
        {
            card.color = new Color(100f / 255f, 100f / 255f, 100f / 255f, 1f);
            price.color = new Color(57f / 255f, 61f / 225f, 42f / 255f);
            locName.color = new Color(57f / 255f, 61f / 225f, 42f / 255f);
        }
    }

    public void OnTap()// событие долгого тапа по карте
    {
        Messanger<string, Events>.SendMessage(Events.PlayEffect, SoundClips.CardShowing);
        CollectionCard.I.OnDescriptionActivate(Card, Card.Price);
    }

    public bool IsUsed()
    {
        if (Card == null) return false;
        else return true;
    }

    private void OnDestroy()
    {
        Messanger<Events>.Unsubscribe(Events.StartGame, Delete);
        Messanger<CardInfo, int, Events>.Unsubscribe(Events.OnSendCard, SetCard);
        Messanger<string, Events>.Unsubscribe(Events.OnChangePlay, OnChangePlay);
    }
}
