﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBoard : MonoBehaviour {
    public Text Health;
    public Text Energy;
    public Text Crystal;
    public ParticleSystem testPrt;

    ParticleSystem particleSystem;
    ParticleSystem.MainModule mainParticleSystem;
    ParticleSystem.EmissionModule emission;
    private Transform healthAnchorParent, energyAnchorParent, crystalsAnchorParent;
    private GameObject flyingParametr;
    public Vector3 StartPosAnimation { get; set; }

    public static ScoreBoard I { get; private set; }

    public void Awake()
    {
        I = this;
    }

    private void Start()
    {
        Messanger<Events>.Subscribe(Events.UpdateStats, UpdateParametrs);

        InitAnimation();
    }

    private void InitAnimation()
    {
        healthAnchorParent = TablePanel.I.GetObject("healthAnchor").transform;
        healthAnchorParent.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);

        energyAnchorParent = TablePanel.I.GetObject("energyAnchor").transform;
        energyAnchorParent.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);

        crystalsAnchorParent = TablePanel.I.GetObject("crystalsAnchor").transform;
        crystalsAnchorParent.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);

        flyingParametr = TablePanel.I.GetObject("flyingParametr");
        GameObject particleSystemObj = new GameObject();
        particleSystemObj.name = "GameParticleSystem";
        particleSystemObj.transform.SetParent(flyingParametr.transform);
        flyingParametr.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);

        float particleSystemX = 0f;
        float particleSystemY = 0f;
        particleSystemObj.transform.localPosition = new Vector3(particleSystemX, particleSystemY, 0f);
        particleSystem = particleSystemObj.AddComponent<ParticleSystem>();

        var colorOverLifetime = particleSystem.colorOverLifetime;
        colorOverLifetime.enabled = true;
        colorOverLifetime.color = new Color(1f, 1f, 1f, 1f);

        mainParticleSystem = particleSystem.main;
        mainParticleSystem.startLifetime = new ParticleSystem.MinMaxCurve(4f);
        mainParticleSystem.startSpeed = new ParticleSystem.MinMaxCurve(0f);
        mainParticleSystem.startSize = new ParticleSystem.MinMaxCurve(0.2f);
        mainParticleSystem.gravityModifier = new ParticleSystem.MinMaxCurve(1f);
        mainParticleSystem.simulationSpace = ParticleSystemSimulationSpace.World;
        mainParticleSystem.maxParticles = 10000;

        emission = particleSystem.emission;

        ParticleSystem.ShapeModule shape = particleSystem.shape;
        shape.shapeType = ParticleSystemShapeType.Circle;
        shape.radius = 0.2236807f;

        ParticleSystemRenderer renderer = particleSystemObj.GetComponent<ParticleSystemRenderer>();
        renderer.sortingLayerName = "TablePanel";
        renderer.sortingOrder = 20;
        renderer.material = Resources.Load<Material>("Materials/ParticleMaterial");
    }

    public void UpdateParametrs()
    {
        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(UpdateParametrsCoroutine());
        }
        else
        {
            Health.text = PlayerParameters.Health.ToString();
            Energy.text = PlayerParameters.Energy.ToString();
            Crystal.text = PlayerParameters.Crystal.ToString();
        }
    }

    private IEnumerator UpdateParametrsCoroutine()
    {
        yield return new WaitForSeconds(GameConstants.WaitMoveAnimationTextTime);

        Health.text = PlayerParameters.Health.ToString();
        Energy.text = PlayerParameters.Energy.ToString();
        Crystal.text = PlayerParameters.Crystal.ToString();
    }

    private void OnDestroy()
    {
        Messanger<Events>.Unsubscribe(Events.UpdateStats, UpdateParametrs);
    }

    public IEnumerator FlyParametrCoroutine(CardType cardType)
    {
        float emissionValue = 100f;
        emission.rateOverTime = new ParticleSystem.MinMaxCurve(emissionValue);

        Vector3 targetPos = new Vector3();
        Color color = new Color();
        switch (cardType)
        {
            case CardType.Health:
                targetPos = healthAnchorParent.transform.GetChild(0).GetChild(0).position;
                color = new Color(0f / 255f, 108f / 255f, 0f / 255f);
                break;
            case CardType.Energy:
                targetPos = energyAnchorParent.transform.GetChild(0).GetChild(0).position;
                color = new Color(160f / 255f, 0f / 255f, 191f / 255f);
                break;
            case CardType.Monster:
                targetPos = healthAnchorParent.transform.GetChild(0).GetChild(0).position;
                color = new Color(255f / 255f, 0f / 255f, 0f / 255f);
                break;
            case CardType.Crystal:
                targetPos = crystalsAnchorParent.transform.GetChild(0).GetChild(0).position;
                color = new Color(235f / 255f, 118f / 255f, 0f / 255f);
                break;
        }
        mainParticleSystem.startColor = color;
        flyingParametr.GetComponent<Image>().color = color;

        GameObject flyingParametrClone = Instantiate(flyingParametr, flyingParametr.transform.parent);

        MoveAnimation moveAnimation = flyingParametrClone.GetComponent<MoveAnimation>();
        moveAnimation.Target = targetPos;
        moveAnimation.T = GameConstants.ParticleSystemMoveAnimationTime;
        moveAnimation.gameObject.SetActive(true);
        moveAnimation.gameObject.transform.position = StartPosAnimation;
        
        moveAnimation.ActionForUpdate = () =>
        {
            emissionValue -= 5;
            if (emissionValue >= 0)
                emission.rateOverTime = new ParticleSystem.MinMaxCurve(emissionValue);
        };

        moveAnimation.OnComplete = () =>
        {
            AlphaAnimation alphaAnimationParent = flyingParametrClone.AddComponent<AlphaAnimation>();
            alphaAnimationParent.Target = 0f;
            alphaAnimationParent.T = GameConstants.ParticleSystemAlphaAnimationTime;
            alphaAnimationParent.DoAlpha(ComponentType.Image);

            AlphaAnimation alphaAnimationChild = flyingParametrClone.transform.GetChild(0).gameObject.AddComponent<AlphaAnimation>();
            alphaAnimationChild.Target = 0f;
            alphaAnimationChild.T = GameConstants.ParticleSystemAlphaAnimationTime;
            alphaAnimationChild.OnComplete = () => Destroy(flyingParametrClone);
            alphaAnimationChild.DoAlpha(ComponentType.ParticleSystem);
        };

        moveAnimation.DoMove();

        yield return new WaitForSeconds(GameConstants.ParticleSystemMoveAnimationTime);
    }
}
