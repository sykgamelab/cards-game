﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class AbilityController : MonoBehaviour
{
    public static AbilityController I;
    public CardInfo LastUsed;
    private Dictionary<string, Action> abilities;

    void Awake()
    {
        I = this;
        Messanger<Events>.Subscribe(Events.StartGame, OnStartGame);
        Messanger<Events>.Subscribe(Events.FirstGame, OnStartGame); // Событие другое
        abilities = new Dictionary<string, Action>();
        foreach (Ability type in Enum.GetValues(typeof(Ability))) abilities.Add(type.ToString(), (Action)Delegate.CreateDelegate(typeof(Action), this, type.ToString()));
    }
    public void OnPlayAbility(string name) => abilities[name]();

    private void OnStartGame()
    {
        LastUsed = null;
    }

    public void SetLastUsed(CardInfo card) => LastUsed = card;


    /**************************************  Карты способностей  ***************************************/

    public void EquateHealthEnergy()
    {
        if (PlayerParameters.Health < PlayerParameters.Energy) Helper.TrackTaskInProgress(DailyTasks.RestoreBraveryAbility.ToString(), PlayerParameters.Energy - PlayerParameters.Health);
        PlayerParameters.AddTo(PlayerParameters.Energy - PlayerParameters.Health, PlayerParameters.Characteristic.Health);
    }

    public void EquateEnergyHealth()
    {
        int energy = 0;
        /*foreach (CardPosition card in TableController.I.cardOnTable) if (card.IsPlayed)
            {
                energy = card.Card.CardPriceEnergy;
            }   */

        if (PlayerParameters.Energy < PlayerParameters.Health) Helper.TrackTaskInProgress(DailyTasks.AddEnergyAbility.ToString(), PlayerParameters.Health - PlayerParameters.Energy);
        PlayerParameters.AddTo(PlayerParameters.Health - PlayerParameters.Energy + energy, PlayerParameters.Characteristic.Energy);
    }
    public void RestoreHealth()
    {   
        Helper.TrackTaskInProgress(DailyTasks.RestoreBraveryAbility.ToString(), PlayerParameters.MaxHealth - PlayerParameters.Health);    // не сдвигать 
        PlayerParameters.Health = PlayerParameters.MaxHealth;
    }

    public void AddHealthIfEnergyZero()
    {
        foreach (CardPosition card in TableController.I.cardOnTable) if (card.IsPlayed) card.Card.CardPriceEnergy = 0;
        if (PlayerParameters.Energy == 0)
        {
            PlayerParameters.AddTo(5, PlayerParameters.Characteristic.Health);
            Helper.TrackTaskInProgress(DailyTasks.RestoreBraveryAbility.ToString(), 5);
        }
        //foreach (CardPosition card in TableController.I.cardOnTable) if (card.IsPlayed) PlayerParameters.Energy += card.Card.CardPriceEnergy;
    }
    public void AddCrystalIfEnergyZero()
    {
        if (PlayerParameters.Energy == 0) PlayerParameters.AddTo(8, PlayerParameters.Characteristic.Crystal);
        if (Helper.GetTraining5Passed())
        {
            if (PlayerParameters.Energy > 0 && Helper.GetAchievement("empty_cache") == 0)
            {
                Helper.SetHiddenAchievementDone("empty_cache");
            }
        }
        Helper.TrackTaskInProgress(DailyTasks.AddCrystalsAbility.ToString(), 8);
    }

    // Наносит 1 урон всем картам в игре и возвращает их в колоду с новыми значениями 
    public void DamageAllUndraw()
    {
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.IsUsed() && !card.IsPlayed)
            {
                if (card.Card.Type != CardType.Ability && card.Card.Type != CardType.Madness)
                {
                    card.Card.Price--;
                    card.CardPrice.text = card.Card.Price.ToString();
                    if (card.Card.Price == 0)
                    {
                        Helper.TrackTaskInProgress(InGameTasks.DeleteCardsAbility.ToString(), 1);
                        if (card && card.Card.Type == CardType.Monster)
                        {
                            Helper.TrackTaskInProgress(InGameTasks.KillMonsterAbility.ToString(), 1);
                            Helper.SetAchievement("kill_monster", Helper.GetAchievement("kill_monster") + 1);
                        }
                        card.DestroyCard();  //карта уничтожается 
                    }
                    else
                    {
                        if (card.Card.Type == CardType.Monster) Helper.TrackTaskInProgress(DailyTasks.AbilityDamageMonster.ToString(), 1/*урон*/);
                        Helper.TrackTaskInProgress(DailyTasks.DamageCardsAbility.ToString(), 1);
                    }
                }

                if (card.Card != null)
                {
                    Deck.I.AddToDeck(card.Card);
                    Helper.TrackTaskInProgress(DailyTasks.ReturnCardsToDeck.ToString(), 1);
                }
                card.Delete(); // карта ушла в деку, опустошаем CardPosition
            }
        }
        SceneController.I.GameState();
        Deck.I.Shuffle();
    }

    public void AddCrystalsFromDeckTwoPrice()
    {
        foreach (CardInfo card in Deck.I.deck)
            if (card.Type == CardType.Crystal)
            {
                card.Price += 2;   Debug.Log($"КАЗНА: {card.Price}");
                Helper.TrackTaskInProgress(DailyTasks.AddCardsCountAbility.ToString(), 2);
            }
    }

    /// <summary>
    /// "МЕТКИЙ ВЫСТРЕЛ": Убирает 2 карты из колоды
    /// </summary>
    public void DeleteCardFromDeck()
    {
        int tmp = Deck.I.deck.Count < 2 ? Deck.I.deck.Count : 2;
        for (int i = 0; i < tmp; i++)
        {
            int f = UnityEngine.Random.Range(0, Deck.I.deck.Count);

            if (Deck.I.deck[f].Type == CardType.Monster)
            {
                Helper.TrackTaskInProgress(DailyTasks.DestroyDeckMonsters.ToString(), 1);
                Helper.TrackTaskInProgress(InGameTasks.KillMonsterAbility.ToString(), 1);
            }
            Helper.TrackTaskInProgress(DailyTasks.DestroyDeckCardsAbility.ToString(), 1);
            Helper.TrackTaskInProgress(InGameTasks.DeleteCardsAbility.ToString(), 1);

            Deck.I.DeleteAt(f);
        }
        Helper.TrackTaskInProgress(InGameTasks.UseAccuracyShot.ToString(), 1);
    }

    public void ReturnMonsterToDeck()// рандомного монстра со стола
    {
        List<CardPosition> monsterCards = new List<CardPosition>();
        foreach (CardPosition card in TableController.I.cardOnTable) if (card.IsUsed()) if (card.Card.Type == CardType.Monster) monsterCards.Add(card);

        if (monsterCards.Count > 0)
        {
            int rnd = UnityEngine.Random.Range(0, monsterCards.Count);
            Deck.I.AddToDeck(monsterCards[rnd].Card);
            monsterCards[rnd].Delete();    // карта ушла в деку, опустошаем CardPosition
            Deck.I.Shuffle();

            Helper.TrackTaskInProgress(DailyTasks.ReturnCardsToDeck.ToString(), 1);
        }
    }

    public void AddDoublePrice()
    {
        //foreach (CardInfo card in Deck.I.deck) if (card.Type != CardType.Ability && card.Type != CardType.Madness) card.Price *= 2;
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.IsUsed())
            {
                if (card.Card.Type != CardType.Ability && card.Card.Type != CardType.Madness)
                {
                    if (Helper.GetTraining5Passed())
                    {
                        if (card.Card.Type == CardType.Monster && card.Card.Price == 18 && Helper.GetAchievement("no_brakes") == 0)
                        {
                            Helper.SetHiddenAchievementDone("no_brakes");
                        }
                    }
                    Helper.TrackTaskInProgress(DailyTasks.AddCardsCountAbility.ToString(), card.Card.Price);
                    card.Card.Price *= 2;
                    card.CardPrice.text = card.Card.Price.ToString();
                }
            }
        }
    }

    // Наносит 3 единицы урона случайному монстру в игре
    public void DamageRandomMonster()
    {
        int damage = 3;
        List<CardInfo> monsterCard = new List<CardInfo>();
        foreach (CardInfo card in Deck.I.deck) if (card.Type == CardType.Monster) monsterCard.Add(card);
        foreach (CardPosition card in TableController.I.cardOnTable) if (card.IsUsed()) if (card.Card.Type == CardType.Monster) monsterCard.Add(card.Card);
        if (monsterCard.Count > 0)
        {
            int f = UnityEngine.Random.Range(0, monsterCard.Count);

            if ((monsterCard[f].Price -= damage) <= 0)
            {
                if (monsterCard[f].PositionOnTable == -1)
                {
                    Deck.I.Delete(monsterCard[f]);
                    Helper.TrackTaskInProgress(DailyTasks.DestroyDeckMonsters.ToString(), 1);
                    Helper.TrackTaskInProgress(DailyTasks.DestroyDeckCardsAbility.ToString(), 1);
                }
                else TableController.I.cardOnTable[monsterCard[f].PositionOnTable].DestroyCard();

                Helper.TrackTaskInProgress(InGameTasks.KillMonsterAbility.ToString(), 1);
                Helper.TrackTaskInProgress(InGameTasks.DeleteCardsAbility.ToString(), 1);
            }
            else
            {
                Helper.TrackTaskInProgress(DailyTasks.AbilityDamageMonster.ToString(), damage);
                Helper.TrackTaskInProgress(DailyTasks.DamageCardsAbility.ToString(), damage);
                if (monsterCard[f].PositionOnTable != -1) TableController.I.cardOnTable[monsterCard[f].PositionOnTable].CardPrice.text = monsterCard[f].Price.ToString();
            }
        }

        Helper.TrackTaskInProgress(InGameTasks.UseRayLight.ToString(), 1);
    }

    public void AddTwoCrystalAndOneMonsterToDeck()
    {
        Helper.TrackTaskInProgress(DailyTasks.AddDeckCardsAbility.ToString(), 3);
        Deck.I.AddToDeck(CardInfo.Random(CardType.Crystal));
        Deck.I.AddToDeck(CardInfo.Random(CardType.Crystal));
        Deck.I.AddToDeck(CardInfo.Random(CardType.Monster));
        Deck.I.Shuffle();
    }

    public void AddTwoMadnessAndOneMonsterToDeck()
    {
        Helper.TrackTaskInProgress(DailyTasks.AddDeckCardsAbility.ToString(), 3);
        Deck.I.AddToDeck(new CardInfo(CardType.Madness));
        Deck.I.AddToDeck(new CardInfo(CardType.Madness));
        Deck.I.AddToDeck(CardInfo.Random(CardType.Crystal));
        Deck.I.Shuffle();
    }

    public void DestroyThreeRandomCard()
    {
        List<CardInfo> AllCard = new List<CardInfo>();
        foreach (CardInfo card in Deck.I.deck) AllCard.Add(card);
        foreach (CardPosition card in TableController.I.cardOnTable) if (card.IsUsed() && !card.IsPlayed) AllCard.Add(card.Card);

        int count = AllCard.Count < 3 ? AllCard.Count : 3;

        for (int i = 0; i < count; i++)
        {
            int f = UnityEngine.Random.Range(0, AllCard.Count);

            if (AllCard[f].Type == CardType.Monster)
            {
                Helper.TrackTaskInProgress(InGameTasks.KillMonsterAbility.ToString(), 1);
                if (AllCard[f].PositionOnTable == -1) Helper.TrackTaskInProgress(DailyTasks.DestroyDeckMonsters.ToString(), 1);
            }
            Helper.TrackTaskInProgress(InGameTasks.DeleteCardsAbility.ToString(), 1);
            if (AllCard[f].PositionOnTable == -1)
            {
                Helper.TrackTaskInProgress(DailyTasks.DestroyDeckCardsAbility.ToString(), 1);
                Deck.I.Delete(AllCard[f]);
            }
            else TableController.I.cardOnTable[AllCard[f].PositionOnTable].DestroyCard();

            AllCard.RemoveAt(f);
        }
    }

    /// <summary>
    /// "ПОЛНАЯ КАПИТУЛЯЦИЯ": Снижает значение всех карт на столе до 1 // был переделан (дисбалансная карта)
    /// </summary>
    public void DestroyAllMonster()
    {
        foreach (CardPosition card in TableController.I.cardOnTable)
            if (card.IsUsed())
            {
                if (card.Card.Type == CardType.Crystal || card.Card.Type == CardType.Energy || card.Card.Type == CardType.Health || card.Card.Type == CardType.Monster)
                {
                    if (card.Card.Type == CardType.Monster && card.Card.Price != 1) Helper.TrackTaskInProgress(DailyTasks.AbilityDamageMonster.ToString(), card.Card.Price - 1);
                    if (card.Card.Price != 1) Helper.TrackTaskInProgress(DailyTasks.DamageCardsAbility.ToString(), card.Card.Price - 1);
                    card.CardPrice.text = "1";
                    card.Card.Price = 1;
                }
            }
    }

    public void AddThreeRandomCardsToDeck()
    {
        Helper.TrackTaskInProgress(DailyTasks.AddDeckCardsAbility.ToString(), 3);
        for (int i = 0; i < 3; i++) Deck.I.AddToDeck(CardInfo.Random());
        Deck.I.Shuffle();
    }

    public void DestroyRandomNeighbor()
    {
        int[] Neighbors = TableController.I.ReturnNeighbors();
        List<CardPosition> Neigh = new List<CardPosition>();
        foreach (int pos in Neighbors) if (TableController.I.cardOnTable[pos].IsUsed()) Neigh.Add(TableController.I.cardOnTable[pos]);
        if (Neigh.Count > 0)
        {
            CardPosition card = TableController.I.cardOnTable[Neigh[UnityEngine.Random.Range(0, Neigh.Count)].position];

            Helper.TrackTaskInProgress(InGameTasks.DeleteCardsAbility.ToString(), 1);
            if (card.Card.Type == CardType.Monster)
            {
                Helper.TrackTaskInProgress(InGameTasks.KillMonsterAbility.ToString(), 1);
            }

            if (card.Card.Type == CardType.Health && card.Card.Price == 9 && Helper.GetAchievement("risky") == 0)
            {
                Helper.SetHiddenAchievementDone("risky");
            }
            card.DestroyCard();
        }
    }

    public void ThreeDamageNeighbor()
    {
        int brav = 0;
        int[] Neighbors = TableController.I.ReturnNeighbors();
        List<CardPosition> Neigh = new List<CardPosition>();
        foreach (int pos in Neighbors) if (TableController.I.cardOnTable[pos].IsUsed() && TableController.I.cardOnTable[pos].Card.Type != CardType.Ability && TableController.I.cardOnTable[pos].Card.Type != CardType.Madness) Neigh.Add(TableController.I.cardOnTable[pos]);
        foreach (CardPosition card in Neigh)
        {
            if (card.Card.Type == CardType.Health) brav += 3;
            card.Card.Price -= 3;
            if (card.Card.Price <= 0)
            {
                if (card.Card.Type == CardType.Monster) Helper.TrackTaskInProgress(InGameTasks.KillMonsterAbility.ToString(), 1);
                card.DestroyCard();
                Helper.TrackTaskInProgress(InGameTasks.DeleteCardsAbility.ToString(), 1);
            }
            else
            {
                card.CardPrice.text = card.Card.Price.ToString();
                if (card.Card.Type == CardType.Monster) Helper.TrackTaskInProgress(DailyTasks.AbilityDamageMonster.ToString(), 3);
                Helper.TrackTaskInProgress(DailyTasks.DamageCardsAbility.ToString(), 3);
            }
        }

        if (Helper.GetTraining5Passed())
            if (brav >= 9 && Helper.GetAchievement("on_a_grand_scale") == 0)
            {
                Helper.SetHiddenAchievementDone("on_a_grand_scale");
            }
    }

    public void DestroyAllNeighborAndPriceToCrystal()
    {
        if (Helper.GetTraining5Passed())
            if (TableController.I.OnGetFreePositions().Count == 7 && Helper.GetAchievement("i_did_everything_right") == 0)
            {
                Helper.SetHiddenAchievementDone("i_did_everything_right");
            }
        int[] Neighbors = TableController.I.ReturnNeighbors();
        List<CardPosition> Neigh = new List<CardPosition>();
        foreach (int pos in Neighbors) if (TableController.I.cardOnTable[pos].IsUsed()) Neigh.Add(TableController.I.cardOnTable[pos]);
        int monster = 0; int bravery = 0;
        foreach (CardPosition card in Neigh)
        {
            if (card.Card.Type == CardType.Health) bravery++;
            if (card.Card.Type != CardType.Ability && card.Card.Type != CardType.Madness) PlayerParameters.AddTo(card.Card.Price, PlayerParameters.Characteristic.Crystal);

            if (card.Card.Type == CardType.Monster)
            {
                monster++;
                Helper.TrackTaskInProgress(InGameTasks.KillMonsterAbility.ToString(), 1);
            }
            Helper.TrackTaskInProgress(DailyTasks.AddCrystalsAbility.ToString(), card.Card.Price);
            Helper.TrackTaskInProgress(InGameTasks.DeleteCardsAbility.ToString(), 1);
            card.DestroyCard();
        }

        if (Helper.GetTraining5Passed())
        {
            if (monster == 4 && Helper.GetAchievement("rapid_enrichment") == 0)
            {
                Helper.SetHiddenAchievementDone("rapid_enrichment");
            }
            if (bravery == 4 && Helper.GetAchievement("exchange_wizard") == 0)
            {
                Helper.SetHiddenAchievementDone("exchange_wizard");
            }
        }
    }

    public void AddThreeNeighbor()
    {
        int[] Neighbors = TableController.I.ReturnNeighbors();
        List<CardPosition> Neigh = new List<CardPosition>();
        foreach (int pos in Neighbors) if (TableController.I.cardOnTable[pos].IsUsed() && TableController.I.cardOnTable[pos].Card.Type != CardType.Ability && TableController.I.cardOnTable[pos].Card.Type != CardType.Madness) Neigh.Add(TableController.I.cardOnTable[pos]);
        foreach (CardPosition card in Neigh)
        {
            if (Helper.GetTraining5Passed())
            {
                if (card.Card.Type == CardType.Monster && card.Card.Price == 12 && Helper.GetAchievement("feasible_help") == 0)
                {
                    Helper.SetHiddenAchievementDone("feasible_help");
                }
            }
            Helper.TrackTaskInProgress(DailyTasks.AddCardsCountAbility.ToString(), 3);
            card.Card.Price += 3;
            card.CardPrice.text = card.Card.Price.ToString();
        }
    }

    public void ConvertNeighborToCrystal()
    {
        int[] Neighbors = TableController.I.ReturnNeighbors();
        List<CardPosition> Neigh = new List<CardPosition>();
        foreach (int pos in Neighbors) if (TableController.I.cardOnTable[pos].IsUsed()) Neigh.Add(TableController.I.cardOnTable[pos]);
        if (Neigh.Count > 0)
        {
            int f = Neigh[UnityEngine.Random.Range(0, Neigh.Count)].position;
            CardType c = TableController.I.cardOnTable[f].Card.Type;
            bool infected = TableController.I.cardOnTable[f].Card.Infected;
            TableController.I.cardOnTable[f].Delete();  // замена карты
            if (Helper.GetTraining5Passed())
            {
                if (c == CardType.Crystal && TableController.I.cardOnTable[f].Card.Type == CardType.Crystal && Helper.GetAchievement("playful_hands") == 0)
                {
                    Helper.SetHiddenAchievementDone("playful_hands");
                }
            }
            if (c == CardType.Madness) Helper.TrackTaskInProgress(DailyTasks.TransformMadness.ToString(), 1);
            Helper.TrackTaskInProgress(DailyTasks.TransformToCrystal.ToString(), 1);
            TableController.I.cardOnTable[f].SetCard(CardInfo.Random(CardType.Crystal), TableController.I.cardOnTable[f].position);
            TableController.I.cardOnTable[f].Card.Infected = infected;
            PlayerParameters.TransformCard++;
        }
    }

    public void ReplaceNeighborToRandomEnergy()
    {
        int[] Neighbors = TableController.I.ReturnNeighbors();
        List<CardPosition> Neigh = new List<CardPosition>();
        foreach (int pos in Neighbors) if (TableController.I.cardOnTable[pos].IsUsed()) Neigh.Add(TableController.I.cardOnTable[pos]);
        if (Neigh.Count > 0)
        {
            int f = Neigh[UnityEngine.Random.Range(0, Neigh.Count)].position;
            CardType c = TableController.I.cardOnTable[f].Card.Type;
            if (c == CardType.Madness) Helper.TrackTaskInProgress(DailyTasks.TransformMadness.ToString(), 1);
            bool infected = TableController.I.cardOnTable[f].Card.Infected;
            TableController.I.cardOnTable[f].Delete();   // замена карты
            TableController.I.cardOnTable[f].SetCard(CardInfo.Random(CardType.Energy), TableController.I.cardOnTable[f].position);
            TableController.I.cardOnTable[f].Card.Infected = infected;
            if (Helper.GetTraining5Passed())
                if (TableController.I.cardOnTable[f].Card.Type == CardType.Energy && c == CardType.Energy && Helper.GetAchievement("energetic") == 0)
                {
                    Helper.SetHiddenAchievementDone("energetic");
                }
            PlayerParameters.TransformCard++;
        }
    }

    public void AllNeighborMonstersToMadness()
    {
        int m = 0;
        foreach (CardPosition c in TableController.I.cardOnTable) if (c.Card != null) if (c.Card.Type == CardType.Monster) m++;
        if (Helper.GetTraining5Passed())
        {
            if (m == 0 && Helper.GetAchievement("i_do_not_see_monsters") == 0)
            {
                Helper.SetHiddenAchievementDone("i_do_not_see_monsters");
            }
            if (m >= 4 && Helper.GetAchievement("triumph_of_madness") == 0)
            {
                Helper.SetHiddenAchievementDone("triumph_of_madness");
            }
        }
        int[] Neighbors = TableController.I.ReturnNeighbors();
        List<CardPosition> Neigh = new List<CardPosition>();
        foreach (int pos in Neighbors)
            if (TableController.I.cardOnTable[pos].IsUsed() && TableController.I.cardOnTable[pos].Card.Type == CardType.Monster)
                Neigh.Add(TableController.I.cardOnTable[pos]);
        foreach (CardPosition card in Neigh)
        {
            card.SetInfect(false);
            card.Delete();   // замена карты
            card.SetCard(new CardInfo(CardType.Madness), card.position);
            Helper.TrackTaskInProgress(DailyTasks.TransformToMadness.ToString(), 1);
            PlayerParameters.TransformCard++;
        }
    }

    public void AddCloneLastUsed()
    {
        if (LastUsed != null)
        {
            if (Helper.GetTraining5Passed())
            {
                if (LastUsed.Type == CardType.Monster && LastUsed.Price == 9 && Helper.GetAchievement("zoologist") == 0)
                {
                    Helper.SetHiddenAchievementDone("zoologist");
                }
                Helper.TrackTaskInProgress(DailyTasks.AddDeckCardsAbility.ToString(), 1);
            }
            Deck.I.AddToDeck(LastUsed);
            Deck.I.Shuffle();
        }
    }

    public void RepeatPreviousCard()
    {
        if (LastUsed != null)
        {
            if (LastUsed.Type == CardType.Monster && PlayerParameters.Health <= LastUsed.Price && Helper.GetAchievement("wonderful_rake") == 0)
            {
                Helper.SetHiddenAchievementDone("wonderful_rake");
            }
            LastUsed.Play();
            Messanger<Events>.SendMessage(Events.UpdateStats);
            if (LastUsed.Type == CardType.Ability) OnPlayAbility(LastUsed.Name);
        }
    }

    public void SeeMonsterInDeck()
    {
        TablePanel.I.GetObject("Monsters").SetActive(true);
    }

    /// <summary>
    /// "ЖЕЛЕЗНЫЙ УДАР": Уничтожает 1 карту монстра в игре
    /// </summary>
    public void DestroyOneMonster()
    {
        List<CardInfo> monsterCard = new List<CardInfo>();
        foreach (CardInfo card in Deck.I.deck) if (card.Type == CardType.Monster) monsterCard.Add(card);
        foreach (CardPosition card in TableController.I.cardOnTable) if (card.IsUsed()) if (card.Card.Type == CardType.Monster) monsterCard.Add(card.Card);
        if (monsterCard.Count > 0)
        {
            int f = UnityEngine.Random.Range(0, monsterCard.Count);
            if (monsterCard[f].PositionOnTable == -1)
            {
                Helper.TrackTaskInProgress(DailyTasks.DestroyDeckMonsters.ToString(), 1);
                Helper.TrackTaskInProgress(DailyTasks.DestroyDeckCardsAbility.ToString(), 1);

                Deck.I.Delete(monsterCard[f]);
            }
            else
            {
                TableController.I.cardOnTable[monsterCard[f].PositionOnTable].DestroyCard();
            }
            Helper.TrackTaskInProgress(InGameTasks.DeleteCardsAbility.ToString(), 1);
            Helper.TrackTaskInProgress(InGameTasks.KillMonsterAbility.ToString(), 1);
        }
    }

    public void DestroyAllMadness()
    {
        List<CardInfo> madnessCard = new List<CardInfo>();
        foreach (CardInfo card in Deck.I.deck) if (card.Type == CardType.Madness) madnessCard.Add(card);
        foreach (CardInfo card in madnessCard)
        {
            Helper.TrackTaskInProgress(DailyTasks.DestroyDeckCardsAbility.ToString(), 1);
            Deck.I.Delete(card);
        }

        foreach (CardPosition card in TableController.I.cardOnTable)
            if (card.IsUsed())
                if (card.Card.Type == CardType.Madness)
                {
                    Helper.TrackTaskInProgress(InGameTasks.DeleteCardsAbility.ToString(), 1);
                    card.DestroyCard();
                }
    }

    public void InfectThreeMonstersInDeck()
    {
        if (Helper.GetTraining5Passed())
            if (Deck.I.deck.Count == 0 && Helper.GetAchievement("and_so_come_down") == 0)
            {
                Helper.SetHiddenAchievementDone("and_so_come_down");
            }
        List<CardInfo> monsterCard = new List<CardInfo>();
        foreach (CardInfo card in Deck.I.deck) if (card.Type == CardType.Monster) monsterCard.Add(card);

        int count = monsterCard.Count < 3 ? monsterCard.Count : 3;

        for (int i = 0; i < count; i++)
        {
            Helper.TrackTaskInProgress(InGameTasks.InfectedMonsters.ToString(), 1);
            Helper.TrackTaskInProgress(DailyTasks.InfectMonsters.ToString(), 1);

            monsterCard[UnityEngine.Random.Range(0, monsterCard.Count)].Infected = true;
        }
    }

    public void InfectRandomMoster()
    {
        List<CardInfo> monsterCard = new List<CardInfo>();
        foreach (CardInfo card in Deck.I.deck) if (card.Type == CardType.Monster) monsterCard.Add(card);
        foreach (CardPosition card in TableController.I.cardOnTable) if (card.IsUsed() && !card.IsPlayed) if (card.Card.Type == CardType.Monster) monsterCard.Add(card.Card);
        if (monsterCard.Count != 0)
        {
            Helper.TrackTaskInProgress(InGameTasks.InfectedMonsters.ToString(), 1);
            Helper.TrackTaskInProgress(DailyTasks.InfectMonsters.ToString(), 1);

            int f = UnityEngine.Random.Range(0, monsterCard.Count);
            monsterCard[f].Infected = true;
            if (monsterCard[f].PositionOnTable != -1) monsterCard[f].Price++;
        }

    }

    public void InfectAllOnField()
    {
        foreach (CardPosition card in TableController.I.cardOnTable)
            if (card.IsUsed() && !card.IsPlayed)
            {
                if (card.Card.Type == CardType.Monster)
                {
                    Helper.TrackTaskInProgress(InGameTasks.InfectedMonsters.ToString(), 1);
                    Helper.TrackTaskInProgress(DailyTasks.InfectMonsters.ToString(), 1);
                }

                card.Card.Price++;
                card.Card.Infected = true;
            }
    }

    public void ReplaceRandomsOnFieldWithCardsFromDeckNumberMonsterCardsOnField()
    {
        int monsters = 0;
        List<CardPosition> AllCard = new List<CardPosition>();
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.IsUsed() && !card.IsPlayed)
            {
                AllCard.Add(card);
                if (card.Card.Type == CardType.Monster) monsters++;
            }
        }
        monsters = Deck.I.deck.Count < monsters ? Deck.I.deck.Count : monsters;
        for (int i = 0; i < monsters; i++)
        {
            int f = UnityEngine.Random.Range(0, AllCard.Count);
            int lastCard = Deck.I.deck.Count - 1;
            if (AllCard[f].Card.Type == CardType.Madness) Helper.TrackTaskInProgress(DailyTasks.TransformMadness.ToString(), 1);
            AllCard[f].SetCard(Deck.I.deck[lastCard], AllCard[f].position);
            Deck.I.DeleteAt(lastCard);
            PlayerParameters.TransformCard++;
        }
    }

    public void RandomlyChangeAllCards()
    {
        int t = 0;
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.IsUsed() && !card.IsPlayed)
            {
                CardType c = card.Card.Type;
                bool infected = card.Card.Infected;
                card.Delete();  // замена карты
                card.SetCard(CardInfo.Random(), card.position);

                if (card.Card.Type == CardType.Madness) Helper.TrackTaskInProgress(DailyTasks.TransformToMadness.ToString(), 1);
                if (card.Card.Type == CardType.Crystal) Helper.TrackTaskInProgress(DailyTasks.TransformToCrystal.ToString(), 1);

                card.SetInfect(infected);
                if (card.Card.Type == CardType.Monster && c == CardType.Health) t++;
            }
        }
        if (Helper.GetTraining5Passed()) if (t == 4 && Helper.GetAchievement("mad_prince") == 0)
            {
                Helper.SetHiddenAchievementDone("mad_prince");
            }
    }

    public void FreeDeal()
    {
        if (Helper.GetTraining5Passed())
            if (TableController.I.OnGetFreePositions().Count == 0 && Helper.GetAchievement("over_the_edge") == 0)
            {
                Helper.SetHiddenAchievementDone("over_the_edge");
            }
        //Helper.TrackTaskInProgress(InGameTasks.FreeDealCards.ToString(), 1); // Начисляется в колоде
        Deck.I.FreeDealCards(false);
    }

    public void FreeDealGame() => Deck.I.FreeDealCards(true);

    public void AddHealthEmptyField()
    {
        // Карта, которая разыгрывается, ещё на столе, т.е. если на столе 7 пустых ячеек из 8), то добавляем 5 жизней
        if (TableController.I.OnGetFreePositions().Count == 7)
        {
            if (Helper.GetTraining5Passed()) Helper.TrackTaskInProgress(DailyTasks.RestoreBraveryAbility.ToString(), 5);
            PlayerParameters.AddTo(5, PlayerParameters.Characteristic.Health);
        }
    }

    public void AddCrystalsAsNumberMonstersOnField()
    {
        int monsters = 0;
        foreach (CardPosition card in TableController.I.cardOnTable) if (card.IsUsed() && !card.IsPlayed) if (card.Card.Type == CardType.Monster) monsters++;
        if (Helper.GetTraining5Passed())
        {
            if (monsters == 0 && Helper.GetAchievement("self_sufficiency") == 0)
            {
                Helper.SetHiddenAchievementDone("self_sufficiency");
            }
        }
        Helper.TrackTaskInProgress(DailyTasks.AddCrystalsAbility.ToString(), monsters);
        PlayerParameters.AddTo(monsters, PlayerParameters.Characteristic.Crystal);
    }

    public void MadnessToCrystals() // на столе
    {
        int i = 0;
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.IsUsed())
            {
                if (card.Card.Type == CardType.Madness)
                {
                    Helper.TrackTaskInProgress(DailyTasks.TransformMadness.ToString(), 1);
                    Helper.TrackTaskInProgress(DailyTasks.TransformToCrystal.ToString(), 1);

                    card.Delete();   // замена карты
                    card.SetCard(CardInfo.Random(CardType.Crystal), card.position);
                    PlayerParameters.TransformCard++;
                    i++;
                }
            }
        }
        if (Helper.GetTraining5Passed())
        {
            if (i == 0 && Helper.GetAchievement("im_not_sorry") == 0)
            {
                Helper.SetHiddenAchievementDone("im_not_sorry");
            }
            if (i == 4 && Helper.GetAchievement("triumph_of_greed") == 0)
            {
                Helper.SetHiddenAchievementDone("triumph_of_greed");
            }
        }
    }

    public void DamageMonstersValueAboveSix() // на столе
    {
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.IsUsed() && !card.IsPlayed)
            {
                if (card.Card.Type == CardType.Monster && card.Card.Price >= 6)
                {
                    card.Card.Price -= 2;
                    card.CardPrice.text = card.Card.Price.ToString();

                    Helper.TrackTaskInProgress(DailyTasks.AbilityDamageMonster.ToString(), 2);
                    Helper.TrackTaskInProgress(DailyTasks.DamageCardsAbility.ToString(), 2);
                }
            }
        }
    }

    public void AddCharacteristicAsNumberMonstersOnField()
    {
        int monsters = 0; int energy = -1;
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.IsUsed()) if (card.Card.Type == CardType.Monster) monsters++;
            if (card.IsPlayed) energy = card.Card.CardPriceEnergy;
        }

        int addToHealth, addToEnergy;
        addToHealth = monsters + PlayerParameters.Health > PlayerParameters.MaxHealth ? PlayerParameters.MaxHealth - PlayerParameters.Health : monsters;
        addToEnergy = monsters + PlayerParameters.Energy > PlayerParameters.MaxEnergy ? PlayerParameters.MaxEnergy - PlayerParameters.Energy : monsters;
        PlayerParameters.AddTo(monsters, PlayerParameters.Characteristic.Health);
        PlayerParameters.AddTo(monsters + energy, PlayerParameters.Characteristic.Energy);

        PlayerParameters.AddTo(monsters, PlayerParameters.Characteristic.Crystal);

        Helper.TrackTaskInProgress(DailyTasks.AddCrystalsAbility.ToString(), monsters);
        Helper.TrackTaskInProgress(DailyTasks.RestoreBraveryAbility.ToString(), addToHealth);
        Helper.TrackTaskInProgress(DailyTasks.AddEnergyAbility.ToString(), addToEnergy);
    }

    public void AddOnePriceOnField()
    {
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.IsUsed() && !card.IsPlayed)
                if (card.Card.Type != CardType.Ability && card.Card.Type != CardType.Madness)
                {
                    if (Helper.GetTraining5Passed())
                    {
                        if (card.Card.Type == CardType.Monster && card.Card.Price == 10 && Helper.GetAchievement("help_strong") == 0)
                        {
                            Helper.SetHiddenAchievementDone("help_strong");
                        }
                    }
                    Helper.TrackTaskInProgress(DailyTasks.AddCardsCountAbility.ToString(), 1);
                    card.Card.Price++;
                    card.CardPrice.text = card.Card.Price.ToString();
                }
        }
    }

    /// <summary>
    /// "ТОТАЛЬНОЕ УНИЧТОЖЕНИЕ": Уничтожает все карты на столе, но снижает храбрость до 1
    /// </summary>
    public void DeleteFromFieldAndOneHealth()
    {
        if (Helper.GetTraining5Passed())
            if (Deck.I.deck.Count == 0 && Helper.GetAchievement("dangerous") == 0)
            {
                Helper.SetHiddenAchievementDone("dangerous");
            }
        foreach (CardPosition card in TableController.I.cardOnTable)
            if (card.IsUsed() && !card.IsPlayed)
            {
                if (card.Card.Type == CardType.Monster)
                {
                    Helper.TrackTaskInProgress(InGameTasks.KillMonsterAbility.ToString(), 1);
                }
                card.DestroyCard();

                Helper.TrackTaskInProgress(InGameTasks.DeleteCardsAbility.ToString(), 1);
            }
        PlayerParameters.Health = 1;
        Messanger<Events>.SendMessage(Events.UpdateStats);
    }

    public void ReplaceMonsterWithCrystalOnField()
    {
        int monster = 0;
        foreach (CardPosition card in TableController.I.cardOnTable)
        {
            if (card.IsUsed() && !card.IsPlayed)
            {
                if (card.Card.Type == CardType.Monster)
                {
                    bool infectedCard = false;
                    if (card.Card.Infected) infectedCard = true;
                    card.SetCard(CardInfo.Random(CardType.Crystal), card.position);
                    card.SetInfect(infectedCard);
                    Helper.TrackTaskInProgress(DailyTasks.TransformToCrystal.ToString(), 1);
                    PlayerParameters.TransformCard++;
                    monster++;
                }
            }
        }
        if (Helper.GetTraining5Passed())
        {
            if (monster == 0 && Helper.GetAchievement("household_philosophy") == 0)
            {
                Helper.SetHiddenAchievementDone("household_philosophy");
            }
            if (monster == 7 && Helper.GetAchievement("alchemist_dream") == 0)
            {
                Helper.SetHiddenAchievementDone("alchemist_dream");
            }
        }
    }

    public void AddMaxHealth() { PlayerParameters.MaxHealth += 5; }
    public void AddMaxEnergy() { PlayerParameters.MaxEnergy += 5; }
    public void NextCrystalsToTimesMore() { Messanger<string, Events>.SendMessage(Events.OnChangePlay, "NextCrystalsToTimesMore"); }
    public void FreeCards()
    {
        if (Helper.GetTraining5Passed())
            if (TableController.I.OnGetFreePositions().Count == 7 && Deck.I.deck.Count == 0 && Helper.GetAchievement("always_on_time") == 0)
            {
                Helper.SetHiddenAchievementDone("always_on_time");
            }
        Messanger<string, Events>.SendMessage(Events.OnChangePlay, "FreeCards");
    }
    public void NextAddEnergyHisPrice() { Messanger<string, Events>.SendMessage(Events.OnChangePlay, "NextAddEnergyHisPrice"); }
    public void NextMonsterTwoTimesWeaker() { Messanger<string, Events>.SendMessage(Events.OnChangePlay, "NextMonsterTwoTimesWeaker"); }
    public void NextMonsterEatCrystals() { Messanger<string, Events>.SendMessage(Events.OnChangePlay, "NextMonsterEatCrystals"); }

    private void OnDestroy()
    {
        Messanger<Events>.Unsubscribe(Events.StartGame, OnStartGame);
    }
}
