﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardDistribution : MonoBehaviour
{
    private Canvas canvas;
    private bool firstUpdate = true;
    private Vector3 defaultPosition;
    private Vector3 deckPosition;
    private int sortOrder;   
    MoveAnimation moveAnimation;

    void Awake()
    {
        canvas = gameObject.AddComponent<Canvas>();
        gameObject.AddComponent<GraphicRaycaster>();
        moveAnimation = gameObject.AddComponent<MoveAnimation>();
        Messanger<Events>.Subscribe(Events.UpdateAnchors, UpdateAnchors);
    }

    private void UpdateAnchors()
    {                                       
        defaultPosition = gameObject.transform.position;

        moveAnimation.Target = defaultPosition;
        moveAnimation.T = GameConstants.CardMoveTime;

        deckPosition = TablePanel.I.GetObject("DECK1").transform.position;
        canvas.sortingLayerName = "TablePanel";
        canvas.sortingOrder = 1;
        string str = gameObject.transform.GetChild(0).name;
        sortOrder = 10 - Convert.ToInt32(str.Substring(str.Length - 1));
    }

    public void StartAnimation()
    {
        canvas.sortingOrder = sortOrder;
        gameObject.transform.position = deckPosition;
        moveAnimation.DoMove();
    }

    void Update()
    {
        if (!firstUpdate)
        {
            canvas.overrideSorting = true;
        }
        firstUpdate = false;
    }

    private void OnDestroy()
    {
        Messanger<Events>.Unsubscribe(Events.UpdateAnchors, UpdateAnchors);
    }
}
