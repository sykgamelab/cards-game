﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO.Compression;


public class SceneController : MonoBehaviour
{
    [HideInInspector]
    public static SceneController I;
    public CardDestruction cardDestruction;
    private bool needLoadDay = false;

#if UNITY_EDITOR
    int Power = 31;   // для отображения изменения количества сил в логах
#endif

    void Awake()
    {
        I = this;
        Helper.Recount();
        Messanger<Events>.Subscribe(Events.StartGame, OnStartGame);
        Messanger<Events>.Subscribe(Events.FirstGame, OnFirstGame);

        Debug.Log($"SUPPORT TEXTURE COMPRESSION: " +
            $"ASTC_RGBA_4x4 = {SystemInfo.SupportsTextureFormat(TextureFormat.ASTC_RGBA_4x4)}, " +
            $"ASTC_RGBA_5x5 = {SystemInfo.SupportsTextureFormat(TextureFormat.ASTC_RGBA_5x5)}, " +
            $"ASTC_RGBA_6x6 = {SystemInfo.SupportsTextureFormat(TextureFormat.ASTC_RGBA_6x6)}, " +
            $"ASTC_RGBA_8x8 = {SystemInfo.SupportsTextureFormat(TextureFormat.ASTC_RGBA_8x8)}, " +
            $"ASTC_RGBA_10x10 = {SystemInfo.SupportsTextureFormat(TextureFormat.ASTC_RGBA_10x10)}, " +
            $"ASTC_RGBA_12x12 = {SystemInfo.SupportsTextureFormat(TextureFormat.ASTC_RGBA_12x12)}, " +
            $"ETC2_RGBA1 = {SystemInfo.SupportsTextureFormat(TextureFormat.ETC2_RGBA1)}, " +
            $"ETC2_RGBA8 = {SystemInfo.SupportsTextureFormat(TextureFormat.ETC2_RGBA8)}, " +
            $"ETC2_RGBA8Crunched = {SystemInfo.SupportsTextureFormat(TextureFormat.ETC2_RGBA8Crunched)}, " +
            $"PVRTC_RGBA2 = {SystemInfo.SupportsTextureFormat(TextureFormat.PVRTC_RGBA2)}, " +
            $"PVRTC_RGBA4 = {SystemInfo.SupportsTextureFormat(TextureFormat.PVRTC_RGBA4)}");

        //PlayerPrefs.DeleteAll();
        if (!PlayerPrefs.HasKey("Power")) PlayerPrefs.SetInt("Power", 30);
        LoadDay();

        /*foreach (string booster in Enum.GetNames(typeof(Boosts)))
        {
            PlayerPrefs.DeleteKey(booster);
            PlayerPrefs.DeleteKey($"{booster}A");
            PlayerPrefs.DeleteKey($"TimeBooster{booster}");
        } */
        string boosterN = Boosts.x2Power.ToString();
        //PlayerPrefs.SetInt($"{boosterN}", 0);
        //PlayerPrefs.DeleteKey($"{boosterN}A");
        //PlayerPrefs.SetString($"TimeBooster{boosterN}", DateTime.Now.AddMinutes(1).ToString());
        //PlayerPrefs.SetString($"boostedTimeEnd", DateTime.Now.AddMinutes(1).ToString());
        /*PlayerPrefs.SetString($"TimeBooster{boosterN}", DateTime.Now.AddSeconds(180).ToString());*/
        //PlayerPrefs.DeleteKey("Training6Passed");
        //PlayerPrefs.DeleteKey("FirstOpenAchievement");
        //PlayerPrefs.SetInt("kill_monster", 0);
        //PlayerPrefs.SetInt("kill_monster_lvl", 0);
        //Helper.SetAchievement ("kill_monster_infect", 8);

        // Выключить обучение
        //PlayerPrefs.DeleteKey("FirstOpenAchievement");
        //PlayerPrefs.DeleteKey("kill_monster_lvl");


        /*  Helper.SetTraining5Passed();         
            Helper.SetTraining6Passed();         
            Helper.SetTraining7Passed();  
            PlayerPrefs.DeleteKey("Training8Passed");
            for (int i = 0; i < GameConstants.Battles25ForTraining8; i++) Helper.AddBattleCount(); 
        PlayerPrefs.SetInt("FirstGame", 4);  */


        /* Ключи:
         * -1 - начинал выпадала реплика 6 обучения
         *  1 - обучение 6 пройдено
         *  2 - 2я часть обучения (первая - 1-4 реплики; вторая - 5-10 реплики)
         */
        //PlayerPrefs.SetInt("FirstOpenAchievement", 1); 

        // Конец выключения обучения               

        PlayerPrefs.SetInt("BatBeast", 1);
        PlayerPrefs.SetInt("DreadfulNeophron", 1);
        PlayerPrefs.SetInt("FangedDog", 1);
        PlayerPrefs.SetInt("HellLarva", 1);
        PlayerPrefs.SetInt("RogueSpike", 1);
        PlayerPrefs.SetInt("Rogue", 1);
        PlayerPrefs.SetInt("Scorpio", 1);
        PlayerPrefs.SetInt("Spider", 1);
        PlayerPrefs.SetInt("WildSlug", 1);

        // выключение обучения для тестирования ачивок
        // PlayerPrefs.SetInt("FirstGameOver", 4);
        /* PlayerPrefs.SetInt("CompleteTraining", 1);
         PlayerPrefs.SetInt("Training7Done", 1);*/

        RestoreParams();
    }

    private void OnApplicationFocus(bool focus)
    {
        if (focus)
        {
            RestoreParams();
        }
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause) SaveParams();
    }

    private void OnDestroy()
    {
        Messanger<Events>.Unsubscribe(Events.StartGame, OnStartGame);
        SaveParams();
    }

    private static string CodingValue(int value)
    {
        char[] chars = Convert.ToString(value, 2).ToCharArray();
        for (int i = 0; i < chars.Length; i++)
        {
            chars[i] = chars[i] == '0' ? '1' : '0';
        }

        return new string(chars);
    }

    private int EncodingValue(string value)
    {
        char[] chars = value.ToCharArray();
        for (int i = 0; i < chars.Length; i++)
        {
            chars[i] = chars[i] == '0' ? '1' : '0';
        }

        return Convert.ToInt32(new string(chars), 2);
    }

    public static void SaveParams()
    {
        int crystals = Helper.GetCrystals();
        string codingCrystals = CodingValue(crystals);
        PlayerPrefs.SetString(GameConstants.MD5HashCrystals, codingCrystals);

        int winSeries = Helper.GetWinSeries();
        string codingWinSeries = CodingValue(winSeries);
        PlayerPrefs.SetString(GameConstants.MD5HashWinSeries, codingWinSeries);
    }

    private void RestoreParams()
    {
        if (PlayerPrefs.HasKey(GameConstants.MD5HashCrystals))
        {
            string codingCrystals = PlayerPrefs.GetString(GameConstants.MD5HashCrystals);
            int encodingCrystals = EncodingValue(codingCrystals);
            PlayerPrefs.SetInt("Crystal", encodingCrystals);
        }
        
        if (PlayerPrefs.HasKey(GameConstants.MD5HashWinSeries))
        {
            string codingWinSeries = PlayerPrefs.GetString(GameConstants.MD5HashWinSeries);
            int encodingWinSeries = EncodingValue(codingWinSeries);
            PlayerPrefs.SetInt("WinSeries", encodingWinSeries);
        }
    }

    public void GameState()
    {
        if (TableController.I.OnGetFreePositions().Count < 8) return; // если есть хоть одна карта на поле, то ничего не делать
        if (Deck.I.deck.Count == 0 && PlayerParameters.Health > 0) GameResultsPanel.I.GameResult(true);
    } 

    public void OnStartGame()
    {
        PlayerParameters.MaxHealth = 10;
        PlayerParameters.MaxEnergy = 10;
        PlayerParameters.Health = PlayerParameters.MaxHealth;
        PlayerParameters.Energy = PlayerParameters.MaxEnergy;
        PlayerParameters.Crystal = 3;
        PlayerParameters.DeadMonsters = 0;
        PlayerParameters.TransformCard = 0;
        Messanger<Events>.SendMessage(Events.UpdateStats);
    }

    public void OnFirstGame()
    {
        OnStartGame(); // хз зачем так
    }

    private void Start()
    {
        if (PlayerPrefs.GetInt("GameIsNotOver") == 1) GameOver();
        //if (!PlayerPrefs.HasKey("Crystal")) PlayerPrefs.SetInt("Crystal", 100000);
        FirstGame training= gameObject.GetComponent<FirstGame>();
        if (PlayerPrefs.GetInt("DeleteCardFromDeck") == 0)
        {
            PlayerPrefs.SetString("ability1", "Madness");
            PlayerPrefs.SetInt("DeleteCardFromDeck", 2);
            PlayerPrefs.SetString("ability2", "DeleteCardFromDeck");
            PlayerPrefs.SetInt("DamageRandomMonster", 2);
            PlayerPrefs.SetString("ability3", "DamageRandomMonster");
            PlayerPrefs.SetInt("FreeDeal", 2);
            PlayerPrefs.SetString("ability4", "FreeDeal");
        }
        Screen.orientation = ScreenOrientation.Portrait;
    }

    private void Update()
    {
#if UNITY_EDITOR
        if (Power != PlayerPrefs.GetInt("Power", GameConstants.maxPower))
        {
            Power = PlayerPrefs.GetInt("Power", GameConstants.maxPower);
            Debug.Log($"Power: <color=red>{Power}</color>");
        }
#endif
        GameObject piece = GameObject.Find($"/{cardDestruction.name}(Clone) piece");    // "/" указывает чтобы искал в корне 
        if (piece?.transform.position.y < -30f || piece?.transform.position.y > 100f) Destroy(piece);  // ограничение области существования обломков

        if (needLoadDay)
        {
            int timeNowTimestamp = (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            int timeTimestamp = (int)(InstallTime().Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            if (timeNowTimestamp == timeTimestamp + GameConstants.DailyGiftSeconds) LoadDay();
        }

        
    }

    public void GameOver()
    {                                  
        PlayerPrefs.SetInt("GameIsNotOver", 0);
        Helper.ResetWinSeries();
    }
    
    private void LoadDay()
    {
        DateTime timeUtcNow = DateTime.UtcNow;
        int timeNowTimestamp = (int)(timeUtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        int timeTimestamp = (int)(InstallTime().Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        int days = Helper.GetProgress(InGameTasks.DailyGift.ToString());

        if (timeNowTimestamp == timeTimestamp + GameConstants.DailyGiftSeconds)
            days++;
        else
            days = 0;

        SaveTime(timeUtcNow);
        if (Helper.TaskInProgress(InGameTasks.DailyGift.ToString())) Helper.SetProgress(InGameTasks.DailyGift.ToString(), days);
        needLoadDay = true;
    }

    private void SaveTime(DateTime time)
    {
        PlayerPrefs.SetString("DailyDate", time.ToString());
    }

    private void DeleteTime()
    {
        PlayerPrefs.DeleteKey("DailyDate");
    }

    private DateTime InstallTime()
    {
        DateTime dt = new DateTime();
        if (DateTime.TryParse(PlayerPrefs.GetString("DailyDate", DateTime.UtcNow.ToString()), out dt)) return dt;
        else return dt = DateTime.UtcNow;
    }
}