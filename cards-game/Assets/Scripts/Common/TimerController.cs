﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TimerController : MonoBehaviour
{
    public static TimerController I;

    public static Slider PowerSlider { get; private set; }
    private static Button btnPlay;
    private static Text txtTimer;
    private static Text txtPower;
    private static Image imgFullPower;
    private static Text txtMaxPower;
    
    public static TimeSpan Discounter { get; private set; }
    private static TimeSpan minuteCount;                            // переменная-счётчик - количество накопленных секунд, не превышает RecoveryTimeSec = 60
    private static DateTime savedTime;                              // сохранённое время
    private static int RecoveryTimeSec { get; set; } = 60;          // время восстановления 1 энергии (секунд)
    private const float defaultRecoverySpeed = 1f;
    public static float RecoverySpeed { get; private set; } = defaultRecoverySpeed;   // скорость восстановления [1, 2]; 
                                                                                      // если != defaultRecoverySpeed, то бустер активный
    private static TimeSpan oneSecond = new TimeSpan(0, 0, 1);
    private static DateTime boostedTimeEnd;
    private static bool powerBoosterActive = false;

    private static bool initialized = false;

    private void Awake()
    {
        I = this;
        RestoreValues();
        Messanger<Events>.Subscribe(Events.GameLoadingDone, InitObjects);
        Messanger<int, Events>.Subscribe(Events.SubtractPower, SubtractPower);
        Messanger<int, Events>.Subscribe(Events.AddPower, AddPower);
    }

    void Start()
    {
                    
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            SaveValues();
        }
        else
        {
            RestoreValues();    
        }
    }

    private void OnDestroy()
    {
        SaveValues();
        //Debug.Log($"<color=green>boostedTimeEnd: {boostedTimeEnd}</color>");
        //Debug.Log($"<color=green>Discounter:     {Discounter}</color>");
        //Debug.Log($"<color=green>x2Power:        {PlayerPrefs.GetString("TimeBooster" + Boosts.x2Power.ToString())}</color>");
        //Debug.Log($"<color=green>x2Power:        {DateTime.Parse(PlayerPrefs.GetString("TimeBooster" + Boosts.x2Power.ToString())) - DateTime.Now}</color>");
        //Debug.Log($"<color=green>Power:          {GetPower()}</color>");
        Messanger<int, Events>.Unsubscribe(Events.SubtractPower, SubtractPower);
    }

    private void InitObjects()
    {
        PowerSlider = MenuPanel.I.GetObject("PwrSlider").transform.GetChild(1).GetComponent<Slider>();
        btnPlay = MenuPanel.I.GetObject("Play").GetComponent<Button>();
        txtTimer = MenuPanel.I.GetObject("Timer").GetComponent<Text>();
        txtPower = MenuPanel.I.GetObject("PwrSlider").transform.GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>();
        imgFullPower = MenuPanel.I.GetObject("imgFullPower").GetComponent<Image>();
        txtMaxPower = MenuPanel.I.GetObject("MaxPower").GetComponent<Text>();
        initialized = true;
        StartCoroutine(TimerCoroutine());
    }

    public static void ActivateBooster(DateTime boosterEndTime, float recoverySpeed)
    {
        boostedTimeEnd = boosterEndTime;
        RecoverySpeed = recoverySpeed; Debug.Log($"ActivateBooster({boosterEndTime}, {recoverySpeed}): RecoverySpeed = {RecoverySpeed} at RecountRecoveredPowerInPause");
        powerBoosterActive = true;
    }

    private static void RestoreValues()
    {
        DateTime.TryParse(PlayerPrefs.GetString("boostedTimeEnd", DateTime.Now.ToString()), out boostedTimeEnd);
        Debug.Log($"RestoreValues():  boostedTimeEnd: {boostedTimeEnd}");
        RecoverySpeed = PlayerPrefs.GetFloat("RecoverySpeed", 1f);
        Debug.Log($"RestoreValues(): RecoverySpeed = {RecoverySpeed}");
        if (boostedTimeEnd <= DateTime.Now) powerBoosterActive = false;
        else powerBoosterActive = true;
        TimeSpan.TryParse(PlayerPrefs.GetString("minuteCount", TimeSpan.Zero.ToString()), out minuteCount);
        Debug.Log($"RestoreValues():  minuteCount: {minuteCount}");
        DateTime.TryParse(PlayerPrefs.GetString("savedTime", DateTime.Now.ToString()), out savedTime);
        Debug.Log($"RestoreValues():  savedTime: {savedTime}");
        RecountRecoveredPowerInPause();
        
    }
    private static void SaveValues()
    {
        PlayerPrefs.SetString("boostedTimeEnd", boostedTimeEnd != null ? boostedTimeEnd.ToString() : DateTime.Now.ToString());
        Debug.Log($"SaveValues(): boostedTimeEnd: {boostedTimeEnd}");
        PlayerPrefs.SetString("savedTime", DateTime.Now.ToString());
        Debug.Log($"SaveValues(): savedTime: {savedTime}");
        PlayerPrefs.SetString("minuteCount", minuteCount.ToString());
        Debug.Log($"SaveValues(): minuteCount: {minuteCount}");
        PlayerPrefs.SetFloat("RecoverySpeed", RecoverySpeed);
        Debug.Log($"SaveValues(): RecoverySpeed: {RecoverySpeed}");
    }

    private static void RecountTimer()
    {
        int power = GameConstants.maxPower - GetPower();
        Discounter = new TimeSpan(0, 0, (int)(power * RecoveryTimeSec));
        Discounter -= minuteCount;
    }

    private static TimeSpan RecountRecoveredTimeInPause(DateTime until)
    {
        return until - savedTime;
    }

    // Возвращает значение восстановленных сил в от последнего сохранённого времени, до until с укзанием скорости восстановления recoverySpeed
    private static int RecoveredPower(DateTime until, float recoverySpeed, out TimeSpan leftOver) // DateTime from = savedTime
    {
        int power = 0;
        TimeSpan recoveredTime = RecountRecoveredTimeInPause(until);
        power = (int)(recoveredTime.TotalSeconds / (RecoveryTimeSec / recoverySpeed));
        int seconds = (int)(recoveredTime.TotalSeconds - power * RecoveryTimeSec / recoverySpeed);
        seconds = seconds >= 0 ? seconds : 0;
        leftOver = new TimeSpan(0, 0, seconds);
        return power > 0 ? power : 0;
    }

    private static void RecountRecoveredPowerInPause()
    {
        TimeSpan leftOver;
        int power;

        if (RecoverySpeed != defaultRecoverySpeed)     // если активен или был активным бустер
        {
            if (!powerBoosterActive)                   // если бустер закончился
            {   
                power = RecoveredPower(boostedTimeEnd + minuteCount, RecoverySpeed, out leftOver);  
                AddPower(power);                                                                   
                savedTime = boostedTimeEnd - leftOver;    // изменяем сохранённое время
                minuteCount = leftOver;
                RecoverySpeed = defaultRecoverySpeed;        
            } 
        }

        power = RecoveredPower(DateTime.Now + minuteCount, RecoverySpeed, out leftOver);   
        AddPower(power);                                                                 
        minuteCount = leftOver;
        RecountTimer();                                      
    }    

    public static int GetPower() => PlayerPrefs.GetInt("Power", 30);

    public static void AddPower(int num)
    {
        int power = GetPower();                                                              
        power = power + num < GameConstants.maxPower ? power + num : GameConstants.maxPower; 
        PlayerPrefs.SetInt("Power", power);
        if (power == GameConstants.maxPower) minuteCount = TimeSpan.Zero;
        RecountTimer();
        UpdatePowerParameters();
    }
    public static void SubtractPower(int num)
    {
        int power = GetPower();
        power = power - num;
        PlayerPrefs.SetInt("Power", power);
        RecountTimer();
        UpdatePowerParameters();
    }

    static IEnumerator TimerCoroutine()
    {
        while (true)
        {   
            if (GetPower() < GameConstants.maxPower)
            {
                if (boostedTimeEnd - DateTime.Now <= TimeSpan.Zero) RecoverySpeed = defaultRecoverySpeed;
                Discounter -= oneSecond;
                minuteCount += oneSecond;
                if (minuteCount == new TimeSpan(0, 1, 0))
                {
                    minuteCount = TimeSpan.Zero;
                    AddPower(1);
                }
                DisplayTimer();
            }
            else
            {
                // пустой таймер
                // 30 белым цветом
                // нет powerTxt
            }
            
            UpdatePowerParameters();
            yield return new WaitForSeconds(1f / RecoverySpeed);
        }
    }

    private static void UpdatePowerParameters()
    {
        if (initialized)
        {
            MenuPanel.I.GetObject("PowerCountZero").SetActive(false);
            PowerSlider.gameObject.SetActive(true);
            imgFullPower.enabled = false;
            txtMaxPower.color = new Color(71f / 255f, 42f / 255f, 18f / 255f);
            txtTimer.gameObject.SetActive(true);
            txtPower.text = GetPower().ToString();
            int power = GetPower();
            DisplaySliderState();
            if (power == 0)
            {
                MenuPanel.I.GetObject("PowerCountZero").SetActive(true);
                PowerSlider.gameObject.SetActive(false);
                btnPlay.interactable = false;
            }
            else if (power < GameConstants.maxPower)
            {
                if (power >= GameConstants.priceForGame)
                {
                    btnPlay.interactable = true;
                }
                else // power < 2 (priceForGame)
                {
                    btnPlay.interactable = false;
                }
            }
            else if (power == GameConstants.maxPower)
            {
                btnPlay.interactable = true;
                imgFullPower.enabled = true;
                txtMaxPower.color = new Color(1f, 1f, 1f);
                txtTimer.gameObject.SetActive(false);
            }
        }
    }

    private static void DisplayTimer()
    {
        txtTimer.text = String.Format("{0:mm\\:ss}", Discounter);
    }
    
    private static void DisplaySliderState()
    {
        PowerSlider.value = 0.2f + (1f / GameConstants.maxPower * GetPower()) * 0.8f;
    }

    
}
