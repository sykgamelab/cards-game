﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Добавлять компонент BoostTimer нужно к GameObject у которого есть компонент Text на котором отображается время
public class BoostTimer : MonoBehaviour
{
    string boostName;

    private DateTime timeEnd;
    private TimeSpan delta;
    private Text text;
    private bool startTimer = false;

    void Awake()
    {
        Messanger<Events>.Subscribe(Events.StartBoostTimer, StartTimer);
        Messanger<string, Events>.Subscribe(Events.AddHourBoost, AddHours);
    }

    private void OnDestroy()
    {
        Messanger<Events>.Unsubscribe(Events.StartBoostTimer, StartTimer);
        Messanger<string, Events>.Unsubscribe(Events.AddHourBoost, AddHours);
    }

    public void AddHours(string boost)
    {
        if (boostName != boost) return;
        timeEnd = timeEnd.AddHours(1f);
        Helper.SetTimeBooster(boost, timeEnd);
    }

    // вызывать после добавления компонента
    public void Init(string boost) 
    {
        boostName = boost;
        StartTimer();
    }

    private void Update()
    {
        if (startTimer)
        {
            if (delta.TotalSeconds > 0)
            {
                delta = timeEnd - DateTime.Now;
                text.text = delta.Hours.ToString("00") + ":" + delta.Minutes.ToString("00") + ":" + delta.Seconds.ToString("00");
                Helper.SetTimeBooster(boostName, timeEnd); // сохраняем время каждый кадр
            }
            else // время кончилось
            {
                startTimer = false;
                Helper.SetBoosterActivated(boostName, 0);
                Helper.DeleteTimeBooster(boostName);
                //PlayPanel.I.ApplyBoosterManager();
                PlayPanel.I.InitBoosters(true);
                Messanger<Events>.SendMessage(Events.OnCheckSimilarBoost);
                Messanger<ShopTabs, Events>.SendMessage(Events.OpenShopTab, ShopTabs.Boosts);
            }
        }
    }

    void StartTimer()// Helper.GetBoosterActivated(boostName) : 1 - на 1 час, 3 - на 3 часа
    {
        text = gameObject.GetComponent<Text>();

        if (boostName == "ReAbilities")
        {
            if (Helper.GetBooster("ReAbilities") == 0) gameObject.transform.parent.parent.gameObject.SetActive(false);
            startTimer = false;
            text.text = "";
            Helper.DeleteTimeBooster(boostName);
            //PlayPanel.I.ApplyBoosterManager();
            PlayPanel.I.InitBoosters(true);
            return;
        }

        //if (boostName == Boosts.x2Power.ToString()) PowerController.RecoverySpeed = 2;

        if (Helper.GetBoosterActivated(boostName) != 0 && !Helper.HasTimeBooster(boostName)) // буст активировали прямо сейчас. 
        {
            timeEnd = DateTime.Now.AddHours((float)Helper.GetBoosterActivated(boostName));
            Helper.SetTimeBooster(boostName, timeEnd);
            try
            {
                delta = timeEnd - DateTime.Now;
                startTimer = true;
            }
            catch (Exception ex)
            {
                Debug.Log(ex);
            }

        }
        else if (Helper.GetBoosterActivated(boostName) != 0) //буст был активирован раннее. 
        {
            timeEnd = Helper.GetTimeBooster(boostName);

            if (timeEnd < DateTime.Now) //время вышло
            {
                //if (boostName == Boosts.x2Power.ToString()) PowerController.SetDefaultSpeed();
                startTimer = false;
                Helper.DeleteTimeBooster(boostName);
                Helper.SetBoosterActivated(boostName, 0);
                if (Helper.GetBoostDescription(boostName).price <= 0)
                {
                    Debug.Log($"<color=blue>{gameObject.transform.parent.parent.name}</color>");
                    if (boostName != Boosts.x2Power.ToString()) gameObject.transform.parent.parent.gameObject.SetActive(false);
                    //PlayPanel.I.ApplyBoosterManager();
                    PlayPanel.I.InitBoosters(true);
                }
                else text.text = Helper.GetBoostDescription(boostName).price.ToString();
                Messanger<Events>.SendMessage(Events.OnCheckSimilarBoost);
            }
            else // время еще есть
            {
                Helper.SetTimeBooster(boostName, timeEnd);
                try
                {
                    delta = timeEnd - DateTime.Now;
                    startTimer = true;
                }
                catch (Exception ex)
                {
                    Debug.Log(ex);
                }
            }
        }
        //буст дизактивирован
        else
        {
            startTimer = false;
            Helper.DeleteTimeBooster(boostName);
            Helper.SetBoosterActivated(boostName, 0);
            if (Helper.GetBoostDescription(boostName).price == 0 && Helper.GetBoosterActivated(boostName) != 1)
            {
                if (Helper.GetBooster(boostName) < 1)
                {
                    gameObject.transform.parent.parent.gameObject.SetActive(false);
                    //PlayPanel.I.ApplyBoosterManager();
                    PlayPanel.I.InitBoosters(true);
                }
                else text.text = Helper.GetBoostDescription(boostName).price.ToString();
            }
            Messanger<Events>.SendMessage(Events.OnCheckSimilarBoost);
        }
    }
}