﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TrainingButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    float timeElapsed;
    Image background;
    float minColor = 230f / 255f;
    float maxColor = 1f;
    float speed = 0.9f;
    Color pressedColor;

    void Start()
    {
        background = TrainingPanel.I.GetObject("Background").GetComponent<Image>();
        pressedColor = new Color(minColor, minColor, minColor, 1f);
    }    

    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        speed = -Mathf.Abs(speed);
        StartCoroutine(AlphaAnim());
    }

    void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
    {
        speed = Mathf.Abs(speed);
        StartCoroutine(AlphaAnim());
    }

    IEnumerator AlphaAnim()
    {
        Color color;
        while (minColor <= background.color.r && background.color.r <= maxColor)
        {
            float time = Time.deltaTime;
            color = new Color(speed * time, speed * time, speed * time);
            background.color += color;
            yield return null;
        }
        if (background.color.r <= minColor) background.color = pressedColor;
        else background.color = Color.white;
    }
}
