﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class Helper
{
    public static List<string> KeyCrystal = new List<string>();
    private static DictionaryList StringsList = new DictionaryList();

    private static Dictionary<string, object> progress = new Dictionary<string, object>();

    public static int CountCardsPurchased { get; set; }
    public static int CountCard { get; set; }
    public static int Size { get; private set; }

    [Serializable]
    private class DescriptionAbility
    {
        public string key = "";
        public string name = "";
        public string description = "";
        public int price = 0;
        public int significance = 1;
        public int sprite;
    }

    [Serializable]
    private class DescriptionBoost
    {
        public string key = "";
        public string description = "";
        public string name = "";
        public int price = 0;
        public int[] sprite = new int[2];
        public int tasksSprite = 3;
    }

    [Serializable]
    private class DescriptionCrystal
    {
        public string id = "";
        public int crystal = 0;
        public float money = 0f;
        public bool sale = false;
    }

    [Serializable]
    private class DescriptionMonster
    {
        public string key = "";
        public string name = "";
        public int strength = 1;
        public int numberSprite = 12;
        public string description = "This is monster?";
    }
    [Serializable]
    private class DescriptionTasks
    {
        public int number = 0;
        public string Key = "TaskKey";
        public int need = 9;
        public string reward = "crystals";
        public string description = "This is Task";
    }
    [Serializable]
    private class DescriptionDaily
    {
        public int number = 0;
        public string Key = "DailyKey";
        public int[] need = new int[4] { 9, 9, 9, 9 };
        public string description = "This is Daily task";
    }
    [Serializable]
    private class DescriptionAchievements
    {
        public string key = "AchievementKey";
        public string name = "achievement";
        public int sprite = 1;
        public string description = "It's good achievement!";
    }
    [Serializable]
    private class DescriptionOpenAchievements
    {
        public string key = "OpenAchievementKey";
        public string name = "openachievement";
        public int[] sprite = new int[4];
        public int[] need = new int[3] { 9, 9, 9 };
        public string description = "It's good achievement!";
    }

    [Serializable]
    private class DescriptionAbilityCardsLimits
    {
        public int openCards = 4;
        public int openCardsWeight = 5;
        public int maxSetWeight = 9;
    }

    [Serializable]
    private class LocalizationStrings
    {
        public string key = "";
        public string localeString = "";
    }

    [Serializable]
    private class DescriptionList
    {
        public DescriptionAbility Madness;
        public DescriptionAbility[] ability;
        public DescriptionBoost[] boosters;
        public DescriptionCrystal[] crystals;
        public DescriptionMonster[] Monsters;
        public DescriptionTasks[] Tasks;
        public DescriptionDaily[] Daily;
        public DescriptionAchievements[] Achievement;
        public DescriptionOpenAchievements[] OpenAchievement;
        public DescriptionAbilityCardsLimits[] AbilityCardsLimits;

        public InfoOfReplica[] FirstGame;
        public InfoOfReplica[] SecondGame;
        public InfoOfReplica[] ThirdGame;
        public InfoOfReplica[] FourthGame;
        public InfoOfReplica[] TourOfMenu;
        public InfoOfReplica[] FirstAchivement;
        public InfoOfReplica[] Buy13Cards;
        public InfoOfReplica[] Played25fights;
        public InfoOfReplica[] GameOverInTraining;
        public InfoOfReplica[] FirstGameOver;
        public InfoOfReplica[] FirstGoToShop;
        public InfoOfReplica[] NewMonsterReplica;
        public InfoOfReplica FirstEnergyZero;
        public InfoOfReplica DailyTaskNotTaken;

        public InfoOfReplica[] RandomSessionReplics;
        public InfoOfReplica[] Win5TimesInARow;
        public InfoOfReplica[] Win6TimesInARow;
        public InfoOfReplica[] Win7TimesInARow;
        public InfoOfReplica[] Win8TimesInARow;
        public InfoOfReplica[] Win9TimesInARow;
        public InfoOfReplica[] Win10TimesInARow;

        public InfoOfReplica[] InGame3Day;
        public InfoOfReplica[] InGame4Day;
        public InfoOfReplica[] InGame5Day;
        public InfoOfReplica[] InGame7Day;
        public InfoOfReplica[] InGame10Day;
        public InfoOfReplica[] InGame15Day;
        public InfoOfReplica[] InGame20Day;
        public InfoOfReplica[] InGame25Day;
        public InfoOfReplica[] InGame30Day;

        public InfoOfReplica[] Opened18CardsAbilities;
        public InfoOfReplica[] Opened25CardsAbilities;
        public InfoOfReplica[] Opened35CardsAbilities;
        public InfoOfReplica[] Opened51CardsAbilities;

        public InfoOfReplica[] Spent1000Crystals;
        public InfoOfReplica[] Spent4000Crystals;
        public InfoOfReplica[] Spent7000Crystals;

        public LocalizationStrings[] LocaleStrings;
    }

    private class DictionaryList
    {
        public AbilityStrings Madness;
        public Dictionary<string, AbilityStrings> AbilityList;
        public Dictionary<string, BoostersStrings> BoosterList;
        public Dictionary<string, CrystalsStrings> CrystalList;
        public Dictionary<string, MonstersStrings> MonstersList;
        public Dictionary<int, TasksStrings> TasksList;
        public Dictionary<int, DailyStrings> DailyList;
        public Dictionary<string, AchievementsStrings> AchievementsList;
        public Dictionary<string, OpenAchievementsStrings> OpenAchievementsList;
        public Dictionary<int, AbilityCardsLimits> AbilityCardsLimitsList;
        public TrainingList TrainingStrings;
        public Dictionary<string, string> LocaleList;
    }

    [Serializable]
    private class InfoOfReplica
    {
        public int NumberOfReplica;
        public string Emotion;
        public string Replica;
    }

    public static void Recount()
    {
        if (StringsList == null)
            StringsList = new DictionaryList();
        if (StringsList.AbilityList == null) StringsList.AbilityList = new Dictionary<string, AbilityStrings>();
        else StringsList.AbilityList.Clear();
        if (StringsList.BoosterList == null) StringsList.BoosterList = new Dictionary<string, BoostersStrings>();
        else StringsList.BoosterList.Clear();
        if (StringsList.CrystalList == null) StringsList.CrystalList = new Dictionary<string, CrystalsStrings>();
        else StringsList.CrystalList.Clear();
        if (StringsList.MonstersList == null) StringsList.MonstersList = new Dictionary<string, MonstersStrings>();
        else StringsList.MonstersList.Clear();
        if (StringsList.TasksList == null) StringsList.TasksList = new Dictionary<int, TasksStrings>();
        else StringsList.TasksList.Clear();
        if (StringsList.DailyList == null) StringsList.DailyList = new Dictionary<int, DailyStrings>();
        else StringsList.DailyList.Clear();
        if (StringsList.AchievementsList == null) StringsList.AchievementsList = new Dictionary<string, AchievementsStrings>();
        else StringsList.AchievementsList.Clear();
        if (StringsList.OpenAchievementsList == null) StringsList.OpenAchievementsList = new Dictionary<string, OpenAchievementsStrings>();
        else StringsList.OpenAchievementsList.Clear();
        if (StringsList.AbilityCardsLimitsList == null) StringsList.AbilityCardsLimitsList = new Dictionary<int, AbilityCardsLimits>();
        else StringsList.AbilityCardsLimitsList.Clear();
        StringsList.TrainingStrings = new TrainingList(StringsList.TrainingStrings == null);
        if (StringsList.LocaleList == null) StringsList.LocaleList = new Dictionary<string, string>();
        else StringsList.LocaleList.Clear();
        StringsList.Madness = new AbilityStrings("", "", 0, 0, 3);
        KeyCrystal.Clear();
        LoadStrings();
    }

    public static AbilityStrings GetAbilityDescription(string key) { if (StringsList.AbilityList.ContainsKey(key)) return StringsList.AbilityList[key]; else { Debug.Log($"Error! Key {key} not found!"); return StringsList.AbilityList["FreeDeal"]; } }
    public static BoostersStrings GetBoostDescription(string key) { if (StringsList.BoosterList.ContainsKey(key)) return StringsList.BoosterList[key]; else { Debug.Log($"Error! Key {key} not found!"); return StringsList.BoosterList["InfinitePower"]; } }
    public static CrystalsStrings GetCrystalsDescription(string key) { if (StringsList.CrystalList.ContainsKey(key)) return StringsList.CrystalList[key]; else { Debug.Log($"Error! Key {key} not found!"); return StringsList.CrystalList["1000crystal"]; } }
    public static AbilityStrings GetMadnessDescription() => StringsList.Madness;
    public static MonstersStrings GetMonstersStrings(string key) { if (StringsList.MonstersList.ContainsKey(key)) return StringsList.MonstersList[key]; else { Debug.Log($"Error! Key {key} not found!"); return StringsList.MonstersList["BatBeast"]; } }
    public static TasksStrings GetTasksStrings(int taskNumber) => StringsList.TasksList[taskNumber];
    public static DailyStrings GetDailyStrings(int i) => StringsList.DailyList[i];
    public static AchievementsStrings GetAchievementStrings(string key) => StringsList.AchievementsList[key];
    public static OpenAchievementsStrings GetOpenAchievementStrings(string key) => StringsList.OpenAchievementsList[key];
    public static TrainingList GetTrainingStrings() => StringsList.TrainingStrings;
    public static string GetLocalString(string key) { if (StringsList.LocaleList.ContainsKey(key)) return StringsList.LocaleList[key]; else { Debug.Log($"Error! Key {key} not found!"); return $"{key}"; } }


    public static int GetLevel()
    {
        Recount();
        CountCardsPurchased = 0;
        CountCard = 0;
        foreach (KeyValuePair<string, AbilityStrings> a in StringsList.AbilityList) if (PlayerPrefs.GetInt(a.Key) != 0)
        {
            CountCardsPurchased += a.Value.significance;
            CountCard++;
        }
        CountCardsPurchased += StringsList.Madness.significance;

        if (CountCardsPurchased <= 11) return 1;
        else if (CountCardsPurchased <= 49) return 2;
        else if (CountCardsPurchased <= 89) return 3;
        else if (CountCardsPurchased <= 129) return 4;
        else return 5;
    }

    public static AbilityCardsLimits GetCardLimit(int OpenCards)
    {
        foreach (int a in StringsList.AbilityCardsLimitsList.Keys)
        {
            if (a == OpenCards)
            {
                return StringsList.AbilityCardsLimitsList[a];
            }
        }
        if (StringsList.AbilityCardsLimitsList[StringsList.AbilityCardsLimitsList.Count - 1 + 4].openCards < OpenCards) return StringsList.AbilityCardsLimitsList[StringsList.AbilityCardsLimitsList.Count - 1 + 4];
        else return StringsList.AbilityCardsLimitsList[4];
    }

    public static bool AbilityCardIsLocked(string name) => PlayerPrefs.GetInt(name) == 0 || PlayerPrefs.GetInt(name) == 1;

    public static string GetLangCode()
    {
        SystemLanguage lang = SystemLanguage.Russian;
        switch (lang)
        {

            case SystemLanguage.German:
                return "de";
            case SystemLanguage.English:
                return "en";
            case SystemLanguage.Spanish:
                return "es";
            case SystemLanguage.French:
                return "fr";
            case SystemLanguage.Portuguese:
                return "pt";
            case SystemLanguage.Russian:
            case SystemLanguage.Ukrainian:
                return "ru";

            /*            case SystemLanguage.Italian:
                            return "it";
                        case SystemLanguage.Thai:
                            return "th";
                        case SystemLanguage.Turkish:
                            return "tr";
                        case SystemLanguage.Arabic:
                            return "ar";
                        case SystemLanguage.Indonesian:
                            return "ind";
                        case SystemLanguage.Vietnamese:
                            return "vi";   */
            default:
                return "en";
        }
    }

    public static int GetScore() => PlayerPrefs.GetInt("score", 0);
    public static bool GetHaveScoreToReport() => PlayerPrefs.GetInt("have_score_to_report", 0) != 0;
    public static void SetHaveScoreToReport(bool has) => PlayerPrefs.SetInt("have_score_to_report", has ? 1 : 0);

    private static void LoadStrings()
    {
        string path = $"Descriptions/{GetLangCode()}/strings";
        TextAsset descriptions = Resources.Load<TextAsset>(path);
        DescriptionList dictionary = JsonUtility.FromJson<DescriptionList>(descriptions.text);
        foreach (DescriptionAbility d in dictionary.ability) StringsList.AbilityList.Add(d.key, new AbilityStrings(d.name, d.description, d.price, d.significance, d.sprite));
        foreach (DescriptionBoost d in dictionary.boosters) StringsList.BoosterList.Add(d.key, new BoostersStrings(d.name, d.description, d.price, d.sprite, d.tasksSprite));
        foreach (DescriptionCrystal d in dictionary.crystals)
        {
            StringsList.CrystalList.Add(d.id, new CrystalsStrings(d.crystal, d.money, d.sale));
            KeyCrystal.Add(d.id);
        }
        foreach (DescriptionMonster d in dictionary.Monsters) StringsList.MonstersList.Add(d.key, new MonstersStrings(d.name, d.strength, d.numberSprite, d.description));
        foreach (DescriptionTasks d in dictionary.Tasks) StringsList.TasksList.Add(d.number, new TasksStrings(d.Key, d.need, d.reward, d.description));
        foreach (DescriptionDaily d in dictionary.Daily) StringsList.DailyList.Add(d.number, new DailyStrings(d.Key, d.need, d.description));
        foreach (DescriptionAchievements d in dictionary.Achievement) StringsList.AchievementsList.Add(d.key, new AchievementsStrings(d.name, d.sprite, d.description));
        foreach (DescriptionOpenAchievements d in dictionary.OpenAchievement) StringsList.OpenAchievementsList.Add(d.key, new OpenAchievementsStrings(d.name, d.sprite, d.need, d.description));
        foreach (DescriptionAbilityCardsLimits d in dictionary.AbilityCardsLimits) StringsList.AbilityCardsLimitsList.Add(d.openCards, new AbilityCardsLimits(d.openCards, d.openCardsWeight, d.maxSetWeight));
        StringsList.Madness = new AbilityStrings(dictionary.Madness.name, dictionary.Madness.description, dictionary.Madness.price, dictionary.Madness.significance, dictionary.Madness.sprite);

        foreach (InfoOfReplica r in dictionary.FirstGame) StringsList.TrainingStrings.FirstGame.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.SecondGame) StringsList.TrainingStrings.SecondGame.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.ThirdGame) StringsList.TrainingStrings.ThirdGame.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.FourthGame) StringsList.TrainingStrings.FourthGame.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.TourOfMenu) StringsList.TrainingStrings.TourOfMenu.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.FirstAchivement) StringsList.TrainingStrings.FirstAchivement.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.Buy13Cards) StringsList.TrainingStrings.Buy13Cards.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.Played25fights) StringsList.TrainingStrings.Played25fights.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.GameOverInTraining) StringsList.TrainingStrings.GameOverInTraining.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.FirstGameOver) StringsList.TrainingStrings.FirstGameOver.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.FirstGoToShop) StringsList.TrainingStrings.FirstGoToShop.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.NewMonsterReplica) StringsList.TrainingStrings.NewMonsterReplica.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));

        foreach (InfoOfReplica r in dictionary.RandomSessionReplics)      StringsList.TrainingStrings.RandomSessionReplics.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));

        foreach (InfoOfReplica r in dictionary.Win5TimesInARow)           StringsList.TrainingStrings.Win5TimesInARow.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.Win6TimesInARow)           StringsList.TrainingStrings.Win6TimesInARow.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.Win7TimesInARow)           StringsList.TrainingStrings.Win7TimesInARow.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.Win8TimesInARow)           StringsList.TrainingStrings.Win8TimesInARow.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.Win9TimesInARow)           StringsList.TrainingStrings.Win9TimesInARow.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.Win10TimesInARow)           StringsList.TrainingStrings.Win10TimesInARow.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));

        foreach (InfoOfReplica r in dictionary.InGame3Day)            StringsList.TrainingStrings.InGame3Day.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.InGame4Day)            StringsList.TrainingStrings.InGame4Day.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.InGame5Day)            StringsList.TrainingStrings.InGame5Day.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.InGame7Day)            StringsList.TrainingStrings.InGame7Day.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.InGame10Day)           StringsList.TrainingStrings.InGame10Day.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.InGame15Day)           StringsList.TrainingStrings.InGame15Day.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.InGame20Day)           StringsList.TrainingStrings.InGame20Day.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.InGame25Day)           StringsList.TrainingStrings.InGame25Day.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.InGame30Day)           StringsList.TrainingStrings.InGame30Day.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));

        foreach (InfoOfReplica r in dictionary.Opened18CardsAbilities)           StringsList.TrainingStrings.Opened18CardsAbilities.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.Opened25CardsAbilities)           StringsList.TrainingStrings.Opened25CardsAbilities.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.Opened35CardsAbilities)           StringsList.TrainingStrings.Opened35CardsAbilities.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.Opened51CardsAbilities)           StringsList.TrainingStrings.Opened51CardsAbilities.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));

        foreach (InfoOfReplica r in dictionary.Spent1000Crystals)           StringsList.TrainingStrings.Spent1000Crystals.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.Spent4000Crystals)           StringsList.TrainingStrings.Spent4000Crystals.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));
        foreach (InfoOfReplica r in dictionary.Spent7000Crystals)           StringsList.TrainingStrings.Spent7000Crystals.Add(r.NumberOfReplica, new Replica(r.Emotion, r.Replica));

        StringsList.TrainingStrings.FirstEnergyZero = new Replica(dictionary.FirstEnergyZero.Emotion, dictionary.FirstEnergyZero.Replica);
        StringsList.TrainingStrings.DailyTaskNotTaken = new Replica(dictionary.DailyTaskNotTaken.Emotion, dictionary.DailyTaskNotTaken.Replica);
        foreach (LocalizationStrings locStr in dictionary.LocaleStrings) StringsList.LocaleList.Add(locStr.key, locStr.localeString);
    }

    public static byte[] EncodeTimestamp(long timestamp)
    {
        byte[] intBytes = BitConverter.GetBytes(timestamp);
        if (BitConverter.IsLittleEndian)
            Array.Reverse(intBytes);
        byte[] result = intBytes;

        return result;
    }

    public static void FixButton(Button button)
    {
        button.transition = Selectable.Transition.None;
        button.interactable = false;
    }

    public static void UnfixButton(Button button)
    {
        button.transition = Selectable.Transition.ColorTint;
        button.interactable = true;
    }

    public static bool HasAchievementToReport(string id) => PlayerPrefs.GetInt($"{id}_to_report", 0) != 0;
    public static void SetAchievementToReport(string id, bool has) => PlayerPrefs.SetInt($"{id}_to_report", has ? 1 : 0);

    public static int GetSyncTimestamp() => PlayerPrefs.GetInt("sync_timestamp", 0);
    public static void SetSyncTimestamp(int timestamp) => PlayerPrefs.SetInt("sync_timestamp", timestamp);

    public static string GetTicket()
    {
        string ticket = null;
        int time = (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        int expTime = PlayerPrefs.GetInt("expiration_time", 0);
        if (expTime < time)
        {
            PlayerPrefs.SetString("ticket", null);
            PlayerPrefs.SetInt("expiration_time", 0);
        }
        else ticket = PlayerPrefs.GetString("ticket", null);

        return ticket;
    }
    public static void SetTicket(string ticket, int expirationTime)
    {
        PlayerPrefs.SetString("ticket", ticket);
        PlayerPrefs.SetInt("expiration_time", expirationTime);
    }

    private static void FillProgressByEnums()
    {
        foreach (string ability in Enum.GetNames(typeof(Ability)))
            progress.Add(ability, PlayerPrefs.GetInt(ability));

        foreach (string monster in Enum.GetNames(typeof(Monsters)))
            progress.Add(monster, PlayerPrefs.GetInt(monster));

        int i = 0;
        foreach (string openAchievement in Enum.GetNames(typeof(OpenAchievement)))
        {
            progress.Add(openAchievement, PlayerPrefs.GetInt(openAchievement));
            progress.Add($"{openAchievement}_lvl", GetOpenAchievementLevel((openAchievement)));

            if (i == 3) i = 1;
            else i++;
            progress.Add($"{openAchievement}{i}_to_report", HasAchievementToReport($"{openAchievement}{i}_to_report"));
        }

        foreach (string hiddenAchievement in Enum.GetNames(typeof(HiddenAchievement)))
        {
            progress.Add(hiddenAchievement, PlayerPrefs.GetInt(hiddenAchievement));
            progress.Add($"{hiddenAchievement}_to_report", HasAchievementToReport($"{hiddenAchievement}_to_report"));
        }

        foreach (string booster in Enum.GetNames(typeof(Boosts)))
        {
            progress.Add(booster, PlayerPrefs.GetInt(booster));
            progress.Add($"{booster}A", GetBoosterActivated(booster));
            progress.Add($"TimeBooster{booster}", GetTimeBooster(booster));
        }

        foreach (string dailyTask in Enum.GetNames(typeof(DailyTasks)))
        {
            progress.Add(dailyTask, PlayerPrefs.GetInt(dailyTask));
            progress.Add($"DailyRewardReceived{dailyTask}", GetDailyRewardReceived(dailyTask));
        }

        foreach (string task in Enum.GetNames(typeof(InGameTasks)))
            progress.Add(task, PlayerPrefs.GetInt(task));
    }

    private static void FillProgressByKeysWithoutLoop()
    {
        progress.Add("Crystal", GetCrystals());
        progress.Add("score", GetScore());
        progress.Add("have_score_to_report", GetHaveScoreToReport());
        progress.Add("sync_timestamp", GetSyncTimestamp());
        progress.Add("expiration_time", PlayerPrefs.GetInt("expiration_time", 0));
        progress.Add("ticket", GetTicket());
        progress.Add("LastTask", GetLastTaskNumber());
        progress.Add("DailyLocalNumbers", GetDailyLocalNumbers());
        progress.Add("LastDailyServerNumbers", GetLastDailyServerNumbers());
        progress.Add("music_active", IsMusicActive());
        progress.Add("sound_active", IsSoundActive());
        progress.Add("IndexGameMusics", GetIndexGameMusics());
        progress.Add("FullRecoveryTime", PlayerPrefs.GetString("FullRecoveryTime", DateTime.Now.ToString()));
        progress.Add("SavedTime", PlayerPrefs.GetString("SavedTime", DateTime.Now.ToString()));
        progress.Add("FirstGame", PlayerPrefs.GetInt("FirstGame", 0));
        progress.Add("Power", PlayerPrefs.GetInt("Power", 0));
        progress.Add("FirstGameOver", PlayerPrefs.GetInt("FirstGameOver", 0));
        progress.Add("FirstOpenAchievement", PlayerPrefs.GetInt("FirstOpenAchievement", 0));
        progress.Add("FirstGoToShop", PlayerPrefs.GetInt("FirstGoToShop", 0));
        progress.Add("winGame", PlayerPrefs.GetInt("winGame", 0));
        progress.Add("GameIsNotOver", PlayerPrefs.GetInt("GameIsNotOver", 0));
        progress.Add("DailyDate", PlayerPrefs.GetString("DailyDate", DateTime.Now.ToString()));
        progress.Add("WinSeries", GetWinSeries());
        progress.Add("Battles", GetBattleCount());
        progress.Add("CompleteTraining", PlayerPrefs.GetInt("CompleteTraining", 0));
        progress.Add("Training3Passed", GetTraining3Passed());
        progress.Add("Training5Passed", GetTraining5Passed());
        progress.Add("Training6Passed", GetTraining6Passed());
        progress.Add("Training7Passed", GetTraining7Passed());
        progress.Add("Training8Passed", GetTraining8Passed());
    }

    private static void FillProgressByKeysWithLoop()
    {
        for (int i = 1; i <= 3; i++)
            progress.Add($"Task{i}", GetTaskNumber($"Task{i}"));

        for (int i = 1; i <= 12; i++)
            progress.Add($"ability{i}", PlayerPrefs.GetString($"ability{i}", ""));
    }

    public static Dictionary<string, object> GetProgress()
    {
        progress.Clear();

        FillProgressByEnums();
        FillProgressByKeysWithoutLoop();
        FillProgressByKeysWithLoop();

        int timestamp = (int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
        progress.Add("timestamp", timestamp);

        return progress;
    }

    private static void SaveProgressByEnums(Dictionary<string, object> progress)
    {
        foreach (string ability in Enum.GetNames(typeof(Ability)))
            if (progress.ContainsKey(ability)) PlayerPrefs.SetInt(ability, Convert.ToInt32(progress[ability]));

        foreach (string monster in Enum.GetNames(typeof(Monsters)))
            if (progress.ContainsKey(monster)) PlayerPrefs.SetInt(monster, Convert.ToInt32(progress[monster]));

        int i = 0;
        string key;
        foreach (string openAchievement in Enum.GetNames(typeof(OpenAchievement)))
        {
            if (progress.ContainsKey(openAchievement)) PlayerPrefs.SetInt(openAchievement, Convert.ToInt32(progress[openAchievement]));

            key = $"{openAchievement}_lvl";
            if (progress.ContainsKey(key)) PlayerPrefs.SetInt(key, Convert.ToInt32(progress[key]));

            if (i == 3) i = 1;
            else i++;
            key = $"{openAchievement}{i}_to_report";
            if (progress.ContainsKey(key)) SetAchievementToReport(key, Convert.ToBoolean(progress[key]));
        }

        foreach (string hiddenAchievement in Enum.GetNames(typeof(HiddenAchievement)))
        {
            if (progress.ContainsKey(hiddenAchievement)) PlayerPrefs.SetInt(hiddenAchievement, Convert.ToInt32(progress[hiddenAchievement]));

            key = $"{hiddenAchievement}_to_report";
            if (progress.ContainsKey(key)) SetAchievementToReport(key, Convert.ToBoolean(progress[key]));
        }

        foreach (string booster in Enum.GetNames(typeof(Boosts)))
        {
            if (progress.ContainsKey(booster)) PlayerPrefs.SetInt(booster, Convert.ToInt32(progress[booster]));

            key = $"{booster}A";
            if (progress.ContainsKey(key)) SetBoosterActivated(booster, Convert.ToInt32(progress[key]));

            key = $"TimeBooster{booster}";
            if (progress.ContainsKey(key)) SetTimeBooster(booster, Convert.ToDateTime(progress[key]));
        }

        foreach (string dailyTask in Enum.GetNames(typeof(DailyTasks)))
        {
            if (progress.ContainsKey(dailyTask)) PlayerPrefs.SetInt(dailyTask, Convert.ToInt32(progress[dailyTask]));

            key = $"DailyRewardReceived{dailyTask}";
            if (progress.ContainsKey(key)) SetDailyRewardReceived(dailyTask, Convert.ToInt32(progress[key]));
        }

        foreach (string task in Enum.GetNames(typeof(InGameTasks)))
            if (progress.ContainsKey(task)) PlayerPrefs.SetInt(task, Convert.ToInt32(progress[task]));
    }

    private static void SaveProgressByKeysWithoutLoop(Dictionary<string, object> progress)
    {
        string[] keysValuesInt = { "Crystal", "score", "IndexGameMusics", "FirstGame", "Power", "FirstGameOver",
            "FirstOpenAchievement", "FirstGoToShop", "winGame", "GameIsNotOver", "WinSeries", "Battles" };
        foreach (string k in keysValuesInt)
            if (progress.ContainsKey(k)) PlayerPrefs.SetInt(k, Convert.ToInt32(progress[k]));

        Messanger<Events>.SendMessage(Events.CrystalUpdate);

        string[] keysValuesString = { "FullRecoveryTime", "SavedTime", "DailyDate" };
        foreach (string k in keysValuesString)
            if (progress.ContainsKey(k)) PlayerPrefs.SetString(k, Convert.ToString(progress[k]));

        string key = "have_score_to_report";
        if (progress.ContainsKey(key)) SetHaveScoreToReport(Convert.ToBoolean(progress[key]));

        key = "sync_timestamp";
        if (progress.ContainsKey(key)) SetSyncTimestamp(Convert.ToInt32(progress[key]));

        if (progress.ContainsKey("ticket") && progress.ContainsKey("expiration_time"))
            SetTicket(Convert.ToString(progress["ticket"]), Convert.ToInt32(progress["expiration_time"]));

        key = "LastTask";
        if (progress.ContainsKey(key)) SetLastTaskNumber(Convert.ToInt32(progress[key]));

        key = "DailyLocalNumbers";
        if (progress.ContainsKey(key)) SetDailyLocalNumbers(Convert.ToString(progress[key]));

        key = "LastDailyServerNumbers";
        if (progress.ContainsKey(key)) SetLastDailyServerNumbers(Convert.ToString(progress[key]));

        key = "music_active";
        if (progress.ContainsKey(key)) SetMusicActive(Convert.ToBoolean(progress[key]));

        key = "sound_active";
        if (progress.ContainsKey(key)) SetSoundActive(Convert.ToBoolean(progress[key]));

        key = "CompleteTraining";
        if (progress.ContainsKey(key)) PlayerPrefs.SetInt(key, Convert.ToInt32(progress[key]));

        key = "Training3Passed";
        if (progress.ContainsKey(key) && Convert.ToBoolean(progress[key])) SetTraining3Passed();

        key = "Training5Passed";
        if (progress.ContainsKey(key) && Convert.ToBoolean(progress[key])) SetTraining5Passed();

        key = "Training6Passed";
        if (progress.ContainsKey(key) && Convert.ToBoolean(progress[key])) SetTraining6Passed();

        key = "Training7Passed";
        if (progress.ContainsKey(key) && Convert.ToBoolean(progress[key])) SetTraining7Passed();

        key = "Training8Passed";
        if (progress.ContainsKey(key) && Convert.ToBoolean(progress[key])) SetTraining8Passed();
    }

    private static void SaveProgressByKeysWithLoop(Dictionary<string, object> progress)
    {
        string key;
        for (int i = 1; i <= 3; i++)
        {
            key = $"Task{i}";
            if (progress.ContainsKey(key)) SetPageTask(key, Convert.ToInt32(progress[key]));
        }

        for (int i = 1; i <= 12; i++)
        {
            key = $"ability{i}";
            if (progress.ContainsKey(key)) PlayerPrefs.SetString(key, Convert.ToString(progress[key]));
        }
    }

    private static void RestoreUIAfterSynchronization()
    {
        ProgressPanel.I.RefreshOpenAchievementStats();
        ProgressPanel.I.RefreshHiddenAchievementStats();

        Transform transformTraining = TrainingPanel.I.GetObject("TrainingButton").transform;
        transformTraining.position = new Vector3(transformTraining.position.x, TrainingPanel.I.GetObject("TrainingParent").transform.position.y, 1f);

        if (PlayerPrefs.GetInt("FirstGame", 0) != 0)
        {
            TrainingPanel.I.GetObject("Arrow").SetActive(false);
            TrainingPanel.I.GetObject("EnergyArrow").SetActive(false);
            TrainingPanel.I.GetObject("DeckArrow").SetActive(false);
            TrainingPanel.I.GetObject("BackArrow").SetActive(false);
        }
        TrainingPanel.I.gameObject.SetActive(false);

        if (GetTraining3Passed() && !GetTraining5Passed())
        {
            MenuPanel.I.GetObject("Tasks").SetActive(false);
            MenuPanel.I.GetObject("Reward").SetActive(false);
            MenuPanel.I.GetObject("Shop").GetComponent<Button>().interactable = false;
            MenuPanel.I.GetObject("Collection").GetComponent<Button>().interactable = false;
            MenuPanel.I.GetObject("Achievements").GetComponent<Button>().interactable = false;
            MenuPanel.I.GetObject("CrystalButton2").GetComponent<Button>().interactable = false;
        }
        else if (GetTraining5Passed() && !GetTraining6Passed())
        {
            MenuPanel.I.GetObject("Tasks").SetActive(false);
            MenuPanel.I.GetObject("Reward").SetActive(false);
            MenuPanel.I.GetObject("Shop").GetComponent<Button>().interactable = true;
            MenuPanel.I.GetObject("Collection").GetComponent<Button>().interactable = true;
            MenuPanel.I.GetObject("Achievements").GetComponent<Button>().interactable = true;
            MenuPanel.I.GetObject("CrystalButton2").GetComponent<Button>().interactable = true;

            GetLevel();
            if (CountCard >= 13 && PlayerPrefs.GetInt("CompleteTraining") != 1) FirstGame.I.OnTraining(false);
        }
        else if (GetTraining6Passed() && !GetTraining7Passed())
        {
            MenuPanel.I.GetObject("Tasks").SetActive(true);
            MenuPanel.I.GetObject("Reward").SetActive(true);
            MenuPanel.I.GetObject("Shop").GetComponent<Button>().interactable = true;
            MenuPanel.I.GetObject("Collection").GetComponent<Button>().interactable = true;
            MenuPanel.I.GetObject("Achievements").GetComponent<Button>().interactable = true;
            MenuPanel.I.GetObject("CrystalButton2").GetComponent<Button>().interactable = true;

            GetLevel();
            if (CountCard >= 13 && PlayerPrefs.GetInt("CompleteTraining") != 1) FirstGame.I.OnTraining(false);
        }
        else if (GetTraining7Passed())
        {
            MenuPanel.I.GetObject("Tasks").SetActive(true);
            MenuPanel.I.GetObject("Reward").SetActive(true);
            MenuPanel.I.GetObject("Shop").GetComponent<Button>().interactable = true;
            MenuPanel.I.GetObject("Collection").GetComponent<Button>().interactable = true;
            MenuPanel.I.GetObject("Achievements").GetComponent<Button>().interactable = true;
            MenuPanel.I.GetObject("CrystalButton2").GetComponent<Button>().interactable = true;
        }

        foreach (CardPosition card in TableController.I.cardOnTable) card.SetCardActive(true);
        Deck.I.Interact(true);
        GameResultsPanel.I.GetObject("Menu").GetComponent<Button>().interactable = true;
        GameResultsPanel.I.GetObject("Restart").GetComponent<Button>().interactable = true;
    }

    public static void SetProgress(Dictionary<string, object> progress)
    {
        SaveProgressByEnums(progress);
        SaveProgressByKeysWithoutLoop(progress);
        SaveProgressByKeysWithLoop(progress);

        RestoreUIAfterSynchronization();
    }

    public static void SetLastTaskNumber(int taskNumber) => PlayerPrefs.SetInt("LastTask", taskNumber);  // мб это сделать непубличным методом
    public static int GetLastTaskNumber() => PlayerPrefs.GetInt("LastTask", 29); // для того, чтобы он считал следующим задание 1

    public static void SetPageTask(string taskPage, int taskNumber) => PlayerPrefs.SetInt(taskPage, taskNumber);
    public static int GetTaskNumber(string taskPage) => PlayerPrefs.GetInt(taskPage, 1);

    /// <summary>
    /// Засчитывывает очки прогресса с учётом наличия задачи
    /// </summary>
    /// <param name="taskKey"></param>
    /// <param name="progressUnits">Количество добавляемых очков прогресса</param>
    public static void TrackTaskInProgress(string taskKey, int progressUnits)
    {
        if (TaskInProgress(taskKey)) PlayerPrefs.SetInt(taskKey, PlayerPrefs.GetInt(taskKey) + progressUnits);
    }

    /// <summary>
    /// Устанавливает задачу на прогресс
    /// </summary>
    /// <param name="taskKey"></param>
    public static void SetToProgress(string taskKey) { if (GetProgress(taskKey) == -1) PlayerPrefs.SetInt(taskKey, 0); }
    public static void SetProgress(string taskKey, int progress) => PlayerPrefs.SetInt(taskKey, progress);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="taskKey"></param>
    /// <returns>Возвращает прогресс задачи. Если задача не установлена, то возвращает -1</returns>
    public static int GetProgress(string taskKey) => TaskInProgress(taskKey) ? PlayerPrefs.GetInt(taskKey) : -1;
    public static void DeleteProgress(string taskKey) => PlayerPrefs.DeleteKey(taskKey);
    public static bool TaskInProgress(string taskKey) => PlayerPrefs.HasKey(taskKey);

    public static void AddOpenAchievementLevel(string achievement, int value) => PlayerPrefs.SetInt($"{achievement}_lvl", value < 3 ? value + 1 : 3);
    public static int GetOpenAchievementLevel(string achievement) => PlayerPrefs.GetInt($"{achievement}_lvl", 0);

    public static void SetTraining3Passed() => PlayerPrefs.SetInt("Training3Passed", 1);
    public static bool GetTraining3Passed() => PlayerPrefs.HasKey("Training3Passed");

    public static void SetTraining5Passed() => PlayerPrefs.SetInt("Training5Passed", 1);
    public static bool GetTraining5Passed() => PlayerPrefs.HasKey("Training5Passed");

    public static void SetTraining6Passed() => PlayerPrefs.SetInt("Training6Passed", 1);
    public static bool GetTraining6Passed() => PlayerPrefs.HasKey("Training6Passed");

    public static void SetTraining7Passed() => PlayerPrefs.SetInt("Training7Passed", 1);
    public static bool GetTraining7Passed() => PlayerPrefs.HasKey("Training7Passed");

    public static void SetTraining8Passed() => PlayerPrefs.SetInt("Training8Passed", 1);
    public static bool GetTraining8Passed() => PlayerPrefs.HasKey("Training8Passed");

    public static void AddWinSeries() => PlayerPrefs.SetInt("WinSeries", GetWinSeries() + 1);
    public static int GetWinSeries() => PlayerPrefs.GetInt("WinSeries", 0);
    public static void ResetWinSeries() => PlayerPrefs.SetInt("WinSeries", 0);

    public static int GetCrystals() => PlayerPrefs.GetInt("Crystal", 0);
    public static bool AddCrystals(int crystals)
    {
        if (GetCrystals() + crystals < 0) return false;
        else
        {
            PlayerPrefs.SetInt("Crystal", GetCrystals() + crystals);
            return true;
        }
    }

    public static void AddBattleCount() => PlayerPrefs.SetInt("Battles", PlayerPrefs.GetInt("Battles", 0) + 1);
    public static int GetBattleCount() => PlayerPrefs.GetInt("Battles", 0);

    public static bool IsAchievementOpen(string name) => PlayerPrefs.GetInt(name, 0) != 0;

    public static int GetDailyRewardReceived(string name) => PlayerPrefs.GetInt($"DailyRewardReceived{name}", -1);
    public static void SetDailyRewardReceived(string dailyName, int rewardIndex)
    {
        PlayerPrefs.SetInt($"DailyRewardReceived{dailyName}", rewardIndex);
    }
    public static void ResetDailyRewardReceived(string name) => PlayerPrefs.SetInt($"DailyRewardReceived{name}", -1);

    public static string GetDailyLocalNumbers() => PlayerPrefs.GetString("DailyLocalNumbers", "0;1;2");
    public static void SetDailyLocalNumbers(string numbers) => PlayerPrefs.SetString("DailyLocalNumbers", numbers);

    public static string GetLastDailyServerNumbers() => PlayerPrefs.GetString("LastDailyServerNumbers", "0;1;2");
    public static void SetLastDailyServerNumbers(string numbers) => PlayerPrefs.SetString("LastDailyServerNumbers", numbers);

    public static void SetStateRewardIcon()
    {
        bool stateRewardIcon = false;
        if (GetTraining6Passed())
        {
            if (Application.internetReachability != NetworkReachability.NotReachable && GetLastDailyServerNumbers() != GetDailyLocalNumbers())
            {
                stateRewardIcon = true;
            }
            else
            {
                CurrTask currTask;
                DailyTask dailyTask;
                DailyStrings dailyStrings;
                int number;
                string[] numbers = GetDailyLocalNumbers().Split(';');
                if (numbers.Length == 3)
                {
                    for (int i = 1; i <= 3; i++)
                    {
                        currTask = TasksPanel.I.GetObject($"Task{i}").GetComponent<CurrTask>();
                        currTask.Init();
                        stateRewardIcon = currTask.IsTaskDone;
                        if (stateRewardIcon) break;

                        number = int.Parse(numbers[i - 1]);
                        dailyStrings = GetDailyStrings(number);
                        dailyTask = TasksPanel.I.GetObject($"DailyTask{i}").GetComponent<DailyTask>();
                        dailyTask.Init(dailyStrings);
                        stateRewardIcon = dailyTask.IsDailyDone;
                        if (stateRewardIcon) break;
                    }
                }
            }
        }
        MenuPanel.I.GetObject("Reward").SetActive(stateRewardIcon);
    }

    public static int GetAchievement(string name) => PlayerPrefs.GetInt(name, 0);
    public static void SetAchievement(string name, int value) => PlayerPrefs.SetInt(name, value);
    public static void DeleteAchievement(string name)
    {
        if (PlayerPrefs.HasKey(name)) PlayerPrefs.DeleteKey(name);
    }

    public static int GetBooster(string name) => PlayerPrefs.GetInt(name, 0);
    public static void SetBooster(string name, int value) => PlayerPrefs.SetInt(name, value);
    public static void DeleteBooster(string name)
    {
        if (PlayerPrefs.HasKey(name)) PlayerPrefs.DeleteKey(name);
    }

    public static int GetBoosterActivated(string name) => PlayerPrefs.GetInt($"{name}A", 0);
    public static void SetBoosterActivated(string name, int value) => PlayerPrefs.SetInt($"{name}A", value);
    public static void DeleteBoosterActivated(string name)
    {
        if (PlayerPrefs.HasKey($"{name}A")) PlayerPrefs.DeleteKey($"{name}A");
    }

    public static DateTime GetTimeBooster(string name)
    {
        DateTime dt = new DateTime();
        if (DateTime.TryParse(PlayerPrefs.GetString($"TimeBooster{name}", DateTime.Now.ToString()), out dt)) return dt;
        else return dt = DateTime.Now;
    }
    public static void SetTimeBooster(string name, DateTime time)
    {
        //Debug.Log($"START SetTimeBooster: name = {name}, time = {time.ToString()}");
        PlayerPrefs.SetString($"TimeBooster{name}", time.ToString());
    }
    public static void DeleteTimeBooster(string name)
    {
        if (PlayerPrefs.HasKey(name)) PlayerPrefs.DeleteKey($"TimeBooster{name}");
    }
    public static bool HasTimeBooster(string name) => PlayerPrefs.HasKey($"TimeBooster{name}") ? true : false;

    public static bool IsMusicActive() => PlayerPrefs.GetInt("music_active", 1) != 0;
    public static void SetMusicActive(bool active) => PlayerPrefs.SetInt("music_active", active ? 1 : 0);
    public static bool IsSoundActive() => PlayerPrefs.GetInt("sound_active", 1) != 0;
    public static void SetSoundActive(bool active) => PlayerPrefs.SetInt("sound_active", active ? 1 : 0);

    public static bool GameResultSended { get; set; }

    public static void SetHiddenAchievementDone(string achievement)
    {
        //Messanger<string, Events>.SendMessage(Events.PlayAchievementEffect, SoundClips.GetAchievement);
        SocialManager.I.ReportHiddenAchievement(achievement);
        SetAchievement(achievement, 1);
        TrackTaskInProgress(InGameTasks.GetHiddenAchievement.ToString(), 1);
    }

    public static bool IsAchievementSuperstarOpened()
    {
        string[] achievements = Enum.GetNames(typeof(HiddenAchievement));
        int count = 0;
        foreach (string achievement in achievements)
        {
            if (GetAchievement(achievement) == 1) count++;
        }

        return count == achievements.Length - 1 ? true : false;
    }

    public static int GetIndexGameMusics() => PlayerPrefs.GetInt("IndexGameMusics", 0);
    public static void SetNextIndexGameMusics()
    {
        int index = GetIndexGameMusics();
        if (index == SoundClips.GameMusics.Length - 1) index = 0;
        else index++;
        PlayerPrefs.SetInt("IndexGameMusics", index);
    }
}
























public class AbilityStrings
{
    public string name = "";
    public string description = "";
    public int price = 0;
    public int significance = 1;
    public int sprite;
    public AbilityStrings(string name, string description, int price, int significance, int sprite)
    {
        this.name = name;
        this.description = description;
        this.price = price;
        this.significance = significance;
        this.sprite = sprite;
    }
}

public class BoostersStrings
{
    public string description = "";
    public string name = "";
    public int price = 0;
    public int[] sprite;
    public int tasksSprite;
    public BoostersStrings(string name, string description, int price, int[] sprite, int tasksSprite)
    {
        this.name = name;
        this.description = description;
        this.price = price;
        this.sprite = sprite;
        this.tasksSprite = tasksSprite;
    }
}

public class CrystalsStrings
{
    public int crystal = 0;
    public float money = 0f;
    public bool sale = false;
    public CrystalsStrings(int crystal, float money, bool sale)
    {
        this.crystal = crystal;
        this.money = money;
        this.sale = sale;
    }
}

public class MonstersStrings
{
    public string name = "";
    public int strength = 1;
    public int numberSprite = 12;
    public string description = "This is monster?";
    public MonstersStrings(string name, int strength, int numberSprite, string description)
    {
        this.name = name;
        this.strength = strength;
        this.numberSprite = numberSprite;
        this.description = description;
    }
}

public class TasksStrings
{
    public string Key = "TaskKey";
    public int need = 9;
    public string reward = "crystals";
    public string description = "This is Task";
    public TasksStrings(string Key, int need, string reward, string description)
    {
        this.Key = Key;
        this.need = need;
        this.reward = reward;
        this.description = description;
    }
}

public class DailyStrings
{
    public string Key = "DailyKey";
    public int[] need = new int[4] { 9, 9, 9, 9 };
    public string description = "It's daily task!";
    public DailyStrings(string Key, int[] need, string description)
    {
        this.Key = Key;
        this.need = need;
        this.description = description;
    }
}

public class AchievementsStrings
{
    public string name = "LocalNameAchievement";
    public int sprite = 0;
    public string description = "It`s achievement!";
    public AchievementsStrings(string name, int sprite, string description)
    {
        this.name = name;
        this.sprite = sprite;
        this.description = description;
    }
}

public class OpenAchievementsStrings
{
    string name = "LocalNameAchievement";
    public int[] sprite;
    public int[] need;
    public string description = "It`s achievement!";
    public OpenAchievementsStrings(string name, int[] sprite, int[] need, string description)
    {
        this.name = name;
        this.sprite = sprite;
        this.need = need;
        this.description = description;
    }
}

public class AbilityCardsLimits
{
    public int openCards = 4;
    public int openCardsWeight = 5;
    public int maxSetWeight = 9;

    public AbilityCardsLimits(int openCards, int openCardsWeight, int maxSetWeight)
    {
        this.openCards = openCards;
        this.openCardsWeight = openCardsWeight;
        this.maxSetWeight = maxSetWeight;
    }
}

public class LocaleStrings
{
    public string key;
    public string localeString;

    public LocaleStrings(string key, string localeString)
    {
        this.key = key;
        this.localeString = localeString;
    }
}

public class TrainingList
{
    public Dictionary<int, Replica> FirstGame;
    public Dictionary<int, Replica> SecondGame;
    public Dictionary<int, Replica> ThirdGame;
    public Dictionary<int, Replica> FourthGame;
    public Dictionary<int, Replica> TourOfMenu;
    public Dictionary<int, Replica> FirstAchivement;
    public Dictionary<int, Replica> Buy13Cards;
    public Dictionary<int, Replica> Played25fights;
    public Dictionary<int, Replica> GameOverInTraining;
    public Dictionary<int, Replica> FirstGameOver;
    public Dictionary<int, Replica> FirstGoToShop;
    public Dictionary<int, Replica> NewMonsterReplica;

    public Dictionary<int, Replica> Win5TimesInARow;
    public Dictionary<int, Replica> Win6TimesInARow;
    public Dictionary<int, Replica> Win7TimesInARow;
    public Dictionary<int, Replica> Win8TimesInARow;
    public Dictionary<int, Replica> Win9TimesInARow;
    public Dictionary<int, Replica> Win10TimesInARow;

    public Dictionary<int, Replica> RandomSessionReplics;

    public Dictionary<int, Replica> InGame3Day;
    public Dictionary<int, Replica> InGame4Day;
    public Dictionary<int, Replica> InGame5Day;
    public Dictionary<int, Replica> InGame7Day;
    public Dictionary<int, Replica> InGame10Day;
    public Dictionary<int, Replica> InGame15Day;
    public Dictionary<int, Replica> InGame20Day;
    public Dictionary<int, Replica> InGame25Day;
    public Dictionary<int, Replica> InGame30Day;

    public Dictionary<int, Replica> Opened18CardsAbilities;
    public Dictionary<int, Replica> Opened25CardsAbilities;
    public Dictionary<int, Replica> Opened35CardsAbilities;
    public Dictionary<int, Replica> Opened51CardsAbilities;

    public Dictionary<int, Replica> Spent1000Crystals;
    public Dictionary<int, Replica> Spent4000Crystals;
    public Dictionary<int, Replica> Spent7000Crystals;

    public Replica FirstEnergyZero;
    public Replica DailyTaskNotTaken;

    public TrainingList(bool New)
    {
        FirstGame = new Dictionary<int, Replica>();
        SecondGame = new Dictionary<int, Replica>();
        ThirdGame = new Dictionary<int, Replica>();
        FourthGame = new Dictionary<int, Replica>();
        TourOfMenu = new Dictionary<int, Replica>();
        FirstAchivement = new Dictionary<int, Replica>();
        Buy13Cards = new Dictionary<int, Replica>();
        Played25fights = new Dictionary<int, Replica>();
        GameOverInTraining = new Dictionary<int, Replica>();
        FirstGameOver = new Dictionary<int, Replica>();
        FirstGoToShop = new Dictionary<int, Replica>();
        NewMonsterReplica = new Dictionary<int, Replica>();

        RandomSessionReplics = new Dictionary<int, Replica>();

        Win5TimesInARow = new Dictionary<int, Replica>();
        Win6TimesInARow = new Dictionary<int, Replica>();
        Win7TimesInARow = new Dictionary<int, Replica>();
        Win8TimesInARow = new Dictionary<int, Replica>();
        Win9TimesInARow = new Dictionary<int, Replica>();
        Win10TimesInARow = new Dictionary<int, Replica>();

        InGame3Day = new Dictionary<int, Replica>();
        InGame4Day = new Dictionary<int, Replica>();
        InGame5Day = new Dictionary<int, Replica>();
        InGame7Day = new Dictionary<int, Replica>(); 
        InGame10Day = new Dictionary<int, Replica>();
        InGame15Day = new Dictionary<int, Replica>();
        InGame20Day = new Dictionary<int, Replica>();
        InGame25Day = new Dictionary<int, Replica>();
        InGame30Day = new Dictionary<int, Replica>();

        Opened18CardsAbilities = new Dictionary<int, Replica>();
        Opened25CardsAbilities = new Dictionary<int, Replica>();
        Opened35CardsAbilities = new Dictionary<int, Replica>();
        Opened51CardsAbilities = new Dictionary<int, Replica>();

        Spent1000Crystals = new Dictionary<int, Replica>();
        Spent4000Crystals = new Dictionary<int, Replica>();
        Spent7000Crystals = new Dictionary<int, Replica>();
    }
}

public class Replica
{
    public Emotion emotion = Emotion.Fright;
    public string replica = "";

    public Replica(string em, string replica)
    {
        foreach (Emotion e in Enum.GetValues(typeof(Emotion))) if (e.ToString() == em) emotion = e;
        this.replica = replica;
    }
}