﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ClickHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{
    private bool press = false;
    private float timePress = 0f;
    float x, y;
    float radius = 30f;

    public Action LongPress { private get; set; } = () => { Debug.Log("LongPress"); };
    public Action Click { private get; set; } = () => { Debug.Log("Click"); };

    IEnumerator Timer()
    {   
        while (press)
        {
            if (timePress < 5f)
            {
                timePress += 1;
                yield return new WaitForSeconds(0.1f);
            }
            else
            {
                LongPress?.Invoke();
                press = false;
            }
        }
        if (timePress < 5f) timePress = 0f;
    }

    private bool MouseShifted()
    {
        if ((x + radius < Input.mousePosition.x) || (x - radius > Input.mousePosition.x)  ||
            (y + radius < Input.mousePosition.y) || (y - radius > Input.mousePosition.y)) return true;
        else return false;
    }


    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {                       
        x = Input.mousePosition.x;
        y = Input.mousePosition.y;
        timePress = 0f;
        press = true;
        StartCoroutine(Timer());
    }

    void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
    {
        if (press && !MouseShifted()) Click?.Invoke();
        press = false;
    }

    void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
    {
        press = false;
    }
}
