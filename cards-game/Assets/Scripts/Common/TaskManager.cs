﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskManager : MonoBehaviour
{
    public int[] Tasks = new int[Enum.GetNames(typeof(DailyTasks)).Length]; 
    void Update()
    {
        for (int i = 0; i < Enum.GetNames(typeof(DailyTasks)).Length; i++)
        {
            Tasks[i] = Helper.GetProgress(Enum.GetNames(typeof(DailyTasks))[i]);
        }
    }
}
