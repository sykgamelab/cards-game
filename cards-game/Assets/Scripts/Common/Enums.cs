﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Events
{
    //game
    CrystalUpdate,
    StartGame,
    OnSendCard,
    UpdateStats,
    OnChangePlay,
    SubtractPower,
    AddPower,
    //shop
    SwitchShop,
    CloseShop,
    OpenShopTab,
    BuyProduct,
    ChangeContent,
    UnlockShop,
    //boosts
    StartBoostTimer,
    AddHourBoost,
    //other
    InTaskPanel,
    UpdateAnchors,
    GameLoadingDone,
    FirstGame,
    OnCheckSimilarBoost,
    // Audio
    SetSoundActive,
    SetMusicActive,
    PlayMusic,
    PlayUI,
    PlayEffect,
    PlayAchievementEffect
}

public enum ComponentType { Image, ParticleSystem }

public enum CardType
{
    Ability,
    Monster,
    Madness,
    Crystal,
    Energy,
    Health
}

public enum ShopTabs
{
    Cards,
    Boosts,
    Crystals
}

public enum Ability
{
    ReturnMonsterToDeck,
    AddThreeRandomCardsToDeck,
    DestroyAllMadness,
    DeleteCardFromDeck,
    DamageRandomMonster,
    DamageAllUndraw,
    AddHealthEmptyField,
    AddMaxHealth,
    AddMaxEnergy,
    AddOnePriceOnField,
    AddHealthIfEnergyZero,
    SeeMonsterInDeck,
    FreeDeal,
    ReplaceRandomsOnFieldWithCardsFromDeckNumberMonsterCardsOnField,
    InfectRandomMoster,
    RandomlyChangeAllCards,
    AddDoublePrice,
    ThreeDamageNeighbor,
    DestroyRandomNeighbor,
    AddCloneLastUsed,
    AddTwoCrystalAndOneMonsterToDeck,
    NextMonsterEatCrystals,
    DamageMonstersValueAboveSix,
    ReplaceNeighborToRandomEnergy,
    FreeDealGame,
    NextAddEnergyHisPrice,
    AddCrystalsAsNumberMonstersOnField,
    EquateEnergyHealth,
    ConvertNeighborToCrystal,
    FreeCards,
    AddThreeNeighbor,
    AddTwoMadnessAndOneMonsterToDeck,
    NextCrystalsToTimesMore,
    AddCrystalsFromDeckTwoPrice,
    AddCrystalIfEnergyZero,
    NextMonsterTwoTimesWeaker,
    DestroyOneMonster,
    DestroyThreeRandomCard,
    RestoreHealth,
    MadnessToCrystals,
    RepeatPreviousCard,
    EquateHealthEnergy,
    DestroyAllNeighborAndPriceToCrystal,
    InfectAllOnField,
    AllNeighborMonstersToMadness,
    AddCharacteristicAsNumberMonstersOnField,
    InfectThreeMonstersInDeck,
    DeleteFromFieldAndOneHealth,
    ReplaceMonsterWithCrystalOnField,
    DestroyAllMonster
}

public enum Monsters
{
    BatBeast,  
    Crabopod, 
    DreadfulNeophron,
    EvilEye, 
    EvilKid,
    FangedDog,
    FlyFisher,
    GreedyMushroom,
    HatefulRat,
    HellLarva,
    HorrendousCavalier,
    InfernalPimple,
    PettyBoss,
    ReptileInsect,
    ReptileMolluskus, 
    RogueSpike, 
    Rogue,
    ScarySnake,
    Scorpio, 
    SnakePoisonous, 
    Spider, 
    SpikedBall, 
    SpikyWings, 
    Sprut, 
    Toadfall, 
    WildMaggot,
    WildSlug 
};

public enum Boosts
{
    InfinitePower,
    IncreaseCrystals50,
    BraveryDealCard,
    DamageMonsters,
    InfectedMonsters15,
    IncreaseCrystals75,
    IncreaseCrystals25,
    TwoCrystForDeal,
    x2Power,
    x3Power,
    InfectedMonsters20,
    InfectedMonsters10,
    CrystalsWin10,
    CrystalsWin15,
    CrystalsForLose,
    OneBraveryEnergy,
    OneBraveryCrystals,
    OneBraveryAbility,
    ReAbilities
}

public enum OpenAchievement
{
    kill_monster_infect,
    kill_monster,
    spend_crystals,
    done_daily_quests,
    use_ability_cards,
    win_3_games_in_row
}

public enum HiddenAchievement
{
    fears_control,         
    pacifist,
    strategist, 
    on_its_wave,           
    dreamer,               
    and_so_come_down,
    over_the_edge,            
    bravery,               
    megabravery,
    the_will_to_win,              
    help_strong,           
    feasible_help,         
    no_brakes,             
    mad_prince,            
    on_a_grand_scale,      
    risky,                 
    zoologist,             
    energetic,             
    always_on_time,        
    superstar,             
    self_sufficiency,      
    playful_hands,         
    empty_cache,           
    monstrophobia,         
    im_not_sorry,          
    triumph_of_greed,      
    wonderful_rake,        
    i_did_everything_right,
    rapid_enrichment,      
    exchange_wizard,       
    i_do_not_see_monsters, 
    triumph_of_madness,    
    dangerous,             
    extremely_dangerous,   
    household_philosophy,  
    alchemist_dream
}

public enum Emotion
{
    Fright,//(Испуг)
    Reverie,//(задумчивость)
    Joy,//(радость)
    Teach,//(Наставляет)
    Secret,//(По секрету)
    Surprise,//(удивление) 
    Sadness,//(Грусть)
    NoEmotion // Действие без наставника
}

public enum InGameTasks
{
    KillMonster,
    WinGames,
    StashEnergy,
    UseBravery,
    GetHiddenAchievement,
    CollectCrystal,
    KillMonster1Level,
    OpenNewMonster,
    UseAccuracyShot,
    KillMonsterAbility,
    DailyGift,
    GetCrystals,
    DailyCardsForBravery,
    WinGamesLess3Bravery,
    WinGamesInRow,
    UseAbiltyCards,
    FreeDealCards,
    SpendCrystals,
    WinGamesFullEnergy,
    UseMadness,
    KillInfectMonster,
    UseRayLight,
    WinGamesLess3EnergyBravery,
    WinGamesMaxBravery,
    KillMonster9Level,
    BuyAbilityCard,
    PlayGames,
    LoseLastMonster,
    DeleteCardsAbility,
    InfectedMonsters
}

public enum DailyTasks
{   
    UseFreeDeal,              
    AbilityDamageMonster,     
    DestroyDeckCardsAbility,
    InfectMonsters,
    DamageCardsAbility,
    AddDeckCardsAbility,
    AddCrystalsAbility,
    AddCardsCountAbility,
    TransformMadness,
    DamageMonstersBravery,
    PlayMadness,
    ReturnCardsToDeck,
    RestoreBraveryAbility,
    PlayCrystals,
    PlayEnergy,
    AddEnergyAbility,
    TransformToMadness,
    DestroyDeckMonsters,
    SpendCrystalOnDeal,
    TransformToCrystal,
    SpendBraveryNullEnergy,
    TransformCards,
    MurderMonster9Level,
    MurderMonster1Level,
    UseBravery9Level,
    UseBravery1Level,
    UseEnergy7Level,
    UseCrystal9Level,
    UseCrystal3Level,
    KillInfectMonsterBravery
}

public enum RewardType
{
    Booster,
    Crystals
}