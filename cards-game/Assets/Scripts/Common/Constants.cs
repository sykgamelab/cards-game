using System.Collections.Generic;
using System.Collections.ObjectModel;

public static class GameConstants
{
    public static int maxPower = 30;
    public static int priceForGame = 2;   // Power price
    public static float CardMoveTime = 0.6f;

    public const float WaitMoveAnimationTextTime = 0.5f;
    public const float ParticleSystemMoveAnimationTime = 0.5f;
    public const float ParticleSystemAlphaAnimationTime = 0.5f;

    public const float SpeedCardRotation = 300f;

    public const float MinMusicVolume = 0.3f;

    public const int CountBattlesForSetProgressToReport = 5;

    public const int DailyGiftSeconds = 86400;

    public const int MinCountWinForWinSeries = 3;

    public static Dictionary<CardType, string> PathToSprites = new Dictionary<CardType, string>
    {
        { CardType.Ability, "atlas_cards_ability"},
        { CardType.Crystal, "atlas_cards_allcards"},
        { CardType.Energy,  "atlas_cards_allcards"},
        { CardType.Health,  "atlas_cards_allcards"},
        { CardType.Monster, "atlas_cards_monsters"},
        { CardType.Madness, "atlas_cards_ability"}
    };

    public const string MD5HashCrystals  = "A48296AD11ED041E4DB9991AF1057FB6";
    public const string MD5HashWinSeries = "788E980FE55701D0E26B0AA0B4AEDE47";

    public static int GetNumberSprite(CardInfo card)
    {
        switch (card.Type)
        {
            case CardType.Ability: return Helper.GetAbilityDescription(card.Name).sprite;
            case CardType.Monster: return Helper.GetMonstersStrings(card.Name).numberSprite;
            case CardType.Madness: return Helper.GetMadnessDescription().sprite;
            case CardType.Crystal:
                switch (card.Price)
                {
                    case 1: case 2: return 5;
                    case 3: case 4: return 6;
                    case 5: case 6: return 7;
                    case 7: case 8: case 9: return 8;
                    default: return 8;
                }
            case CardType.Energy:
                switch (card.Price)
                {
                    case 1: case 2: return 9;
                    case 3: case 4: return 10;
                    case 5: case 6: return 11;
                    case 7: case 8: case 9: return 0;
                    default: return 0;
                }
            case CardType.Health:
                switch (card.Price)
                {
                    case 1: case 2: return 1;
                    case 3: case 4: return 2;
                    case 5: case 6: return 3;
                    case 7: case 8: case 9: return 4;
                    default: return 4;
                }
            default: return 0;
        }
    }  

    public static int[] TableForMonstersAndHealth = {0, 4, 12, 25, 41, 59, 75, 88, 96, 100 };
    public static int[] TableForEnergy = {0, 17, 37, 63, 83, 100 };
    public static int[] TableForCrystal = {0, 15, 33, 53, 71, 86, 95, 100};
    public static int NumberOfTasks = 30;
    public static int TimeActionPurchasedBooster = 3;
    public static int TimeActionReceivedBooster = 1;
    public static int[] BonusForLevel = { 10, 30, 50, 70, 90 };
    public static int Battles25ForTraining8 = 25;
}

public static class SocialConstants
{
    public enum AchievementType { Unlock, Increment }

    public static readonly ReadOnlyDictionary<string, string> HiddenAchievementsIDs = new ReadOnlyDictionary<string, string>
    (
        new Dictionary<string, string>()
        {
            // ключ для ачивок iOS и Helper.AchievementsStrings.name
            // значение для ачивок Android
            { "fears_control", "CgkI2LjR6PsZEAIQAQ" },
            { "pacifist", "CgkI2LjR6PsZEAIQAg" },
            { "strategist", "CgkI2LjR6PsZEAIQAw" },
            { "on_its_wave", "CgkI2LjR6PsZEAIQBA" },
            { "dreamer", "CgkI2LjR6PsZEAIQBQ" },
            { "and_so_come_down", "CgkI2LjR6PsZEAIQBg" },
            { "over_the_edge", "CgkI2LjR6PsZEAIQBw" },
            { "bravery", "CgkI2LjR6PsZEAIQCA" },
            { "megabravery", "CgkI2LjR6PsZEAIQCQ" },
            { "the_will_to_win", "CgkI2LjR6PsZEAIQCg" },
            { "help_strong", "CgkI2LjR6PsZEAIQCw" },
            { "feasible_help", "CgkI2LjR6PsZEAIQDA" },
            { "no_brakes", "CgkI2LjR6PsZEAIQDQ" },
            { "mad_prince", "CgkI2LjR6PsZEAIQDg" },
            { "on_a_grand_scale", "CgkI2LjR6PsZEAIQDw" },
            { "risky", "CgkI2LjR6PsZEAIQEA" },
            { "zoologist", "CgkI2LjR6PsZEAIQEQ" },
            { "energetic", "CgkI2LjR6PsZEAIQEg" },
            { "always_on_time", "CgkI2LjR6PsZEAIQEw" },
            { "superstar", "CgkI2LjR6PsZEAIQFA" },
            { "self_sufficiency", "CgkI2LjR6PsZEAIQFQ" },
            { "playful_hands", "CgkI2LjR6PsZEAIQFg" },
            { "empty_cache", "CgkI2LjR6PsZEAIQFw" },
            { "monstrophobia", "CgkI2LjR6PsZEAIQGA" },
            { "im_not_sorry", "CgkI2LjR6PsZEAIQGQ" },
            { "triumph_of_greed", "CgkI2LjR6PsZEAIQGg" },
            { "wonderful_rake", "CgkI2LjR6PsZEAIQGw" },
            { "i_did_everything_right", "CgkI2LjR6PsZEAIQHA" },
            { "rapid_enrichment", "CgkI2LjR6PsZEAIQHQ" },
            { "exchange_wizard", "CgkI2LjR6PsZEAIQHg" },
            { "i_do_not_see_monsters", "CgkI2LjR6PsZEAIQHw" },
            { "triumph_of_madness", "CgkI2LjR6PsZEAIQIA" },
            { "dangerous", "CgkI2LjR6PsZEAIQIQ" },
            { "extremely_dangerous", "CgkI2LjR6PsZEAIQIg" },
            { "household_philosophy", "CgkI2LjR6PsZEAIQIw" },
            { "alchemist_dream", "CgkI2LjR6PsZEAIQJA" }
        }
    );

    public static readonly ReadOnlyDictionary<string, string> OpenAchievementsIDs = new ReadOnlyDictionary<string, string>
    (
        new Dictionary<string, string>()
        {
            // ключ для ачивок iOS и Helper.OpenAchievementsStrings.name
            // значение для ачивок Android
            { "kill_monster1", "CgkI2LjR6PsZEAIQJQ" },
            { "kill_monster2", "CgkI2LjR6PsZEAIQJg" },
            { "kill_monster3", "CgkI2LjR6PsZEAIQJw" },
            { "kill_monster_infect1", "CgkI2LjR6PsZEAIQKA" },
            { "kill_monster_infect2", "CgkI2LjR6PsZEAIQKQ" },
            { "kill_monster_infect3", "CgkI2LjR6PsZEAIQKg" },
            { "spend_crystals1", "CgkI2LjR6PsZEAIQKw" },
            { "spend_crystals2", "CgkI2LjR6PsZEAIQLA" },
            { "spend_crystals3", "CgkI2LjR6PsZEAIQLQ" },
            { "done_daily_quests1", "CgkI2LjR6PsZEAIQLg" },
            { "done_daily_quests2", "CgkI2LjR6PsZEAIQLw" },
            { "done_daily_quests3", "CgkI2LjR6PsZEAIQMA" },
            { "use_ability_cards1", "CgkI2LjR6PsZEAIQMQ" },
            { "use_ability_cards2", "CgkI2LjR6PsZEAIQMg" },
            { "use_ability_cards3", "CgkI2LjR6PsZEAIQMw" },
            { "win_3_games_in_row1", "CgkI2LjR6PsZEAIQNA" },
            { "win_3_games_in_row2", "CgkI2LjR6PsZEAIQNQ" },
            { "win_3_games_in_row3", "CgkI2LjR6PsZEAIQNg" }
        }
    );

#if UNITY_IOS
    public const string ScoresTable = "scores_table";
#elif UNITY_ANDROID
    public const string ScoresTable = "CgkI2LjR6PsZEAIQNw";
#endif
}

public static class ServerConstants
{
    public const string UserID = "userID";
    public const string Progress = "progress";
    public const string Timestamp = "timestamp";
    public const string Url = "https://37.140.198.128:8080/";
    //public const string Url = "https://192.168.1.71:8080/";
    public const string UriSetProgress = "InfectedKingdom/SetProgress";
    public const string UriGetProgress = "InfectedKingdom/GetProgress";
    public const string UriAuthIOS = "InfectedKingdom/AuthIOS";
    public const string UriAuthAndroid = "InfectedKingdom/AuthAndroid";
    public const string UriAuthTest = "InfectedKingdom/AuthTest";
    public const string UriGetWinSeries = "InfectedKingdom/GetWinSeries";
    public const string UriSetWinSeries = "InfectedKingdom/SetWinSeries";
    public const string UriGetDailyQuests = "InfectedKingdom/GetDailyQuest";
    public const string Nickname = "nick";
    public const string Ticket = "ticket";
    public const string WinSeries = "series";
    public const string ExpirationTime = "expirationTime";
    public const int RequestRepeatGetProgressMilliseconds = 30000;
    public const string Token = "token";
    public const int Timeout = 10000;
    public const int DailyStartSeconds = 5;
    public const int DailyTimeoutSeconds = 60;
}