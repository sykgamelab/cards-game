﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Arrow : MonoBehaviour
{
    const float defaultSeconds = 3f; 

    RectTransform rectTransform;
    float angle = 0f;
    float rotateAngle = 0f;       
    float seconds = defaultSeconds;
    bool isDefaultValues = false;

    void Awake()
    {
        rectTransform = gameObject.GetComponent<RectTransform>();
        rectTransform.pivot = new Vector2(0.5f, 0.5f);
    }

    public void Rotate(float angle)
    {
        gameObject.transform.Rotate(Vector3.forward, angle);
        this.angle += angle;
        rotateAngle = angle;
    }

    public void ResetPosition(bool allowReset = false)
    {
        if (!isDefaultValues || allowReset)
        {
            rectTransform.offsetMax = new Vector2(0f, 0f);
            rectTransform.offsetMin = new Vector2(0f, 0f);
            Rotate(-rotateAngle);
            rotateAngle = 0f;
            gameObject.SetActive(false);
        }
        isDefaultValues = true;
    }
         
    IEnumerator Animation(float seconds, Action actionAfterAnimation)
    {
        float amplitude = 20f;
        float step = 0f;
        float speed = 50f;
        float time = 0;
        float angleInRadians = angle * Mathf.PI / 180f;
        while (time < seconds && gameObject.activeSelf)
        {
            step += speed * Time.deltaTime;  
            rectTransform.offsetMax = new Vector2(-step * Mathf.Sin(angleInRadians), step * Mathf.Cos(angleInRadians));
            rectTransform.offsetMin = new Vector2(-step * Mathf.Sin(angleInRadians), step * Mathf.Cos(angleInRadians));
            if (step >=  amplitude) speed = -50f;
            if (step <= -amplitude) speed = 50f;
            time += Time.deltaTime;
            yield return null;
        }
        actionAfterAnimation?.Invoke();
        ResetPosition();
        seconds = defaultSeconds;
    }

    public void StartArrowAnimation(Action actionAfterAnimation, float rotateAngle)
    {
        StartArrowAnimation(actionAfterAnimation, rotateAngle, defaultSeconds);
    }

    public void StartArrowAnimation(Action actionAfterAnimation, float rotateAngle, float seconds)
    {
        isDefaultValues = false;
        Rotate(rotateAngle);
        this.seconds = seconds;
        gameObject.SetActive(true);
        StartCoroutine(Animation(seconds, actionAfterAnimation));
    }
}
